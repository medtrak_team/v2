import sys
import os
import logging
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from shared.environment import Environment
env = Environment()

def debug(txt, error=False):
    if error:
        logging.error(txt)
    else:
        logging.info(txt)
    if(env.get("debug") or env.get("verbose")):
        print(txt)
