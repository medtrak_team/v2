# README

What is this app for?
- v2 communications backend, delivering packages to patients based on the communications rules for the pathways and interactions they are on

How do I get set up?
- consult the parent directory README

Testing Your Work
- run `python -m comms.send_comms`
- this will run against your local copy of postgres based on your site_settings.py file
- use --help for options
- use --debug to do a dry run and avoid actually sending things or creating contact log entries

Localization and Text Customization
- Extract all translatable/customizable text from python and jinja2 files:
  - python setup.py extract_messages
  - Note: I get "ValueError: underlying buffer has been detached" but it doesn't seem to interfere with the behavior
  - the pot will appear in text/raw/messages.pot
- Update the translation files if they already exist
  - python setup.py update_catalog -l en
  - -l can be used for other languages
  - for text customization, use the client specific folder
- Generate translation files (.po) for a language for the first time (will overwrite existing translations)
  - python setup.py init_catalog -l en
  - -l can be used for other languages
- Put your translations in the generated or updated .po files
- Compile the translations into .mo files used by Babel
  - python setup.py compile_catalog -l en
  
Pushing your work
- ssh into the messaging server 72.31.30.204
- cd /usr/local/caresense/apps/v2
- sudo git pull
- sudo supervisorctl -c /etc/supervisor/supervisord.conf restart v2:v2comms

Running on the server
- ssh into the messaging server 
- `sudo su -`
- `pyenv activate v2`
- `python -m comms.send_comms`