# -*- coding: utf-8 -*-
import json
import requests
import datetime
import time
import pytz
import random
import secrets
from zoneinfo import ZoneInfo
from jinja2 import Template
from sqlalchemy import text

#add parent dir to path before importing parent path scripts
import sys
import os

from comms.survey_util import get_survey_json, inform_node, new_survey, survey_merge
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

# from comms.fcm_notifications import send_app_notification
from comms.send_comms_test import get_test_contact_next
from comms.build_message import get_take_interaction_link, build_email_message, build_sms_message
from comms.util import debug

from shared.environment import Environment
from shared.db import get_mysql_session, get_postgresql_session, close_sessions

#========================== setup constants ====================================
env = Environment()
env.add_argument('--test_phone', help='When test mode is on use the value passed to this argument as the phone for sending sms messages. If this is not set then messages will not be sent in test mode, but it will do everything up to sending to the nsq.')
env.add_argument('--test_email', help='When test mode is on use the value passed to this argument as the email for sending email messages. If this is not set then messages will not be sent in test mode, but it will do everything up to sending to the nsq.')
env.add_argument('--debug', action='store_true', help='Do not send the messages, just print out the process and expected result for debug purposes.')
env.add_argument('--verbose', action='store_true', help='Print out the debug messages. Not needed with the debug flag.')
env.add_argument('--subject_id', help='Only send messages for one subject.')

# a token would need to be passed with each postgrest request
# as well as a header i.e. curl "http://localhost:3000/items" \ -H "Accept-Profile: live"
# if NSQ is handling the callback, it will need additional header info + token
if(env.get("test")):
    CS_URL = "https://staging.caresense.com"
    NSQ_URL = "http://pub.nsq.queue.staging:4151/pub"
    CONTACT_ERROR_CALLBACK = ""
elif(env.get("live")):
    CS_URL = "https://www.caresense.com"
    NSQ_URL = "http://pub.nsq.queue.prod:4151/pub"
    CONTACT_ERROR_CALLBACK = "https://www.caresense.com/v2/comms/logContactError"
else: 
    CS_URL = "https://staging.caresense.com"
    NSQ_URL = "http://pub.nsq.queue.staging:4151/pub"
    CONTACT_ERROR_CALLBACK = "https://staging.caresense.com/v2/comms/logContactError"
    
#========================== utility functions ==================================
def log_error(error,contact_log_id):
    if not env.get("debug"):
        postgresql_session.execute(text("UPDATE contact_log SET error=True,error_message=:error WHERE id=:contact_log_id"), 
            {"error":error,"contact_log_id":contact_log_id})
        postgresql_session.commit()
    debug('Error: '+error, True)

#============================ send_comms behavior ==============================
def deliver_comm(medium, contact, message_json, contact_log_id):
    debug(f'deliver_comm contact: {contact}...')
    debug(f'deliver_comm medium: {medium}...')
    debug(f'deliver_comm message: {message_json}...')

    match medium:
        case 'email':
            topic = 'email_services'
            contact_type = 'Email'
        case 'simple_sms':
            topic = 'simple_sms'
            contact_type = 'Phone'
        case 'sms':
            topic = 'sms'
            contact_type = 'Phone'
        case _:
            log_error('No contact medium defined for communication',contact_log_id)
            return False
    
    debug(f'deliver_comm contact_type: {contact_type}...')
    debug(f'deliver_comm topic: {topic}...')
    debug(f'deliver_comm NSQ_URL: {NSQ_URL}...')

    if(env.get("live")):
        if check_if_patient_blacklisted(contact_type, contact):
            log_error('Patient is blacklisted',contact_log_id)
            return False
    else:
        if not check_if_patient_whitelisted(contact_type, contact):
            log_error('Patient not whitelisted',contact_log_id)
            return False
    
    if env.get("debug"):
        return True
    
    try:
        debug('attempting to post to NSQ...')
        with requests.Session() as http_session:
            http_session.post(NSQ_URL, params={
            'topic': topic
        }, data=message_json)
    except requests.exceptions.RequestException as e:
        log_error('Message could not be queued',contact_log_id)
        debug(e)
        return False

    debug('sent')
    return True

def check_if_patient_whitelisted(contact_type, contact):
    if contact_type == "Email" and contact.endswith("@caresense.com"):
        return True
    try:
        with get_mysql_session() as mysql_session:
            result = mysql_session.execute(text("""
                SELECT True FROM CareSensePathways.WhitelistedContacts
                WHERE Type = :contact_type
                AND Contact = :contact
                """),{"contact_type": contact_type,"contact": contact})
            return result.scalar()
    except:
        debug('Could not connect to WhitelistedContacts table')
        return False
        
def check_if_patient_blacklisted(contact_type, contact):
    try:
        with get_mysql_session() as mysql_session:
            result = mysql_session.execute(text("""
                SELECT True FROM CareSensePathways.BlacklistedContacts
                WHERE Type = :contact_type
                AND Contact = :contact
                """),{"contact_type": contact_type,"contact": contact})
            return result.scalar()
    except:
        debug('Could not connect to BlacklistedContacts table')
        return True
    
def get_contact_info_for_subject(subject_id):
    contact_info = {}
    email = None
    phone = None
    timezone = "America/New_York"
    fromAddress = "no-reply@caresense.com"
    if(env.get("test")):
        contact_info["Email"] = env.get("test_email") if env.get("test_email") else email
        contact_info["MobilePhone"] = env.get("test_phone") if env.get("test_phone") else phone
        contact_info["Timezone"] = timezone if timezone else None
        contact_info["FromAddress"] = fromAddress if fromAddress else None
    else:
        with get_mysql_session() as mysql_session:
            result = mysql_session.execute(text("""
                SELECT 
                    CurrentContactInfo.Email, 
                    CurrentContactInfo.MobilePhone, 
                    COALESCE(NULLIF(ContactSettings.TimeZone,''),NULLIF(Users.Timezone,''),'America/New_York') AS Timezone, 
                    CONCAT(REPLACE(COALESCE(UserSettings.Value, 'no-reply'), ' ', ''), '@caresense.com') AS FromAddress, 
                    ContactSettings.PreferredLanguage, 
                    ContactSettings.NotifyDevices  
                FROM
                    CareSenseData.CurrentContactInfo
                JOIN CareSense.Users
                    ON CurrentContactInfo.Owner = Users.UserID
                LEFT JOIN CareSensePathways.ContactSettings
                    ON ContactSettings.PatientID = CurrentContactInfo.PatientID
                LEFT JOIN CareSense.UserSettings
                    ON Users.UserID = UserSettings.UserID
                    AND UserSettings.Name = 'PathwaysFromEmail'
                WHERE 
                    CurrentContactInfo.PatientID = :id and Status=4
                LIMIT 1
                """),{"id": subject_id})
            contact_info = result.mappings().one_or_none()
    return contact_info

def create_contact_log(primary_comm,all_comms,medium="",priority_value=0,communication_type="",contact_info="",timezone=""):
    if not env.get("debug"):
        debug('inserting into contact_log')
        scheduled_at = datetime.datetime.now(datetime.timezone.utc)
        result = postgresql_session.execute(text("""
            INSERT INTO contact_log (subject_id, medium, priority_value, communication_type, contact_info, sent, timezone) 
            VALUES(:subject_id, :medium, :priority_value, :communication_type, :contact_info, :sent, :timezone) RETURNING id"""),{
                "subject_id":primary_comm['subject_id'],
                "medium":medium,
                "priority_value": priority_value,
                "communication_type": communication_type,
                "contact_info":contact_info,
                "sent":schedule_at,
                "timezone":timezone,
            })
        contact_log_id = result.scalar()
        if all_comms:
            for comm in all_comms:
                result = postgresql_session.execute(text("""
                    INSERT INTO contact_log_interaction (contact_log_id, communication_id, interaction_id, instance, message_iter)
                    VALUES(:contact_log_id, :communication_id, :interaction_id, :instance, :message_iter)"""), {
                        "contact_log_id": contact_log_id,
                        "communication_id": comm['communication_id'], 
                        "interaction_id": comm['interaction_id'], 
                        "instance": json.dumps(primary_comm['instance'],default=str),
                        "message_iter": comm['message_iter'],
                })
        postgresql_session.commit()
    else:
        contact_log_id = None
    return contact_log_id

def send_email_comm(primary_comm,all_comms,contact_info):
    debug('Calling send_comm for email...')
    if env.get("test"):
        anchor = primary_comm['anchor']
    else:
        anchor = primary_comm['anchor_id']
    message_properties = primary_comm['communication_properties']['message']
    email = contact_info.get("Email")
    timezone = contact_info.get("Timezone")
    fromAddress = contact_info.get("FromAddress")
    preferredLanguage = contact_info.get("PreferredLanguage")
    contact_log_id = create_contact_log(primary_comm,all_comms,"email",primary_comm['priority_value'],primary_comm['type'],email,timezone)
    if email is None:
        log_error('No email address for patient to send email.',contact_log_id)
        return
    if preferredLanguage not in message_properties:
        preferredLanguage = 'en'
    message_email = message_properties[preferredLanguage]['body']
    subject = message_properties[preferredLanguage]['subject']
    template = None
    if primary_comm['type'] == 'notification':
        template = 'notification_email'
        if 'template' in primary_comm['communication_properties'] and primary_comm['communication_properties']['template']:
            template = primary_comm['communication_properties']['template']
    if not env.get("live"):
        message_email = message_email + " (subject:" + str(primary_comm['subject_id']) + ")"
    try:
        subject, message_email = build_email_message(anchor=anchor,interval_name=primary_comm['interval_name'],message=message_email,subject=subject,interaction_id=primary_comm['interaction_id'],template=template,language=preferredLanguage)
    except Exception as e:
        log_error('Could not generate email message.',contact_log_id)
        debug(e)
        return
    message_json = json.dumps({
        "from_addr": fromAddress,
        "recipients": [email],
        "subject": subject,
        "html_body": message_email,
        "attachments": [],
        "timezone": timezone,
        "table_name": "contact_log",
        "fk": contact_log_id,
        "error_callback": CONTACT_ERROR_CALLBACK
    })
    deliver_comm('email', email, message_json, contact_log_id)

# @todo remove: this will be replaced by send_sms_queue, but for now that isn't working for notificiation type comms
def send_sms_comm(message_properties,primary_comm,all_comms,contact_info):
    debug('Calling send_comm for sms...')
    if env.get("test"):
        anchor = primary_comm['anchor']
    else:
        anchor = primary_comm['anchor_id']
    message_properties = primary_comm['communication_properties']['message']
    phone = contact_info.get("MobilePhone")
    timezone = contact_info.get("Timezone")
    fromAddress = contact_info.get("FromAddress")
    preferredLanguage = contact_info.get("PreferredLanguage")
    contact_log_id = create_contact_log(primary_comm,all_comms,"sms",primary_comm['priority_value'],primary_comm['type'],phone,timezone)
    if phone is None:
        log_error('No phone for patient to send sms.',contact_log_id)
        return
    if preferredLanguage not in message_properties:
        preferredLanguage = 'en'
    message_sms = message_properties[preferredLanguage]['text']
    if not env.get("live"):
        message_sms = message_sms + " (subject:" + str(primary_comm['subject_id']) + ")"
    try:
        message_sms = build_sms_message(anchor=anchor,interval_name=primary_comm['interval_name'],message=message_sms,interaction_id=primary_comm['interaction_id'],language=preferredLanguage)
    except Exception as e:
        log_error('Could not generate sms message.',contact_log_id)
        debug(e)
        return
    big_pool = "MGeb3bce32200451f591f4b6f1c5d827b1"
    message_json = json.dumps({
        "numberPool": big_pool,
        "number": phone, 
        "message": message_sms,
        "timezone": timezone,
        "table_name": "contact_log",
        "fk": contact_log_id,
        "error_callback": CONTACT_ERROR_CALLBACK
    })
    deliver_comm('simple_sms', phone, message_json, contact_log_id)
    
def send_sms_queue(comms, notification_comms):
    debug('Calling send_comm for sms...')

    primary_comm = None
    if len(comms) > 0:
        primary_comm = comms[0]
        subject_id = comms[0].get('subject_id')
    elif len(notification_comms) > 0:
        primary_comm = notification_comms[0]
        subject_id = notification_comms[0].get('subject_id')
    if not subject_id:
        return False
    
    try:
        contact_info = get_contact_info_for_subject(subject_id)
    except Exception as e:
        contact_log_id = create_contact_log(primary_comm, comms + notification_comms)
        log_error('Could not connect to ContactInfo table',contact_log_id)
        debug(e)
        return
    if not contact_info:
        contact_log_id = create_contact_log(primary_comm, comms + notification_comms)
        log_error('No contact info for patient.',contact_log_id)
        return
    
    phone = contact_info.get('MobilePhone')
    timezone = contact_info.get('Timezone')
    preferredLanguage = contact_info.get('PreferredLanguage', 'en')
    phone = contact_info.get("MobilePhone")
    timezone = contact_info.get("Timezone")
    preferredLanguage = contact_info.get("PreferredLanguage")
    if not preferredLanguage:
        preferredLanguage = 'en'

    inform_nodes = []
    survey_json_list = []

    # add the primary notification message to be processed as an inform
    comm_type = 'direct'
    priority_value = 0
    if len(notification_comms) > 0:
        primary_comm = notification_comms[0]
        comm_type = 'notification'
        priority_value = primary_comm['priority_value']
        message = primary_comm['communication_properties']['message']
        sms_content = build_sms_message(
            primary_comm['anchor_id'], 
            primary_comm['interval_name'],
            message.get(preferredLanguage, message['en'])['text'],
            interaction_id=primary_comm['interaction_id']
        )
        inform_nodes.append(inform_node('qInteractionInform' + str(primary_comm['interaction_id']), sms_content))
    
    for comm in comms:
        interaction_id = comm['interaction_id']
        interaction = None
        with get_postgresql_session() as postgresql_session:
            result = postgresql_session.execute(text("SELECT * FROM interaction WHERE id = :id LIMIT 1"),{"id": interaction_id})
            interaction = result.mappings().one_or_none()
        if not interaction:
            continue

        if isinstance(interaction['properties'], str):
            interaction['properties'] = json.loads(interaction['properties'])

        type = interaction['properties'].get('type', 'unknown')

        message = comm['communication_properties']['message']
        if comm['medium'] != 'sms':
            continue

        if type == 'cs1survey':
            if 'cacheFile' in interaction['properties']:
                survey_json_list.append(get_survey_json(CS_URL, interaction['properties']['cacheFile']))
        else:
            sms_content = build_sms_message(
                comm['anchor_id'], 
                comm['interval_name'],
                message.get(preferredLanguage, message['en'])['text'],
                interaction_id=comm['interaction_id']
            )
            inform_nodes.append(inform_node('qInteractionInform' + str(comm['interaction_id']), sms_content))

    sms_queue = new_survey('sms_queue_informs', inform_nodes)
    if survey_json_list:
        sms_queue = survey_merge('sms_queue_surveys', [sms_queue] + survey_json_list)

    # gather the CS1 patient data required in the JSON for the messaging app
    with get_mysql_session() as cur:
        result = cur.execute(text("""SELECT `patients`.`Owner`,
                `patients`.`PatientID`,
                `patients`.`FirstName`,
                `patients`.`LastName`,
                `conset`.`PreferredPhoneNum` AS `HomePhone`,
                `conset`.`PreferredPhoneNum` AS `WorkPhone`,
                `conset`.`PreferredPhoneNum` AS `MobilePhone`,
                `conset`.`TimeZone` AS `TimeZone`,
                `conset`.`TextEnabled` AS `TextEnabled`,
                `conset`.`IsSmartPhone`,
                `conset`.`ContactType`,
                `conset`.`TextLinksEnabled`,
                `conset`.`EmailLinksEnabled`,
                `conset`.`PreferredLanguage`,
                `contact`.`Email`
                FROM `CareSenseData`.`Patients` AS `patients`
                LEFT JOIN
                ( SELECT *
                FROM `CareSensePathways`.`ContactSettings`
                WHERE `PatientID` = :patientid
                ORDER BY id DESC LIMIT 1 ) AS `conset` ON `conset`.`PatientID` = `patients`.`PatientID`
                LEFT JOIN
                ( SELECT *
                FROM `CareSenseData`.`ContactInfo`
                WHERE `PatientID` = :patientid
                    AND `Email` IS NOT NULL
                    AND TRIM(`Email`) != ''
                    AND Status = 4
                ORDER BY `EntryDate` DESC LIMIT 1 ) AS `contact` ON `contact`.`PatientID` = `patients`.`PatientID`
                WHERE `patients`.`PatientID` = :patientid
                LIMIT 1
            """), {
            'patientid': subject_id,
        })
        patient_object = result.one_or_none()._asdict()

    contact_log_id = create_contact_log(primary_comm,comms + notification_comms,"sms",priority_value,comm_type,phone,timezone)
    big_pool = 'MGeb3bce32200451f591f4b6f1c5d827b1'
    message_json = json.dumps(sms_queue | {
        'bypassHourRestriction': False,
        'numberPoolSID': big_pool,
        'phoneNumber': phone, 
        'timezone': timezone,
        'userID': patient_object['Owner'],
        'patient_id': subject_id,
        'patient': patient_object,
        'table_name': 'contact_log',
        'fk': contact_log_id,
        'error_callback': CONTACT_ERROR_CALLBACK,
    })
    deliver_comm('sms', phone, message_json, None)
    
def send_app_comm(message_properties,primary_comm,all_comms,contact_info):
    debug('Calling send_comm for app...')
    contact_log_id = create_contact_log(primary_comm,all_comms,"app",primary_comm['type'])
    email = contact_info.get("Email")
    phone = contact_info.get("MobilePhone")
    timezone = contact_info.get("Timezone")
    fromAddress = contact_info.get("FromAddress")
    preferredLanguage = contact_info.get("PreferredLanguage")
    notifyDevices = contact_info.get("NotifyDevices")

    # they do need an email and phone # for the app
    if email is None or phone is None:
        log_error('No contact info for patient to send app message.',contact_log_id)
    try:
        # notifyDevices needed for sending app notifications, it only exists if they are a user
        if notifyDevices is None:
            log_error('Patient has not completed their patient app registration.',contact_log_id)
        else:
            # TODO/FIXME - 
            # In addition to pushing FCM notifications to the app,
            # we will also need to update _eventsGet
            # patientapp\js\actions\timeline.js in app calls application\sections\api\v1\controllers\pathwayController.php --> index()
            # which in turn calls _eventsGet.
            # We will need to update these functions so they also return any needed v2 events for the patient.
            # turn this off until we fully wire up app per above
            # TODO - will need to test this stuff with PHP/react app stuff
            # send_app_notification(json.loads(notifyDevices).get("firebase"), badge=1)
            pass
    except Exception as e:
        log_error(f'Error sending push notification to patient app',contact_log_id)
        debug(e)

def send_comm(primary_comm,all_comms=None):
    subject_id = primary_comm.get('subject_id', 'unknown')
    anchor_id = primary_comm.get('anchor_id', 'unknown')
    medium = primary_comm.get('medium')
    interaction_id = primary_comm.get('interaction_id', 'unknown')
    interval_id = primary_comm.get('interval_id', 'unknown')
    debug(
        f'send message for subject_id={subject_id}, anchor_id={anchor_id}, '
        f'interaction_id={interaction_id}, interval_id={interval_id}'
    )
    
    # function now provides the contactinfo defaults
    try:
        contact_info = get_contact_info_for_subject(primary_comm['subject_id'])
    except Exception as e:
        contact_log_id = create_contact_log(primary_comm,all_comms)
        log_error('Could not connect to ContactInfo table',contact_log_id)
        debug(e)
        return
    if not contact_info:
        contact_log_id = create_contact_log(primary_comm,all_comms)
        log_error('No contact info for patient.',contact_log_id)
        return
    
    reported_empty_message = False
    message = primary_comm['communication_properties']['message']
    if not isinstance(message, dict):
        if not reported_empty_message:
            reported_empty_message = True
            debug(f"message not dict, actual type: {type(message)}")
    medium = primary_comm.get('medium','unknown')
    if medium == 'email':
        send_email_comm(primary_comm, all_comms, contact_info)
    elif medium == 'app':
        send_app_comm(primary_comm, all_comms, contact_info)
    else:
        debug(f'message medium {medium} not supported')
        
def send_comms_for_subject(comms):
    if not comms:
        debug('comms empty, nothing to send')
        return False
    found_notification = False
    notification_comm = None
    notification_comms = []
    sms_notifications = []

    sms_queue_comms = []

    # get required localization for determining whether to send now
    subject_id = comms[0]['subject_id']
    contact_info = get_contact_info_for_subject(subject_id)
    try:
        contact_info = get_contact_info_for_subject(subject_id)
    except Exception as e:
        contact_log_id = create_contact_log(comms[0], comms)
        log_error('Could not connect to ContactInfo table',contact_log_id)
        debug(e)
        return
    try:
        local_tz = ZoneInfo(contact_info['Timezone'] or 'America/New_York')
    except Exception:
        local_tz = ZoneInfo("America/New_York")
    local_now = datetime.datetime.now(local_tz)
    default_send_hour = 9

    for comm in comms:
        send_hour = comm['send_time'].hour if comm.get('send_time') else default_send_hour
        now_utc = datetime.datetime.now(datetime.timezone.utc)
        scheduled_at = datetime.datetime(
            year=now_utc.year,
            month=now_utc.month,
            day=now_utc.day,
            hour=send_hour,
            minute=0,
            second=0,
            microsecond=0,
            tzinfo=local_tz
        )        
        send_hour = comm['send_time'].hour if comm.get('send_time') else default_send_hour
        if scheduled_at < local_now:
            continue

        if isinstance(comm['communication_properties'], str):
            comm['communication_properties'] = json.loads(comm['communication_properties'])

        #only send messages immediately if they're not SMS; otherwise, queue them
        if comm['medium'] == 'sms':
            if comm['type'] == 'notification':
                sms_notifications.append(comm)
            else:
                sms_queue_comms.append(comm)
        else:
            if comm['type'] == 'notification':
                if not found_notification:
                    found_notification = True
                    notification_comm = comm
                else:
                    notification_comms.append(comm)
            elif comm['type'] == 'direct':
                send_comm(comm)
            else:
                debug('bad comm:')
                debug(comm)
            
    if found_notification:
        send_comm(notification_comm,notification_comms)

    # if our highest priority message is not ready to send, don't send any yet
    if len(sms_queue_comms) != 0 or len(sms_notifications) != 0:
        send_sms_queue(sms_queue_comms, sms_notifications)

    return True

def send_comms():
    debug("sending comms...")
        
    global postgresql_session 
    postgresql_session = get_postgresql_session()

    if(env.get("test")):
        #@todo replace this very basic test mechanism with some kind of testing framework that sets up test cases and runs through them
        #@todo the test framework could use the pgunit setup functions to create the test cases in the test schema and then query them the current else statement does
        contact_next = get_test_contact_next()
        print(contact_next)
    else:
        postgresql_session.execute(text("SELECT fn_interaction_schedule_recache()"))
        #@todo we also need to order by ordinal, but we need a "universal ordinal value" of some kind for nested ordinals in the interaction_schedule view/table, not 100% sure how that would work
        if(env.get("subject_id")):
            result = postgresql_session.execute(text("""SELECT * FROM "contact_next" WHERE "subject_id" = :subject_id ORDER BY fn_priority_value(communication_properties->>'priority')"""), {"subject_id":env.get("subject_id")})
        else:
            result = postgresql_session.execute(text("SELECT * FROM contact_next ORDER BY fn_priority_value(communication_properties->>'priority')"))
        contact_next = result.mappings()
        if not contact_next:
            debug('no comms to send')
            return False

    #organize by subject so that we don't oversend
    subject_communications = {}
    for contact_item in contact_next:
        subject_id = contact_item['subject_id']
        if not subject_id in subject_communications:
            subject_communications[subject_id] = []
        subject_communications[subject_id].append(contact_item)

    unique_subject_count = len(subject_communications)
    debug(f'found comms for {unique_subject_count} subjects, attempting to send...')

    for subject_id in subject_communications:
        send_comms_for_subject(subject_communications[subject_id])
    
    close_sessions()
    
    debug('send comms complete')
        
    return True

if __name__ == '__main__':
    send_comms()