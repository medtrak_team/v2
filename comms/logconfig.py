import logging.config
import sys


LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "incremental": False,
        "formatters": {
    "debug": {
        "format": "%(levelname)-7s: [%(asctime)s] [%(process)d:%(threadName)s:%(module)s:%(lineno)05d] [%(name)s] %(message)s",
        "datefmt": "%Y-%m-%d %H:%M:%S",
        # "class": "pythonjsonlogger.jsonlogger.JsonFormatter", # <-------- install python-json-logger to get JSON logs
    },
    "info": {
        "format": "%(levelname)-7s: [%(asctime)s] [%(module)s:%(lineno)d] [%(name)s] %(message)s",
        "datefmt": "%Y-%m-%d %H:%M:%S",
    },
    "simple": {"format": "%(levelname)s %(message)s"},
    "email": {"format": "[%(asctime)s] [%(pathname)s] [%(module)s:%(lineno)d]\n%(message)s"},
},
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "debug",
            "stream": sys.stdout,
        },
        # Additional handlers...
    },
    "loggers": {
        "": {
            "handlers": ["console"],
            "level": "DEBUG",
            "propagate": True,
        },
    },
}

