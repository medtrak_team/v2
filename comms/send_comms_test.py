import json
import datetime

# this emulates objects that are otherwise 
# returned by the contact_next view in the db
def get_test_contact_next():
    return [{
        "subject_id": 1097277027,
        "anchor": {
            "subject_id":1097277027,
            "interaction_id":0,
            "date": datetime.date.today(),
            "properties": {"anatomy": "left knee"}
        },
        "interaction_id": 0,
        "instance":	"{""anatomy"": ""left knee""}",
        "interval_id": 0,
        "interval_name": "Preoperative",
        "communication_id": 0,
        "start_date": "2024-08-03",
        "end_date":	"2024-10-27",
        "communication_properties":	json.dumps({
            "type": "notification", 
            "message": [{
                "en": {
                    "text": "{{site_name}} here! Reminder -  {{anchor.clinician}} would like you to complete the following survey related to your appointment or surgery on {{anchor.date}} by clicking this link: {{link}}"
                },
                "medium": "sms"
            }, {
                "en": {
                    "body": "{{anchor.clinician}} here! Reminder - I would like you to complete the following survey related to your appointment or surgery on {{anchor.date}} by clicking this link before your surgery:", 
                    "subject": "{{anchor.clinician}} | Please click to take a quick survey"
                }, 
                "medium": "email"
            }], 
            "priority": "gentle reminder", 
            "notification_strategy": "sendonaregularschedule", 
            "notification_minimum_frequency": "12%", 
            "notification_minimum_frequency_unit": "Percent"
        })
    }]