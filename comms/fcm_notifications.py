"""
Firebase app/push notifications.
See https://firebase.google.com/docs/cloud-messaging/migrate-v1
Also see https://console.firebase.google.com/u/1/project/caresenseapp/overview
as medtrakinc@gmail.com
"""
from datetime import datetime
import logging
import sys
import os
import firebase_admin
from firebase_admin import credentials, messaging
if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
firebase_admin.initialize_app(
    credentials.Certificate(
        os.getenv("GOOGLE_APPLICATION_CREDENTIALS", "fcm-service-account.json")
    )
)

def send_app_notification(token, title=None, body=None, badge=None):
    logger.info(
        "outbound fcm notification: token=`%s` title=`%s` body=`%s` badge=%s",
        token,
        title,
        body,
        badge,
    )
    notification = None
    if isinstance(title, str) and isinstance(body, str):
        title = title.strip()
        body = body.strip()
        if title and body:
            notification = messaging.Notification(title=title, body=body)
        else:
            title = body = None
    apns_config = android_config = None
    if isinstance(badge, int) and badge >= 0:
        apns_config = messaging.APNSConfig(
            payload=messaging.APNSPayload(aps=messaging.Aps(badge=badge))
        )
        android_config = messaging.AndroidConfig(
            notification=messaging.AndroidNotification(
                notification_count=badge
            )
        )
    try:
        messaging.send(
            messaging.Message(
                notification=notification,
                token=token,
                apns=apns_config,
                android=android_config,
            )
        )
    except Exception as e:
        logger.exception(f"failed to send fcm notification: {e}")
        raise Exception(f"failed to send fcm notification: {e}")



def test_send(to_address):
    # Title and body
    title = "Test Notification Title"
    body = "At the tone, the time will be " + datetime.now().isoformat() + " ... BEEP!"
    logger.info("FCM TEST 1")
    send_app_notification(to_address, title=title, body=body)
    # Just badge
    badge = 42
    logger.info("FCM TEST 2")
    send_app_notification(to_address, badge=42)
    # Title, body, and badge
    send_app_notification(to_address, title=title, body=body, badge=42)

if __name__ == "__main__":
    token = sys.argv[1]
    assert token
    test_send(token)