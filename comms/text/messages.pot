# Translations template for UNKNOWN.
# Copyright (C) 2025 ORGANIZATION
# This file is distributed under the same license as the UNKNOWN project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2025.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: UNKNOWN 0.0.0\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2025-02-05 10:12-0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.14.0\n"

#: templates/notification_email.html:84
msgid "Click here to start"
msgstr ""

#: templates/notification_email.html:118
#, python-format
msgid "Hi %(first_name)s,"
msgstr ""

#: templates/notification_email.html:146
msgid "Click here to<br><span style=\"font-size:22px\">start</span>"
msgstr ""

#: templates/notification_email.html:149
msgid ""
"If you have any questions, please respond to this email and we'll get "
"back to you,"
msgstr ""

