from setuptools import setup, find_packages
from babel.messages import frontend as babel

setup(
    packages=find_packages(),
    cmdclass = {'compile_catalog': babel.compile_catalog,
                'extract_messages': babel.extract_messages,
                'init_catalog': babel.init_catalog,
                'update_catalog': babel.update_catalog},
)