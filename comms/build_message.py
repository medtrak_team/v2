import secrets
import bleach
import markdown
import json
import logging
from sqlalchemy import text
from types import SimpleNamespace
from babel.support import Translations, Locale
from babel.dates import format_date, format_time
from jinja2 import TemplateNotFound, Template, Environment, FileSystemLoader, select_autoescape
from flask import jsonify # @todo remove

#add parent dir to path before importing parent path scripts
import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from shared.environment import Environment as AppEnvironment
from shared.db import get_mysql_session, get_postgresql_session, close_sessions
from comms.util import debug

logger = logging.getLogger(__name__)
env = AppEnvironment()

def get_take_interaction_link(subject_id):
    # Get the take interaction link based on the environment
    if(env.get("test")):
        take_interaction_link = "LINK"
    elif(env.get("live")):
        take_interaction_link = "https://caresense.com/survey/v2/"
    else: 
        take_interaction_link = "https://staging.caresense.com/survey/v2/"
        
    apiKey = ""
    if not env.get("test"):
        with get_mysql_session() as mysql_session:
            #@todo figure out the expire date from the comm so that we make sure they have enough time to take the survey... for now we'll just do one month
            result = mysql_session.execute(text("""
                SELECT APIKey, DATE(Expires) FROM CareSense.APIKeys
                WHERE PatientID = :id
                AND Expires IS NOT NULL 
                AND DATEDIFF(Expires, NOW()) >= 28
                ORDER BY Created DESC
                LIMIT 1
                """),{"id": subject_id})
            apiKeyRow = result.mappings().one_or_none()
            if apiKeyRow:
                apiKey = apiKeyRow.get("APIKey")
            else:
                apiKey = secrets.token_urlsafe(10)
                mysql_session.execute(text("""
                    INSERT INTO CareSense.APIKeys (APIKey, UserID, PatientID, Expires)
                    SELECT :apiKey, Owner, PatientID, DATE_ADD(NOW(), INTERVAL 54 DAY) 
                    FROM CareSenseData.Patients
                    WHERE PatientID = :id
                    """),{"id": subject_id,"apiKey": apiKey})
                mysql_session.commit()
                
    take_interaction_link = take_interaction_link + str(apiKey)
        
    return take_interaction_link

def build_placeholders(anchor, interaction, interval_name, language, target):
    locale = Locale(language)

    placeholders = {'link': get_take_interaction_link(anchor.get('subject_id')), 'interval_name': interval_name}
    
    # Get target data
    if(target is not None):
        placeholders['target'] = SimpleNamespace(owner=target.get("Owner"), first_name=target.get("FirstName"))
    else:
        with get_mysql_session() as mysql_session:
            result = mysql_session.execute(text("SELECT * FROM CareSenseData.Patients WHERE PatientID = :id LIMIT 1"),{"id": anchor.get('subject_id')})
            subject = result.mappings().one_or_none()
            placeholders['target'] = SimpleNamespace(owner=subject.get("Owner"), first_name=subject.get("FirstName"))
    owner = placeholders['target'].owner

    if anchor.get('date') is None:
        formatted_date = None
    else:
        formatted_date = anchor.get('date').strftime('%m/%d/%Y')  # MM/DD/YYYY
        #@NOTE not using below because other systems just always use american style, may need to reassess if we're ever international
        #formatted_date = format_date(anchor.get('date'), format='short', locale=locale)
    if anchor.get('time') is None:
        formatted_time = None
    else:
        formatted_time = anchor.get('time').strftime('%I:%M %p')  # HH:MM AM/PM (12-hour format)
        #@NOTE not using below because other systems just always use american style, may need to reassess if we're ever international
        #formatted_time = format_time(anchor.get('time'), format='short', locale=locale)

    placeholders['site_name'] = owner
    oc_settings_json = '{}'
    with get_mysql_session() as mysql_session:
        result = mysql_session.execute(text("""
            SELECT * FROM `CareSense`.`UserSettings` 
            WHERE `UserID` = :owner
            AND `Name` = 'SiteName'
            LIMIT 1
            """), {'owner': owner})
        row = result.mappings().one_or_none()
        if row:
            placeholders['site_name'] = row.get('Value')
        result = mysql_session.execute(text("""
            SELECT * FROM `CareSense`.`UserSettings` 
            WHERE `UserID` = :owner
            AND `Name` = 'OutcomesComplianceSettings'
            LIMIT 1
            """), {'owner': owner})
        row = result.mappings().one_or_none()
        if row:
            oc_settings_json = row.get('Value')
    oc_settings = json.loads(oc_settings_json)

    anchor_properties = {
        'clinician':placeholders['site_name'],
        **anchor.get('properties', {}),
        'date':formatted_date,
        'time':formatted_time
    }
    placeholders['anchor'] = SimpleNamespace(**anchor_properties)
    if interaction:
        interaction_properties = interaction.get('properties', {}).copy()
        interaction_properties.update(interaction_properties.get("localization", {}).get(language, {}))
        placeholders['interaction'] = SimpleNamespace(**(interaction_properties))

    if oc_settings.get("siteNameOnly"):
        placeholders['anchor'].clinician = placeholders['site_name']
    else:
        clinician_name = placeholders['anchor'].clinician
        clinician_name_parts = clinician_name.split(", ")
        placeholders['anchor'].clinician = f"{clinician_name_parts[1]} {clinician_name_parts[0]}" if len(clinician_name_parts) == 2 else clinician_name
        if oc_settings.get("clinician") and clinician_name in oc_settings["clinician"]:
            if "photo" in  oc_settings["clinician"][clinician_name]:
                placeholders['clinician_photo'] = oc_settings["clinician"][clinician_name]["photo"]

            if "signature" in  oc_settings["clinician"][clinician_name]:
                placeholders['anchor'].clinician = oc_settings["clinician"][clinician_name]["signature"]

            if "signature_desc" in  oc_settings["clinician"][clinician_name]:
                placeholders['anchor'].clinician_desc = oc_settings["clinician"][clinician_name]["signature_desc"]

    placeholders['header_logo'] = oc_settings.get("headerLogo")
    placeholders['header_color'] = oc_settings.get("headerColor")
    placeholders['button_color'] = oc_settings.get("buttonColor")
    
    return placeholders
    

def build_sms_message(anchor, interval_name, message, interaction_id = None, language = 'en', target = None):
    # Get db rows if not passed in
    if(isinstance(anchor,int)):
        with get_postgresql_session() as postgresql_session:
            result = postgresql_session.execute(text("SELECT * FROM anchor WHERE id = :id LIMIT 1"),{"id": anchor})
            anchor = result.mappings().one_or_none()

    interaction = None
    if interaction_id:
        with get_postgresql_session() as postgresql_session:
            result = postgresql_session.execute(text("SELECT * FROM interaction WHERE id = :id LIMIT 1"),{"id": interaction_id})
            interaction = result.mappings().one_or_none()

    if anchor is None :
        raise Exception("Anchor not found")
    
    logging.info(anchor)

    placeholders = build_placeholders(anchor, interaction, interval_name, language, target)
    jinja_template = Template(message)
    renderedTemplate = jinja_template.render(**placeholders)
    # print(renderedTemplate)
    return renderedTemplate    
    

# @note this assumes the subject is the target, it gets more complicated if they aren't
def build_email_message(anchor, interval_name, message, subject, template, interaction_id = None, language = 'en', target = None):
    # Get db rows if not passed in
    if(isinstance(anchor,int)):
        with get_postgresql_session() as postgresql_session:
            result = postgresql_session.execute(text("SELECT * FROM anchor WHERE id = :id LIMIT 1"),{"id": anchor})
            anchor = result.mappings().one_or_none()

    if anchor is None :
        raise Exception("Anchor not found")
    
    logging.info(anchor)

    interaction = None
    if interaction_id:
        with get_postgresql_session() as postgresql_session:
            result = postgresql_session.execute(text("SELECT * FROM interaction WHERE id = :id LIMIT 1"),{"id": interaction_id})
            interaction = result.mappings().one_or_none()

    placeholders = build_placeholders(anchor, interaction, interval_name, language, target)

    jinja_template = Template(subject)
    subject = jinja_template.render(**placeholders)

    owner = placeholders['target'].owner
    
    locale = Locale(language)
    
    # Load translations for the user's custom text and then apply it
    translation_path = os.path.join(
        'comms', 'text', 'custom', owner
    )

    if os.path.exists(translation_path):
        translations = Translations.load(
            dirname=translation_path, locales=[locale]
        )
    else:
        translation_path = os.path.join(
            'comms', 'text'
        )
        translations = Translations.load(
            dirname=translation_path, locales=[locale]
        )
        
    translations.install()

    _ = translations.gettext
    placeholders['_'] = _

    # Clean some input text and then activate markdown
    message = bleach.clean(message)
    message = markdown.markdown(message)
    message_template = Template(message)
    message = message_template.render(**placeholders)

    if template:
        placeholders['message'] = message
        try:
            shared_templates = os.path.join('shared', 'templates','custom', owner)
            env = Environment(
                loader=FileSystemLoader(shared_templates),
                autoescape=select_autoescape(['html'])
            )
            logging.info(f'Trying to load template from: {shared_templates}...')
            jinja_template = env.get_template(f'{template}.html')
            logging.info(f'Successfully loaded template from shared custom templates for {owner}.')
        except TemplateNotFound:
            logging.info(f'shared/templates/custom/{owner}/{template}.html not found. Attempting to use root/templates{template}.html')
            # change jinja env to point to comms
            # standard_templates = os.path.join(project_root, 'templates')
            standard_templates = os.path.join('comms','templates')

            env = Environment(
                loader=FileSystemLoader(standard_templates),
                autoescape=select_autoescape(['html'])
            )

            logging.info(f'Trying to load standard fallback template from: {standard_templates}...')
            jinja_template = env.get_template(f'{template}.html')
            logging.info(f'Successfully loaded standard fallback template from: {standard_templates}')
        
        message = jinja_template.render(**placeholders)
        logging.info(f'subject: {subject}')
        logging.info(f'message: {message}')

    # print(renderedTemplate)
    return (subject, message)