# -*- coding: utf-8 -*-
import signal
import datetime
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.events import EVENT_JOB_EXECUTED, EVENT_JOB_ERROR
from send_comms import send_comms
import logging
import sys

from apscheduler.schedulers.blocking import BlockingScheduler

from comms.logconfig import LOGGING_CONFIG

logging.config.dictConfig(LOGGING_CONFIG)
logger = logging.getLogger(__name__)

logging.info("initializing V2 Comms App Scheduler...")

print("initializing V2 Comms App Scheduler...")

COMMUNICATION_SENDER = "V2 Communication Sender"
COMMUNICATION_SENDER_JOB_ID = "{}.{}".format(__name__, COMMUNICATION_SENDER)

# for allowing foreground process to be killed
def signal_handler(signum, frame):
    scheduler.shutdown(wait=False)
    print("Scheduler shut down successfully!")
    logging.info("Scheduler shut down successfully!")


scheduler = BlockingScheduler()
signal.signal(signal.SIGTERM, signal_handler)
signal.signal(signal.SIGINT, signal_handler)

# Event listener for job execution
def job_listener(event):
    if event.exception:
        print(f"Job {event.job_id} failed to execute: {event.exception}")
        logging.error(f"Job {event.job_id} failed to execute: {event.exception}")
    else:
        print(f"Job {event.job_id} executed successfully")
        logging.info(f"Job {event.job_id} executed successfully")

scheduler.add_listener(job_listener, EVENT_JOB_EXECUTED | EVENT_JOB_ERROR)

#@todo when we figure out the time piece we also want to schedule it more often
scheduler.add_job(send_comms, 
                  id=COMMUNICATION_SENDER_JOB_ID, 
                  replace_existing=True,
                  name=COMMUNICATION_SENDER, 
                  trigger="interval",
                  seconds=60*60, 
                  coalesce=True,
                  misfire_grace_time=5,
                  next_run_time=datetime.datetime.now())
print("starting V2 Comms App Scheduler...")
logging.info("starting V2 Comms App Scheduler...")

scheduler.start()
