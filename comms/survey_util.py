import json
import requests

def inform_node(id, text):
    return {
        'id': id,
        'type': 'question',
        'template': 'inform',
        'text': text,
        'survey_id': '**nomapping**',
        'table_name': '**nomapping**',
        'field_name': '**nomapping**'
    }

def new_survey(id, node_list):
    root_id = 'g_' + id
    survey_obj = { 
        'rootNodeID': root_id, 
        'nodes': {},
        'SurveyID': 'v2_' + id,
    }

    node_cnt = 0
    child_list = []
    for node in node_list:
        node_cnt += 1

        node_id = node.get('id', node.get('rootNodeID'))
        survey_obj['nodes'][node_id] = node
        child_list.append(node_id)
    
    root_node = {
        'id': root_id,
        'type': 'group',
        'numChildren': node_cnt,
        'order': child_list,
    }
    survey_obj['nodes'][root_id] = root_node

    return survey_obj

def survey_merge(id, survey_list):
    survey_obj = new_survey(id, [])    
    rootNodeID = survey_obj['rootNodeID']
    for survey_json in survey_list:
        # Get all node_ids already existing in combined survey
        existing_nodeids = list(survey_obj['nodes'].keys())
        # If the nodeid already exists in the combined, we rename it in combined and not in the survey we are looping over.
        for node_id in survey_json['nodes']:
            if node_id in existing_nodeids:
                node = survey_json['nodes'].pop(node_id)
                idx = 1
                to_id = '{}~{}'.format(node_id, idx)
                while to_id in existing_nodeids:
                    idx += 1
                    to_id = '{}~{}'.format(node_id, idx)
                node.id = to_id
                if rootNodeID == node_id:
                    rootNodeID = to_id
                survey_json['nodes'][to_id] = node
                for node_id in survey_json['nodes']:
                    nodei = survey_json['nodes'][node_id]
                    if hasattr(nodei, 'order'):
                        try:
                            for ii, val in enumerate(nodei['order']):
                                if val == node_id:
                                    survey_json['nodes'][node_id]['order'][ii] = to_id
                        except ValueError:
                            pass

                    if hasattr(nodei, 'conditions'):
                        try:
                            for ii, val in enumerate(nodei['conditions']):
                                hyph = val.find("-")
                                if hyph != -1:
                                    stripped_val = val[0:hyph]
                                    end = val[hyph:]

                                    if stripped_val == node_id:
                                        survey_json['nodes'][node_id]['conditions'][to_id + end] = survey_json['nodes'][node_id]['conditions'][val]
                                        survey_json['nodes'][node_id]['conditions'].pop(val)
                        except ValueError:
                            pass
        
        # Add nodes to combined 'nodes' object
        for node_id, node in survey_json['nodes'].items():
            survey_obj['nodes'][node_id] = node
        survey_obj['nodes'][rootNodeID]['order'].append(survey_json['rootNodeID'])
        survey_obj['nodes'][rootNodeID]['numChildren'] += 1
    return survey_obj

def get_survey_json(cs_url, cache_file):
    with requests.Session() as http_session:
        req = http_session.post(cs_url + '/api/json/show',
                data={
                    'SurveyName': cache_file,
                    'DeviceType': 'Phone',
                    'APIKey': '4360d10e19af3f6c4dba878033e2de1108dd886d',
            })
        survey_json = json.loads(req.text)

    # type errors are present in cached settings values that need to be corrected
    for node in survey_json.get('nodes', {}).values():
        settings = node.get('settings', [])
        if settings:
            settings["customResponseTag"] = settings.get("customResponseTag", False) in ["true", "True"]
            try:
                settings["startResponseAtNumber"] = int(settings.get("startResponseAtNumber", 1))
            except:
                settings["startResponseAtNumber"] = 1
    
    return survey_json