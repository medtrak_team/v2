# README

What is this app for?
- v2 communications backend, delivering packages to patients based on the communications rules for the pathways and interactions they are on

How do I get set up?
- Ensure you have Python 3 or greater installed
- Clone the entire v2 repo and run from that directory, it contains several utilities that share common code
- run `pip install -r requirements.txt`
- create a file `shared/site_settings.py` containing the None values from shared/defaultsettings.py

```py
SITE_SETTINGS = {
    "STAGING_MYSQL_PASSWORD":"",
    "PRODUCTION_MYSQL_PASSWORD":"",
    "POSTGRESQL_HOST":"localhost",
    "POSTGRESQL_PASSWORD":"",
    "JWT_SECRET":"",
}
```

- NOTE: to use the caresense v1 side you may need to activate the pgsql extension in your php.ini
- NOTE: On local servers site_settings.py should NEVER define PROD_MYSQL_* constants to avoid the risk of mistakes. However, on production servers those variables should be defined

Contribution Guidelines
- branch off of `master`
- write tests locally to ensure your changes are working
- (not yet set up) if additional testing is needed, create a pr into `test` with a reviewer and run against the test db 
- when your changes are tested, create a pr into `master` with a reviewer

Each system is in a subfolder of this parent directory. Consult the README for more information