# Make sure to define None values in your environment or site_settings.py
DEFAULT_SETTINGS = {
    "staging_mysql_host": "staging-cluster.cluster-c640ju7hdv2k.us-east-1.rds.amazonaws.com",
    "staging_mysql_username": "CSWebUser",
    "staging_mysql_password": None,
    "production_mysql_host": "prod-cluster.cluster-c640ju7hdv2k.us-east-1.rds.amazonaws.com",
    "production_mysql_username": "CSWebUser",
    "production_mysql_password": None,
    "postgresql_host": "v2.c640ju7hdv2k.us-east-1.rds.amazonaws.com",
    "postgresql_username": "postgres",
    "postgresql_password": None,
    "jwt_secret": None,
    "live": False,
}