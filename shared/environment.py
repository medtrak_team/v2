import argparse
import os
from shared.default_settings import DEFAULT_SETTINGS
from shared.site_settings import SITE_SETTINGS


class Environment:
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(Environment, cls).__new__(cls)
            cls._instance._init_parser()
        return cls._instance

    def _init_parser(self):
        self.parser = argparse.ArgumentParser(description="Application Environment Configuration")
        self.args = None  # Will hold parsed arguments

    def add_argument(self, *args, **kwargs):
        """
        Add a command-line argument to the parser.
        Accepts standard argparse argument parameters (e.g., name, type, help, etc.).
        """
        self.parser.add_argument(*args, **kwargs)

    def parse(self):
        """
        Parse command-line arguments.
        """
        self.args = self.parser.parse_args()

    def get(self, key):
        """
        Resolve a configuration value:
        Priority: Command-line > Local Site Settings > Environment Variable > Default Values
        """
        if self.args is None:
            self.parse()
        return (
            getattr(self.args, key, None)
            or SITE_SETTINGS.get(key, None)
            or os.getenv(key, None)
            or DEFAULT_SETTINGS.get(key, None)
        )
