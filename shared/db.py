# -*- coding: utf-8 -*-

from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker
#add parent dir to path before importing parent path scripts
import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from shared.environment import Environment

env = Environment()
env.add_argument('--postgresql_host', help='Postgres host')
env.add_argument('--postgresql_username', help='Postgres user')
env.add_argument('--postgresql_password', help='Postgres password')
env.add_argument('--live', action='store_true', help='Use production environment')
env.add_argument('--test', action='store_true', help='Use test environment')

PostgresSession = None
postgres_engine = None
def get_postgresql_session():
    global PostgresSession, postgres_engine
    if PostgresSession is None:
        POSTGRES_URL = f"postgresql://{env.get('postgresql_username')}:{env.get('postgresql_password')}@{env.get('postgresql_host')}"
        postgres_engine = create_engine(POSTGRES_URL, pool_pre_ping=True)
        PostgresSession = sessionmaker(bind=postgres_engine)
    session = PostgresSession()
    schema = "test" if env.get("test") else "live" if env.get("live") else "build"
    session.execute(text(f"SET search_path TO {schema}"))
    return session

StagingMySQLSession = None
staging_mysql_engine = None
def get_staging_mysql_session():
    global StagingMySQLSession, staging_mysql_engine
    if StagingMySQLSession is None:
        STAGING_MYSQL_URL = f"mysql://{env.get('staging_mysql_username')}:{env.get('staging_mysql_password')}@{env.get('staging_mysql_host')}"
        staging_mysql_engine = create_engine(STAGING_MYSQL_URL, pool_pre_ping=True)
        StagingMySQLSession = sessionmaker(bind=staging_mysql_engine)
    return StagingMySQLSession()

ProductionMySQLSession = None
production_mysql_engine = None
def get_production_mysql_session():
    global ProductionMySQLSession, production_mysql_engine
    if ProductionMySQLSession is None:    
        PRODUCTION_MYSQL_URL = f"mysql://{env.get('production_mysql_username')}:{env.get('production_mysql_password')}@{env.get('production_mysql_host')}"
        production_mysql_engine = create_engine(PRODUCTION_MYSQL_URL, pool_pre_ping=True)
        ProductionMySQLSession = sessionmaker(bind=production_mysql_engine)
    return ProductionMySQLSession()

def get_mysql_session():
    if(env.get("test")):
        return get_staging_mysql_session()
    elif(env.get("live")):
        return get_production_mysql_session()
    else:
        return get_staging_mysql_session()

def close_sessions():
    global postgres_engine, staging_mysql_engine, production_mysql_engine
    if postgres_engine is not None:
        postgres_engine.dispose()
    if staging_mysql_engine is not None:
        staging_mysql_engine.dispose()
    if production_mysql_engine is not None:
        production_mysql_engine.dispose()

if __name__ == '__main__':
    with get_mysql_session() as mysql_session:
        result = mysql_session.execute("SELECT 1 FROM CareSense.Users LIMIT 1").fetchall()
        if result:
            print("MySQL Test Query Passed")