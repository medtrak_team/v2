CREATE OR REPLACE FUNCTION tf_set_modified() RETURNS trigger AS $$
BEGIN
  NEW.modified := NOW();

  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION tf_set_created() RETURNS trigger AS $$
BEGIN
  NEW.created := NOW();

  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION tf_log_change() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    INSERT INTO changelog
    VALUES (TG_TABLE_NAME,OLD.id,now(),current_user,shared.fn_get_authenticated_user(),TG_OP,row_to_json(OLD.*)::jsonb);

    RETURN NEW;
END;$$;

CREATE OR REPLACE FUNCTION changelog_restore(
    arg_table TEXT,
    arg_fk BIGINT,
    arg_date TIMESTAMP
)
RETURNS VOID AS $$
DECLARE
    changelog_entry RECORD;
    record_exists BOOLEAN;
BEGIN
    -- Fetch the closest changelog record for the table and FK after the specified date
    SELECT *
    INTO changelog_entry
    FROM changelog
    WHERE "table" = arg_table
      AND fk = arg_fk
      AND modified >= arg_date
    ORDER BY modified ASC
    LIMIT 1;

    -- If no changelog entry is found, raise an exception
    IF NOT FOUND THEN
        RAISE EXCEPTION 'No changelog record found for table %, FK %, and date %', arg_table, arg_fk, arg_date;
    END IF;

    -- Check if the record already exists in the target table
    EXECUTE format('SELECT EXISTS (SELECT 1 FROM %I WHERE id = %L)', arg_table, arg_fk)
    INTO record_exists;

    IF record_exists THEN
        -- Update the existing record
        EXECUTE format(
            'UPDATE %I SET %s WHERE id = %L',
            arg_table,
            (
                SELECT string_agg(format('%I = %L', key, value::TEXT), ', ')
                FROM jsonb_each_text(changelog_entry.record) AS t(key, value)
            ),
            arg_fk
        );

        RAISE NOTICE 'Record for table %, FK % updated to values from changelog at %',
            arg_table, arg_fk, changelog_entry.modified;

    ELSE
        -- Insert the new record
        EXECUTE format(
            'INSERT INTO %I (%s) VALUES (%s)',
            arg_table,
            (
                SELECT string_agg(key, ', ')
                FROM jsonb_object_keys(changelog_entry.record) AS t(key)
            ),
            (
                SELECT string_agg(format('%L', value::TEXT), ', ')
                FROM jsonb_each_text(changelog_entry.record) AS t(key, value)
            )
        );

        RAISE NOTICE 'Record for table %, FK % inserted with values from changelog at %',
            arg_table, arg_fk, changelog_entry.modified;
    END IF;
END;
$$ LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS jsonb_deep_set;
CREATE OR REPLACE FUNCTION jsonb_deep_set(
    "target" JSONB,       -- The original JSONB object to modify (can be NULL)
    "path" TEXT[],        -- The array of keys representing the path
    "new_value" JSONB     -- The new value to set
) RETURNS JSONB AS $$
DECLARE
    "v_current" JSONB := COALESCE("target", '{}'::JSONB);   -- Initialize target if it's NULL
    "i" INT;                       -- Loop counter
BEGIN
    -- Iterate over the path except the last element
    IF array_length("path", 1) IS NOT NULL AND array_length("path", 1) > 1 THEN
        FOR "i" IN 1 .. array_length("path", 1) - 1 LOOP
            -- If the key doesn't exist, initialize it as an empty object
            IF NOT ("v_current" ? ("path"["i"])) THEN
                "v_current" := jsonb_set("v_current", "path"[1:"i"], '{}'::JSONB);
            END IF;
        END LOOP;
    END IF;

    -- Now set the new value at the last element of the path
    RETURN jsonb_set("v_current", "path", "new_value");
END;
$$ LANGUAGE plpgsql;