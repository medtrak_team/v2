ALTER TABLE ONLY anchor
    ADD CONSTRAINT fk_anchor_subject FOREIGN KEY (subject_id) REFERENCES "user"(id)
    ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY survey
    ADD CONSTRAINT fk_survey_subject FOREIGN KEY (subject_id) REFERENCES "user"(id)
    ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY interaction_complete_log
    ADD CONSTRAINT fk_interaction_complete_log_subject FOREIGN KEY (subject_id) REFERENCES "user"(id)
    ON DELETE CASCADE ON UPDATE CASCADE;
