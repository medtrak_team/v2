CREATE TYPE enum_privilege_action AS ENUM
    ('use', 'view', 'follow', 'edit', 'share');
CREATE TYPE enum_privilege_target AS ENUM
    ('pathway', 'patient_group', 'anchor_condition', 'interaction_set');

