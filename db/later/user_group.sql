-- ******************** User Group Memberships ********************
-- flatten memberships so we can use one query to find all groups that a user or group are a member of
CREATE OR REPLACE FUNCTION tf_user_group_membership_flatten_insert()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    AS $$
BEGIN

    IF NEW.child_user_group IS NOT NULL THEN
        -- make the child of this membership a member of the parent's parents
        INSERT INTO user_group_membership (parent_user_group_id, child_user_group, parent_connection_id, child_connection_id)
        SELECT parent_user_group_id, NEW.child_user_group, id, NEW.id
        FROM user_group_membership
        WHERE child_user_group = NEW.parent_user_group_id
        ON CONFLICT DO NOTHING;

        -- make the child's children a member of the parent of this membership
        INSERT INTO user_group_membership (parent_user_group_id, child_user_group, child_user, parent_connection_id, child_connection_id)
        SELECT NEW.parent_user_group_id, child_user_group, child_user, NEW.id, id
        FROM user_group_membership
        WHERE parent_user_group_id = NEW.child_user_group
        ON CONFLICT DO NOTHING;
    ELSEIF NEW.child_user IS NOT NULL THEN
        -- make the user in this membership a member of the parent's parents
        INSERT INTO user_group_membership (parent_user_group_id, child_user, parent_connection_id, child_connection_id)
        SELECT parent_user_group_id, NEW.child_user, id, NEW.id
        FROM user_group_membership
        WHERE child_user_group = NEW.parent_user_group_id
        ON CONFLICT DO NOTHING;
    END IF;
    
    RETURN NEW;
END;$$;
-- TODO disallow modifying group memberships to simplify queries

CALL dbinfo.fn_create_or_update_table('user_group','{
    id,
    name text NOT NULL,
    modified timestamp
    }');
DROP TRIGGER IF EXISTS tf_log_change ON user_group;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON user_group FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON user_group;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON user_group FOR EACH ROW EXECUTE FUNCTION tf_set_modified();

-- NOTE We can’t use built in postgres role system because we can’t grant privileges to modify sets of roles, you can either modify roles or you can’t, so we need our own tables to control that
CALL dbinfo.fn_create_or_update_table('user_group_membership','{
    id,
    parent_user_group_id bigint NOT NULL,
    child_user_group bigint,
    child_user bigint,
    assigner bigint DEFAULT shared.fn_get_authenticated_user(),
    child_connection_id bigint,
    parent_connection_id bigint,
    modified timestamp
    }');

CREATE UNIQUE INDEX in_user_group_membership_unique ON user_group_membership (parent_user_group_id, (child_user_group IS NOT NULL), (child_user IS NOT NULL), (assigner IS NOT NULL), (child_connection_id IS NOT NULL), (parent_connection_id IS NOT NULL));
DROP TRIGGER IF EXISTS tf_log_change ON user_group_membership;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON user_group_membership FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON user_group_membership;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON user_group_membership FOR EACH ROW EXECUTE FUNCTION tf_set_modified();
DROP TRIGGER IF EXISTS tf_user_group_membership_flatten_insert ON user_group_membership;
CREATE TRIGGER tf_user_group_membership_flatten_insert AFTER INSERT ON user_group_membership FOR EACH ROW EXECUTE FUNCTION tf_user_group_membership_flatten_insert();
COMMENT ON COLUMN user_group_membership.child_connection IS 'The user_group_membership entry that infromed the child of this entry when flattening memberships';
COMMENT ON COLUMN user_group_membership.parent_connection IS 'The user_group_membership entry that infromed the parent of this entry when flattening memberships';

-- user group privilege
CALL dbinfo.fn_create_or_update_table('user_group_privilege','{
    id,
    user_group_id bigint,
    user_id bigint,
    action shared.enum_privilege_action NOT NULL,
    target shared.enum_privilege_target NOT NULL,
    value text NOT NULL,
    modified timestamp
    }');
DROP TRIGGER IF EXISTS tf_log_change ON user_group_privilege;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON user_group_privilege FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON user_group_privilege;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON user_group_privilege FOR EACH ROW EXECUTE FUNCTION tf_set_modified();

-- meta
CALL dbinfo.fn_alter_table_add_constraint('user_group_membership','fk_user_group_memembership_child_connection_id','
    FOREIGN KEY (child_connection_id)
    REFERENCES user_group_membership (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('user_group_membership','fk_user_group_memembership_parent_connection_id','
    FOREIGN KEY (parent_connection_id)
    REFERENCES user_group_membership (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('user_group_membership','fk_user_group_memberhsip_child_group','
    FOREIGN KEY (child_user_group)
    REFERENCES user_group (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('user_group_membership','fk_user_group_membership_assigner','
    FOREIGN KEY (assigner)
    REFERENCES "user" (id) MATCH SIMPLE
    ON UPDATE SET NULL
    ON DELETE SET NULL');
CALL dbinfo.fn_alter_table_add_constraint('user_group_membership','fk_user_group_membership_child_user','
    FOREIGN KEY (child_user)
    REFERENCES "user" (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('user_group_membership','fk_user_group_membership_parent','
    FOREIGN KEY (parent_user_group_id)
    REFERENCES user_group (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE');


CALL dbinfo.fn_alter_table_add_constraint('user_group_privilege','fk_user_group_privilege_group','
    FOREIGN KEY (user_group_id)
    REFERENCES user_group (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('user_group_privilege','fk_user_group_privilege_user','
    FOREIGN KEY (user_id)
    REFERENCES "user" (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE');

-- test
create or replace function test_case_membership_flatten() returns void as $$
declare
	found_record boolean;
begin
	-- test: adding a parent to my parent makes me a member of that super parent
	INSERT INTO user_group_membership (parent_user_group_id,child_user_group) VALUES (2,1);
	SELECT EXISTS( SELECT 1 FROM user_group_membership WHERE parent_user_group_id = 2 and child_user = 1 ) INTO found_record;
	perform test_assertTrue('Adding super group does not cause child to become member', found_record);
	-- test: above also makes me a member of the parent of that parent
	SELECT EXISTS( SELECT 1 FROM user_group_membership WHERE parent_user_group_id = 4 and child_user = 1 ) INTO found_record;
	perform test_assertTrue('Adding super group does not cause child to become member', found_record);
	-- test: removing parent to super parent membership removes the child from the super parent
	DELETE FROM user_group_membership WHERE parent_user_group_id = 2 and child_user_group = 1;
	SELECT EXISTS( SELECT 1 FROM user_group_membership WHERE parent_user_group_id = 2 and child_user = 1 ) INTO found_record;
	perform test_assertTrue('Removing super group does not cause child to be removed from membership in super group', NOT found_record);
	-- test: above also removes me from the parent of that parent
	SELECT EXISTS( SELECT 1 FROM user_group_membership WHERE parent_user_group_id = 4 and child_user = 1 ) INTO found_record;
	perform test_assertTrue('Removing super group does not cause child to be removed from membership in super group', NOT found_record);
	-- test: adding a new child to a group causes that child to become a member of a super group
	INSERT INTO user_group_membership (parent_user_group_id,child_user) VALUES (3,1);
	SELECT EXISTS( SELECT 1 FROM user_group_membership WHERE parent_user_group_id = 4 and child_user = 1 ) INTO found_record;
	perform test_assertTrue('Adding child to group does not cause child to become member of the parent of that group', found_record);	
	-- test: removing that child from the group causes that child to no longer be a member of the super group
	DELETE FROM user_group_membership WHERE parent_user_group_id = 3 and child_user = 1;
	SELECT EXISTS( SELECT 1 FROM user_group_membership WHERE parent_user_group_id = 4 and child_user = 1 ) INTO found_record;
	perform test_assertTrue('Removing super group does not cause child to be removed from membership in super group', NOT found_record);
end;$$ language plpgsql set search_path from current;

-- ******************** Generate interaction_schedules ********************
-- not currently valid, not using privileges yet
/*
create or replace function test_case_interaction_schedule_privilege_change() returns void as $$
declare
	found_record boolean;
begin
	DELETE FROM "user_group_privilege" WHERE id = 1;
	SELECT EXISTS( SELECT 1 FROM interaction_schedule where subject_id = 1 and anchor_id = 1 and interaction_link_interval_id = 1) INTO found_record;
	perform test_assertTrue('Removing user privilege when group privilege still exists removed interaction_schedule', found_record);
	DELETE FROM "user_group_privilege" WHERE id = 2;
	SELECT EXISTS( SELECT 1 FROM interaction_schedule where subject_id = 1 and anchor_id = 1 and interaction_link_interval_id = 1) INTO found_record;
	perform test_assertTrue('Removing both user and group privilege did not remove interaction_schedule', NOT found_record);
	INSERT INTO "user_group_privilege" VALUES (1,null,1,'use','pathway','5');
	SELECT EXISTS( SELECT 1 FROM interaction_schedule where subject_id = 1 and anchor_id = 1 and interaction_link_interval_id = 1) INTO found_record;
	perform test_assertTrue('Readding user privilege did not readd interaction_schedule', found_record);
end;$$ language plpgsql set search_path from current;
*/

