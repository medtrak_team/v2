# -*- coding: utf-8 -*-
"""
    -- each local server needs to set up the following
    -- a secret for generating jwts
    select set_config('auth.secret', 'bestsecretever', false);
"""

from sqlalchemy import text
from tabulate import tabulate

#add parent dir to path before importing parent path scripts
import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

try:
    os.chdir('db')
except FileNotFoundError:
    pass

from shared.db import get_postgresql_session, close_sessions
from shared.environment import Environment

env = Environment()
env.add_argument('--install', action='store_true', help='install v2 database and support functions and libraries from scratch')
env.add_argument('--reinstall', action='store_true', help='delete and reinstall v2 database and support functions and libraries from scratch')
env.add_argument('--run_tests', action='store_true', help='run test suite for v2')
env.add_argument('--local', action='store_true', help='for local installs do not connect to the cs1 tables on test/staging/prod')


def main():
    global session
    session = get_postgresql_session()

    if env.get("install"):
        install()
    if env.get("reinstall") and env.get("local"):
        reinstall()
    if env.get("run_tests"):
        test()
    
    close_sessions()

def install():
    """
        create types, functions, schema, and views in that order (each relies on the previous)
        files should be written to be rerunnable (use exceptions or check if the item exists before adding it)
        schema files must have a "-- meta" comment before any foreign keys but after any unique constraints, the system will run the "-- meta" section after all the schemas have been set up
        this also includes running the files for CareSense v1 integration 
        once all of that is done set up the testing framework and tests
        finally set up privileges
    """
    print('installing schema...')
    
    result = session.execute(text('SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = \'dbinfo\' AND tablename  = \'migration_last_run\') as does_exist'))
    does_exist = result.scalar()

    session.execute(text('CREATE SCHEMA IF NOT EXISTS shared'))

    if does_exist:
        session.execute(text(f'SET search_path TO dbinfo'))
        execute_file('special/dbinfo.sql')
    else:
        with open('special/dbinfo.sql', 'r') as file:
            session.execute(text(file.read()))    
        session.execute(text(f'SET search_path TO dbinfo'))
        
    execute_file('special/auth.sql')
    
    #if not env.get("local"):
    #    execute_file('special/cs1_setup.sql')

    if env.get("live"):
        schema_names = ['live', 'test']
    else:
        schema_names = ['build', 'test']

    for schema_name in schema_names:
        # Process each schema here
        print('...building '+schema_name+' schema')
        session.execute(text(f'CREATE SCHEMA IF NOT EXISTS {schema_name}'))
        session.execute(text(f'SET search_path TO {schema_name}'))

        result = session.execute(text(f'SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = \'{schema_name}\' AND tablename  = \'migration_last_run\') as does_exist'))
        does_exist = result.scalar()

        # @todo replace with a better migration system
        if not does_exist or check_migration_last_run('special/migration_temp.sql'):
            with open('special/migration_temp.sql', 'r') as file:
                print('......running special/migration_temp.sql')
                session.execute(text(file.read()))


        function_files = find_migration_files_in_folder("functions")
        schema_files = find_migration_files_in_folder("schema")

        for filename in function_files:
            with open(filename, 'r') as file:
                print('......running '+filename)
                session.execute(text(file.read()))    
            
        #if not env.get("local"):
        #    if check_migration_last_run('special/cs1_tables.sql'):
        #        with open('special/cs1_tables.sql', 'r') as file:
        #            session.execute(text(file.read().replace('{{server}}',('mysql_production' if schema_name=='live' else 'mysql_staging' if schema_name=='build' else 'mysql_test'))))
        #        set_migration_last_run('special/cs1_tables.sql')
        metasql = {}
        for filename in schema_files:
            with open(filename, 'r') as file:
                contents = file.read().split('-- meta')
                if len(contents) > 1 and contents[1] != "":
                    metasql[filename] = contents[1]
                if len(contents) > 0 and contents[0] != "":
                    print('......running '+filename)
                    session.execute(text(contents[0]))
        session.commit()
        if metasql:
            for filename in metasql:
                print('......running meta '+filename)
                session.execute(text(metasql[filename]))
        for filename in schema_files:
            set_migration_last_run(filename)
        for filename in function_files:
            set_migration_last_run(filename)
        set_migration_last_run('special/migration_temp.sql')

    session.execute(text(f'SET search_path TO dbinfo'))
    execute_file('special/build.sql')

    session.execute(text('CREATE SCHEMA IF NOT EXISTS pgunit;'))
    test_files = find_migration_files_in_folder("test")
    for filename in test_files:
        session.execute(text('SET search_path TO pgunit,test;'))
        execute_file(filename)

    if env.get("local"):
        session.execute(
            text("ALTER DATABASE postgres SET \"auth.secret\" TO :jwt_secret"),
            {"jwt_secret": env.get("jwt_secret")},
        )
    session.commit()

def find_migration_files_in_folder(folder):
    filenames = []
    for filename in os.listdir(folder):
        filepath = os.path.join(folder, filename)
        if os.path.isfile(filepath) and filename.endswith('.sql'):
            if check_migration_last_run(filepath):
                filenames.append(filepath)
        if os.path.isdir(filepath):
            filenames.extend(find_migration_files_in_folder(filepath))
    return filenames

def execute_file(filename):
    if check_migration_last_run(filename):
        with open(filename, 'r') as file:
            print('...running '+filename)
            session.execute(text(file.read()))
        set_migration_last_run(filename)

def check_migration_last_run(filename):
    result = session.execute(text('SELECT last_run < to_timestamp(:filemtime) as was_run FROM migration_last_run WHERE filepath = :filename'),{"filename":filename,"filemtime":os.path.getmtime(filename)})
    row = result.fetchone()
    return True if row is None else row[0]

def set_migration_last_run(filename):
    session.execute(text('UPDATE migration_last_run SET last_run = NOW() WHERE filepath = :filename'),{"filename":filename})
    session.execute(text('INSERT INTO migration_last_run VALUES (:filename,NOW()) ON CONFLICT DO NOTHING'),{"filename":filename})

def reinstall():
    print('dropping and reinstalling schema...')
    
    #TODO This will need to be replaced with something less agressive, it should not be this easy to destroy the live or build dbs!
    session.execute(text("DROP SCHEMA IF EXISTS dbinfo CASCADE"))
    session.execute(text("DROP SCHEMA IF EXISTS shared CASCADE"))
    session.execute(text("DROP SCHEMA IF EXISTS live CASCADE"))
    session.execute(text("DROP SCHEMA IF EXISTS build CASCADE"))
    session.execute(text("DROP SCHEMA IF EXISTS test CASCADE"))
    session.execute(text("DROP SCHEMA IF EXISTS pgunit CASCADE"))
    
    session.commit()

    install();    


def test():
    session.execute(text("select * from pgunit.truncate_test()"))
    session.commit()
    
    result = session.execute(text("select * from pgunit.test_run_all()"))
    rows = result.mappings().all()

    if rows:
        print(tabulate(rows, headers="keys", tablefmt="grid"))
    else:
        print("No results found.")

    session.commit()


if __name__ == '__main__':
   main()