DO $$
BEGIN

-- Build privileges
GRANT USAGE ON SCHEMA build to caresense_admin;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA build to caresense_admin;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA build TO caresense_admin;
GRANT SELECT, UPDATE ON ALL SEQUENCES IN SCHEMA build to caresense_admin;

-- Test privileges
GRANT USAGE ON SCHEMA test to caresense_admin;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA test to caresense_admin;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA test TO caresense_admin;
GRANT SELECT, UPDATE ON ALL SEQUENCES IN SCHEMA test to caresense_admin;

-- Live privileges
GRANT USAGE ON SCHEMA live to caresense_admin, webuser;
GRANT SELECT ON ALL TABLES IN SCHEMA live to caresense_admin, webuser;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA live TO caresense_admin, webuser;

-- Shared privileges
GRANT USAGE ON SCHEMA shared to caresense_admin, webuser, anon;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA shared TO caresense_admin, webuser;
GRANT EXECUTE ON FUNCTION shared.login(text,text) TO anon;

-- Public privileges
GRANT USAGE ON SCHEMA public to caresense_admin, webuser, anon;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO caresense_admin, webuser, anon;
GRANT SELECT ON ALL TABLES IN SCHEMA public to caresense_admin;

GRANT ALL ON SCHEMA build to system_admin;
GRANT ALL ON ALL TABLES IN SCHEMA build to system_admin;
GRANT ALL ON ALL FUNCTIONS IN SCHEMA build TO system_admin;
GRANT ALL ON ALL SEQUENCES IN SCHEMA build to system_admin;

GRANT ALL ON SCHEMA live to system_admin;
GRANT ALL ON ALL TABLES IN SCHEMA live to system_admin;
GRANT ALL ON ALL FUNCTIONS IN SCHEMA live TO system_admin;
GRANT ALL ON ALL SEQUENCES IN SCHEMA live to system_admin;

GRANT ALL ON SCHEMA test to system_admin;
GRANT ALL ON ALL TABLES IN SCHEMA test to system_admin;
GRANT ALL ON ALL FUNCTIONS IN SCHEMA test TO system_admin;
GRANT ALL ON ALL SEQUENCES IN SCHEMA test to system_admin;

GRANT ALL ON SCHEMA pgcrypto to system_admin;
GRANT ALL ON ALL TABLES IN SCHEMA pgcrypto to system_admin;
GRANT ALL ON ALL FUNCTIONS IN SCHEMA pgcrypto TO system_admin;
GRANT ALL ON ALL SEQUENCES IN SCHEMA pgcrypto to system_admin;

GRANT ALL ON SCHEMA shared to system_admin;
GRANT ALL ON ALL TABLES IN SCHEMA shared to system_admin;
GRANT ALL ON ALL FUNCTIONS IN SCHEMA shared TO system_admin;
GRANT ALL ON ALL SEQUENCES IN SCHEMA shared to system_admin;

GRANT ALL ON SCHEMA public to system_admin;
GRANT ALL ON ALL TABLES IN SCHEMA public to system_admin;
GRANT ALL ON ALL FUNCTIONS IN SCHEMA public TO system_admin;
GRANT ALL ON ALL SEQUENCES IN SCHEMA public to system_admin;

EXCEPTION WHEN duplicate_object THEN RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
END
$$;
