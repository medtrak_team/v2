DO $$
BEGIN
-- Basic role used by postgrest that has no privileges but can "become" other roles
-- All roles that the website uses must be granted to authenticator
-- https://postgrest.org/en/stable/references/auth.html
CREATE ROLE authenticator LOGIN NOINHERIT NOCREATEDB NOCREATEROLE NOSUPERUSER;

-- Anonymous requests
GRANT anon TO authenticator;

-- Client requests
CREATE ROLE webuser NOINHERIT;
GRANT webuser TO authenticator;

-- CareSense Admin requests
CREATE ROLE caresense_admin NOINHERIT;
GRANT caresense_admin TO authenticator;

-- System Admin
-- postgrest SHOULD NOT have access to this role, since it can update anything
CREATE ROLE system_admin NOINHERIT;

EXCEPTION WHEN duplicate_object THEN RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
END
$$;