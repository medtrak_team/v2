DROP FUNCTION IF EXISTS json_determine_comm_set_id;
CREATE OR REPLACE FUNCTION json_determine_comm_set_id(
    "comm_name" text
)
RETURNS int
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    "comm_set_id" int;
BEGIN
    SELECT "id" FROM "communication_set"
    WHERE "shared" IS TRUE 
    AND "name" = "comm_name"
    INTO "comm_set_id";

    IF "comm_set_id" IS NULL THEN
        RAISE EXCEPTION 'Unable to find comm set with name %', "comm_name"
            USING HINT = 'Check that you are using only standard communication sets';
    END IF;

    RETURN "comm_set_id";
END;
$BODY$;

DROP FUNCTION IF EXISTS strip_nested_fields;
CREATE OR REPLACE FUNCTION strip_nested_fields(
    "arg_data" jsonb
)
RETURNS jsonb
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
    -- Remove modified column, which should only be managed in the database
    "arg_data" = "arg_data" - 'modified';
	RETURN "arg_data";
END;
$BODY$;

DROP FUNCTION IF EXISTS json_nest_interaction;
CREATE OR REPLACE FUNCTION json_nest_interaction(
    "pathway_json" jsonb,
	"interaction_id" int,
	"link" jsonb DEFAULT NULL
)
RETURNS jsonb
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
	"interaction" jsonb;
    "interval" jsonb;
	"intervals" jsonb;
	"interval_id" jsonb;
    "comm_name" varchar;
	"children" jsonb;
	"child_link" jsonb;
	"child" jsonb;
BEGIN
	"interaction" = ("pathway_json"->'interaction'->("interaction_id"::varchar))::jsonb;
    "interaction" = strip_nested_fields("interaction");

    -- Set fields that are used by the UI
    "interaction" = jsonb_set("interaction", '{selected}', to_jsonb(FALSE));
    "interaction" = jsonb_set("interaction", '{expanded}', to_jsonb(FALSE));
    "interaction" = jsonb_set("interaction", '{locked}', to_jsonb(FALSE));
	
    -- For each interval this interaction is linked to, find the interval data itself and add to object
	"intervals" = '[]'::json;
	FOR "interval_id" IN SELECT * FROM json_array_elements(("link"->'interval_ids')::json) as "interval_ids"
	LOOP
        "interval" = ("pathway_json"->'interval'->("interval_id"::varchar))::jsonb;
        "interval" = strip_nested_fields("interval");

        -- Standardize to a 'shared' field
        "interval" = jsonb_set("interval", '{shared}', to_jsonb("interval"->>'interval_set_id' IS NOT NULL));
        
        -- Trim unused fields
        "interval" = "interval" - 'interval_set_id';
        "interval" = "interval" - 'recurrence';

        -- Convert comm set id to a standard string that's easier for OpenAI to understand
        select "name" from communication_set 
        where "id" = ("interval"->'communication_set_id')::int 
        and "shared" IS TRUE
        into "comm_name";
        IF "comm_name" IS NULL THEN
            "comm_name" = 'custom';
        END IF;
        "interval" = jsonb_set("interval", '{communication}', to_jsonb("comm_name"));
        "interval" = "interval" - 'communication_set_id';

		"intervals" = "intervals" || "interval";
	END LOOP;
	"interaction" = jsonb_set("interaction", '{intervals}', "intervals");
	
    -- Process any interaction_links and convert to children interactions
	"children" = '[]'::jsonb;
	FOR "child_link" IN SELECT * FROM json_array_elements(("interaction"->'interaction_links')::json) as "links"
    LOOP
		"child" = json_nest_interaction("pathway_json", ("child_link"->>'child_interaction_id')::int, "child_link");
		"children" = "children" || "child";
	END LOOP;
	"interaction" = jsonb_set("interaction", '{children}', "children");
	"interaction" = "interaction" - 'interaction_links';

	RETURN "interaction";
END;
$BODY$;

DROP FUNCTION IF EXISTS json_import_nested_interaction;
CREATE OR REPLACE FUNCTION json_import_nested_interaction(
	"parent_id" bigint,
    "root_id" int,
    "interaction_json" jsonb,
    "index" int DEFAULT 0
)
RETURNS int
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    "cur_index" int;

    "interaction_id" bigint;
    "link_id" int;

    "child_json" jsonb;
    "child_id" int;
    "child_ids" jsonb = '[]';
BEGIN
    IF "root_id" IS NULL THEN
        RAISE EXCEPTION 'Must have a root to save any interactions.';
    END IF;

    "interaction_id" = "interaction_json"->>'id';
    IF "interaction_id" IS NULL THEN
        RAISE EXCEPTION 'Interaction is missing an ID, unable to save.';
    END IF;

    IF "parent_id" IS NULL THEN
        -- This means we're importing the root of an interaction set
        "interaction_id" = "root_id";
        "link_id" = NULL;
        -- We also assume the interaction exists to begin with, so we just need to update it, not insert anything
        UPDATE "interaction"
        SET 
            "name" = "interaction_json"->>'name',
            "shared" = ("interaction_json"->>'shared')::BOOLEAN,
            "properties" = "interaction_json"->'properties'
        WHERE "id" = "interaction_id";
    ELSE
        IF "interaction_id" > 0 THEN
            -- Update properties about the interaction, assuming it isn't shared
            UPDATE "interaction"
            SET 
                "properties" = "interaction_json"->'properties'
            WHERE "id" = "interaction_id"
            AND "shared" IS NOT TRUE;

            -- Find the interaction_link id for this child
            SELECT "id"
            FROM "interaction_link"
            WHERE "root_interaction_id" = "root_id"
            AND "parent_interaction_id" = "parent_id"
            AND "child_interaction_id" = "interaction_id"
            INTO "link_id";

            -- Update ordinal based on position in array
            UPDATE "interaction_link"
            SET "ordinal" = "index"
            WHERE "id" = "link_id";
        ELSE
            -- Create New Interaction
            INSERT INTO "interaction"
            ("name", "shared", "properties")
            VALUES
            (
                "interaction_json"->>'name', 
                ("interaction_json"->>'shared')::BOOLEAN, 
                "interaction_json"->'properties'
            )
            RETURNING "id" INTO "interaction_id";

            -- Create New Interaction Link
            INSERT INTO "interaction_link"
            ("root_interaction_id", "parent_interaction_id", "child_interaction_id", "ordinal")
            VALUES
            ("root_id", "parent_id", "interaction_id", "index")
            RETURNING "id" INTO "link_id";

        END IF;
    END IF;

    -- Process intervals for the interaction (if it's not the root)
    PERFORM json_import_nested_interaction_intervals("interaction_json"->'intervals', "link_id");

    -- Process children interactions
    -- First import all children in the JSON
    "cur_index" = 0;
    FOR "child_json" IN SELECT * FROM json_array_elements(("interaction_json"->'children')::json) as "children"
    LOOP
		SELECT json_import_nested_interaction("interaction_id", "root_id", "child_json", "cur_index") into "child_id";
        "child_ids" = "child_ids" || to_jsonb("child_id");
        "cur_index" = "cur_index" + 1;
	END LOOP;

    -- Delete any anonymous interactions that were removed
    DELETE FROM "interaction"
    WHERE "shared" IS NOT TRUE
    AND "id" IN (
        SELECT "child_interaction_id" 
        FROM "interaction_link"
        WHERE "root_interaction_id" = "root_id"
        AND "parent_interaction_id" = "interaction_id"
    )
    AND "id" NOT IN (
        SELECT value::int FROM jsonb_array_elements("child_ids")
    );

    -- Delete any links to shared interactions
    DELETE FROM "interaction_link"
    WHERE "root_interaction_id" = "root_id"
    AND "parent_interaction_id" = "interaction_id"
    AND "child_interaction_id" NOT IN (
        SELECT value::int FROM jsonb_array_elements("child_ids")
    )
    AND "child_interaction_id" IN (
        SELECT "id" FROM "interaction"
        WHERE "shared" IS TRUE
    );

    RETURN "interaction_id";
END;
$BODY$;

DROP FUNCTION IF EXISTS json_import_nested_interaction_intervals;
CREATE OR REPLACE FUNCTION json_import_nested_interaction_intervals(
    "intervals_json" jsonb,
    "link_id" int
)
RETURNS void
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    "interval_json" jsonb;
    "cur_interval_id" bigint;
    "interval_ids" jsonb = '[]';
    "comm_set_id" int;
BEGIN
    IF "link_id" IS NULL THEN
        -- Without a link, don't import intervals
        -- This should mean this is the root interaction
        RETURN;
    END IF;

    FOR "interval_json" IN SELECT * FROM json_array_elements(("intervals_json")::json) as "intervals"
    LOOP
        "cur_interval_id" = "interval_json"->>'id';
        IF "cur_interval_id" < 0 THEN
            "cur_interval_id" = json_import_nested_interval("interval_json");
            INSERT INTO "interaction_link_interval"
            ("interaction_link_id", "interval_id")
            VALUES
            ("link_id", "cur_interval_id");
        ELSE
            "comm_set_id" = json_determine_comm_set_id("interval_json"->>'communication');
            UPDATE "interval" 
            SET
                "start_day" = ("interval_json"->>'start_day')::numeric,
                "end_day" = ("interval_json"->>'end_day')::numeric,
                "after_created" = ("interval_json"->>'after_created')::numeric,
                "communication_set_id" = "comm_set_id"
            WHERE "id" = "cur_interval_id";
        END IF;

        "interval_ids" = "interval_ids" || to_jsonb("cur_interval_id");
    END LOOP;

    -- Clean up any anonymous intervals that were removed
    DELETE FROM "interval"
    WHERE "id" IN (
        SELECT "interval_id" FROM "interaction_link_interval"
        WHERE "interaction_link_id" = "link_id"
        AND "interval_id" NOT IN (
            SELECT value::int FROM jsonb_array_elements("interval_ids")
        )
    );

    -- Remove links to intervals that weren't deleted (i.e. shared ones in an interval set)
    -- DELETE FROM "interaction_link_interval"
    -- WHERE "interaction_link_id" = "link_id"
    -- AND "interval_id" NOT IN (
    --     SELECT value::int FROM jsonb_array_elements("interval_ids")
    -- );
END;
$BODY$;


DROP FUNCTION IF EXISTS json_import_nested_interval;
CREATE OR REPLACE FUNCTION json_import_nested_interval(
    "interval_json" jsonb
)
RETURNS int
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    "interval_id" int;
    "comm_set_id" int;
BEGIN
    "comm_set_id" = json_determine_comm_set_id("interval_json"->>'communication');

    IF "comm_set_id" IS NULL THEN
        RAISE EXCEPTION 'Unable to find comm set for interval with name %', "interval_json"->>'name'
            USING HINT = 'Check that you are using only standard communication sets';
    END IF;

    IF ("interval_json"->>'shared')::bool THEN
        -- For shared intervals, we want to find the matching interval and return that ID
        SELECT "id" FROM "interval"
        WHERE "interval_set_id" IS NOT NULL -- We should probably store the ID in the JSON somewhere, just not sure where it makes sense yet
        AND "name" = "interval_json"->>'name'
        AND "start_day" = ("interval_json"->>'start_day')::int
        AND "end_day" = ("interval_json"->>'end_day')::int
        AND "after_created" = ("interval_json"->>'after_created')::int
        AND "communication_set_id" = "comm_set_id"
        INTO "interval_id";

        IF "interval_id" IS NULL THEN
            RAISE EXCEPTION 'Unable to find standard interval with name %', "interval_json"->>'name'
                USING HINT = 'Check that the interval already exists and that provided JSON data does not differ from existing interval';
        END IF;
    ELSE
        -- For anonymous intervals, we need to create a new interval record
        INSERT INTO "interval"
        ("name", "start_day", "end_day", "after_created", "communication_set_id")
        VALUES
        (
            "interval_json"->>'name',
            ("interval_json"->>'start_day')::int, 
            ("interval_json"->>'end_day')::int, 
            ("interval_json"->>'after_created')::int,
            "comm_set_id"
        )
        RETURNING "id" INTO "interval_id";
    END IF;

    RETURN "interval_id";
END;
$BODY$;
