CREATE OR REPLACE FUNCTION  create_new_communication_set(thename text)
returns integer
language plpgsql
as
$$
declare
   ret_id integer;
begin

    INSERT INTO communication_set("name") VALUES (thename)
       RETURNING id 
       into ret_id;
       return ret_id;
end;
$$;


CREATE OR REPLACE FUNCTION create_new_communication(
	communication_set_id bigint,
	thestart text,
	theend text,
    theproperties json DEFAULT '{}'::json)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
   ret_id integer;
begin

    INSERT INTO communication(communication_set_id, "start", "end", properties) VALUES (communication_set_id, thestart, theend, theproperties)
       RETURNING id 
       into ret_id;
       return ret_id;
end;
$BODY$;
  
-- copy an interaction
DROP FUNCTION IF EXISTS copy_interaction;
CREATE OR REPLACE FUNCTION copy_interaction(interactionid bigint, setid bigint,parentinteractionid bigint DEFAULT NULL::bigint)
RETURNS json AS $$
DECLARE
    TABLE_RECORD record;
	TABLE_RECORD_interaction_link_INTERVAL record;
	ret_id_interaction integer; -- new copied interaction
	ret_id_interaction_link integer; -- individual interaction_link results
	ret_id_interaction_link_interval integer; -- individual interaction_link_interval results
    needed_interaction_links json[];
	needed_interaction_link_intervals json[];
	json_obj_interaction_result json; -- individual root interaction result
	json_obj_interaction_link_result json; -- individual root interaction result
	json_obj_interaction_link_intervals_result json; -- individual root interaction result
    json_obj_result json; -- all results
BEGIN
	
	
	-- create new interaction for the copied pathway, store it for returning
	FOR TABLE_RECORD in SELECT * FROM interaction WHERE interaction.id = interactionid
    LOOP
		SELECT create_new_interaction(TABLE_RECORD.name, TABLE_RECORD.properties::json) into ret_id_interaction;
		SELECT json_build_object('id', ret_id_interaction, 'name',TABLE_RECORD.name,'shared', TABLE_RECORD.shared,'properties',TABLE_RECORD.properties) into json_obj_interaction_result;
    END LOOP;
	
	-- if this interaction is on a node, update that node's child_interaction_id ref
	  IF parentinteractionid IS NOT NULL THEN

		  UPDATE interaction_link
		  SET child_interaction_id = ret_id_interaction 
		  WHERE child_interaction_id = interactionid and parent_interaction_id = parentinteractionid and root_interaction_id = setid;
	  END IF;
	  
	-- find the DIRECT CHILD nodes for this pathway (except this one) and copy them into new nodes with same interaction refs
	-- HOWEVER, their direct parents should change to now be the new copied interaction id
	-- return/store the new nodes by appending to needed_interaction_links array 
    FOR TABLE_RECORD in SELECT * FROM interaction_link WHERE interaction_link.parent_interaction_id=interactionid and interaction_link.root_interaction_id = setid and interaction_link.child_interaction_id != interactionid and interaction_link.child_interaction_id != ret_id_interaction and interaction_link.parent_interaction_id = interactionid  
    LOOP
		SELECT create_new_interaction_link(ret_id_interaction::bigint,ret_id_interaction,TABLE_RECORD.ordinal,TABLE_RECORD.child_interaction_id,TABLE_RECORD.condition_id) into ret_id_interaction_link;
		-- the parent AND set id for the direct children should be that of the newly created interaction copy
		SELECT json_build_object('id', ret_id_interaction_link, 'root_interaction_id',ret_id_interaction,'parent_interaction_id', ret_id_interaction,'ordinal',TABLE_RECORD.ordinal,'child_interaction_id',TABLE_RECORD.child_interaction_id,'condition_id',TABLE_RECORD.condition_id, 'modified',TABLE_RECORD.modified) into json_obj_interaction_link_result;
		SELECT ARRAY_APPEND(needed_interaction_links, json_obj_interaction_link_result) into needed_interaction_links; 
			-- for this found interaction_link, find all existing corresponding interaction_link_intervals for it
		FOR TABLE_RECORD_interaction_link_INTERVAL in SELECT * FROM interaction_link_interval WHERE interaction_link_interval.interaction_link_id = TABLE_RECORD.id
		LOOP
			SELECT create_new_interaction_link_interval(ret_id_interaction_link::bigint,TABLE_RECORD_interaction_link_INTERVAL.interval_id,TABLE_RECORD_interaction_link_INTERVAL.condition_id,TABLE_RECORD_interaction_link_INTERVAL.communication_set_id) into ret_id_interaction_link_interval;
			SELECT json_build_object('id', ret_id_interaction_link_interval, 'interaction_link_id',TABLE_RECORD.id,'interval_id', TABLE_RECORD_interaction_link_INTERVAL.interval_id,'condition_id',TABLE_RECORD_interaction_link_INTERVAL.condition_id,'communication_set_id',TABLE_RECORD_interaction_link_INTERVAL.communication_set_id, 'modified',TABLE_RECORD_interaction_link_INTERVAL.modified) into json_obj_interaction_link_intervals_result;
			SELECT ARRAY_APPEND(needed_interaction_link_intervals, json_obj_interaction_link_intervals_result) into needed_interaction_link_intervals; 
		END LOOP;
    END LOOP;
	
    /*
    @todo this no longer works because we're not using interaction_parent this way, we should just recursively copy the children in the function above as long as they aren't shared
    @note for now the above will work fine with one level deep copies with some weirdness around not sharing the now shared subchildren
	-- find the NON-DIRECT (AKA more deeply nested) nodes for this pathway (except this one) and copy them into new nodes with same interaction refs
	-- return/store the new nodes by appending to needed_interaction_links array 
    FOR TABLE_RECORD in SELECT * FROM interaction_link WHERE interaction_link.id in (select child_node_id from interaction_parent where parent_interaction_id=interactionid and child_connection_id IS NOT NULL and parent_connection_id IS NOT NULL ) and interaction_link.root_interaction_id = setid and interaction_link.child_interaction_id != interactionid and interaction_link.child_interaction_id != ret_id_interaction and interaction_link.child_interaction_id != interactionid and interaction_link.parent_interaction_id != interactionid and interaction_link.parent_interaction_id != ret_id_interaction -- also check that these nodes arent direct children of the new parent node, as we already handled that
    LOOP
		SELECT create_new_interaction_link(ret_id_interaction::bigint,TABLE_RECORD.parent_interaction_id,TABLE_RECORD.ordinal,TABLE_RECORD.child_interaction_id,TABLE_RECORD.condition_id) into ret_id_interaction_link;
		-- the set id for grandchildren that arent the direct children should be that of the newly created interaction copy
		SELECT json_build_object('id', ret_id_interaction_link, 'root_interaction_id',ret_id_interaction,'parent_interaction_id', TABLE_RECORD.parent_interaction_id,'ordinal',TABLE_RECORD.ordinal,'child_interaction_id',TABLE_RECORD.child_interaction_id,'condition_id',TABLE_RECORD.condition_id, 'modified',TABLE_RECORD.modified) into json_obj_interaction_link_result;
		SELECT ARRAY_APPEND(needed_interaction_links, json_obj_interaction_link_result) into needed_interaction_links; 
			-- for this found interaction_link, find all existing corresponding interaction_link_intervals for it
		FOR TABLE_RECORD_interaction_link_INTERVAL in SELECT * FROM interaction_link_interval WHERE interaction_link_interval.interaction_link_id = TABLE_RECORD.id
		LOOP
			SELECT create_new_interaction_link_interval(ret_id_interaction_link::bigint,TABLE_RECORD_interaction_link_INTERVAL.interval_id,TABLE_RECORD_interaction_link_INTERVAL.condition_id,TABLE_RECORD_interaction_link_INTERVAL.communication_set_id) into ret_id_interaction_link_interval;
			SELECT json_build_object('id', ret_id_interaction_link_interval, 'interaction_link_id',TABLE_RECORD.id,'interval_id', TABLE_RECORD_interaction_link_INTERVAL.interval_id,'condition_id',TABLE_RECORD_interaction_link_INTERVAL.condition_id,'communication_set_id',TABLE_RECORD_interaction_link_INTERVAL.communication_set_id, 'modified',TABLE_RECORD_interaction_link_INTERVAL.modified) into json_obj_interaction_link_intervals_result;
			SELECT ARRAY_APPEND(needed_interaction_link_intervals, json_obj_interaction_link_intervals_result) into needed_interaction_link_intervals; 
		END LOOP;
    END LOOP;
    */
    
	SELECT json_build_object('interaction_id', ret_id_interaction, 'needed_interaction_links', needed_interaction_links,'needed_interaction_link_intervals', needed_interaction_link_intervals) into json_obj_result;
	return json_obj_result;
END;
$$ LANGUAGE plpgsql;

-- copy a commset and its corresponding communications by comm_set_id, return json with the inserted data
CREATE OR REPLACE FUNCTION copy_comm_set_by_id(setid bigint)
RETURNS json AS $$
DECLARE
    TABLE_RECORD record;
	ret_id_comm_set integer; -- new copied commset id
	ret_id_comm integer; -- individual comm results
    needed_comms json[];
	json_obj_comm_result json; -- individual comms results
    json_obj_result json; -- all results
BEGIN
	
    SELECT create_new_communication_set(null) into ret_id_comm_set;
	
    FOR TABLE_RECORD in SELECT * FROM communication WHERE communication.communication_set_id = setid
    LOOP
		SELECT create_new_communication(ret_id_comm_set::bigint,TABLE_RECORD."start",TABLE_RECORD."end",TABLE_RECORD.properties::json) into ret_id_comm;
		SELECT json_build_object('comm_id', ret_id_comm, 'comm_set_id',ret_id_comm_set,'start', TABLE_RECORD."start",'end',TABLE_RECORD."end",'properties',TABLE_RECORD.properties) into json_obj_comm_result;
		SELECT ARRAY_APPEND(needed_comms, json_obj_comm_result) into needed_comms; 
    END LOOP;
	SELECT json_build_object('comm_set_id', ret_id_comm_set, 'comms', needed_comms) into json_obj_result;
	return json_obj_result;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS create_new_interaction;
CREATE OR REPLACE FUNCTION create_new_interaction(
	thename text,
	theproperties json DEFAULT '{}'::json)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
   ret_id integer;
   interaction_shared boolean;
begin
	
	interaction_shared = false;
	IF theproperties->>'type' = 'cs1survey' THEN
		interaction_shared = true;
	END IF;
	INSERT INTO interaction (name, shared, properties) VALUES (thename,interaction_shared, theproperties)
	   RETURNING id 
	   into ret_id;
	   return ret_id;
end;
$BODY$;

CREATE OR REPLACE FUNCTION update_ordinality_by_parent(
    nodeid bigint, 
    parentid bigint,
    old_ordinal bigint,
    new_ordinal bigint
) RETURNS VOID AS $$
BEGIN
  -- First, update the row with the old old_ordinal to the new new_ordinal
  -- if inserting a brand new node, new_ordinal and old_ordinal will be the same
  -- so this wont really do anything
  UPDATE interaction_link
  SET ordinal = new_ordinal
  WHERE ordinal = old_ordinal and parent_interaction_id = parentid and id = nodeid;

  IF new_ordinal > old_ordinal THEN
      -- Now, decrement all other IDs in between the new and old value (inclusive)
      UPDATE interaction_link
      SET ordinal = ordinal - 1
      WHERE ordinal >= old_ordinal and ordinal <= new_ordinal and parent_interaction_id = parentid and id <> nodeid;
  ELSE
       -- Now, increment all other IDs
      UPDATE interaction_link
      SET ordinal = ordinal + 1
      WHERE ordinal >= new_ordinal and parent_interaction_id = parentid and id <> nodeid;
  END IF;
  perform fix_ordinality_for_parent(parentid);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION create_new_interaction_link(
	theset bigint,
	theparent bigint,
	theordinal smallint,
	thechild bigint,
	thecondition_id bigint DEFAULT NULL::bigint)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
   ret_id integer;
begin

    INSERT INTO interaction_link (root_interaction_id,parent_interaction_id,ordinal,child_interaction_id,condition_id) VALUES (theset,theparent,theordinal,thechild,thecondition_id)
       RETURNING id 
       into ret_id;
       perform update_ordinality_by_parent(ret_id,theparent,theordinal,theordinal);
       return ret_id;
end;
$BODY$;


CREATE OR REPLACE FUNCTION create_new_interaction_link_interval(
	interaction_link_id bigint,
	interval_id bigint,
	condition_id bigint DEFAULT NULL::bigint,
	communication_set_id bigint DEFAULT NULL::bigint)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
   ret_id integer;
begin

    INSERT INTO interaction_link_interval (interaction_link_id,interval_id,condition_id,communication_set_id) VALUES (interaction_link_id,interval_id,condition_id,communication_set_id)
       RETURNING id 
       into ret_id;
       return ret_id;
end;
$BODY$;



CREATE OR REPLACE FUNCTION  create_new_interval(interval_set_id bigint, name text, start_day integer, end_day integer, communication_set_id bigint default null)
returns integer
language plpgsql
as
$$
declare
   ret_id integer;
begin

    INSERT INTO interval (interval_set_id,name,start_day,end_day,communication_set_id) VALUES (interval_set_id,name,start_day,end_day,communication_set_id)
       RETURNING id 
       into ret_id;
       return ret_id;
end;
$$;

CREATE OR REPLACE FUNCTION create_new_interval_set(
	name text,
	parentid bigint,
	communication_set_id bigint DEFAULT NULL::bigint)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $$
declare
   ret_id integer;
begin

    INSERT INTO interval_set (name,communication_set_id) VALUES (name,communication_set_id)
       RETURNING id 
       into ret_id;
	   INSERT INTO interaction_parent_interval_set (interaction_parent_id,interval_set_id) VALUES (parentid,ret_id);

       return ret_id;
end;
$$;


 -- build a new interval_set row, create new interaction_link_interval for each node by default
DROP FUNCTION IF EXISTS create_new_interaction_matrix_row;
CREATE OR REPLACE FUNCTION create_new_interaction_matrix_row(
	interaction_id bigint,
	associated_intervals text,
	parent_interaction_id bigint,
	root_interaction_id bigint,
	theordinal smallint)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    ret_id_interaction_link bigint;
    ret_id_interaction_link_interval bigint;
    ids INT[];
    var INT;
    new_ids json[] := ARRAY[]::json[];
    json_obj_result json;
    interaction_record json;
    interaction_link_record json;
    interaction_link_interval_records json;
BEGIN
    ids = string_to_array(associated_intervals, ',');
    

    SELECT row_to_json(t) INTO interaction_record FROM (SELECT * FROM interaction WHERE id = interaction_id) t;
    
    SELECT create_new_interaction_link(root_interaction_id::bigint, parent_interaction_id::bigint, theordinal::smallint, interaction_id::bigint) INTO ret_id_interaction_link;
    -- Fetch the full interaction node record
    SELECT row_to_json(t) INTO interaction_link_record FROM (SELECT * FROM interaction_link WHERE id = ret_id_interaction_link) t;
    
    FOREACH var IN ARRAY ids LOOP
        SELECT create_new_interaction_link_interval(ret_id_interaction_link::bigint, var::bigint) INTO ret_id_interaction_link_interval;
        -- Fetch each full interaction node interval record and append to array
        SELECT row_to_json(t) INTO interaction_link_interval_records FROM (SELECT * FROM interaction_link_interval WHERE id = ret_id_interaction_link_interval) t;
        new_ids := array_append(new_ids, interaction_link_interval_records);
    END LOOP;
    
    -- Construct the final JSON object to return
    SELECT json_build_object(
        'interaction', interaction_record,
        'interaction_link', interaction_link_record,
        'interaction_link_intervals', json_agg(elem) -- Aggregate array elements into a JSON array
    ) INTO json_obj_result
    FROM unnest(new_ids) AS elem;
    
    RETURN json_obj_result;
END;
$BODY$;

DROP FUNCTION IF EXISTS create_new_interaction_matrix_column;
CREATE OR REPLACE FUNCTION create_new_interaction_matrix_column(
    interval_set_id bigint,
    name text,
    start_day integer,
    end_day integer,
    associated_interaction_links text,
    communication_set_id bigint DEFAULT NULL::bigint)
RETURNS json
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    ret_id_interval integer;
    ret_id_interaction_link_interval integer;
    ids INT[];
    var INT;
    new_ids json[] := ARRAY[]::json[];
    interval_record json;
    interaction_link_interval_record json;
    json_obj_result json;
BEGIN
    ids = string_to_array(associated_interaction_links, ',');
    
    -- Insert the interval and fetch its full record
    SELECT create_new_interval(interval_set_id, name, start_day, end_day, communication_set_id) INTO ret_id_interval;
    SELECT row_to_json(t) INTO interval_record FROM (SELECT * FROM interval WHERE id = ret_id_interval) t;
    
    FOREACH var IN ARRAY ids LOOP
        -- Insert the interaction node interval and fetch its full record
        SELECT create_new_interaction_link_interval(var::bigint, ret_id_interval::bigint) INTO ret_id_interaction_link_interval;
        SELECT row_to_json(t) INTO interaction_link_interval_record FROM (SELECT * FROM interaction_link_interval WHERE id = ret_id_interaction_link_interval) t;
        new_ids := array_append(new_ids, interaction_link_interval_record);
    END LOOP;
    
    -- Construct and return the JSON object with full records
    SELECT json_build_object(
        'interval', interval_record,
        'interaction_link_intervals', json_agg(elem) -- Aggregate the array elements into a JSON array
    ) INTO json_obj_result
    FROM unnest(new_ids) AS elem;
    
    RETURN json_obj_result;
END;
$BODY$;

-- not fully used yet, but will be useful down the road
-- we are still fleshing out what all of the interaction instance props should be,
-- but while we do we can build up this function
-- eventually it can be added to any "create new interaction" hook
CREATE OR REPLACE FUNCTION create_new_interaction_instance_property(
	interaction_id bigint,
	thetype text DEFAULT ''::text)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
   ret_id integer;
begin
    

    INSERT INTO interaction_instance_property (interaction_id, type)
    VALUES (
        interaction_id,
        CASE thetype
            WHEN 'cs1survey' THEN 'anatomy'
            WHEN 'pathway' THEN 'other'
            ELSE thetype -- Default case 
        END
    )
    RETURNING id INTO ret_id;
    RETURN ret_id;
end;
$BODY$;
    
-- Get everything needed for front-end by selected set
-- @parentid: Sometimes we dont want to get the entire set, just the set relative to its parent    
-- @nodeid: When "popping out" interactions in context, or accessing them via breadcrumbs nav, 
--          we need to get conditions and intervals attached to parent. 
--          So for that case, we will need the parent NODE passed in as well.
CREATE OR REPLACE FUNCTION get_data_for_set(
	setid bigint,
	parentid bigint DEFAULT NULL::bigint,
	nodeid bigint DEFAULT NULL::bigint)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    TABLE_RECORD record;
    json_obj_result json; -- all results
    interaction_links json[];
    interactions json[];
    intervals json[];
    interaction_link_intervals json[];
    interaction_parent_intervals json[];
    interval_sets json[];
	interaction_parent_interval_sets json[];
	communication_sets json[]; -- new
    needed_interaction_ids bigint[];
    needed_interval_ids bigint[];
    needed_interval_set_ids bigint[];
    needed_interaction_link_ids bigint[];
	needed_communication_set_ids bigint[]; 
    i json;
BEGIN
    -- find interaction_links from setID param
	
	IF parentid IS NOT NULL THEN
        FOR TABLE_RECORD in SELECT * FROM interaction_link WHERE interaction_link.parent_interaction_id = parentid
        LOOP
			SELECT json_build_object('id',TABLE_RECORD.id, 'root_interaction_id',TABLE_RECORD.root_interaction_id,'parent_interaction_id',TABLE_RECORD.parent_interaction_id, 'ordinal',TABLE_RECORD.ordinal,'child_interaction_id',TABLE_RECORD.child_interaction_id,'condition_id',TABLE_RECORD.condition_id,'modified',TABLE_RECORD.modified ) into json_obj_result;
			SELECT ARRAY_APPEND(interaction_links, json_obj_result) into interaction_links;
			-- save needed_interaction_ids and needed_interaction_link_ids for subsequent queries
			SELECT ARRAY_APPEND(needed_interaction_ids, TABLE_RECORD.child_interaction_id) into needed_interaction_ids;
			SELECT ARRAY_APPEND(needed_interaction_link_ids, TABLE_RECORD.id) into needed_interaction_link_ids;
        END LOOP;
		-- add root interaction into array, since that isn't a node child
    	SELECT ARRAY_APPEND(needed_interaction_ids, parentid) into needed_interaction_ids;
    ELSE
        FOR TABLE_RECORD in SELECT * FROM interaction_link WHERE interaction_link.root_interaction_id = setID
        LOOP
			SELECT json_build_object('id',TABLE_RECORD.id, 'root_interaction_id',TABLE_RECORD.root_interaction_id,'parent_interaction_id',TABLE_RECORD.parent_interaction_id, 'ordinal',TABLE_RECORD.ordinal,'child_interaction_id',TABLE_RECORD.child_interaction_id,'condition_id',TABLE_RECORD.condition_id,'modified',TABLE_RECORD.modified ) into json_obj_result;
			SELECT ARRAY_APPEND(interaction_links, json_obj_result) into interaction_links;
			-- save needed_interaction_ids and needed_interaction_link_ids for subsequent queries
			SELECT ARRAY_APPEND(needed_interaction_ids, TABLE_RECORD.child_interaction_id) into needed_interaction_ids;
			SELECT ARRAY_APPEND(needed_interaction_link_ids, TABLE_RECORD.id) into needed_interaction_link_ids;
        END LOOP;
		-- add root interaction into array, since that isn't a node child
    	SELECT ARRAY_APPEND(needed_interaction_ids, setID) into needed_interaction_ids;
    END IF;
                                    
	IF nodeid IS NOT NULL THEN
		-- get immediate parent node (passed in)
		FOR TABLE_RECORD in SELECT * FROM interaction_link WHERE interaction_link.id = nodeid
		LOOP
			SELECT json_build_object('id',TABLE_RECORD.id, 'root_interaction_id',TABLE_RECORD.root_interaction_id,'parent_interaction_id',TABLE_RECORD.parent_interaction_id, 'ordinal',TABLE_RECORD.ordinal,'child_interaction_id',TABLE_RECORD.child_interaction_id,'condition_id',TABLE_RECORD.condition_id,'modified',TABLE_RECORD.modified ) into json_obj_result;
			SELECT ARRAY_APPEND(interaction_links, json_obj_result) into interaction_links;
			-- save needed_interaction_ids and needed_interaction_link_ids for subsequent queries
			SELECT ARRAY_APPEND(needed_interaction_ids, TABLE_RECORD.child_interaction_id) into needed_interaction_ids;
			SELECT ARRAY_APPEND(needed_interaction_link_ids, TABLE_RECORD.id) into needed_interaction_link_ids;
		END LOOP;
    END IF;

    -- find interactions from needed_interaction_ids
    FOR TABLE_RECORD IN
        SELECT * FROM interaction WHERE interaction.id = ANY(needed_interaction_ids) 
    LOOP
        SELECT json_build_object('id', TABLE_RECORD.id, 'name', TABLE_RECORD.name,  'shared', TABLE_RECORD.shared, 'properties', TABLE_RECORD.properties, 'modified', TABLE_RECORD.modified ) INTO json_obj_result;
        SELECT ARRAY_APPEND(interactions, json_obj_result) INTO interactions;
        -- Pull interval_set_id from properties and append to needed_interval_set_ids
        SELECT ARRAY_APPEND(needed_interval_set_ids, (TABLE_RECORD.properties->>'interval_set_id')::bigint) INTO needed_interval_set_ids;
    END LOOP;

    -- find interaction_link_intervals for needed_interaction_link_ids
    FOR TABLE_RECORD in SELECT * FROM interaction_link_interval WHERE interaction_link_interval.interaction_link_id = ANY(needed_interaction_link_ids)
    LOOP
        SELECT json_build_object('id',TABLE_RECORD.id, 'interaction_link_id',TABLE_RECORD.interaction_link_id,'interval_id',TABLE_RECORD.interval_id, 'condition_id',TABLE_RECORD.condition_id,'communication_set_id',TABLE_RECORD.communication_set_id, 'modified',TABLE_RECORD.modified ) into json_obj_result;
        SELECT ARRAY_APPEND(interaction_link_intervals, json_obj_result) into interaction_link_intervals;
        SELECT ARRAY_APPEND(needed_interval_ids, TABLE_RECORD.interval_id) into needed_interval_ids;
		SELECT ARRAY_APPEND(needed_communication_set_ids, TABLE_RECORD.communication_set_id) into needed_communication_set_ids;
    END LOOP;
	
	-- find additional needed_interval_ids from needed_interval_set_ids
    FOR TABLE_RECORD in SELECT * FROM interval WHERE interval.interval_set_id = ANY(needed_interval_set_ids)
    LOOP
        SELECT ARRAY_APPEND(needed_interval_ids, TABLE_RECORD.id) into needed_interval_ids;
    END LOOP;
    
    -- find intervals for needed_interval_ids
    FOR TABLE_RECORD in SELECT * FROM interval WHERE interval.id = ANY(needed_interval_ids)
    LOOP
        SELECT json_build_object('id',TABLE_RECORD.id, 'interval_set_id',TABLE_RECORD.interval_set_id,'name',TABLE_RECORD.name, 'start_day',TABLE_RECORD.start_day,'end_day',TABLE_RECORD.end_day,'communication_set_id',TABLE_RECORD.communication_set_id, 'modified',TABLE_RECORD.modified ) into json_obj_result;
        SELECT ARRAY_APPEND(intervals, json_obj_result) into intervals;
        SELECT ARRAY_APPEND(needed_interval_set_ids, TABLE_RECORD.interval_set_id) into needed_interval_set_ids;
		SELECT ARRAY_APPEND(needed_communication_set_ids, TABLE_RECORD.communication_set_id) into needed_communication_set_ids;
    END LOOP;
	
	FOR TABLE_RECORD in SELECT * FROM communication_set WHERE communication_set.id = ANY(needed_communication_set_ids)
    LOOP
        SELECT json_build_object('id',TABLE_RECORD.id, 'name',TABLE_RECORD.name, 'shared', TABLE_RECORD.shared, 'modified',TABLE_RECORD.modified ) into json_obj_result;
        SELECT ARRAY_APPEND(communication_sets, json_obj_result) into communication_sets;
    END LOOP;
	    
    -- find interval_sets for needed_interval_set_ids
    FOR TABLE_RECORD in SELECT * FROM interval_set WHERE interval_set.id = ANY(needed_interval_set_ids)
    LOOP
        SELECT json_build_object('id',TABLE_RECORD.id, 'name',TABLE_RECORD.name,'name',TABLE_RECORD.name, 'shared',TABLE_RECORD.shared, 'modified',TABLE_RECORD.modified ) into json_obj_result;
        SELECT ARRAY_APPEND(interval_sets, json_obj_result) into interval_sets;
    END LOOP;
	
    SELECT json_build_object('interaction_links',interaction_links,'interactions',interactions, 'interaction_link_intervals',interaction_link_intervals, 'intervals', intervals, 'interval_sets', interval_sets,'interaction_parent_intervals', interaction_parent_intervals,'interaction_parent_interval_sets',interaction_parent_interval_sets,'communication_sets',communication_sets) into json_obj_result;
    return json_obj_result;
END;
$BODY$;

-- get all parents for search list
CREATE OR REPLACE FUNCTION get_all_parent_interactions(
	)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    TABLE_RECORD record;
    interaction_list jsonb[];
    json_obj_result jsonb;
  
BEGIN
    
    -- find all parent interactions and their key metadata
    FOR TABLE_RECORD in SELECT interaction.id, interaction.name, interaction.properties FROM interaction where shared
    LOOP
        SELECT json_build_object('id',TABLE_RECORD.id, 'name',TABLE_RECORD.name,'properties',TABLE_RECORD.properties ) into json_obj_result;
        SELECT ARRAY_APPEND(interaction_list, json_obj_result) into interaction_list;
     END LOOP;
    SELECT jsonb_build_object('interaction_list',interaction_list) into json_obj_result;
    return json_obj_result;
END;
$BODY$;

-- get all roots for search list
CREATE OR REPLACE FUNCTION get_all_root_interactions(
	)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    TABLE_RECORD record;
    interaction_list jsonb[];
    json_obj_result jsonb;
  
BEGIN
    
    -- find all parent interactions and their key metadata
    FOR TABLE_RECORD in SELECT interaction.id, interaction.name, interaction.properties FROM interaction where id in (SELECT distinct "root_interaction_id" FROM interaction_link)
    LOOP
        SELECT json_build_object('id',TABLE_RECORD.id, 'name',TABLE_RECORD.name,'properties',TABLE_RECORD.properties ) into json_obj_result;
        SELECT ARRAY_APPEND(interaction_list, json_obj_result) into interaction_list;
     END LOOP;
    SELECT jsonb_build_object('interaction_list',interaction_list) into json_obj_result;
    return json_obj_result;
END;
$BODY$;

CREATE OR REPLACE FUNCTION update_interaction_set_id(
    parent_id bigint, -- not used?
    current_set_id bigint, 
    child_id bigint, 
    new_set_id bigint
)
RETURNS void AS $$
DECLARE
    node RECORD;
BEGIN


    -- Find the node the parent is on, recursively update for all child nodes
    --      If shared, we want to update the setID for all children of this node to match the root interaction ID
    --      (All child nodes with corresponding shared interactions should have their setid updated, 
    --      but do not run the function again recursively for THOSE children since they already have the setid they need)
    FOR node IN
        SELECT interaction_link.id, child_interaction_id, interaction.shared as shared 
        FROM interaction_link
		join interaction on interaction_link.child_interaction_id = interaction.id -- nodes could be shared = true OR false
        WHERE parent_interaction_id = child_id 
    LOOP
        -- Update the root_interaction_id of the current child node
        UPDATE interaction_link
        SET root_interaction_id = new_set_id
        WHERE id = node.id;

        -- we only want to update the children that arent shared interactions
        IF node.shared = false THEN
            -- Call the function recursively for the current child node
            PERFORM update_interaction_set_id(child_id, current_set_id, node.child_interaction_id, new_set_id);
        END IF;
    END LOOP;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION get_interactions_for_nav_history(
	input_string text)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    json_array JSONB := '[]'::JSONB;
    values_array TEXT[] := string_to_array(input_string, '_');
    value TEXT;
    this_set text;
    this_parent text;
    this_node text;
    entire_node text[];
    this_set_array text[];
    this_parent_array text[];
    this_node_array text[];
    json_obj_result jsonb; -- individual fetch results
    get_data_from_set_results jsonb;
    return_results jsonb = '{}'::jsonb; -- all results
BEGIN
    FOREACH value IN ARRAY values_array -- s1,s1p9217n12289,s10241n13313
    LOOP 
        select string_to_array(value,'p') into this_parent_array; -- s1p9217n12289 - see if parent is in array
        select string_to_array(value,'n') into this_node_array; -- see if node is in array
        select string_to_array(this_parent_array[1],'s') into this_set_array;
        this_parent = '';
        this_node = '';
        this_set =  this_set_array[2]; -- 
        IF array_length(this_parent_array,1)>1 OR array_length(this_node_array,1)>1 THEN -- only worry about parent in set if they exist
            -- if parent, we might need to handle parent and THEN node too
            IF array_length(this_parent_array,1)>1 THEN
                this_parent = this_parent_array[2]; --e.g. this_parent_array = [s1p,9217n12289], this_parent = 9217n12289
                select string_to_array(this_parent,'n') into this_node_array;  --e.g.  this_node_array = [9217,12289], this_parent = 9217, this_node = 12289
                IF array_length(this_node_array,1)>1 THEN
                    this_parent = this_node_array[1];
                    this_node = this_node_array[2];
                END IF;
            ELSE
                IF array_length(this_node_array,1)>1 THEN -- e.g. s10241n13313, this_node_array = [s10241,13313]
                    this_node = this_node_array[2];
                    select string_to_array(this_node_array[1],'s') into this_set_array; -- e.g. s10241n13313, this_set_array = [s,10241]
                    this_set = this_set_array[2];
                END IF;
                this_parent = '';
            END IF;
        END IF;
        
        
        IF this_node != '' THEN
            IF this_parent != '' THEN
                select get_data_for_set(this_parent::bigint,this_set::bigint,this_node::bigint) into get_data_from_set_results;
            ELSE
                select get_data_for_set(this_set::bigint,null::bigint,this_node::bigint) into get_data_from_set_results;
            END IF;        
        ELSE
            IF this_parent != '' THEN
                select get_data_for_set(this_set::bigint,this_parent::bigint) into get_data_from_set_results;
            ELSE
                select get_data_for_set(this_set::bigint) into get_data_from_set_results;
            END IF;    
        END IF;
        SELECT json_build_object('setid',this_set::text, 'parentid',this_parent::text,'nodeid',this_node::text,'state',get_data_from_set_results::jsonb ) into json_obj_result;
        json_array = jsonb_insert(json_array, '{0}', to_jsonb(json_obj_result));
    END LOOP;
    RETURN json_array;
END;
$BODY$;

DROP FUNCTION IF EXISTS get_interaction_link_conditions;
CREATE OR REPLACE FUNCTION get_interaction_link_conditions(
    interaction_link_id bigint,
    root_interaction_id bigint
)
    RETURNS json
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $$
DECLARE
    TABLE_RECORD record;
    conditions_list json[];
    direct_conditions json[];
    json_obj_result json;
BEGIN

    SELECT interaction_link.condition_id, interaction_link."root_interaction_id" INTO TABLE_RECORD FROM interaction_link WHERE id = interaction_link_id;
    IF TABLE_RECORD.condition_id IS NOT NULL THEN
        SELECT array_agg(row_to_json(t)::jsonb) INTO conditions_list FROM ( SELECT * FROM condition WHERE "root_condition_id" = TABLE_RECORD.condition_id) t;
        SELECT ARRAY_CAT(direct_conditions, conditions_list) INTO direct_conditions;
    END IF;
    /*
    @todo we're no longer using interaction_parent this way and also this function was likely going to be very slow since it uses an any. 
    ... If we want to do this we should pass in a path that we've maintained in the ui and append the conditions for each item in the path
    ... otherwise we may be getting conditoins that aren't related to the path we used to get to this item so it would be confusing
    
    FOR TABLE_RECORD IN 
        SELECT DISTINCT interaction_link.condition_id 
        FROM interaction_parent 
        JOIN interaction_link ON interaction_link.id = ANY(interaction_parent.node_path)
        WHERE interaction_parent.parent_interaction_id = interaction_link.root_interaction_id
        AND interaction_parent.child_node_id = interaction_link_id 
        AND interaction_link.ID != interaction_link_id
    LOOP
        SELECT array_agg(row_to_json(t)::jsonb) INTO conditions_list FROM ( SELECT * FROM condition WHERE "root_condition_id" = TABLE_RECORD.condition_id) t;
        SELECT ARRAY_CAT(parent_conditions, conditions_list) INTO parent_conditions;
    END LOOP;
    */
    SELECT json_build_object('direct_conditions',direct_conditions,'parent_conditions',ARRAY[]::json[]) INTO json_obj_result;
    RETURN json_obj_result;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS get_interaction_link_interval_conditions;
CREATE OR REPLACE FUNCTION get_interaction_link_interval_conditions(
    interaction_link_interval_id bigint,
    root_interaction_id bigint
)
    RETURNS json
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $$
DECLARE
    INTERVAL_RECORD record;
    TABLE_RECORD record;
    conditions_list json[];
    direct_conditions json[];
    json_obj_result json;
BEGIN

    SELECT interaction_link_interval.condition_id, interaction_link_interval.interaction_link_id INTO INTERVAL_RECORD FROM interaction_link_interval WHERE id = interaction_link_interval_id;
    IF INTERVAL_RECORD.condition_id IS NOT NULL THEN
        SELECT array_agg(row_to_json(t)::jsonb) INTO conditions_list FROM ( SELECT * FROM condition WHERE "root_condition_id" = INTERVAL_RECORD.condition_id) t;
        SELECT ARRAY_CAT(direct_conditions, conditions_list) INTO direct_conditions;
    END IF;
    /*
    @todo we're no longer using interaction_parent this way and also this function was likely going to be very slow since it uses an any. 
    ... If we want to do this we should pass in a path that we've maintained in the ui and append the conditions for each item in the path
    ... otherwise we may be getting conditoins that aren't related to the path we used to get to this item so it would be confusing

    FOR TABLE_RECORD IN 
        SELECT DISTINCT interaction_link.condition_id 
        FROM interaction_parent 
        JOIN interaction_link ON interaction_link.ID = ANY(interaction_parent.node_path)
        WHERE interaction_parent.parent_interaction_id = root_interaction_id 
        AND interaction_parent.child_node_id = INTERVAL_RECORD.interaction_link_id
    LOOP
        SELECT array_agg(row_to_json(t)::jsonb) INTO conditions_list FROM ( SELECT * FROM condition WHERE "root_condition_id" = TABLE_RECORD.condition_id) t;
        SELECT ARRAY_CAT(parent_conditions, conditions_list) INTO parent_conditions;
    END LOOP;
    */
    
    SELECT json_build_object('direct_conditions',direct_conditions,'parent_conditions',ARRAY[]::json[]) INTO json_obj_result;
    RETURN json_obj_result;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION find_closest_shared_parent(p_child bigint)
RETURNS bigint
LANGUAGE sql
AS $$
  SELECT COALESCE(
    -- If the given interaction is shared, return its id immediately.
    (SELECT id 
     FROM interaction 
     WHERE id = p_child 
       AND shared = true),
    (
      WITH RECURSIVE parent_search AS (
        -- Anchor: start with the direct parent(s) of p_child.
        SELECT
          il.parent_interaction_id AS parent_id,
          1 AS depth
        FROM interaction_link il
        WHERE il.child_interaction_id = p_child
        
        UNION ALL
        
        -- Recursive step: find the parent(s) of the current node(s).
        SELECT
          il.parent_interaction_id,
          ps.depth + 1
        FROM interaction_link il
        JOIN parent_search ps 
          ON il.child_interaction_id = ps.parent_id
      )
      SELECT ps.parent_id
      FROM parent_search ps
      JOIN interaction i ON i.id = ps.parent_id
      -- Qualify the candidate if either:
      --   a) i.shared = true, or
      --   b) i.properties->>'type' equals 'account'
      WHERE i.shared = true 
         OR (i.properties->>'type' = 'account')
      ORDER BY ps.depth
      LIMIT 1
    )
  ) AS closest_shared;
$$;

-- update root_interaction_id for all qualifying children
-- do not update child shared interactions or child accounts
CREATE OR REPLACE FUNCTION update_and_get_interaction_subtree(
    p_parent_interaction_id   bigint,
    p_new_root_interaction_id bigint
)
RETURNS json
LANGUAGE sql
AS $$
WITH RECURSIVE subtree AS (
  -- Anchor: find direct children of the given parent that are non-shared and not accounts.
  SELECT
    il.id AS link_id,
    il.child_interaction_id,
    1 AS depth
  FROM interaction_link il
  JOIN interaction i
    ON il.child_interaction_id = i.id
  WHERE il.parent_interaction_id = p_parent_interaction_id
    AND i.shared = false
    AND COALESCE(i.properties->>'type', '') <> 'account'
  
  UNION ALL
  
  -- Recursive step: for each child, find its qualifying children.
  SELECT
    il.id AS link_id,
    il.child_interaction_id,
    s.depth + 1 AS depth
  FROM interaction_link il
  JOIN subtree s
    ON il.parent_interaction_id = s.child_interaction_id
  JOIN interaction i
    ON il.child_interaction_id = i.id
  WHERE i.shared = false
    AND COALESCE(i.properties->>'type', '') <> 'account'
),
upd AS (
  -- Update all rows found in the subtree and return their ids.
  UPDATE interaction_link
  SET root_interaction_id = p_new_root_interaction_id
  WHERE id IN (SELECT link_id FROM subtree)
  RETURNING id
)
-- Return the subtree as JSON.
SELECT COALESCE(json_agg(row_to_json(t)), '[]'::json)
FROM (
  SELECT
    s.link_id,
    s.child_interaction_id,
    s.depth,
    p_new_root_interaction_id AS new_root_interaction_id
  FROM subtree s
) t;
$$;