-- meta
DROP FUNCTION IF EXISTS create_pathway;
CREATE OR REPLACE FUNCTION create_pathway(
    "account_interaction_id" bigint,
    "title" text,
    "condition_json" json,
    "pathway_group_interaction_id" bigint DEFAULT NULL,
	"template_pathway_interaction_id" bigint DEFAULT NULL
)
RETURNS TABLE (
    interaction_link_id bigint,
    interaction_id bigint,
    pathway_json json
)
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
	"pathway_id" bigint;
    "pathway_link_id" bigint;
    "condition_id" bigint;
BEGIN
    INSERT INTO "interaction" ("name", "shared", "properties") 
    VALUES ("title", false, '{"type": "pathway"}')
    RETURNING "id" 
    INTO "pathway_id";

    SELECT create_pathway_condition("condition_json"::jsonb, null::bigint, null::bigint, 1::bigint) INTO "condition_id";
    IF "condition_id" IS NULL THEN
        RAISE EXCEPTION 'At least one valid condition must be set when creating a new pathway';
    END IF;

    IF "pathway_group_interaction_id" IS NULL THEN
        "pathway_group_interaction_id" = "account_interaction_id";
    END IF;

    INSERT INTO "interaction_link" ("root_interaction_id", "parent_interaction_id", "child_interaction_id", "condition_id", "ordinal") 
    VALUES ("account_interaction_id", "pathway_group_interaction_id", "pathway_id", "condition_id", 0)
    RETURNING "id" 
    INTO "pathway_link_id";

	RETURN QUERY
    SELECT "pathway_link_id" AS "interaction_link_id", "pathway_id" as "interaction_id", json_nest_interaction_set(json_export_interaction("pathway_id"))::json AS "pathway_json";
END;
$BODY$;
	
DROP FUNCTION IF EXISTS create_pathway_condition;
CREATE OR REPLACE FUNCTION create_pathway_condition(
	"condition_json" jsonb,
	"my_parent_condition_id" bigint,
    "my_root_condition_id" bigint,
    "index" bigint
)
RETURNS bigint
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
	"condition_id" bigint;
    "condition_not" boolean;
    "condition_operator" shared.enum_operator;
	"child_condition" jsonb;
    "child_index" bigint;
BEGIN
    IF NOT "condition_json" ? 'conditional' THEN 
        RETURN NULL;
    END IF;

    EXECUTE format('
        INSERT INTO "condition" ("root_condition_id", "parent_condition_id", "ordinal", "conditional", "not", "operator", "key", "value")
        VALUES ($1, $2, $3, ($4->>''conditional'')::shared.enum_conditional, %s, %s, %s, %s)
        RETURNING "id"'
        ,CASE WHEN "condition_json"->>'not' IS NOT NULL THEN '($4->>''not'')::boolean' ELSE 'DEFAULT' END
        ,CASE WHEN "condition_json"->>'operator' IS NOT NULL THEN '($4->>''operator'')::shared.enum_operator' ELSE 'DEFAULT' END
        ,CASE WHEN "condition_json"->>'key' IS NOT NULL THEN '$4->>''key''' ELSE 'DEFAULT' END
        ,CASE WHEN "condition_json"->>'value' IS NOT NULL THEN '$4->>''value''' ELSE 'DEFAULT' END
    )
    INTO  "condition_id"
    USING "my_root_condition_id", "my_parent_condition_id", "index", "condition_json";

    IF "condition_json"->>'conditional' IN ('and', 'or') THEN
        IF "my_root_condition_id" IS NULL THEN
            "my_root_condition_id" = "condition_id";
		END IF;

        "child_index" = 1;
        FOR "child_condition" IN SELECT * FROM json_array_elements(("condition_json"->>'child_conditions')::json) as "conditions"
        LOOP
            PERFORM create_pathway_condition("child_condition"::jsonb, "condition_id"::bigint, "my_root_condition_id"::bigint, "child_index"::bigint);
            "child_index" = "child_index" + 1;
        END LOOP;
    END IF;

    RETURN "condition_id";
END;
$BODY$;

DROP FUNCTION IF EXISTS json_nest_interaction_set;
CREATE OR REPLACE FUNCTION json_nest_interaction_set(
    "pathway_json" jsonb
)
RETURNS jsonb
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
	RETURN json_nest_interaction("pathway_json", ("pathway_json"->>'root')::int);
END;
$BODY$;

DROP FUNCTION IF EXISTS json_export_nested_interaction_set;
CREATE OR REPLACE FUNCTION json_export_nested_interaction_set(
    "root_id" int
)
RETURNS jsonb
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
	RETURN json_nest_interaction_set(json_export_interaction("root_id"));
END;
$BODY$;

DROP FUNCTION IF EXISTS json_import_nested_interaction_set;
CREATE OR REPLACE FUNCTION json_import_nested_interaction_set(
	"root_interaction_id" int,
    "interaction_json" jsonb
)
RETURNS int
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
	"link_ids" jsonb;
BEGIN
    IF EXISTS(SELECT 1 FROM "interaction" WHERE "id" = "root_interaction_id") THEN
        RETURN json_import_nested_interaction(NULL, root_interaction_id, interaction_json);
    ELSE
        RAISE EXCEPTION 'Cannot find interaction with passed in criteria'
            USING HINT = 'Try using create_pathway() first to make an interaction to import this json data to';
    END IF;
END;
$BODY$;
