CALL dbinfo.fn_create_or_update_table('user','{
    id,
    first_name text NOT NULL,
    last_name text NOT NULL,
    full_name text NOT NULL,
    email text NOT NULL,
    password text NOT NULL,
    verified boolean DEFAULT false NOT NULL,
    test boolean DEFAULT false NOT NULL,
    modified timestamp
    }');
DROP TRIGGER IF EXISTS tf_log_change ON "user";
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON "user" FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON "user";
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON "user" FOR EACH ROW EXECUTE FUNCTION tf_set_modified();
DROP TRIGGER IF EXISTS tf_encrypt_pass ON "user";
CREATE TRIGGER tf_encrypt_pass BEFORE INSERT OR UPDATE ON "user" FOR EACH ROW EXECUTE FUNCTION shared.tf_encrypt_pass();

-- meta