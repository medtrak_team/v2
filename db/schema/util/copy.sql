CREATE OR REPLACE FUNCTION fn_copy_interaction(
	interactionid bigint,
	nodeid bigint,
	copied_ids jsonb,
	setid bigint DEFAULT NULL::bigint,
	parentid bigint DEFAULT NULL::bigint)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    TABLE_RECORD record;
	TABLE_RECORD_interaction_link_INTERVAL record;
	TABLE_RECORD_INTERVAL_SET record;
	TABLE_RECORD_CONDITION record;
	TABLE_RECORD_COMMUNICATION_SET record;
	TABLE_RECORD_INTERACTION_INSTANCE_PROPERTY record;
	ret_id_interaction integer; -- new copied interaction
	ret_id_interaction_link integer; -- individual interaction_link results
	ret_id_interaction_link_interval integer; -- individual interaction_link_interval results
    needed_interaction_links json[];
	needed_interaction_link_intervals json;
	ret_id_condition integer;
	ret_id_condition_ini  integer;
	ret_id_communication_set_ini  integer;
	ret_id_interaction_instance_property integer;
	extracted_value integer;
	extracted_value_2 text;
	extracted_interval_id integer;
	extracted_interaction_link_id integer;
	local_copied_ids jsonb;
	shared boolean;
    interval_set_shared boolean;
	new_interaction_properties_json json;
	returned_interaction_ids integer[];
	json_obj_interaction_result json; -- individual root interaction result
	json_obj_interaction_link_result json; -- individual root interaction result
	json_obj_interaction_link_intervals_result json; -- individual root interaction result
	json_obj_interaction_instance_property_result json;
    json_obj_result json; -- all results
BEGIN
-- for now, just copy everything, dont worry about "copying in place" since we can just delete the old node later
	-- handle for THIS interaction (and node, if applicable)
	
	local_copied_ids = '{}'::jsonb;
	shared = false;
    interval_set_shared = false;

	IF setid is not null THEN
		FOR TABLE_RECORD in SELECT interaction_link.*, interaction.name, interaction.shared, interaction.properties  FROM interaction_link join interaction on interaction.id = interaction_link.child_interaction_id  WHERE interaction_link.id = nodeid AND interaction_link.child_interaction_id = interactionid
		LOOP
			new_interaction_properties_json = TABLE_RECORD.properties;
			
			IF TABLE_RECORD.shared = true THEN
				shared = true;
			END IF;
						
			-- Copy interval_set if one is associated and not shared (recursively) 
			-- needs to happen first so we can give the new interaction the new interval_set_id
			FOR TABLE_RECORD_INTERVAL_SET in SELECT * FROM interval_set  WHERE interval_set.id = (TABLE_RECORD.properties->>'interval_set_id')::BIGINT
			LOOP
				IF TABLE_RECORD_INTERVAL_SET.shared = false THEN
					copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,fn_copy_interval_set(TABLE_RECORD_INTERVAL_SET.id::bigint,copied_ids::jsonb)::jsonb);
					SELECT fn_get_dynamic_json_value(copied_ids::jsonb, TABLE_RECORD_INTERVAL_SET.id::text, 'interval_set') into extracted_value_2; -- get new interval set ID from returned map
					select fn_update_interval_set_id_on_interaction(TABLE_RECORD.properties::jsonb,extracted_value_2::integer) into new_interaction_properties_json;
				ELSE 
                    interval_set_shared = true;
					new_interaction_properties_json = TABLE_RECORD.properties; -- if shared, just use original props on new interaction
				END IF;

			END LOOP;
			
			-- Handle Copy condition (recursively) 
			-- needs to happen first so we can give the new interaction the new condition_id
			FOR TABLE_RECORD_CONDITION in SELECT * FROM condition  WHERE condition.id = TABLE_RECORD.condition_id
			LOOP
                -- if condition is shared, just use that one
                -- otherwise copy the condition, return that new id, and use that
                IF TABLE_RECORD_CONDITION.shared = true THEN
				    ret_id_condition = TABLE_RECORD.condition_id;
                ELSE
                    copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,fn_copy_condition(TABLE_RECORD_CONDITION.id::bigint,copied_ids::jsonb)::jsonb);
                    SELECT fn_get_dynamic_json_value(copied_ids::jsonb, TABLE_RECORD_CONDITION.id::text, 'condition') into ret_id_condition; -- get new condition ID from returned map
			    END IF;

			END LOOP;

			-- create the new interaction, and return its id
			IF shared = true THEN
				-- this is root interaction, we want to handle everything for this even though its shared
				-- if not, we dont want to handle new interaction or instance props
				IF parentid is not null THEN 
					SELECT create_new_interaction(TABLE_RECORD.name, new_interaction_properties_json::json) into ret_id_interaction;
					-- map the new id to the old one
					SELECT json_build_object(interactionid, ret_id_interaction) into json_obj_interaction_result;
					-- append to interaction map
					local_copied_ids = local_copied_ids || jsonb_build_object('interaction', json_obj_interaction_result);
					copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);

					-- Handle Copy interaction_instance_property
					FOR TABLE_RECORD_INTERACTION_INSTANCE_PROPERTY in SELECT * FROM interaction_instance_property  WHERE interaction_instance_property.interaction_id = interactionid
					LOOP

						INSERT INTO interaction_instance_property (interaction_id, type)
						VALUES (ret_id_interaction, TABLE_RECORD_INTERACTION_INSTANCE_PROPERTY.type)
						RETURNING id INTO ret_id_interaction_instance_property;

						SELECT json_build_object(TABLE_RECORD_INTERACTION_INSTANCE_PROPERTY.id, ret_id_interaction_instance_property) into json_obj_interaction_instance_property_result;
						-- append to interaction map
						local_copied_ids = local_copied_ids || jsonb_build_object('interaction_instance_property', json_obj_interaction_instance_property_result);
						copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);
					END LOOP;				
				END IF;
			ELSE
					SELECT create_new_interaction(TABLE_RECORD.name, new_interaction_properties_json::json) into ret_id_interaction;
					-- map the new id to the old one
					SELECT json_build_object(interactionid, ret_id_interaction) into json_obj_interaction_result;
					-- append to interaction map
					local_copied_ids = local_copied_ids || jsonb_build_object('interaction', json_obj_interaction_result);
					copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);

					-- Handle Copy interaction_instance_property
					FOR TABLE_RECORD_INTERACTION_INSTANCE_PROPERTY in SELECT * FROM interaction_instance_property  WHERE interaction_instance_property.interaction_id = interactionid
					LOOP
						INSERT INTO interaction_instance_property (interaction_id, type)
						VALUES (ret_id_interaction, TABLE_RECORD_INTERACTION_INSTANCE_PROPERTY.type)
						RETURNING id INTO ret_id_interaction_instance_property;

						SELECT json_build_object(TABLE_RECORD_INTERACTION_INSTANCE_PROPERTY.id, ret_id_interaction_instance_property) into json_obj_interaction_instance_property_result;
						-- append to interaction map
						local_copied_ids = local_copied_ids || jsonb_build_object('interaction_instance_property', json_obj_interaction_instance_property_result);
						copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);
					END LOOP;	
			END IF;

			IF TABLE_RECORD.shared = true THEN 
				IF parentid is not null THEN 
					SELECT create_new_interaction_link(setid, parentid, TABLE_RECORD.ordinal,ret_id_interaction,ret_id_condition) into ret_id_interaction_link;
				ELSE 
					-- get what parent should be by getting the (OLD version of the node)'s parent id, and then look up that interaction in the map
					SELECT fn_get_dynamic_json_value(copied_ids::jsonb, TABLE_RECORD.parent_interaction_id::text, 'interaction') into extracted_value; 
					SELECT create_new_interaction_link(setid, extracted_value, TABLE_RECORD.ordinal,interactionid,ret_id_condition) into ret_id_interaction_link;

					SELECT json_build_object(TABLE_RECORD.id, ret_id_interaction_link) into json_obj_interaction_link_result;
					-- append to interaction_link map
					local_copied_ids := local_copied_ids || jsonb_build_object('interaction_link', json_obj_interaction_link_result); 
					copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);
					return copied_ids;
				END IF;
			ELSE
				IF parentid is not null THEN 
					SELECT create_new_interaction_link(setid, parentid, TABLE_RECORD.ordinal,ret_id_interaction,ret_id_condition) into ret_id_interaction_link;
				ELSE 
					-- get what parent should be by getting the (OLD version of the node)'s parent id, and then look up that interaction in the map
					SELECT fn_get_dynamic_json_value(copied_ids::jsonb, TABLE_RECORD.parent_interaction_id::text, 'interaction') into extracted_value; 
					SELECT create_new_interaction_link(setid, extracted_value, TABLE_RECORD.ordinal,ret_id_interaction,ret_id_condition) into ret_id_interaction_link;
					SELECT json_build_object(TABLE_RECORD.id, ret_id_interaction_link) into json_obj_interaction_link_result;
					-- append to interaction_link map
					local_copied_ids := local_copied_ids || jsonb_build_object('interaction_link', json_obj_interaction_link_result); 
					copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);
				END IF;

			END IF;
			
			-- handle interaction_link_intervals
			FOR TABLE_RECORD_interaction_link_INTERVAL in SELECT * FROM interaction_link_interval  WHERE interaction_link_interval.interaction_link_id = TABLE_RECORD.id
			LOOP			
				-- if interval set was shared, we want to use OLD interval ids with the new nodes
                -- if NOT shared, copy everything and use new
                IF interval_set_shared = true THEN
                    SELECT fn_get_dynamic_json_value(copied_ids::jsonb, TABLE_RECORD_interaction_link_INTERVAL.interval_id::text, 'interval') into extracted_interval_id; -- get/convert new interval ID from returned map
				ELSE
                    extracted_interval_id = TABLE_RECORD_interaction_link_INTERVAL.interval_id;
                END IF;
				-- copy any conditions on this interaction_link_interval
				FOR TABLE_RECORD_CONDITION in SELECT * FROM condition  WHERE condition.id = TABLE_RECORD_interaction_link_INTERVAL.condition_id
				LOOP
                    IF TABLE_RECORD_CONDITION.shared = true THEN
                        ret_id_condition_ini = TABLE_RECORD_interaction_link_INTERVAL.condition_id;
                    ELSE
                        copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,fn_copy_condition(TABLE_RECORD_interaction_link_INTERVAL.id::bigint,copied_ids::jsonb)::jsonb);
                        SELECT fn_get_dynamic_json_value(copied_ids::jsonb, TABLE_RECORD_interaction_link_INTERVAL.id::text, 'condition') into ret_id_condition_ini; -- get new condition ID from returned map
                    END IF;
				END LOOP;
				
				-- copy any communication_sets on this interaction_link_interval
				FOR TABLE_RECORD_COMMUNICATION_SET in SELECT * FROM communication_set  WHERE communication_set.id = TABLE_RECORD_interaction_link_INTERVAL.communication_set_id
				LOOP
					copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,fn_copy_comm_set(TABLE_RECORD_COMMUNICATION_SET.id::bigint,copied_ids::jsonb)::jsonb);
					SELECT fn_get_dynamic_json_value(copied_ids::jsonb, TABLE_RECORD_COMMUNICATION_SET.id::text, 'communication_set') into ret_id_communication_set_ini; -- get new condition ID from returned map
				END LOOP;
				
				SELECT create_new_interaction_link_interval(ret_id_interaction_link, extracted_interval_id, TABLE_RECORD_interaction_link_INTERVAL.target, ret_id_condition_ini, ret_id_communication_set_ini) into ret_id_interaction_link_interval;
				-- todo add to copy map
				SELECT json_build_object(TABLE_RECORD_interaction_link_INTERVAL.id, ret_id_interaction_link_interval) into json_obj_interaction_link_intervals_result;
				-- append to interaction_link map
				local_copied_ids := local_copied_ids || jsonb_build_object('interaction_link_interval', json_obj_interaction_link_intervals_result); 
				copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);
			END LOOP;
		END LOOP;
	END IF;
	
	-- merge the mappings for THIS interaction with the global copy thats passed through
	copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);
	
	-- if this is the root of the copied tree, the set for all children should be equal to the first interaction inserted
    -- (because as a shared set, that was already the case)
	IF parentid is not null THEN
		setid = ret_id_interaction;
	END IF;

	-- Find/Copy all child nodes (just use parent, because this will be recursive)
	FOR TABLE_RECORD in SELECT interaction_link.*, interaction.name, interaction.properties FROM interaction_link join interaction on interaction.id = interaction_link.child_interaction_id WHERE interaction_link.parent_interaction_id = interactionid
    LOOP
		-- copy this interaction, any nodes its on, and do the same for its children
		copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,fn_copy_interaction(TABLE_RECORD.child_interaction_id, TABLE_RECORD.id, copied_ids, setid, null)::jsonb);
    END LOOP;

	RETURN copied_ids;
END;
$BODY$;

  
CREATE OR REPLACE FUNCTION fn_copy_interval_set(
	intervalsetid bigint,
	copied_ids jsonb
)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $$
DECLARE
    TABLE_RECORD record;
	TABLE_RECORD_interaction_link_INTERVAL record;
	TABLE_RECORD_COMM_SET record;
	ret_id_interaction integer; -- new copied interaction
	ret_id_interaction_link integer; -- individual interaction_link results
	ret_id_interaction_link_interval integer; -- individual interaction_link_interval results
    needed_interaction_links json[];
	needed_interaction_link_intervals json;
	new_interval_set_id integer;
	new_interval_id integer;
	ret_id_condition integer;
	extracted_value integer;
	local_copied_ids jsonb;
	returned_interaction_ids integer[];
	json_obj_interaction_result json; -- individual root interaction result
	json_obj_interaction_link_result json; -- individual root interaction result
	json_obj_interaction_link_intervals_result json; -- individual root interaction result
	json_obj_interval_set_result json;
	json_obj_interval_result json;
    json_obj_result json; -- all results
BEGIN
-- for now, just copy everything, dont worry about "copying in place" since we can just delete the old node later
	-- handle for THIS interaction (and node, if applicable)
	
	local_copied_ids = '{}'::jsonb;

	FOR TABLE_RECORD in SELECT * FROM interval_set  WHERE interval_set.id = intervalsetid
	LOOP
		-- create the new interaction, and return its id
		-- Copy interval sets
		INSERT INTO interval_set (name, shared)
		VALUES (TABLE_RECORD.name, TABLE_RECORD.shared)
		RETURNING id INTO new_interval_set_id;
		SELECT json_build_object(intervalsetid, new_interval_set_id) into json_obj_interval_set_result; -- change map entry creation
		-- append to interaction map
		local_copied_ids = local_copied_ids || jsonb_build_object('interval_set', json_obj_interval_set_result);
		copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);


		-- find intervals associated with comm set
		-- pull their associated comm sets, copy those. Save into map
		-- return those copied comm set and save new copies of intervals with new comm set ids on them
		-- save old and new intervals into map
		FOR TABLE_RECORD in SELECT * FROM interval  WHERE interval.interval_set_id = intervalsetid
		LOOP
			-- FIXME need to probably have new communication set ids at this point
			FOR TABLE_RECORD_COMM_SET in SELECT * FROM communication_set  WHERE communication_set.id = TABLE_RECORD.communication_set_id
			LOOP
				IF TABLE_RECORD_COMM_SET.shared = false THEN
					copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,fn_copy_comm_set(TABLE_RECORD_COMM_SET.id::bigint,copied_ids::jsonb)::jsonb);
                    SELECT fn_get_dynamic_json_value(copied_ids::jsonb, TABLE_RECORD.communication_set_id::text, 'communication_set') into extracted_value; -- the new commsetID
				ELSE
                    extracted_value = TABLE_RECORD.communication_set_id;
				END IF;			
            END LOOP;
			
			INSERT INTO interval (interval_set_id, name, start_day, end_day,communication_set_id, recurrence)
			VALUES (new_interval_set_id, TABLE_RECORD.name, TABLE_RECORD.start_day, TABLE_RECORD.end_day, extracted_value, TABLE_RECORD.recurrence)
			RETURNING id INTO new_interval_id;
			SELECT json_build_object(TABLE_RECORD.id, new_interval_id) into json_obj_interval_result; -- change map entry creation
			local_copied_ids = local_copied_ids || jsonb_build_object('interval', json_obj_interval_result); -- append to interaction map
			copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);
		END LOOP;
	END LOOP;
	
	-- merge the mappings for THIS interaction with the global copy thats passed through
	copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);

	RETURN copied_ids;
END;$$;


CREATE OR REPLACE FUNCTION fn_copy_comm_set(
	setid bigint,
	copied_ids jsonb)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
	local_copied_ids jsonb;
    TABLE_RECORD record;
	TABLE_RECORD_COMMUNICATION_SET record;
	ret_id_comm_set integer; -- new copied commset id
	ret_id_comm integer; -- individual comm results
    needed_comms json[];
	json_obj_comm_result json; -- individual comms results
    json_obj_result json; -- all results
	json_obj_communication_set_result json;
	json_obj_communication_result json;

BEGIN
	
	local_copied_ids = '{}'::jsonb;
	
	FOR TABLE_RECORD_COMMUNICATION_SET in SELECT * FROM communication_set WHERE communication_set.id = setid
    LOOP
    	SELECT create_new_communication_set(TABLE_RECORD_COMMUNICATION_SET.name || ' - COPY'  || FLOOR(RANDOM() * 1000 + 1)::text) into ret_id_comm_set; -- create the new communication set
    END LOOP;
	
	SELECT json_build_object(setid, ret_id_comm_set) into json_obj_communication_set_result; -- set map entry creation
	local_copied_ids = local_copied_ids || jsonb_build_object('communication_set', json_obj_communication_set_result); 	-- append to local change map
	copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb); -- update final change map

    FOR TABLE_RECORD in SELECT * FROM communication WHERE communication.communication_set_id = setid
    LOOP
		SELECT create_new_communication(ret_id_comm_set::bigint,TABLE_RECORD."start",TABLE_RECORD."end",TABLE_RECORD.properties::json) into ret_id_comm;
		SELECT json_build_object(TABLE_RECORD.id, ret_id_comm) into json_obj_communication_result; -- change map entry creation
		local_copied_ids = local_copied_ids || jsonb_build_object('communication', json_obj_communication_result); 	-- append to local change map
		copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);  -- update final change map
    END LOOP;
	
	copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);
	
	return copied_ids;
END;
$BODY$;

CREATE OR REPLACE FUNCTION fn_copy_communication(
	communication_id bigint)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    TABLE_RECORD record;
	ret_id_comm integer; 
	json_obj_communication_result json;
BEGIN
    FOR TABLE_RECORD in SELECT * FROM communication WHERE communication.id = communication_id
    LOOP
		SELECT create_new_communication(TABLE_RECORD.communication_set_id,TABLE_RECORD."start",TABLE_RECORD."end",TABLE_RECORD.properties::json) into ret_id_comm;
    END LOOP;

	SELECT row_to_json(t) INTO json_obj_communication_result FROM (SELECT * FROM communication WHERE id = ret_id_comm) t;

	return json_obj_communication_result;
END;
$BODY$;

CREATE OR REPLACE FUNCTION fn_copy_condition(
	conditionid bigint,
	copied_ids jsonb,
	setid bigint default null::bigint)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
	local_copied_ids jsonb;
    TABLE_RECORD record;
	ret_id_comm_set integer; -- new copied commset id
	ret_id_comm integer; -- individual comm results
	ret_id_condition integer;
	extracted_value integer;
    condition_name text;
    needed_comms json[];
	json_obj_comm_result json; -- individual comms results
    json_obj_result json; -- all results
	json_obj_communication_set_result json;
	json_obj_communication_result json;
	json_obj_condition_result json;

BEGIN
	
	local_copied_ids = '{}'::jsonb;
    -- handle this condition
	-- create a new one with no set provided
    FOR TABLE_RECORD in SELECT * FROM condition WHERE condition.id = conditionid
    LOOP
        condition_name = null;

        IF TABLE_RECORD.name is not null THEN
            condition_name = TABLE_RECORD.name  || FLOOR(RANDOM() * 1000 + 1)::text;
        END IF;

		IF setid is null THEN
			INSERT INTO condition (
				parent_condition_id,
				name,
				shared,
				conditional,
				"from",  
				"as",
				"key",
				"value",
				"operator",
				"not",
				ordinal
			) VALUES (
				TABLE_RECORD.parent_condition_id,
				condition_name,
				TABLE_RECORD.shared,
				TABLE_RECORD.conditional,
				TABLE_RECORD."from",  
				TABLE_RECORD."as",
				TABLE_RECORD."key",
				TABLE_RECORD."value",
				TABLE_RECORD."operator",
				TABLE_RECORD."not",
				TABLE_RECORD.ordinal
			) RETURNING id INTO setid;
			SELECT json_build_object(conditionid, setid) into json_obj_condition_result; -- change map entry creation
			-- append to interaction map
			local_copied_ids = local_copied_ids || jsonb_build_object('condition', json_obj_condition_result);
			copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);
		ELSE
			-- get the id of new parent
			SELECT fn_get_dynamic_json_value(copied_ids::jsonb, TABLE_RECORD.parent_condition_id::text, 'condition') into extracted_value; -- get new parent condition ID from returned map
			INSERT INTO condition (root_condition_id,parent_condition_id,name,shared,conditional,"from","as","key","value","operator","not",ordinal) -- if its the root condition, we dont need to add parent or set. Set will be created automatically
			VALUES (
				setid,
				extracted_value,
				condition_name,
				TABLE_RECORD.shared,
				TABLE_RECORD.conditional,
				TABLE_RECORD."from",  
				TABLE_RECORD."as",
				TABLE_RECORD."key",
				TABLE_RECORD."value",
				TABLE_RECORD."operator",
				TABLE_RECORD."not",
				TABLE_RECORD.ordinal
			) RETURNING id INTO ret_id_condition;
			SELECT json_build_object(conditionid, ret_id_condition) into json_obj_condition_result; -- change map entry creation
			-- append to interaction map
			local_copied_ids = local_copied_ids || jsonb_build_object('condition', json_obj_condition_result);
			copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);
		END IF;
    END LOOP;
	
	-- handle children for this condition, recursively
    -- TODO/FIXME - not sure if we need to handle a condition trees' shared children in a different way?
	FOR TABLE_RECORD in SELECT * FROM condition WHERE condition.parent_condition_id = conditionid and condition.shared = false
    LOOP
		copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,fn_copy_condition(TABLE_RECORD.id, copied_ids, setid)::jsonb);

    END LOOP;
	
	copied_ids = fn_merge_interaction_maps(copied_ids::jsonb,local_copied_ids::jsonb);
	
	return copied_ids;
END;
$BODY$;

	
CREATE OR REPLACE FUNCTION fn_get_dynamic_json_value(json_data JSONB, key TEXT, tablename TEXT) RETURNS TEXT AS $$
	DECLARE
		result TEXT;
	BEGIN
	    -- TODO - needs to handle more than interaction
		-- Access the JSON property dynamically using the variable `key`
		-- tablename is 'interaction', 'interval_set' ....setc
		SELECT jsonb_extract_path_text(json_data, tablename, key)
		INTO result;

		-- Return the result
		RETURN result;
	END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION fn_merge_interaction_maps(A JSONB, B JSONB) RETURNS JSONB AS $$
DECLARE
    merged_interaction JSONB;
    merged_interaction_link JSONB;
	merged_interval_set JSONB;
	merged_interval JSONB;
	merged_communication_set JSONB;
	merged_communication JSONB;
	merged_interaction_link_interval JSONB;
	merged_condition JSONB;
	merged_interaction_instance_property JSONB;
    merged_result JSONB;
BEGIN
    -- Handle NULL by replacing with an empty JSONB object
    A := COALESCE(A, '{}'::jsonb);
    B := COALESCE(B, '{}'::jsonb);

    -- Extract and merge nested objects
    merged_interaction := COALESCE((A->'interaction'), '{}'::jsonb) || COALESCE((B->'interaction'), '{}'::jsonb);
    merged_interaction_link := COALESCE((A->'interaction_link'), '{}'::jsonb) || COALESCE((B->'interaction_link'), '{}'::jsonb);
    merged_interval_set := COALESCE((A->'interval_set'), '{}'::jsonb) || COALESCE((B->'interval_set'), '{}'::jsonb);
    merged_interval := COALESCE((A->'interval'), '{}'::jsonb) || COALESCE((B->'interval'), '{}'::jsonb);
    merged_communication_set := COALESCE((A->'communication_set'), '{}'::jsonb) || COALESCE((B->'communication_set'), '{}'::jsonb);
    merged_communication := COALESCE((A->'communication'), '{}'::jsonb) || COALESCE((B->'communication'), '{}'::jsonb);
    merged_interaction_link_interval := COALESCE((A->'interaction_link_interval'), '{}'::jsonb) || COALESCE((B->'interaction_link_interval'), '{}'::jsonb);
    merged_condition := COALESCE((A->'condition'), '{}'::jsonb) || COALESCE((B->'condition'), '{}'::jsonb);
	merged_interaction_instance_property := COALESCE((A->'interaction_instance_property'), '{}'::jsonb) || COALESCE((B->'interaction_instance_property'), '{}'::jsonb);

	

    -- Combine the merged nested objects into the final result
    merged_result := jsonb_deep_set('{}'::jsonb, '{interaction}', merged_interaction);
    merged_result := jsonb_deep_set(merged_result, '{interaction_link}', merged_interaction_link);
    merged_result := jsonb_deep_set(merged_result, '{interval_set}', merged_interval_set);
    merged_result := jsonb_deep_set(merged_result, '{interval}', merged_interval);
    merged_result := jsonb_deep_set(merged_result, '{communication_set}', merged_communication_set);
    merged_result := jsonb_deep_set(merged_result, '{communication}', merged_communication);
    merged_result := jsonb_deep_set(merged_result, '{interaction_link_interval}', merged_interaction_link_interval);
    merged_result := jsonb_deep_set(merged_result, '{condition}', merged_condition);
	merged_result := jsonb_deep_set(merged_result, '{interaction_instance_property}', merged_interaction_instance_property);


    RETURN merged_result;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION fn_update_interval_set_id_on_interaction(var_jsonb JSONB, new_id integer)
RETURNS JSONB AS $$
BEGIN
    -- Update the email field in the JSONB variable
    var_jsonb := jsonb_deep_set(var_jsonb, '{interval_set_id}', to_jsonb(new_id));
    RETURN var_jsonb;
END;
$$ LANGUAGE plpgsql;