-- ******************** Changelog ********************
CALL dbinfo.fn_create_or_update_table('changelog','{
    table text NOT NULL,
    fk bigint NOT NULL,
    modified timestamp without time zone NOT NULL,
    role text NOT NULL,
    user_id bigint,
    operation text NOT NULL,
    record jsonb NOT NULL
    }');
