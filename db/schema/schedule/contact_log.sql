CALL dbinfo.fn_create_or_update_table('contact_log','{
    id,
    subject_id bigint,
    target_id bigint,
    medium text,
    priority_value integer,
    communication_type text,
    contact_info text,
    sent timestamp,
    timezone text,
    error boolean NOT NULL DEFAULT false,
    error_message text,
    created timestamp
    }');
COMMENT ON TABLE contact_log IS 'This is a log of contact attempts for a given target.';DROP TRIGGER IF EXISTS tf_set_created ON contact_log;
COMMENT ON COLUMN contact_log.subject_id IS 'This is the Patients.PatientID for v1 integration';
COMMENT ON COLUMN contact_log.target_id IS 'This is a Users.ID (numeric) for v1 integration, if not set then the contact was for the patient';
CREATE TRIGGER tf_set_created BEFORE INSERT ON contact_log FOR EACH ROW EXECUTE FUNCTION tf_set_created();

CREATE INDEX IF NOT EXISTS idx_contact_log_subject_id ON "contact_log" ("subject_id","sent");
CREATE INDEX IF NOT EXISTS idx_contact_log_subject_id_error ON "contact_log" ("subject_id","error","sent");
CREATE INDEX IF NOT EXISTS idx_contact_log_subject_id_medium ON "contact_log" ("subject_id","medium","sent");
CREATE INDEX IF NOT EXISTS idx_contact_log_contact_info ON "contact_log" ("contact_info","sent");
CREATE INDEX IF NOT EXISTS idx_contact_log_target_id ON "contact_log" ("target_id","sent");
CREATE INDEX IF NOT EXISTS idx_contact_log_target_id_error ON "contact_log" ("target_id","error","sent");
CREATE INDEX IF NOT EXISTS idx_contact_log_target_id_medium ON "contact_log" ("target_id","medium","sent");

CALL dbinfo.fn_create_or_update_table('contact_log_interaction','{
    id,
    contact_log_id bigint NOT NULL,
    communication_id bigint NOT NULL,
    interaction_id bigint NOT NULL,
    instance jsonb NOT NULL DEFAULT ''[]'',
    message_iter integer
}');
COMMENT ON TABLE contact_log_interaction IS 'This is a table of interactions sent as part of a single contact in contact_log';
CREATE INDEX IF NOT EXISTS idx_contact_log_communication_id ON "contact_log_interaction" ("communication_id");