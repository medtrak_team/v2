CALL dbinfo.fn_create_or_update_table('interaction_schedule_cache','{
    anchor_id bigint not null, 
	subject_id bigint not null, 
	interaction_id bigint not null, 
	interaction_link_id bigint not null,
	interval_id bigint not null, 
	communication_set_id bigint, 
	instance jsonb NOT NULL DEFAULT ''\{\}'',
	start_date date not null,
	end_date date not null,
	effective_start_date date not null, 
	fulfilled boolean not null default false, 
	active boolean not null default false
}');
CALL dbinfo.fn_alter_table_add_constraint('interaction_schedule_cache','interaction_schedule_cache_pkey','
    PRIMARY KEY (anchor_id,interaction_link_id,instance,interval_id)'
);
CREATE INDEX IF NOT EXISTS idx_interaction_schedule_cache_anchor_id ON "interaction_schedule_cache" ("anchor_id","start_date");
CREATE INDEX IF NOT EXISTS idx_interaction_schedule_cache_subject_id ON "interaction_schedule_cache" ("subject_id","start_date");
CREATE INDEX IF NOT EXISTS idx_interaction_schedule_cache_interaction_id ON "interaction_schedule_cache" ("interaction_id","instance","start_date");
CREATE INDEX IF NOT EXISTS idx_interaction_schedule_cache_interaction_link_id ON "interaction_schedule_cache" ("interaction_link_id","start_date");
CREATE INDEX IF NOT EXISTS idx_interaction_schedule_cache_interval_id ON "interaction_schedule_cache" ("interval_id","start_date");
CREATE INDEX IF NOT EXISTS idx_interaction_schedule_cache_communication_set_id ON "interaction_schedule_cache" ("communication_set_id","start_date");

CALL dbinfo.fn_create_or_update_table('interaction_schedule_recache','{
	join_clause text not null,
    where_clause text not null,
    modified timestamp 
}');
CALL dbinfo.fn_alter_table_add_constraint('interaction_schedule_recache','interaction_schedule_recache_pkey','
    PRIMARY KEY ("join_clause","where_clause")'
);
CREATE INDEX IF NOT EXISTS idx_interaction_schedule_recache_modified ON "interaction_schedule_recache" ("modified");

-- @TODO if this starts to slow down we could have a manual cache function for a given interaction/pathway for use on build and then for prod we could just update when committing from build... I thought of this idea after finishing this
-- @TODO This doesn't account for interactions with multiple instances matching with anchors with multiple properties, it should probably do a combination rather than each pair being distinct... hard
DROP FUNCTION IF EXISTS fn_interaction_schedule_cache_generate;
CREATE OR REPLACE FUNCTION fn_interaction_schedule_cache_generate(join_clause text, where_clause text, remove_inactive boolean = false) 
RETURNS VOID AS $$
DECLARE
    sql_query text;
BEGIN
    sql_query := 
    'WITH interaction_schedule_data_instance AS (
        SELECT interaction_schedule_data.subject_id, interaction_schedule_data.anchor_id, interaction_schedule_data.interaction_link_interval_id,
            interaction_schedule_data.interval_id, interaction_schedule_data.interaction_link_id, interaction_schedule_data.interaction_link_path, 
            interaction_schedule_data.interaction_link_path_ordinals, interaction_schedule_data.interaction_id, interaction_schedule_data.communication_set_id,
            interaction_schedule_data.root_interaction_id, interaction_schedule_data.ordinal, interaction_schedule_data.start_date, interaction_schedule_data.end_date,  interaction_schedule_data.min_effective_start_date, interaction_schedule_data.anchor_created,
            COALESCE(
                jsonb_object_agg(
                    interaction_instance_property_type, 
                    anchor_property_value
                ) FILTER (WHERE interaction_instance_property_type IS NOT NULL),
                ''{}''::jsonb
            ) AS instance
        FROM interaction_schedule_data
        ' || join_clause || '
        WHERE TRUE 
        ' || where_clause || '
        GROUP BY interaction_schedule_data.subject_id, interaction_schedule_data.anchor_id, interaction_schedule_data.interaction_link_interval_id,
            interaction_schedule_data.interval_id, interaction_schedule_data.interaction_link_id, interaction_schedule_data.interaction_link_path, 
            interaction_schedule_data.interaction_link_path_ordinals, interaction_schedule_data.interaction_id, interaction_schedule_data.communication_set_id,
            interaction_schedule_data.root_interaction_id, interaction_schedule_data.ordinal, interaction_schedule_data.start_date, interaction_schedule_data.end_date, interaction_schedule_data.min_effective_start_date, interaction_schedule_data.anchor_created,
            interaction_schedule_data.interaction_instance_property_type, interaction_schedule_data.anchor_property_value
        HAVING MAX(interaction_instance_property_type) IS NULL 
            OR NOT bool_or(anchor_property_value IS NULL)
    )
    INSERT INTO interaction_schedule_cache (
        anchor_id,
        subject_id,
        interaction_id,
        interaction_link_id,
        interval_id,
        communication_set_id,
        instance,
        start_date,
        end_date,
        effective_start_date,
        fulfilled,
        active
    )
    SELECT 
        DISTINCT ON (anchor_id, interaction_link_id, instance, interval_id)
        interaction_schedule_data_instance.anchor_id,
        interaction_schedule_data_instance.subject_id,
        interaction_schedule_data_instance.interaction_id,
        interaction_schedule_data_instance.interaction_link_id,
        interaction_schedule_data_instance.interval_id,
        interaction_schedule_data_instance.communication_set_id,
        interaction_schedule_data_instance.instance,
        interaction_schedule_data_instance.start_date,
        interaction_schedule_data_instance.end_date,
        CASE WHEN interaction_schedule_data_instance.start_date < interaction_schedule_data_instance.min_effective_start_date 
            AND interaction_schedule_data_instance.end_date >= interaction_schedule_data_instance.min_effective_start_date
            THEN interaction_schedule_data_instance.min_effective_start_date
            ELSE interaction_schedule_data_instance.start_date
        END AS effective_start_date,
        EXISTS (
            SELECT 1 FROM subject_data
            WHERE interaction_schedule_data_instance.subject_id = subject_data.subject_id
                AND interaction_schedule_data_instance.interaction_id = subject_data.interaction_id
                AND interaction_schedule_data_instance.instance = subject_data.instance
                AND (interaction_schedule_data_instance.start_date IS NULL OR interaction_schedule_data_instance.start_date <= DATE(subject_data.completed))
                AND (interaction_schedule_data_instance.end_date IS NULL OR interaction_schedule_data_instance.end_date >= DATE(subject_data.completed))
            LIMIT 1
        ) AS fulfilled,
        ' || CASE WHEN remove_inactive THEN 'fn_is_interaction_schedule_active(interaction_schedule_data_instance.*)' ELSE 'true' END || ' AS active
    FROM interaction_schedule_data_instance
    ' || CASE WHEN remove_inactive THEN '' ELSE 'WHERE fn_is_interaction_schedule_active(interaction_schedule_data_instance.*)' END || '
    ON CONFLICT (anchor_id, interaction_link_id, instance, interval_id) DO UPDATE
    SET 
        subject_id = EXCLUDED.subject_id, 
        interaction_id = EXCLUDED.interaction_id,
        communication_set_id = EXCLUDED.communication_set_id,
        start_date = EXCLUDED.start_date, 
        end_date = EXCLUDED.end_date, 
        effective_start_date = EXCLUDED.effective_start_date, 
        fulfilled = EXCLUDED.fulfilled,
        active = EXCLUDED.active;';

    -- Execute the dynamically constructed SQL query
    --RAISE NOTICE 'interaction_schedule_cache query: %', sql_query;
    EXECUTE sql_query;
    
    IF remove_inactive THEN
        DELETE FROM interaction_schedule_cache where NOT active;
    END IF;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS fn_interaction_schedule_recache;
CREATE OR REPLACE FUNCTION fn_interaction_schedule_recache()
RETURNS VOID AS $$
DECLARE
    rec RECORD;
BEGIN
    -- Loop through records ordered by 'modified'
    FOR rec IN
        SELECT join_clause, where_clause 
        FROM interaction_schedule_recache
        ORDER BY modified
    LOOP
        -- Call fn_interaction_schedule_cache_generate for each row
        PERFORM fn_interaction_schedule_cache_generate(rec.join_clause, rec.where_clause, TRUE);

        -- Optionally delete the processed row
        DELETE FROM interaction_schedule_recache 
        WHERE join_clause = rec.join_clause AND where_clause = rec.where_clause;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

-- meta
CALL dbinfo.fn_alter_table_add_constraint('interaction_schedule_cache','fk_interaction_schedule_cache_anchor','
    FOREIGN KEY (anchor_id) REFERENCES anchor(id)
    ON DELETE CASCADE ON UPDATE CASCADE
');
CALL dbinfo.fn_alter_table_add_constraint('interaction_schedule_cache','fk_interaction_schedule_cache_interaction','
    FOREIGN KEY (interaction_id) REFERENCES interaction(id)
    ON DELETE CASCADE ON UPDATE CASCADE
');
CALL dbinfo.fn_alter_table_add_constraint('interaction_schedule_cache','fk_interaction_schedule_cache_interaction_link','
    FOREIGN KEY (interaction_link_id) REFERENCES interaction_link(id)
    ON DELETE CASCADE ON UPDATE CASCADE
');
CALL dbinfo.fn_alter_table_add_constraint('interaction_schedule_cache','fk_interaction_schedule_cache_interval','
    FOREIGN KEY (interval_id) REFERENCES interval(id)
    ON DELETE CASCADE ON UPDATE CASCADE
');

CREATE OR REPLACE FUNCTION tf_interaction_schedule_cache()
RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
    v_anchor_id bigint;
    v_interval_id bigint;
BEGIN

    IF (TG_OP = 'INSERT') THEN
        PERFORM fn_communication_cache_generate(anchor_id, interval_id, NULL) 
        FROM (
            SELECT DISTINCT anchor_id, interval_id FROM new_table
        ) AS values_to_update;
    ELSIF (TG_OP = 'UPDATE') THEN
        PERFORM fn_communication_cache_generate(anchor_id, interval_id, NULL) 
        FROM (
            SELECT DISTINCT anchor_id, interval_id FROM new_table
            UNION SELECT DISTINCT anchor_id, interval_id FROM old_table
        ) AS values_to_update;
    ELSIF (TG_OP = 'DELETE') THEN
        PERFORM fn_communication_cache_generate(anchor_id, interval_id, NULL) 
        FROM (
            SELECT DISTINCT anchor_id, interval_id FROM old_table
        ) AS values_to_update;
    END IF;
        
    RETURN NULL;
END;
$$;
DROP TRIGGER IF EXISTS tf_interaction_schedule_cache ON interaction_schedule_cache;
DROP TRIGGER IF EXISTS tf_interaction_schedule_cache_insert ON interaction_schedule_cache;
CREATE TRIGGER tf_interaction_schedule_cache_insert
AFTER INSERT ON interaction_schedule_cache
REFERENCING NEW TABLE AS new_table
FOR EACH STATEMENT EXECUTE FUNCTION tf_interaction_schedule_cache();
DROP TRIGGER IF EXISTS tf_interaction_schedule_cache_update ON interaction_schedule_cache;
CREATE TRIGGER tf_interaction_schedule_cache_update
AFTER UPDATE ON interaction_schedule_cache
REFERENCING OLD TABLE AS old_table NEW TABLE AS new_table
FOR EACH STATEMENT EXECUTE FUNCTION tf_interaction_schedule_cache();
DROP TRIGGER IF EXISTS tf_interaction_schedule_cache_delete ON interaction_schedule_cache;
CREATE TRIGGER tf_interaction_schedule_cache_delete
AFTER DELETE ON interaction_schedule_cache
REFERENCING OLD TABLE AS old_table
FOR EACH STATEMENT EXECUTE FUNCTION tf_interaction_schedule_cache();


DROP VIEW IF EXISTS interaction_schedule_data CASCADE;
CREATE OR REPLACE VIEW interaction_schedule_data AS
SELECT anchor.subject_id, anchor.id as anchor_id, interaction_link_interval.id as interaction_link_interval_id,
    interval.id as interval_id, interaction_link_interval.interaction_link_id, interaction_link_path.interaction_link_path, 
    interaction_link_path.interaction_link_path_ordinals, interaction_link.child_interaction_id as interaction_id, 
    interaction_link.root_interaction_id as root_interaction_id, interaction_link.ordinal as ordinal,
    interaction_instance_property.type as interaction_instance_property_type, 
    anchor_property.value as anchor_property_value,
    COALESCE(interaction_link_interval.communication_set_id, interval.communication_set_id) as communication_set_id,
    COALESCE(CASE
        WHEN interval.start_day IS NULL AND interval.after_created IS NULL THEN NULL 
        WHEN interval.start_day IS NOT NULL 
            AND (interval.after_created IS NULL OR DATE(anchor.created) + interval.after_created < anchor.date + interval.start_day) 
            THEN anchor.date + interval.start_day
        ELSE DATE(anchor.created) + interval.after_created
    END,'1900-01-01') AS start_date,
    COALESCE(CASE 
        WHEN interval.end_day IS NULL THEN NULL 
        ELSE anchor.date + interval.end_day 
    END, '2200-01-01') as end_date,
    CASE 
        WHEN interval.communication_start IS NOT NULL 
        AND interval.start_day IS NOT NULL 
        AND (interval.end_day IS NULL OR interval.start_day + interval.communication_start <= interval.end_day)
        AND anchor.date + interval.start_day + interval.communication_start > DATE(anchor.created)
        THEN anchor.date + interval.start_day + interval.communication_start
        ELSE DATE(anchor.created)
    END AS min_effective_start_date,
    anchor.created as anchor_created
FROM anchor
JOIN interaction_link_path ON anchor.interaction_id = interaction_link_path.anchor_interaction_id
JOIN "interaction_link" ON interaction_link_path.interaction_link_id = interaction_link.id
JOIN interaction_link_interval ON "interaction_link".id = interaction_link_interval.interaction_link_id
JOIN "interval" ON "interval".id = interaction_link_interval.interval_id
LEFT JOIN "interaction_instance_property" ON "interaction_instance_property".interaction_id = "interaction_link".child_interaction_id
LEFT JOIN anchor_property ON anchor_property.anchor_id = anchor.id 
AND interaction_instance_property.type = anchor_property.type
WHERE anchor.active;

DROP VIEW IF EXISTS interaction_schedule_raw CASCADE;
CREATE OR REPLACE VIEW interaction_schedule_raw AS
WITH interaction_schedule_data_instance AS (
    SELECT interaction_schedule_data.subject_id, interaction_schedule_data.anchor_id, interaction_schedule_data.interaction_link_interval_id,
        interaction_schedule_data.interval_id, interaction_schedule_data.interaction_link_id, interaction_schedule_data.interaction_link_path, 
        interaction_schedule_data.interaction_link_path_ordinals, interaction_schedule_data.interaction_id, interaction_schedule_data.communication_set_id,
        interaction_schedule_data.root_interaction_id, interaction_schedule_data.ordinal, interaction_schedule_data.start_date, interaction_schedule_data.end_date,  interaction_schedule_data.min_effective_start_date, interaction_schedule_data.anchor_created,
        COALESCE(
            jsonb_object_agg(
                interaction_instance_property_type, 
                anchor_property_value
            ) FILTER (WHERE interaction_instance_property_type IS NOT NULL),
            '{}'::jsonb
        ) AS instance
    FROM interaction_schedule_data
    GROUP BY interaction_schedule_data.subject_id, interaction_schedule_data.anchor_id, interaction_schedule_data.interaction_link_interval_id,
        interaction_schedule_data.interval_id, interaction_schedule_data.interaction_link_id, interaction_schedule_data.interaction_link_path, 
        interaction_schedule_data.interaction_link_path_ordinals, interaction_schedule_data.interaction_id, interaction_schedule_data.communication_set_id,
        interaction_schedule_data.root_interaction_id, interaction_schedule_data.ordinal, interaction_schedule_data.start_date, interaction_schedule_data.end_date, interaction_schedule_data.min_effective_start_date, interaction_schedule_data.anchor_created,
        interaction_schedule_data.interaction_instance_property_type, interaction_schedule_data.anchor_property_value
    HAVING MAX(interaction_instance_property_type) IS NULL 
        OR NOT bool_or(anchor_property_value IS NULL)
)
SELECT 
    DISTINCT ON (anchor_id, interaction_link_id, instance, interval_id)
    interaction_schedule_data_instance.anchor_id,
    interaction_schedule_data_instance.subject_id,
    interaction_schedule_data_instance.interaction_id,
    interaction_schedule_data_instance.interaction_link_id,
    interaction_schedule_data_instance.interval_id,
    interaction_schedule_data_instance.communication_set_id,
    interaction_schedule_data_instance.instance,
    interaction_schedule_data_instance.start_date,
    interaction_schedule_data_instance.end_date,
    CASE WHEN interaction_schedule_data_instance.start_date < interaction_schedule_data_instance.min_effective_start_date 
        AND interaction_schedule_data_instance.end_date >= interaction_schedule_data_instance.min_effective_start_date
        THEN interaction_schedule_data_instance.min_effective_start_date
        ELSE interaction_schedule_data_instance.start_date
    END AS effective_start_date,
    EXISTS (
        SELECT 1 FROM subject_data
        WHERE interaction_schedule_data_instance.subject_id = subject_data.subject_id
            AND interaction_schedule_data_instance.interaction_id = subject_data.interaction_id
            AND interaction_schedule_data_instance.instance = subject_data.instance
            AND (interaction_schedule_data_instance.start_date IS NULL OR interaction_schedule_data_instance.start_date <= DATE(subject_data.completed))
            AND (interaction_schedule_data_instance.end_date IS NULL OR interaction_schedule_data_instance.end_date >= DATE(subject_data.completed))
        LIMIT 1
    ) AS fulfilled,
    fn_is_interaction_schedule_active(interaction_schedule_data_instance.*) AS active
FROM interaction_schedule_data_instance;
