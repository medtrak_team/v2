CALL dbinfo.fn_create_or_update_table('communication_cache','{
    anchor_id bigint not null,
    interval_id bigint not null, 
    communication_id bigint not null,
    message_iter integer not null,
    frequency_window date not null,
    send_date date not null,
    send_time time
}');
CALL dbinfo.fn_alter_table_add_constraint('communication_cache','communication_cache_pkey','
    PRIMARY KEY (anchor_id,interval_id,communication_id,message_iter)'
);

DROP FUNCTION IF EXISTS fn_communication_cache_generate;
CREATE OR REPLACE FUNCTION fn_communication_cache_generate(
    anchor_id bigint,
    interval_id bigint,
    communication_id bigint
) 
RETURNS VOID AS $$
DECLARE
    delete_query text;
    insert_query text;
BEGIN
    delete_query :=
    'DELETE FROM communication_cache
    WHERE send_date >= CURRENT_DATE ';
    IF anchor_id IS NOT NULL THEN
        delete_query = delete_query || ' AND anchor_id = ' || anchor_id;
    END IF;
    IF interval_id IS NOT NULL THEN
        delete_query = delete_query || ' AND interval_id = ' || interval_id;
    END IF;
    IF communication_id IS NOT NULL THEN
        delete_query = delete_query || ' AND communication_id = ' || communication_id;
    END IF;
    EXECUTE delete_query;

    insert_query :=
    'INSERT INTO communication_cache(
        anchor_id,
        interval_id, 
        communication_id,
        message_iter,
        frequency_window,
        send_date,
        send_time
    )
    SELECT DISTINCT ON (anchor_id,interval_id,communication_id,message_iter)
        interaction_schedule_cache.anchor_id,
        interaction_schedule_cache.interval_id,
        communication.id AS communication_id,
        unrolled_messages.message_iter,
        unrolled_messages.frequency_window,
        unrolled_messages.send_date,
        COALESCE(
            (communication.properties->>''time'')::time,
            CASE WHEN communication.properties->>''type'' = ''notification'' 
                THEN ''8:00''::time + (ROUND(60 * RANDOM()) * ''10 minutes''::interval)
            ELSE NULL END
        ) AS send_time
    FROM interaction_schedule_cache
    JOIN communication
        ON interaction_schedule_cache.communication_set_id = communication.communication_set_id
    JOIN fn_unroll_communication_messages(
        interaction_schedule_cache.effective_start_date,
        communication.start,
        interaction_schedule_cache.end_date,
        communication.end,
        NULLIF(communication.properties->>''notification_minimum_frequency'', ''1 day''),
        communication.properties->>''count''
    ) AS unrolled_messages ON TRUE
    WHERE interaction_schedule_cache.active = TRUE
        AND unrolled_messages.send_date >= CURRENT_DATE ';
    IF anchor_id IS NOT NULL THEN
        insert_query = insert_query || ' AND interaction_schedule_cache.anchor_id = ' || anchor_id;
    END IF;
    IF interval_id IS NOT NULL THEN
        insert_query = insert_query || ' AND interaction_schedule_cache.interval_id = ' || interval_id;
    END IF;
    IF communication_id IS NOT NULL THEN
        insert_query = insert_query || ' AND communication.id = ' || communication_id;
    END IF;
    insert_query = insert_query || 
    ' ON CONFLICT (anchor_id,interval_id,communication_id,message_iter) DO UPDATE
    SET 
        frequency_window = EXCLUDED.frequency_window,
        send_date = EXCLUDED.send_date,
        send_time = EXCLUDED.send_time;';
    EXECUTE insert_query;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS fn_unroll_communication_messages;
CREATE OR REPLACE FUNCTION fn_unroll_communication_messages(
        start_date date, rel_start text,
        end_date date, rel_end text,
        formula text, count text)
    RETURNS TABLE(
        message_iter bigint,
        frequency_window date,
        send_date date
    ) AS $$
DECLARE
    calculated_start_date date;
    calculated_end_date date;
    calculated_interval interval;
BEGIN
    IF start_date IS NULL OR end_date IS NULL THEN
        RETURN;
    END IF;

    IF LEFT(rel_start, 1) = '-' THEN
        calculated_start_date := end_date + rel_start::interval;
    ELSE
        calculated_start_date := start_date + rel_start::interval;
    END IF;
    IF LEFT(rel_end, 1) = '-' THEN
        calculated_end_date := end_date + rel_end::interval;
    ELSE
        calculated_end_date := start_date + rel_end::interval;
    END IF;

    IF calculated_end_date < calculated_start_date THEN
        RETURN;
    END IF;

    IF formula IS NOT NULL THEN
        IF POSITION('%' in formula) <> 0 THEN
            calculated_interval := (GREATEST(1, (end_date - start_date) * (LEFT(formula, POSITION('%' in formula)-1)::int / 100)) || ' days')::interval;
        ELSIF POSITION(' ' in formula) = 0 THEN
            calculated_interval := (formula || ' days')::interval;
        ELSE
            calculated_interval := formula::interval;
        END IF;
    ELSE
        calculated_interval = '1 days'::interval;
    END IF;

    IF count IS NOT NULL THEN
        IF count::int = 1 THEN
            RETURN QUERY
            SELECT 1::bigint, calculated_start_date, calculated_start_date;
        END IF;
        calculated_end_date = calculated_start_date + (count::int * calculated_interval);
    END IF;

    RETURN QUERY
    SELECT
        row_number() OVER(ORDER BY message_series.send_date) AS message_iter,
        GREATEST(calculated_start_date, (message_series.send_date - calculated_interval)::date) AS frequency_window,
        message_series.send_date
    FROM (SELECT DISTINCT
        generate_series(
            calculated_start_date,
            calculated_end_date,
            calculated_interval
        )::date AS send_date) AS message_series;
END;
$$ LANGUAGE 'plpgsql';

DROP FUNCTION IF EXISTS fn_subject_schedule;
CREATE OR REPLACE FUNCTION fn_subject_schedule(param_subject_id bigint)
    RETURNS TABLE(
        subject_id bigint, 
        anchor_id bigint, 
        target_id bigint,
        medium text,
        interval_id bigint,
        priority integer,
        communication_id bigint, 
        message_iter integer,
        send_date date,
        send_time time
    ) AS $$
DECLARE
BEGIN
    RETURN QUERY
    WITH RECURSIVE send_message(subject_id, anchor_id, target_id, medium, interval_id, priority, communication_id, message_iter, send_date) AS (
        SELECT
            sorted_messages.subject_id, 
            sorted_messages.anchor_id, 
            sorted_messages.target_id,
            sorted_messages.medium,
            sorted_messages.interval_id,
            fn_priority_value(sorted_messages.priority) AS priority,
            sorted_messages.communication_id, 
            sorted_messages.message_iter,
            sorted_messages.send_date,
            sorted_messages.send_time
        FROM (SELECT 
                interaction_schedule_cache.subject_id,
                communication_cache.anchor_id,
                (communication.properties->>'target')::bigint AS "target_id",
                communication.properties->>'medium' AS medium,
                communication_cache.interval_id, 
                communication.properties->>'priority' AS "priority",
                communication_cache.communication_id,
                communication_cache.message_iter,
                communication_cache.send_date,
                communication_cache.send_time,
                row_number() OVER (
                    PARTITION BY interaction_schedule_cache.subject_id,
                        (communication.properties->>'target')::bigint,
                        communication.properties->>'medium'
                    ORDER BY communication_cache.send_date, 
                        fn_priority_value(communication.properties->>'priority') DESC,
                        communication_cache.send_time
                ) AS subject_msg_num
            FROM interaction_schedule_cache
            JOIN communication_cache
                ON interaction_schedule_cache.anchor_id = communication_cache.anchor_id
                AND interaction_schedule_cache.interval_id = communication_cache.interval_id
            JOIN communication
                ON communication_cache.communication_id = communication.id
                AND interaction_schedule_cache.communication_set_id = communication.communication_set_id
            WHERE interaction_schedule_cache.subject_id = param_subject_id
                AND interaction_schedule_cache.active = TRUE
                AND communication.properties->>'type' = 'notification'
                AND communication_cache.send_date >= CURRENT_DATE
                AND NOT EXISTS (SELECT 1 FROM contact_log
                    WHERE contact_log.subject_id = interaction_schedule_cache.subject_id
                        AND contact_log.medium = communication.properties->>'medium'
                        AND contact_log.priority_value >= fn_priority_value(communication.properties->>'priority')
                        AND contact_log.communication_type = communication.properties->>'type'
                        AND DATE(contact_log.sent) BETWEEN communication_cache.frequency_window AND communication_cache.send_date
                        AND NOT contact_log.error)) AS sorted_messages
        WHERE subject_msg_num = 1
        UNION ALL
        SELECT 
            communication_cache.subject_id,
            communication_cache.anchor_id, 
            communication_cache.target,
            communication_cache.medium,
            communication_cache.interval_id,
            communication_cache.priority,
            communication_cache.communication_id, 
            communication_cache.message_iter,
            communication_cache.send_date,
            communication_cache.send_time
        FROM send_message, LATERAL(
            SELECT 
                anchor.subject_id, 
                communication_cache.anchor_id, 
                (communication.properties->>'target')::bigint AS target,
                communication.properties->>'medium' AS medium,
                communication_cache.interval_id,
                fn_priority_value(communication.properties->>'priority') AS priority, 
                communication_cache.communication_id, 
                communication_cache.message_iter,
                communication_cache.send_date,
                communication_cache.send_time
            FROM anchor
            JOIN communication_cache ON anchor.id = communication_cache.anchor_id
            JOIN communication ON communication_cache.communication_id = communication.id
            WHERE anchor.subject_id = send_message.subject_id
                AND (communication.properties->>'target')::bigint IS NOT DISTINCT FROM send_message.target_id
                AND communication.properties->>'medium' = send_message.medium
                AND communication_cache.send_date > send_message.send_date
                AND (communication_cache.message_iter = 1
                    OR fn_priority_value(communication.properties->>'priority') > send_message.priority
                    OR send_message.send_date <= communication_cache.frequency_window)
            ORDER BY communication_cache.send_date, 
                fn_priority_value(communication.properties->>'priority') DESC, 
                communication_cache.send_time
            LIMIT 1
        ) AS communication_cache
    )
    SELECT * FROM send_message
    UNION
    SELECT 
        interaction_schedule_cache.subject_id,
        communication_cache.anchor_id, 
        (communication.properties->>'target')::bigint AS target,
        communication.properties->>'medium' AS medium,
        communication_cache.interval_id,
        fn_priority_value(communication.properties->>'priority') AS priority,
        communication_cache.communication_id, 
        communication_cache.message_iter,
        communication_cache.send_date,
        communication_cache.send_time
    FROM interaction_schedule_cache
    JOIN communication_cache
        ON interaction_schedule_cache.anchor_id = communication_cache.anchor_id
        AND interaction_schedule_cache.interval_id = communication_cache.interval_id
    JOIN communication
        ON communication_cache.communication_id = communication.id
        AND interaction_schedule_cache.communication_set_id = communication.communication_set_id
    WHERE interaction_schedule_cache.subject_id = param_subject_id
        AND interaction_schedule_cache.active = TRUE
        AND communication.properties->>'type' != 'notification'
        AND communication_cache.send_date >= CURRENT_DATE
        AND NOT EXISTS (SELECT 1 FROM contact_log
            JOIN contact_log_interaction ON contact_log.id = contact_log_interaction.contact_log_id
            WHERE contact_log.subject_id = interaction_schedule_cache.subject_id
                AND contact_log.target_id IS NOT DISTINCT FROM (communication.properties->>'target')::bigint
                AND contact_log.medium = communication.properties->>'medium'
                AND DATE(contact_log.sent) = communication_cache.send_date
                AND contact_log_interaction.interaction_id = interaction_schedule_cache.interaction_id
                AND contact_log_interaction.communication_id = communication_cache.communication_id
                AND contact_log_interaction.message_iter = communication_cache.message_iter
    )
    ORDER BY send_date, send_time;
END;
$$ LANGUAGE 'plpgsql';