CREATE OR REPLACE FUNCTION tf_subject_data_update_existing_on_insert() 
RETURNS TRIGGER AS $$
DECLARE
    v_existing_subject_data_id bigint;
BEGIN
    SELECT subject_data.id
    INTO v_existing_subject_data_id
    FROM subject_data
    -- Join interaction_schedule with both the new and old subject_data fields where they should be the same
    -- but just the completed date from the old subject_data since it will be different
    JOIN interaction_schedule
    ON interaction_schedule.subject_id = NEW.subject_id
    AND interaction_schedule.interaction_id = NEW.interaction_id
    AND interaction_schedule.instance = NEW.instance
    AND interaction_schedule.subject_id = subject_data.subject_id
    AND interaction_schedule.interaction_id = subject_data.interaction_id
    AND interaction_schedule.instance = subject_data.instance
    AND (interaction_schedule.start_date IS NULL OR interaction_schedule.start_date <= DATE(subject_data.completed))
    AND (interaction_schedule.end_date IS NULL OR interaction_schedule.end_date >= DATE(subject_data.completed))
    GROUP BY subject_data.id
    -- Ensure all interaction_schedule records that match the old subject_data match the new subject_data's completed date
    HAVING bool_and(
        (interaction_schedule.start_date IS NULL OR interaction_schedule.start_date <= DATE(NEW.completed))
        AND (interaction_schedule.end_date IS NULL OR interaction_schedule.end_date >= DATE(NEW.completed))
    );

    -- If a matching record exists, update the old subject_data record
    IF v_existing_subject_data_id IS NOT NULL THEN
        UPDATE subject_data
        SET completed = NEW.completed, 
            submitted = NEW.submitted,
            submitted_by = NEW.submitted_by,
            data = NEW.data
        WHERE id = v_existing_subject_data_id;
        
        -- Return NULL to cancel the insert
        RETURN NULL;
    END IF;

    -- If no match is found, allow the insert to proceed
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CALL dbinfo.fn_create_or_update_table('subject_data','{
    id,
    subject_id bigint NOT NULL,
    interaction_id bigint DEFAULT NULL,
    instance jsonb NOT NULL DEFAULT ''\{\}'',
    completed timestamp DEFAULT NULL,
    submitted timestamp DEFAULT NULL,
    submitted_by bigint DEFAULT NULL,
    data jsonb NOT NULL DEFAULT ''\{\}'',
    modified timestamp
    }');
COMMENT ON TABLE subject_data IS 'For a subject and a particular instance of an interaction this table contains a record of
    when the interaction was completed, when it was submitted to the system (or retrospecitve data), and who submitted it, as well as any
    data that accompanies the interaction, such as survey data.
    The specfic node and schedule that lead to the interaction are not relevant.
    We want this to mark as complete every interaction_schedule for which this record is within the date range,
    and for which the instances match. 
    That way if we have overlapping interactions for the same instances then we only have to have that interaction once.';
CREATE UNIQUE INDEX IF NOT EXISTS uidx_subject_data ON subject_data ("subject_id", "interaction_id", "instance", "completed");
DROP TRIGGER IF EXISTS tf_log_change ON subject_data;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON subject_data FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON subject_data;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON subject_data FOR EACH ROW EXECUTE FUNCTION tf_set_modified();
DROP TRIGGER IF EXISTS tf_subject_data_update_existing_on_insert ON subject_data;
CREATE TRIGGER tf_subject_data_update_existing_on_insert BEFORE INSERT ON subject_data FOR EACH ROW EXECUTE FUNCTION tf_subject_data_update_existing_on_insert();

CREATE INDEX IF NOT EXISTS idx_subject_data_subject_id_completed ON "subject_data" ("subject_id","interaction_id","instance","completed");
CREATE INDEX IF NOT EXISTS idx_subject_data_interaction_id_completed ON "subject_data" ("interaction_id","instance","completed");
CREATE INDEX IF NOT EXISTS idx_subject_data_subject_id_submitted ON "subject_data" ("subject_id","interaction_id","instance","submitted");
CREATE INDEX IF NOT EXISTS idx_subject_data_interaction_id_submitted ON "subject_data" ("interaction_id","instance","submitted");
CREATE INDEX IF NOT EXISTS idx_subject_data_submitted_by ON "subject_data" ("submitted_by","submitted");

CALL dbinfo.fn_create_or_update_table('subject_data_value','{
    subject_data_id bigint NOT NULL,
    key text NOT NULL,
    value text
    }');
CREATE INDEX IF NOT EXISTS idx_subject_data_value_value ON "subject_data_value" ("key","value");

CALL dbinfo.fn_create_or_update_table('subject_data_instance','{
    subject_data_id bigint NOT NULL,
    type text NOT NULL,
    value text
    }');
CREATE INDEX IF NOT EXISTS idx_subject_data_instance_value ON "subject_data_instance" ("type","value");

-- meta
CALL dbinfo.fn_alter_table_add_constraint('subject_data','fk_subject_data_interaction','
    FOREIGN KEY (interaction_id) REFERENCES interaction(id)
    ON DELETE CASCADE ON UPDATE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('subject_data_value','subject_data_value_pkey','
    PRIMARY KEY (subject_data_id,key,value)');
CALL dbinfo.fn_alter_table_add_constraint('subject_data_value','fk_subject_data_value_subject_data','
    FOREIGN KEY (subject_data_id) REFERENCES subject_data(id)
    ON DELETE CASCADE ON UPDATE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('subject_data_instance','subject_data_instance_pkey','
    PRIMARY KEY (subject_data_id,type,value)');
CALL dbinfo.fn_alter_table_add_constraint('subject_data_instance','fk_subject_data_instance_subject_data','
    FOREIGN KEY (subject_data_id) REFERENCES subject_data(id)
    ON DELETE CASCADE ON UPDATE CASCADE
');
    
CREATE OR REPLACE FUNCTION tf_subject_data() RETURNS trigger 
LANGUAGE plpgsql
AS $$
DECLARE
    v_key TEXT;               -- Key in the JSONB object
    v_value JSONB;            -- Value associated with the key (either a single value or an array)
    v_individual_value TEXT;
    v_subject_id bigint;
	v_interaction_id bigint;
	v_instance JSONB;
BEGIN
    /***************************************************************************
    subject_data_value
    ***************************************************************************/

    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        -- Step 1: Create a temporary table to store the new set of subject_data properties
        CREATE TEMP TABLE temp_subject_data_value (
            subject_data_id BIGINT,
            key TEXT,
            value TEXT
        );

        -- Step 2: Populate the temp table with data from NEW.data
        FOR v_key, v_value IN 
            SELECT key, value FROM jsonb_each(NEW.data)
        LOOP
            -- Check if the value is an array
            IF jsonb_typeof(v_value) = 'array' THEN
                -- Insert each element of the array as a separate entry in temp_subject_data_value
                FOR v_individual_value IN 
                    SELECT value FROM jsonb_array_elements_text(v_value)
                LOOP
                    INSERT INTO temp_subject_data_value (subject_data_id, key, value)
                    VALUES (NEW.id, v_key, v_individual_value);
                END LOOP;
            ELSE
                -- If it's not an array, insert the single value directly
                INSERT INTO temp_subject_data_value (subject_data_id, key, value)
                VALUES (NEW.id, v_key, v_value::text);
            END IF;
        END LOOP;

        IF TG_OP = 'UPDATE' AND NEW.data IS DISTINCT FROM OLD.data THEN
            PERFORM set_config('session.tf_subject_data_value_update_interaction_schedule_cache', 'true', true);
    	END IF;

        -- Step 3: Delete any rows in subject_data_value not in the new set
        DELETE FROM subject_data_value
        WHERE subject_data_id = NEW.id
        AND (key, value) NOT IN (SELECT key, value FROM temp_subject_data_value);

        -- Step 4: Insert new values from the temporary table that don't already exist
        INSERT INTO subject_data_value (subject_data_id, key, value)
        SELECT subject_data_id, key, value
        FROM temp_subject_data_value
        ON CONFLICT DO NOTHING;

        IF TG_OP = 'UPDATE' AND NEW.data IS DISTINCT FROM OLD.data THEN
            PERFORM set_config('session.tf_subject_data_value_update_interaction_schedule_cache', NULL, true);
    	END IF;

        -- Step 5: Drop the temporary table
        DROP TABLE temp_subject_data_value;
    END IF;

    /***************************************************************************
    subject_data_instance
    ***************************************************************************/

    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        -- Step 1: Create a temporary table to store the new set of subject_data instance
        CREATE TEMP TABLE temp_subject_data_instance (
            subject_data_id BIGINT,
            type TEXT,
            value TEXT
        );

        -- Step 2: Populate the temp table with instances from NEW.instance
        FOR v_key, v_value IN 
            SELECT key, value FROM jsonb_each(NEW.instance)
        LOOP
            -- If it's not an array, insert the single value directly
            INSERT INTO temp_subject_data_instance (subject_data_id, type, value)
            VALUES (NEW.id, v_key, v_value::text);
        END LOOP;

        IF TG_OP = 'UPDATE' AND NEW.instance IS DISTINCT FROM OLD.instance THEN
            PERFORM set_config('session.tf_subject_data_instance_update_interaction_schedule_cache', 'true', true);
    	END IF;

        -- Step 3: Delete any rows in subject_data_instance not in the new set
        DELETE FROM subject_data_instance
        WHERE subject_data_id = NEW.id
        AND (type, value) NOT IN (SELECT type, value FROM temp_subject_data_instance);

        -- Step 4: Insert new values from the temporary table that don't already exist
        INSERT INTO subject_data_instance (subject_data_id, type, value)
        SELECT subject_data_id, type, value
        FROM temp_subject_data_instance
        ON CONFLICT DO NOTHING;

        IF TG_OP = 'UPDATE' AND NEW.instance IS DISTINCT FROM OLD.instance THEN
            PERFORM set_config('session.tf_subject_data_instance_update_interaction_schedule_cache', NULL, true);
    	END IF;

        -- Step 5: Drop the temporary table
        DROP TABLE temp_subject_data_instance;
    END IF;

    /***************************************************************************
    interaction_schedule_cache
    ***************************************************************************/

    v_subject_id := COALESCE(NEW.subject_id,OLD.subject_id); 
	v_interaction_id := COALESCE(NEW.interaction_id,OLD.interaction_id);
	v_instance := COALESCE(NEW.instance,OLD.instance);

    /* @NOTE
    This used to contain:
    interaction_schedule_data.instance = quote_literal(v_instance::jsonb)
    But we moved the join/where deeper in the query before the aggregate that creates interaction_schedule_data.instance
    This could be run against a later part of the query, but that would be very inefficient
    This approach will work okay and should be efficient becuase it only affects one subject
    But if we want to optimize this more later it would have to be through a custom function that updates fulfilled
    in the interaction_schedule_cache by querying subject, interaction, instance, start_date, and end_date
    */
    PERFORM fn_interaction_schedule_cache_generate('',
        format(
            'AND interaction_schedule_data.subject_id = %L 
             AND interaction_schedule_data.interaction_id = %L',
            v_subject_id, 
            v_interaction_id
        )  
    );
    
    RETURN NULL;
END;
$$;
DROP TRIGGER IF EXISTS tf_subject_data ON subject_data;
CREATE TRIGGER tf_subject_data AFTER INSERT OR UPDATE OR DELETE ON subject_data FOR EACH ROW EXECUTE FUNCTION tf_subject_data();

CREATE OR REPLACE FUNCTION tf_subject_data_value()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    IF current_setting('session.tf_subject_data_value_update_interaction_schedule_cache', true) != 'true' THEN
        RETURN NULL;
    END IF;

    -- @NOTE see note above, this used to have: AND interaction_schedule_data.instance = subject_data.instance
    PERFORM fn_interaction_schedule_cache_generate('
        JOIN subject_data
        ON interaction_schedule_data.subject_id = subject_data.subject_id
        AND interaction_schedule_data.interaction_id = subject_data.interaction_id
        JOIN interaction_link_parent 
        ON interaction_schedule_data.interaction_link_id = interaction_link_parent.interaction_link_id
        JOIN interaction_link_condition 
        ON interaction_link_parent.parent_interaction_link_id = interaction_link_condition.interaction_link_id
        JOIN condition 
        ON interaction_link_condition.condition_id = condition.id
    ',format('
        AND condition.conditional = ''data_value''
    	AND condition.key = %L
    	AND condition.value = %L
        AND subject_data.id = %L',
        COALESCE(NEW.key,OLD.key), 
        COALESCE(NEW.value,OLD.value), 
        COALESCE(NEW.subject_data_id,OLD.subject_data_id)
    ),TRUE);

    RETURN NULL;
END;
$$;
DROP TRIGGER IF EXISTS tf_subject_data_value ON subject_data_value;
CREATE TRIGGER tf_subject_data_value
AFTER INSERT OR DELETE ON subject_data_value
FOR EACH ROW EXECUTE FUNCTION tf_subject_data_value();

CREATE OR REPLACE FUNCTION tf_subject_data_instance()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    IF current_setting('session.tf_subject_data_instance_update_interaction_schedule_cache', true) != 'true' THEN
        RETURN NULL;
    END IF;

    -- @NOTE see note above, this used to have: AND interaction_schedule_data.instance = subject_data.instance
    PERFORM fn_interaction_schedule_cache_generate('
        JOIN subject_data
        ON interaction_schedule_data.subject_id = subject_data.subject_id
        AND interaction_schedule_data.interaction_id = subject_data.interaction_id
        JOIN interaction_link_parent 
        ON interaction_schedule_data.interaction_link_id = interaction_link_parent.interaction_link_id
        JOIN interaction_link_condition 
        ON interaction_link_parent.parent_interaction_link_id = interaction_link_condition.interaction_link_id
        JOIN condition 
        ON interaction_link_condition.condition_id = condition.id
    ',format('
        AND condition.conditional = ''data_instance''
    	AND condition.key = %L
    	AND condition.value = %L
        AND subject_data.id = %L',
        COALESCE(NEW.type,OLD.type), 
        COALESCE(NEW.value,OLD.value), 
        COALESCE(NEW.subject_data_id,OLD.subject_data_id)
    ),TRUE);
  
    RETURN NULL;
END;
$$;
DROP TRIGGER IF EXISTS tf_subject_data_instance ON subject_data_instance;
CREATE TRIGGER tf_subject_data_instance
AFTER INSERT OR DELETE ON subject_data_instance
FOR EACH ROW EXECUTE FUNCTION tf_subject_data_instance();
