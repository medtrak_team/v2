CALL dbinfo.fn_create_or_update_table('anchor','{
    id,
    subject_id bigint NOT NULL,
    interaction_id bigint NULL DEFAULT NULL,
    external_origin text NULL DEFAULT NULL,
    external_id text NULL DEFAULT NULL,
    active boolean DEFAULT true NOT NULL,
    final boolean DEFAULT false NOT NULL,
    date date NOT NULL,
    time time without time zone NULL DEFAULT NULL,
    properties jsonb NOT NULL DEFAULT ''\{\}''::jsonb,
    modified timestamp,
    created timestamp,
    self_reported_properties jsonb
}'); 
DROP TRIGGER IF EXISTS tf_log_change ON anchor;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON anchor FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON anchor;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON anchor FOR EACH ROW EXECUTE FUNCTION tf_set_modified();
DROP TRIGGER IF EXISTS tf_set_created ON anchor;
CREATE TRIGGER tf_set_created BEFORE INSERT ON anchor FOR EACH ROW EXECUTE FUNCTION tf_set_created();
COMMENT ON COLUMN anchor.date IS 'The date from the perspective of the subject, not UTC';
COMMENT ON COLUMN anchor.self_reported_properties IS 'True if the patient reported properties of the anchor, false if the anchor properties were imported from another system or set by a trusted user';

CREATE INDEX IF NOT EXISTS idx_anchor_subject_id ON "anchor" ("subject_id");
CREATE INDEX IF NOT EXISTS idx_anchor_interaction_id_subject_id ON "anchor" ("interaction_id","subject_id");
CREATE INDEX IF NOT EXISTS idx_anchor_interaction_id_date ON "anchor" ("interaction_id","date");
CREATE INDEX IF NOT EXISTS idx_anchor_interaction_id_active ON "anchor" ("interaction_id","active","date");

/*
@todo down the line
- we might need mechanisms for handling aliases or duplicate values that are slightly different
- later: anchor_user table mirroing anchor_property but for users like doctors, care partners, etc. would need a role field and anchor.users should be added
- add a constraint on properties that defines the json schema
*/

CALL dbinfo.fn_create_or_update_table('anchor_property','{
    anchor_id bigint NOT NULL,
    type text NOT NULL,
    value text
}');
COMMENT ON COLUMN anchor_property.value IS 'Value could be null if we do not know the value';
CREATE INDEX IF NOT EXISTS idx_anchor_property_value ON "anchor_property" ("type","value");

-- meta
CALL dbinfo.fn_alter_table_add_constraint('anchor_property','anchor_property_pkey','
    PRIMARY KEY (anchor_id,type,value)
');
CALL dbinfo.fn_alter_table_add_constraint('anchor_property','fk_anchor_property_anchor','
    FOREIGN KEY (anchor_id) REFERENCES anchor(id)
    ON DELETE CASCADE ON UPDATE CASCADE
');

CREATE OR REPLACE FUNCTION tf_anchor() 
RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
    this_anchor_property_key TEXT;               -- Key in the JSONB object
    this_anchor_property_value JSONB;            -- Value associated with the key (either a single value or an array)
    this_anchor_property_individual_value TEXT;
    v_interaction_link RECORD;
BEGIN
    /***************************************************************************
    anchor_property
    ***************************************************************************/

    IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND (NEW.properties IS DISTINCT FROM OLD.properties OR NEW.self_reported_properties IS DISTINCT FROM OLD.self_reported_properties)) THEN
        -- Step 1: Create a temporary table to store the new set of anchor properties
        CREATE TEMP TABLE temp_anchor_properties (
            anchor_id BIGINT,
            type TEXT,
            value TEXT
        );

        -- Step 2: Populate the temp table with properties from NEW.properties
        -- Loop through each key-value pair from both properties and self-reported properties
        FOR this_anchor_property_key, this_anchor_property_value IN 
            (SELECT key, value
            FROM jsonb_each(NEW.properties))
            UNION ALL
            (SELECT key, value 
            FROM jsonb_each(NEW.self_reported_properties)
            WHERE NEW.self_reported_properties IS NOT NULL)
        LOOP
            -- Check if the value is an array
            IF jsonb_typeof(this_anchor_property_value) = 'array' THEN
                -- Insert each element of the array as a separate entry in temp_anchor_properties
                FOR this_anchor_property_individual_value IN 
                    SELECT value 
                    FROM jsonb_array_elements_text(this_anchor_property_value)
                LOOP
                    -- Insert each non-array value into temp_anchor_properties
                    INSERT INTO temp_anchor_properties (anchor_id, type, value)
                    VALUES (NEW.id, this_anchor_property_key, this_anchor_property_individual_value);
                END LOOP;
            ELSE
                -- If it's not an array, insert the single value directly
                INSERT INTO temp_anchor_properties (anchor_id, type, value)
                VALUES (NEW.id, this_anchor_property_key, this_anchor_property_value #>> '{}');
            END IF;
        END LOOP;
        
        -- Cleanup step: delete 'unknown' values if there are other values for the same key
        DELETE FROM temp_anchor_properties
        WHERE value = 'unknown'
        AND type = ANY(
            SELECT type
            FROM temp_anchor_properties
            WHERE value IS DISTINCT FROM 'unknown'
        );
        
        -- Set the session config for update operations
        IF TG_OP = 'UPDATE' THEN
            PERFORM set_config('session.tf_anchor_property_update_interaction_schedule_cache', 'true', true);
        END IF;

        -- Step 3: Delete any rows in anchor_property not in the new set
        DELETE FROM anchor_property
        WHERE anchor_id = NEW.id
        AND NOT EXISTS (
            SELECT 1
            FROM temp_anchor_properties
            WHERE anchor_property.type = temp_anchor_properties.type
            AND anchor_property.value = temp_anchor_properties.value
        );

        -- Step 4: Insert new values from the temporary table that don't already exist
        INSERT INTO anchor_property (anchor_id, type, value)
        SELECT anchor_id, type, value
        FROM temp_anchor_properties
        ON CONFLICT DO NOTHING;

        IF TG_OP = 'UPDATE' THEN
            PERFORM set_config('session.tf_anchor_property_update_interaction_schedule_cache', NULL, true);
        END IF;

        -- Step 5: Drop the temporary table
        DROP TABLE temp_anchor_properties;
    END IF;

    /***************************************************************************
    interaction_link_path
    ***************************************************************************/

    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        -- If a path already exists for this anchor_interaction_id, exit
        IF NOT EXISTS (SELECT 1 FROM interaction_link_path WHERE anchor_interaction_id = NEW.interaction_id) THEN
            -- Start by selecting interaction links where parent_interaction_id matches NEW.interaction_id (the anchor)
            FOR v_interaction_link IN 
                SELECT * FROM interaction_link 
                WHERE parent_interaction_id = NEW.interaction_id
            LOOP
                -- Pass the initial link and anchor to the recursive function
                PERFORM fn_build_interaction_link_path_for_interaction_link(v_interaction_link, NEW.interaction_id);
            END LOOP;
        END IF;
    END IF;
    
    /***************************************************************************
    interaction_schedule_cache
    ***************************************************************************/

    -- Handle DELETE or UPDATE where date has changed: remove relevant records from cache
    -- @todo the properties part is for instances that are no longer valid, but we could pick specifically those instances in tf_anchor_property if we need more performance
    IF TG_OP = 'DELETE' OR (TG_OP = 'UPDATE' AND (NEW.date IS DISTINCT FROM OLD.date OR NEW.created IS DISTINCT FROM OLD.created OR NEW.properties IS DISTINCT FROM OLD.properties OR NEW.self_reported_properties IS DISTINCT FROM OLD.self_reported_properties)) THEN
        DELETE FROM interaction_schedule_cache 
        WHERE anchor_id = OLD.id;
    END IF;

    -- Handle INSERT or UPDATE where date has changed: insert records from the view
    IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND (NEW.date IS DISTINCT FROM OLD.date OR NEW.created IS DISTINCT FROM OLD.created OR NEW.properties IS DISTINCT FROM OLD.properties OR NEW.self_reported_properties IS DISTINCT FROM OLD.self_reported_properties)) THEN
        PERFORM fn_interaction_schedule_cache_generate(
            '',  -- No additional joins needed in this case, so an empty string
            'AND interaction_schedule_data.anchor_id = ' || NEW.id
        );
    END IF;

    RETURN NULL;
END;
$$;
DROP TRIGGER IF EXISTS tf_anchor ON anchor;
CREATE TRIGGER tf_anchor AFTER INSERT OR UPDATE OR DELETE ON anchor FOR EACH ROW EXECUTE FUNCTION tf_anchor();

CREATE OR REPLACE FUNCTION tf_anchor_property()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
	IF current_setting('session.tf_anchor_update_interaction_schedule_cache', true) != 'true' THEN
		RETURN NULL;
	END IF;

    --RAISE NOTICE 'tf_anchor_property % % %', COALESCE(NEW.anchor_id, OLD.anchor_id), COALESCE(NEW.type,OLD.type), COALESCE(NEW.value, OLD.value);
	
    PERFORM fn_interaction_schedule_cache_generate('
        JOIN interaction_link_parent 
        ON interaction_schedule_data.interaction_link_id = interaction_link_parent.interaction_link_id
        JOIN interaction_link_condition 
        ON interaction_link_parent.parent_interaction_link_id = interaction_link_condition.interaction_link_id
        JOIN condition 
        ON interaction_link_condition.condition_id = condition.id
    ', format('
        AND condition.conditional = ''anchor_property''
    	AND condition.key = %L
    	AND condition.value = %L
        AND interaction_schedule_data.anchor_id = %L',
        COALESCE(NEW.type,OLD.type), 
        COALESCE(NEW.value,OLD.value), 
        COALESCE(NEW.anchor_id,OLD.anchor_id)
    ),TRUE);

    RETURN NULL;
END;
$$;
DROP TRIGGER IF EXISTS tf_anchor_property ON anchor_property;
CREATE TRIGGER tf_anchor_property
AFTER INSERT OR DELETE ON anchor_property
FOR EACH ROW EXECUTE FUNCTION tf_anchor_property();
