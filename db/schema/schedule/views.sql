CREATE OR REPLACE FUNCTION fn_calculate_interval_date(
    start_date timestamp, end_date timestamp, formula text)
    RETURNS date
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
BEGIN
    IF POSITION('%' in formula) <> 0 THEN
        IF start_date IS NULL OR end_date IS NULL THEN
            RETURN NULL;
        END IF;
        IF POSITION('-' in formula) = 1 THEN
            RETURN end_date - ((end_date - start_date) * RIGHT(LEFT(formula, POSITION('%' in formula)-1),-1)::int / 100);
        END IF;
        RETURN start_date + ((end_date - start_date) * LEFT(formula, POSITION('%' in formula)-1)::int / 100);
    END IF;
    IF POSITION(' ' in formula) = 0 THEN
        formula = formula || ' days';
    END IF;
    IF POSITION('-' in formula) = 1 THEN
        RETURN end_date - RIGHT(formula,-1)::interval;
    END IF;
    RETURN start_date + formula::interval;
END;$$;

CREATE OR REPLACE FUNCTION fn_calculate_notification_date(
    start_date timestamp, end_date timestamp, formula text)
    RETURNS timestamp
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
BEGIN
    IF POSITION('%' in formula) <> 0 THEN
        IF start_date IS NULL OR end_date IS NULL THEN
            RETURN NULL;
        END IF;
        RETURN current_date - ((end_date - start_date) * LEFT(formula, POSITION('%' in formula)-1)::int / 100);
    END IF;
    IF POSITION(' ' in formula) = 0 THEN
        formula = formula || ' days';
    END IF;
    RETURN current_date - formula::interval;
END;$$;

-- meta
DROP VIEW IF EXISTS interaction_schedule CASCADE;
CREATE OR REPLACE VIEW interaction_schedule AS
SELECT interaction_schedule_cache.*, interaction_link_path.interaction_link_path, interaction_link_path.interaction_link_path_ordinals, 
interaction_link.root_interaction_id as root_interaction_id, anchor.created as anchor_created, interaction_link_interval.id as interaction_link_interval_id
FROM interaction_schedule_cache
join anchor on interaction_schedule_cache.anchor_id = anchor.id
join "interaction_link" on interaction_schedule_cache.interaction_link_id = interaction_link.id
join "interaction_link_interval" 
on interaction_schedule_cache.interaction_link_id = interaction_link_interval.interaction_link_id
and interaction_schedule_cache.interval_id = interaction_link_interval.interval_id
join interaction_link_path 
on anchor.interaction_id = interaction_link_path.anchor_interaction_id
and interaction_schedule_cache.interaction_link_id = interaction_link_path.interaction_link_id
;
    
DROP VIEW IF EXISTS interaction_schedule_readable CASCADE;
CREATE OR REPLACE VIEW interaction_schedule_readable AS
    SELECT anchor.date as anchor_date, interaction.name as interaction_name, interval.name as interval_name, interaction_schedule.* 
    FROM interaction_schedule
    JOIN anchor
    ON anchor.id = interaction_schedule.anchor_id
    JOIN interaction
    ON interaction.id = interaction_schedule.interaction_id
    JOIN interval
    ON interval.id = interaction_schedule.interval_id
;

-- use the set of the parent of the interaction node as for the "set_name"
-- combine multiple anchors with the same date and interval, don't group by anchor_id, group by anchor date and have an array of anchor_ids
DROP VIEW IF EXISTS interval_schedule_readable CASCADE;
CREATE OR REPLACE VIEW interval_schedule_readable AS
SELECT 
interaction_schedule_readable.subject_id, 
interaction_schedule_readable.anchor_id, 
interaction_schedule_readable.anchor_date, 
jsonb_object_agg(
    anchor_property.type, 
    anchor_property.value
) FILTER (WHERE anchor_property.type IS NOT NULL) AS anchor_properties,
anchor.external_id AS anchor_external_id,
interaction_schedule_readable.root_interaction_id, 
interaction.name AS root_name,
interaction_schedule_readable.interval_name, 
interaction_schedule_readable.start_date, 
interaction_schedule_readable.end_date, 
BOOL_AND(interaction_schedule_readable.fulfilled) AS fulfilled
FROM interaction_schedule_readable
JOIN interaction
ON interaction.id = interaction_schedule_readable.root_interaction_id
JOIN anchor
ON anchor.id = interaction_schedule_readable.anchor_id
LEFT JOIN (
    SELECT
        anchor_id, 
        type, 
        jsonb_agg(value) FILTER (WHERE value IS DISTINCT FROM 'unknown') AS value
    FROM anchor_property
    GROUP BY anchor_id, type
) AS anchor_property
ON anchor.id = anchor_property.anchor_id
WHERE anchor.active
AND interaction_schedule_readable.active
GROUP BY 
interaction_schedule_readable.subject_id, 
interaction_schedule_readable.anchor_id, 
interaction_schedule_readable.anchor_date, 
anchor.external_id, 
interaction_schedule_readable.root_interaction_id, 
interaction.name, 
interaction_schedule_readable.interval_name, 
interaction_schedule_readable.start_date, 
interaction_schedule_readable.end_date;

DROP VIEW IF EXISTS anchor_interaction_active CASCADE;
CREATE OR REPLACE VIEW anchor_interaction_active AS
    SELECT DISTINCT anchor.subject_id, anchor.id as anchor_id, 
    anchor.interaction_id as anchor_interaction_id, anchor_interaction.name as anchor_interaction_name,
    anchor.date as anchor_date,
    interaction_schedule_readable.root_interaction_id, root_interaction.name as root_interaction_name,
    anchor.properties as anchor_properties, anchor.self_reported_properties as anchor_self_reported_properties
    FROM anchor
    JOIN interaction as anchor_interaction
    ON anchor_interaction.id = anchor.interaction_id
    LEFT JOIN interaction_schedule_readable
    ON anchor.id = interaction_schedule_readable.anchor_id
    AND interaction_schedule_readable.active
    LEFT JOIN interaction as root_interaction
    ON root_interaction.id = interaction_schedule_readable.root_interaction_id
    WHERE anchor.active
;

DROP VIEW IF EXISTS anchor_interaction_active CASCADE;
CREATE OR REPLACE VIEW anchor_interaction_active AS
    SELECT DISTINCT anchor.subject_id, anchor.id as anchor_id, 
    anchor.interaction_id as anchor_interaction_id, anchor_interaction.name as anchor_interaction_name,
    anchor.date as anchor_date,
    interaction_schedule_readable.root_interaction_id, root_interaction.name as root_interaction_name,
    anchor.properties as anchor_properties, anchor.self_reported_properties as anchor_self_reported_properties
    FROM anchor
    JOIN interaction as anchor_interaction
    ON anchor_interaction.id = anchor.interaction_id
    LEFT JOIN interaction_schedule_readable
    ON anchor.id = interaction_schedule_readable.anchor_id
    AND interaction_schedule_readable.active
    LEFT JOIN interaction as root_interaction
    ON root_interaction.id = interaction_schedule_readable.root_interaction_id
    WHERE anchor.active
;

DROP VIEW IF EXISTS interaction_communication CASCADE;
CREATE OR REPLACE VIEW interaction_communication AS
SELECT 
    interaction_schedule_cache.subject_id, 
    communication_cache.anchor_id, 
    communication_cache.interval_id,
    interaction_schedule_cache.interaction_id, 
    interaction_link.root_interaction_id, 
    interaction_schedule_cache.instance, 
    communication_cache.communication_id as communication_id, 
    communication.properties as communication_properties,
    communication_cache.frequency_window AS start_date,
    communication_cache.send_date AS end_date,
    communication_cache.message_iter,
    interaction_schedule_cache.fulfilled
FROM communication_cache
JOIN communication
    ON communication_cache.communication_id = communication.id
JOIN interaction_schedule_cache
    ON communication_cache.anchor_id = interaction_schedule_cache.anchor_id
    AND communication_cache.interval_id = interaction_schedule_cache.interval_id
    AND communication.communication_set_id = interaction_schedule_cache.communication_set_id
    AND interaction_schedule_cache.active
join interaction_link on interaction_schedule_cache.interaction_link_id = interaction_link.id
;
    
DROP VIEW IF EXISTS interaction_message_readable CASCADE;
DROP VIEW IF EXISTS interaction_communication_readable CASCADE;
CREATE OR REPLACE VIEW interaction_communication_readable AS
SELECT
    anchor.date AS anchor_date,
    interaction.name AS interaction_name,
    interval.name AS interval_name,
    interaction_communication.communication_properties->>'medium' AS medium,
    interaction_communication.communication_properties->>'type' AS "type",
    interaction_communication.communication_properties->'message' AS "message",
    interaction_communication.start_date,
    interaction_communication.end_date,
    interaction_communication.anchor_id,
    interaction_communication.subject_id,
    interaction_communication.communication_properties->>'target' AS "target",
    interaction_communication.communication_properties->>'priority' AS "priority",
    interaction_communication.message_iter,
    interaction_communication.fulfilled
FROM interaction_communication
JOIN anchor ON interaction_communication.anchor_id = anchor.id
JOIN interval ON interaction_communication.interval_id = interval.id
JOIN interaction ON interaction_communication.interaction_id = interaction.id
ORDER BY interaction_communication.end_date, 
    fn_priority_value(interaction_communication.communication_properties->>'priority') DESC
;
    
-- @todo currently this only includes contact for targets who are the subject, we need to later support targets who have a relationship with the target
-- @todo timezones could be weird with this... unless we have local servers for timezones where the contact window might overlap with the invoked send time...
-- @note contact_log.sent contains tz information, we need to discard that when doing comparisons since we don't know or care about the target tz when calculating send time, so we just want to treat it like it's all utc for simplicity
DROP VIEW IF EXISTS possible_contact CASCADE;
CREATE OR REPLACE VIEW possible_contact AS 
SELECT
    NOT EXISTS (SELECT 1 FROM contact_log
        JOIN contact_log_interaction ON contact_log.id = contact_log_interaction.contact_log_id
        WHERE contact_log.subject_id = interaction_schedule_cache.subject_id
            AND contact_log.target_id IS NOT DISTINCT FROM (communication.properties->>'target')::bigint
            AND contact_log.medium = communication.properties->>'medium'
            AND ((communication.properties->>'type' = 'notification' 
                    AND contact_log.priority_value >= fn_priority_value(communication.properties->>'priority')
                    AND contact_log.communication_type = communication.properties->>'type'
                    AND DATE(contact_log.sent) BETWEEN communication_cache.frequency_window AND communication_cache.send_date
                    AND NOT contact_log.error)
                OR (communication.properties->>'type' = 'direct' 
                    AND DATE(contact_log.sent) = communication_cache.send_date
                    AND contact_log_interaction.interaction_id = interaction_schedule_cache.interaction_id
                    AND contact_log_interaction.communication_id = communication_cache.communication_id
                    AND contact_log_interaction.message_iter = communication_cache.message_iter
           ))
    ) AS "send",
    interaction_schedule_cache.subject_id,
    communication_cache.anchor_id,
    interaction_schedule_cache.interaction_id,
    interaction_schedule_cache.instance,
    communication_cache.interval_id,
    interval.name AS interval_name,
    communication_cache.communication_id,
    communication.properties->>'target' AS "target",
    communication.properties->>'priority' AS "priority",
    communication.properties->>'type' AS "type",
    communication.properties->>'medium' AS medium,
    communication.properties AS communication_properties,
    CASE WHEN interaction_schedule_cache.start_date IS NULL THEN NULL 
        ELSE fn_calculate_interval_date(
            interaction_schedule_cache.effective_start_date,
            interaction_schedule_cache.end_date,
            communication.start) 
    END as start_date,
    CASE WHEN interaction_schedule_cache.end_date IS NULL THEN NULL 
        ELSE fn_calculate_interval_date(
            interaction_schedule_cache.effective_start_date,
            interaction_schedule_cache.end_date,
            communication.end)
    END as end_date,
    communication_cache.send_date,
    communication_cache.send_time,
    communication_cache.message_iter
FROM communication_cache
JOIN communication
    ON communication_cache.communication_id = communication.id
JOIN interval ON communication_cache.interval_id = interval.id
JOIN interaction_schedule_cache
    ON communication_cache.anchor_id = interaction_schedule_cache.anchor_id
    AND communication_cache.interval_id = interaction_schedule_cache.interval_id
    AND communication.communication_set_id = interaction_schedule_cache.communication_set_id
WHERE interaction_schedule_cache.active = TRUE
    AND NOT interaction_schedule_cache.fulfilled
    AND communication_cache.send_date = CURRENT_DATE
ORDER BY subject_id, send_date, fn_priority_value(communication.properties->>'priority') DESC, send_time
;

DROP VIEW IF EXISTS possible_contact_readable CASCADE;
CREATE OR REPLACE VIEW possible_contact_readable AS
SELECT DISTINCT
    possible_contact.send,
    possible_contact.subject_id,
    possible_contact.anchor_id,
    anchor.date AS anchor_date,
    interaction.name AS interaction_name,
    possible_contact.interval_name, 
    possible_contact.communication_id,
    possible_contact.target,
    possible_contact.priority,
    fn_priority_value(possible_contact.priority) AS priority_value,
    possible_contact.medium,
    possible_contact.type,
    communication_properties,
    possible_contact.start_date,
    possible_contact.end_date,
    possible_contact.send_date,
    possible_contact.send_time,
    communication_properties->>'count' AS count_limit,
    communication_properties->>'notification_minimum_frequency' AS notification_minimum_frequency
FROM possible_contact
JOIN anchor ON anchor.id = possible_contact.anchor_id
JOIN interaction ON interaction.id = possible_contact.interaction_id
ORDER BY anchor.date DESC, fn_priority_value(possible_contact.priority) ASC
;

DROP VIEW IF EXISTS contact_next CASCADE;
CREATE OR REPLACE VIEW contact_next AS 
SELECT
    subject_id,
    anchor_id,
    interaction_id,
    instance,
    interval_id,
    interval_name,
    communication_id,
    "target",
    fn_priority_value(priority) AS "priority_value",
    "type",
    medium,
    communication_properties,
    start_date,
    end_date,
    send_date,
    send_time,
    message_iter,
    row_number() OVER (
        PARTITION BY possible_contact.subject_id,
            (possible_contact.target)::bigint,
            possible_contact.medium,
            possible_contact.type
        ORDER BY possible_contact.send_date, 
            possible_contact.priority,
            possible_contact.send_time
    ) AS subject_message_num
FROM possible_contact
WHERE send
;

DROP VIEW IF EXISTS contact_log_readable CASCADE;
CREATE OR REPLACE VIEW contact_log_readable AS
SELECT
    contact_log.subject_id,
    contact_log.medium,
    contact_log.sent,
    contact_log.timezone,
    ARRAY_AGG(interaction.name) AS interaction_name,
    ARRAY_AGG(interaction.id) AS interaction_list,
    contact_log.error,
    contact_log.error_message
FROM contact_log
JOIN contact_log_interaction ON contact_log.id = contact_log_interaction.contact_log_id
JOIN interaction ON contact_log_interaction.interaction_id = interaction.id
GROUP BY contact_log.subject_id, contact_log.medium, 
    contact_log.sent, contact_log.timezone, 
    contact_log.error, contact_log.error_message
;

--@NOTE not sure where this is used, if you come by this and know add it. NOT generating SCI, see interval_schedule_readable
DROP VIEW IF EXISTS interval_compliance CASCADE;
CREATE OR REPLACE VIEW interval_compliance AS
    select interaction_schedule.subject_id, interaction_schedule.anchor_id, interaction_schedule.interval_id, 
    interaction_schedule.start_date, interaction_schedule.end_date, 
    bool_and(interaction_schedule.fulfilled) as fulfilled, COUNT(*) AS total_interactions, COUNT(CASE WHEN fulfilled = true THEN 1 END) AS fulfilled_interactions
    from interaction_schedule
    where interaction_schedule.active
    group by interaction_schedule.subject_id, interaction_schedule.anchor_id, interaction_schedule.interval_id, interaction_schedule.start_date, interaction_schedule.end_date
;

DROP VIEW IF EXISTS interval_communication_active_readable CASCADE;
CREATE OR REPLACE VIEW interval_communication_active_readable AS
SELECT DISTINCT interaction_communication.subject_id,
    interaction_communication.anchor_id,
    anchor.date AS anchor_date,
    anchor.properties AS anchor_properties,
    string_agg(interaction.name, ', '::text ORDER BY interaction.name ASC) AS interaction_name,
    interval.name AS interval_name,
    interaction_communication.start_date,
    interaction_communication.end_date,
    interaction_communication.fulfilled,
    interaction_communication.communication_properties ->> 'priority'::text AS priority,
    fn_priority_value(interaction_communication.communication_properties ->> 'priority'::text) AS priority_value,
    interaction_communication.communication_properties ->> 'message'::text AS message
FROM interaction_communication
JOIN anchor ON anchor.id = interaction_communication.anchor_id
JOIN interaction ON interaction.id = interaction_communication.interaction_id
JOIN interval ON interaction_communication.interval_id = interval.id
GROUP BY 
    interaction_communication.subject_id, 
    interaction_communication.anchor_id, 
    anchor.date, 
    anchor.properties, 
    interval.name, 
    interaction_communication.start_date, 
    interaction_communication.end_date, 
    interaction_communication.fulfilled, 
    (interaction_communication.communication_properties ->> 'priority'::text), 
    (fn_priority_value(interaction_communication.communication_properties ->> 'priority'::text)),
    (interaction_communication.communication_properties ->> 'message'::text)
ORDER BY
    anchor.date DESC, 
    interaction_communication.start_date, 
    (fn_priority_value(interaction_communication.communication_properties ->> 'priority'::text));

DROP VIEW IF EXISTS interaction_condition_value CASCADE;
CREATE OR REPLACE VIEW interaction_condition_value AS
SELECT DISTINCT interaction_link_parent.anchor_interaction_id, condition.conditional, condition.key, condition.value
FROM interaction_link_parent
JOIN interaction_link_condition
ON interaction_link_condition.interaction_link_id = interaction_link_parent.parent_interaction_link_id
JOIN condition
ON condition.id = interaction_link_condition.condition_id
WHERE condition.key is not NULL
AND condition.value is not NULL;


DROP VIEW IF EXISTS interaction_condition_and_anchor_property_value CASCADE;
CREATE OR REPLACE VIEW interaction_condition_and_anchor_property_value AS
(select distinct interaction_condition_value.anchor_interaction_id, interaction_condition_value.conditional, interaction_condition_value.key as condition_key, interaction_condition_value.value as condition_value, anchor_property.type as property_key, anchor_property.value as property_value 
from interaction_condition_value 
LEFT JOIN anchor
ON anchor.interaction_id = interaction_condition_value.anchor_interaction_id
LEFT JOIN anchor_property
ON anchor_property.type = interaction_condition_value.key
AND anchor_property.value = interaction_condition_value.value
AND anchor.id = anchor_property.anchor_id
order by interaction_condition_value.key, interaction_condition_value.value
)
UNION DISTINCT
(select distinct interaction_condition_value.anchor_interaction_id, interaction_condition_value.conditional, interaction_condition_value.key as condition_key, interaction_condition_value.value as condition_value, anchor_property.type as property_key, anchor_property.value as property_value 
from anchor_property
JOIN anchor
ON anchor.id = anchor_property.anchor_id
LEFT JOIN interaction_condition_value 
ON anchor_property.type = interaction_condition_value.key
AND anchor_property.value = interaction_condition_value.value
AND anchor.interaction_id = interaction_condition_value.anchor_interaction_id
order by interaction_condition_value.key, interaction_condition_value.value
);
