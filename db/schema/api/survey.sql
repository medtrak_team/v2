
CREATE OR REPLACE FUNCTION update_anchor_self_reported_properties(
    arg_subject_id bigint,
    arg_anchor_id bigint,
    arg_self_reported_properties jsonb
) RETURNS void AS $$
BEGIN
    UPDATE anchor
    SET self_reported_properties = COALESCE(self_reported_properties, '{}'::jsonb) || arg_self_reported_properties
    WHERE id = arg_anchor_id
    AND subject_id = arg_subject_id;
END;
$$ LANGUAGE plpgsql;
