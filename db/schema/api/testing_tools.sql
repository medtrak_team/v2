DROP FUNCTION IF EXISTS util_clear_subject_data;
CREATE OR REPLACE FUNCTION util_clear_subject_data(arg_subject_id BIGINT)
RETURNS VOID AS $$
BEGIN
   -- Delete from contact_log
   DELETE FROM contact_log 
   WHERE subject_id = arg_subject_id;

   -- Delete from subject_data
   DELETE FROM subject_data 
   WHERE subject_id = arg_subject_id;

   -- Set self_reported_properties to NULL in build.anchor
   UPDATE build.anchor
   SET self_reported_properties = NULL
   WHERE subject_id = arg_subject_id;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS util_push_subject_dates;
CREATE OR REPLACE FUNCTION util_push_subject_dates(arg_subject_id BIGINT, arg_days INT)
RETURNS VOID AS $$
BEGIN
    -- Adjust anchor.date
    UPDATE anchor
    SET "date" = "date" + arg_days
	WHERE "subject_id" = arg_subject_id;

    -- Adjust subject_data.completed
    UPDATE subject_data
    SET "completed" = "completed" + (arg_days || ' days')::INTERVAL
	WHERE "subject_id" = arg_subject_id;

    -- Adjust contact_log.sent
    UPDATE contact_log
    SET "sent" = "sent" + (arg_days || ' days')::INTERVAL
	WHERE "subject_id" = arg_subject_id;
END;
$$ LANGUAGE plpgsql;
