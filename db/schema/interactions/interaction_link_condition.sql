CALL dbinfo.fn_create_or_update_table('interaction_link_condition','{
    interaction_link_id BIGINT NOT NULL,
	condition_id BIGINT NOT NULL
    }'
);

CREATE INDEX IF NOT EXISTS idx_interaction_link_condition_interaction_link_id ON "interaction_link_condition" ("interaction_link_id");
CREATE INDEX IF NOT EXISTS idx_interaction_link_condition_condition_id ON "interaction_link_condition" ("condition_id");

-- meta
CALL dbinfo.fn_alter_table_add_constraint('interaction_link_condition','interaction_link_condition_pkey','
    PRIMARY KEY (interaction_link_id,condition_id)'
);
CALL dbinfo.fn_alter_table_add_constraint('interaction_link_condition','fk_interaction_link_condition_interaction_link','
    FOREIGN KEY (interaction_link_id) REFERENCES interaction_link (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE'
);
CALL dbinfo.fn_alter_table_add_constraint('interaction_link_condition','fk_interaction_link_condition_condition','
    FOREIGN KEY (condition_id) REFERENCES condition (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE'
);

CREATE OR REPLACE FUNCTION fn_populate_interaction_link_conditions(arg_interaction_link_id BIGINT, arg_condition_id bigint)
RETURNS VOID LANGUAGE plpgsql AS $$
DECLARE
    this_condition_id BIGINT;
BEGIN
    -- Insert all conditions where root_condition_id matches the condition_id from interaction_link
    INSERT INTO interaction_link_condition (interaction_link_id, condition_id)
    SELECT arg_interaction_link_id, id
    FROM condition
    WHERE root_condition_id = arg_condition_id;
	
	-- Recursively call this function for each condition condition
	FOR this_condition_id IN 
        SELECT value
        FROM condition
        WHERE root_condition_id = arg_condition_id 
        AND conditional = 'condition'
        AND NOT EXISTS (
          SELECT 1 FROM interaction_link_condition 
          WHERE interaction_link_id = arg_interaction_link_id 
          AND condition_id = condition.value::bigint
        )
    LOOP
        PERFORM fn_populate_interaction_link_conditions(arg_interaction_link_id, this_condition_id);
    END LOOP;
END;
$$;

CREATE OR REPLACE FUNCTION tf_interaction_link_populate_interaction_link_condition()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    -- Handle DELETE: remove all records for the deleted interaction_link_id
    IF TG_OP = 'DELETE' OR (TG_OP = 'UPDATE' AND NEW.condition_id IS DISTINCT FROM OLD.condition_id) THEN
        DELETE FROM interaction_link_condition WHERE interaction_link_id = OLD.id;
    END IF;

    -- Handle INSERT and UPDATE (only if condition_id did not change)
    IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND NEW.condition_id IS DISTINCT FROM OLD.condition_id) THEN
        PERFORM fn_populate_interaction_link_conditions(NEW.id, NEW.condition_id);
    END IF;
    
	RETURN NEW;
END;
$$;

-- Create the trigger for INSERT, UPDATE, and DELETE
DROP TRIGGER IF EXISTS tf_interaction_link_populate_interaction_link_condition ON interaction_link;
CREATE TRIGGER tf_interaction_link_populate_interaction_link_condition
AFTER INSERT OR UPDATE OR DELETE ON interaction_link
FOR EACH ROW EXECUTE FUNCTION tf_interaction_link_populate_interaction_link_condition();

CREATE OR REPLACE FUNCTION tf_condtion_populate_interaction_link_condition()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    -- Insert the new condition_id for all interaction_links with the same root_condition_id
    INSERT INTO interaction_link_condition (interaction_link_id, condition_id)
    SELECT interaction_link_id, NEW.id
    FROM interaction_link_condition
    WHERE condition_id = NEW.root_condition_id;

    RETURN NEW;
END;
$$;

-- Create the trigger on the condition table
DROP TRIGGER IF EXISTS tf_condtion_populate_interaction_link_condition ON condition;
CREATE TRIGGER tf_condtion_populate_interaction_link_condition
AFTER INSERT ON condition
FOR EACH ROW EXECUTE FUNCTION tf_condtion_populate_interaction_link_condition();
