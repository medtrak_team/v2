CALL dbinfo.fn_create_or_update_table('interaction_link','{
    id,
    root_interaction_id bigint NOT NULL,
    parent_interaction_id bigint NOT NULL,
    ordinal smallint NOT NULL,
    child_interaction_id bigint NOT NULL,
    condition_id bigint,
    modified timestamp
    }');
DROP TRIGGER IF EXISTS tf_log_change ON interaction_link;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON interaction_link FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON interaction_link;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON interaction_link FOR EACH ROW EXECUTE FUNCTION tf_set_modified();
COMMENT ON TABLE interaction_link IS 'An interaction_link is a single node in an interaction set defining the relationships between interactions. 
An interaction can be a child of any number of other interactions and may have different conditions or communication rules depending on that specific circumstance.';
COMMENT ON COLUMN interaction_link.root_interaction_id IS 'The id of the root interaction for a set of nodes, interactions can be organized into sets for the purposes of useability and efficient querying';
COMMENT ON COLUMN interaction_link.parent_interaction_id IS 'The id of the parent interaction, interaction nodes can have deep structures, this is the immediate parent interaction in that structure';
COMMENT ON COLUMN interaction_link.ordinal IS 'Defines the order of the nodes within a given parent';
COMMENT ON COLUMN interaction_link.child_interaction_id IS 'The id of the interaction that this node contains';
COMMENT ON COLUMN interaction_link.condition_id IS 'The id of the condition that must be fulfilled for this node to be active. Conditions accumulate through parents.';

CREATE INDEX IF NOT EXISTS idx_interaction_link_root_interaction_id ON "interaction_link" ("root_interaction_id");
CREATE INDEX IF NOT EXISTS idx_interaction_link_parent_interaction_id ON "interaction_link" ("parent_interaction_id");
CREATE INDEX IF NOT EXISTS idx_interaction_link_child_interaction_id ON "interaction_link" ("child_interaction_id");
CREATE INDEX IF NOT EXISTS idx_interaction_link_condition_id ON "interaction_link" ("condition_id");

CREATE OR REPLACE FUNCTION get_tree_root_interactions(p_starting_root bigint)
RETURNS TABLE(root_interaction_id bigint, name text)
AS $$
BEGIN
  RETURN QUERY
    WITH RECURSIVE tree AS (
      -- Anchor: start from links with the given starting root.
      SELECT
        il.root_interaction_id,
        il.parent_interaction_id,
        il.child_interaction_id
      FROM build.interaction_link il
      WHERE il.root_interaction_id = p_starting_root
      
      UNION ALL
      
      -- Recursive part: follow the tree via parent-child links.
      SELECT
        il.root_interaction_id,
        il.parent_interaction_id,
        il.child_interaction_id
      FROM build.interaction_link il
      JOIN tree t
        ON il.parent_interaction_id = t.child_interaction_id
    )
    SELECT DISTINCT
      t.root_interaction_id,
      i.name
    FROM tree t
    JOIN build.interaction i
      ON i.id = t.root_interaction_id;
END;
$$ LANGUAGE plpgsql;

-- given an anchor_id, returns all child interactions, their roots, and the name of the root
CREATE OR REPLACE FUNCTION get_interaction_tree_details(p_anchor_id bigint)
RETURNS TABLE(interaction_id bigint, root_interaction_id bigint, name text)
AS $$
BEGIN
  RETURN QUERY
    SELECT 
      il.child_interaction_id AS interaction_id,
      tri.root_interaction_id,
      i.name
    FROM interaction_link il
    JOIN interaction i 
      ON i.id = il.root_interaction_id
    JOIN anchor a 
      ON a.id = p_anchor_id
    JOIN LATERAL build.get_tree_root_interactions(a.interaction_id) AS tri
      ON tri.root_interaction_id = il.root_interaction_id;
END;
$$ LANGUAGE plpgsql;

-- removes gaps in ordinality for parent 
-- repairs ordinal values to make them consecutive
CREATE OR REPLACE FUNCTION fix_ordinality_for_parent(
	parentid bigint)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
	 WITH ordered_data AS (
	  SELECT id,
	  ordinal,
	  ROW_NUMBER() OVER (ORDER BY ordinal) AS new_ordinal
	  FROM interaction_link where parent_interaction_id = parentid
	)
	UPDATE interaction_link AS t
	SET ordinal = od.new_ordinal
	FROM ordered_data AS od
	WHERE t.id = od.id;
	END;
$BODY$;

CREATE OR REPLACE FUNCTION update_ordinality_by_parent_on_delete()
RETURNS TRIGGER AS $$
BEGIN
  UPDATE interaction_link
  SET ordinal = ordinal - 1
  WHERE ordinal > OLD.ordinal and parent_interaction_id = OLD.parent_interaction_id  ;
  perform fix_ordinality_for_parent(OLD.parent_interaction_id);
  RETURN OLD;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tf_update_ordinality_on_node_delete ON interaction_link;
CREATE TRIGGER tf_update_ordinality_on_node_delete AFTER DELETE ON interaction_link FOR EACH ROW EXECUTE FUNCTION update_ordinality_by_parent_on_delete();

CALL dbinfo.fn_create_or_update_table('interaction_link_interval','{
    id,
    interaction_link_id bigint NOT NULL,
    interval_id bigint NOT NULL,
    condition_id bigint,
    communication_set_id bigint default null,    
    modified timestamp
    }');
DROP TRIGGER IF EXISTS tf_log_change ON interaction_link_interval;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON interaction_link_interval FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON interaction_link_interval;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON interaction_link_interval FOR EACH ROW EXECUTE FUNCTION tf_set_modified();
CREATE UNIQUE INDEX IF NOT EXISTS uidx_interaction_link_interval ON interaction_link_interval (interaction_link_id, interval_id);
COMMENT ON TABLE interaction_link_interval IS 'Join between interaction_link and interval representing the schedule for a single interaction in a specific interaction set context.
Each interval that an interaction is on can have specialized conditions and communication rules if needed.';

CREATE INDEX IF NOT EXISTS idx_interaction_link_interval_condition_id ON "interaction_link_interval" ("condition_id");
CREATE INDEX IF NOT EXISTS idx_interaction_link_interval_communication_set_id ON "interaction_link_interval" ("communication_set_id");
CREATE INDEX IF NOT EXISTS idx_interaction_link_interval_interval_id ON "interaction_link_interval" ("interval_id");

-- meta
CALL dbinfo.fn_alter_table_add_constraint('interaction_link','fk_interaction_link_child','
    FOREIGN KEY (child_interaction_id) REFERENCES interaction(id)
    ON DELETE CASCADE ON UPDATE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('interaction_link','fk_interaction_link_condition','
    FOREIGN KEY (condition_id) REFERENCES condition(id)
    ON DELETE SET NULL ON UPDATE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('interaction_link','fk_interaction_link_parent','
    FOREIGN KEY (parent_interaction_id) REFERENCES interaction(id)
    ON DELETE CASCADE ON UPDATE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('interaction_link','fk_interaction_link_set','
    FOREIGN KEY (root_interaction_id) REFERENCES interaction(id)
    ON DELETE CASCADE ON UPDATE CASCADE');

CALL dbinfo.fn_alter_table_add_constraint('interaction_link_interval','fk_interaction_link_interval_condition','
    FOREIGN KEY (condition_id) REFERENCES condition(id)
    ON DELETE SET NULL ON UPDATE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('interaction_link_interval','fk_interaction_link_interval_interaction_link','
    FOREIGN KEY (interaction_link_id) REFERENCES interaction_link(id)
    ON DELETE CASCADE ON UPDATE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('interaction_link_interval','fk_interaction_link_interval_interval','
    FOREIGN KEY (interval_id) REFERENCES "interval"(id)
    ON DELETE CASCADE ON UPDATE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('interaction_link_interval','fk_interaction_link_interval_communication_set','
    FOREIGN KEY (communication_set_id) REFERENCES communication_set(id)
    ON DELETE SET NULL ON UPDATE CASCADE');

CREATE OR REPLACE FUNCTION tf_interaction_link()
RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
    v_parent_interaction_link_path RECORD;
BEGIN
    /***************************************************************************
    interaction_link_path
    ***************************************************************************/

    -- Handle DELETE or UPDATE
    IF TG_OP = 'DELETE' OR (TG_OP = 'UPDATE' AND (NEW.parent_interaction_id IS DISTINCT FROM OLD.parent_interaction_id OR NEW.child_interaction_id IS DISTINCT FROM OLD.child_interaction_id OR NEW.ordinal IS DISTINCT FROM OLD.ordinal)) THEN
        
        -- Find the paths where the interaction_link.id is part of the path, and only delete those where the anchor matches for efficiency
        DELETE FROM interaction_link_path
        WHERE OLD.id = ANY(interaction_link_path)
        AND anchor_interaction_id IN (
            SELECT anchor_interaction_id
            FROM interaction_link_path
            WHERE OLD.id = interaction_link_id
        );
    END IF;

    -- Handle INSERT or UPDATE
    IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND (NEW.parent_interaction_id IS DISTINCT FROM OLD.parent_interaction_id OR NEW.child_interaction_id IS DISTINCT FROM OLD.child_interaction_id OR NEW.ordinal IS DISTINCT FROM OLD.ordinal)) THEN
        
        -- Get all existing paths for the parent interaction_id of the new/updated interaction_link
        FOR v_parent_interaction_link_path IN 
            SELECT interaction_link_path.* FROM interaction_link_path 
            JOIN interaction_link
            ON interaction_link.id = interaction_link_path.interaction_link_id
            WHERE interaction_link.child_interaction_id = NEW.parent_interaction_id
        LOOP
            -- Call the recursive function to build the path for the new interaction_link
            PERFORM fn_build_interaction_link_path_for_interaction_link(
                NEW, 
                v_parent_interaction_link_path.anchor_interaction_id, 
                v_parent_interaction_link_path.interaction_link_path, 
                v_parent_interaction_link_path.interaction_link_path_ordinals
            );
        END LOOP;

        IF EXISTS (
            SELECT 1
            FROM anchor
            WHERE interaction_id = NEW.parent_interaction_id
        ) THEN
            -- Call the recursive function directly for the anchor interaction
            PERFORM fn_build_interaction_link_path_for_interaction_link(
                NEW, 
                NEW.parent_interaction_id
            );
        END IF;
    END IF;

    /***************************************************************************
    interaction_schedule_cache
    ***************************************************************************/
    -- Insert or update records in interaction_schedule_cache based on the interaction_link_id
	IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND (NEW.parent_interaction_id IS DISTINCT FROM OLD.parent_interaction_id OR NEW.child_interaction_id IS DISTINCT FROM OLD.child_interaction_id)) THEN
        -- @todo under these condtions we need to generate the cache including conditions and fulfilled status, but for an ordinal change we don't. let's leave this for a moment though and come back to it
        INSERT INTO interaction_schedule_recache (join_clause,where_clause) VALUES ('
            JOIN interaction_link_parent
    	    ON interaction_schedule_data.interaction_link_id = interaction_link_parent.interaction_link_id
        ','
            AND interaction_link_parent.parent_interaction_link_id = ' || NEW.id
        ) ON CONFLICT DO NOTHING;
	END IF;

    RETURN NULL;
END;
$$;
DROP TRIGGER IF EXISTS tf_interaction_link ON interaction_link;
CREATE TRIGGER tf_interaction_link
AFTER INSERT OR UPDATE OR DELETE ON interaction_link
FOR EACH ROW EXECUTE FUNCTION tf_interaction_link();

CREATE OR REPLACE FUNCTION tf_interaction_link_interval()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    -- Insert or update records in interaction_schedule_cache based on interaction_link_id
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        INSERT INTO interaction_schedule_recache (join_clause,where_clause) VALUES ('','
            AND interaction_schedule_data.interaction_link_id = ' || NEW.interaction_link_id
        ) ON CONFLICT DO NOTHING;
    END IF;

    RETURN NULL;
END;
$$;
DROP TRIGGER IF EXISTS tf_interaction_link_interval ON interaction_link_interval;
CREATE TRIGGER tf_interaction_link_interval
AFTER INSERT OR UPDATE OR DELETE ON interaction_link_interval
FOR EACH ROW EXECUTE FUNCTION tf_interaction_link_interval();

DROP FUNCTION IF EXISTS json_import_interaction_link; 
CREATE OR REPLACE FUNCTION json_import_interaction_link(arg_node_data JSONB, arg_new_ids JSONB, arg_master_data JSONB)
RETURNS JSONB AS $$
DECLARE
    v_new_node_id BIGINT;
    v_condition_id BIGINT;
    v_parent_interaction_id BIGINT;
    v_child_interaction_id BIGINT;
    v_root_interaction_id BIGINT;
    v_node_id BIGINT := (arg_node_data->>'id')::BIGINT;
    v_interval_id BIGINT;
    v_current_intervals JSONB;
BEGIN
    -- Process parent_interaction_id
    IF (arg_node_data->>'parent_interaction_id')::BIGINT < 0 THEN
        IF arg_new_ids->'interaction'->>(arg_node_data->>'parent_interaction_id') IS NULL THEN
            -- Call json_import_interaction for parent if not already processed
            arg_new_ids := json_import_interaction(arg_master_data->'interaction'->(arg_node_data->>'parent_interaction_id'), arg_new_ids, arg_master_data);
        END IF;
        v_parent_interaction_id := (arg_new_ids->'interaction'->>(arg_node_data->>'parent_interaction_id'))::BIGINT;
    ELSE
        v_parent_interaction_id := (arg_node_data->>'parent_interaction_id')::BIGINT;
    END IF;

    -- Process child_interaction_id
    IF (arg_node_data->>'child_interaction_id')::BIGINT < 0 THEN
        IF arg_new_ids->'interaction'->>(arg_node_data->>'child_interaction_id') IS NULL THEN
            -- Call json_import_interaction for child if not already processed
            arg_new_ids := json_import_interaction(arg_master_data->'interaction'->(arg_node_data->>'child_interaction_id'), arg_new_ids, arg_master_data);
        END IF;
        v_child_interaction_id := (arg_new_ids->'interaction'->>(arg_node_data->>'child_interaction_id'))::BIGINT;
    ELSE
        v_child_interaction_id := (arg_node_data->>'child_interaction_id')::BIGINT;
    END IF;

    -- Process root_interaction_id
    IF (arg_node_data->>'root_interaction_id')::BIGINT < 0 THEN
        IF arg_new_ids->'interaction'->>(arg_node_data->>'root_interaction_id') IS NULL THEN
            -- Call json_import_interaction for root if not already processed
            arg_new_ids := json_import_interaction(arg_master_data->'interaction'->(arg_node_data->>'root_interaction_id'), arg_new_ids, arg_master_data);
        END IF;
        v_root_interaction_id := (arg_new_ids->'interaction'->>(arg_node_data->>'root_interaction_id'))::BIGINT;
    ELSE
        v_root_interaction_id := (arg_node_data->>'root_interaction_id')::BIGINT;
    END IF;

    -- Process condition_id
    IF (arg_node_data->>'condition_id')::BIGINT < 0 THEN
        v_condition_id := (arg_new_ids->'condition'->>(arg_node_data->>'condition_id'))::BIGINT;
    ELSE
        v_condition_id := (arg_node_data->>'condition_id')::BIGINT;
    END IF;

    -- Insert or update interaction node
    IF v_node_id < 0 THEN
        -- Insert a new interaction node
        INSERT INTO interaction_link (ordinal, condition_id, root_interaction_id, child_interaction_id, parent_interaction_id)
        VALUES (
            (arg_node_data->>'ordinal')::INT,
            v_condition_id,
            v_root_interaction_id,
            v_child_interaction_id,
            v_parent_interaction_id
        )
        RETURNING id INTO v_new_node_id;

        -- Update new_ids with the mapping from negative to new ID in interaction_link section
        arg_new_ids := jsonb_deep_set(arg_new_ids, ARRAY['interaction_link', (arg_node_data->>'id')], to_jsonb(v_new_node_id));
    ELSE
        -- Update the existing interaction node
        UPDATE interaction_link
        SET ordinal = (arg_node_data->>'ordinal')::INT,
            condition_id = v_condition_id,
            root_interaction_id = v_root_interaction_id,
            child_interaction_id = v_child_interaction_id,
            parent_interaction_id = v_parent_interaction_id
        WHERE id = v_node_id;

        -- Add the positive ID to new_ids, key being the same as the value
        arg_new_ids := jsonb_deep_set(arg_new_ids, ARRAY['interaction_link', (arg_node_data->>'id')], to_jsonb(v_node_id));

        v_new_node_id := v_node_id; -- Set new_node_id for further processing
    END IF;

    -- Loop through the intervals in the node and ensure they exist
    v_current_intervals := '[]'::JSONB;
    FOR v_interval_id IN SELECT jsonb_array_elements(arg_node_data->'interval_ids')
    LOOP
        -- Check if interval_id is negative and needs to be replaced by a value from new_ids
        IF v_interval_id < 0 THEN
            -- If interval_id is not already in new_ids, process the interval first
            IF arg_new_ids->'interval'->>(v_interval_id::TEXT) IS NULL THEN
                -- Call json_import_interval for the interval if not already processed
                arg_new_ids := json_import_interval(arg_master_data->'interval'->(v_interval_id::TEXT), arg_new_ids);
            END IF;

            -- Get the new interval_id from arg_new_ids
            v_interval_id := (arg_new_ids->'interval'->>(v_interval_id::TEXT))::BIGINT;
        END IF;
        v_current_intervals = v_current_intervals || (v_interval_id::TEXT)::JSONB;

        -- Insert if interval_id is missing
        IF NOT EXISTS (SELECT 1 FROM interaction_link_interval WHERE interaction_link_id = v_new_node_id AND interval_id = v_interval_id) THEN
            INSERT INTO interaction_link_interval (interaction_link_id, interval_id)
            VALUES (v_new_node_id, v_interval_id);
        END IF;
    END LOOP;
    
    -- Remove any intervals that are no longer present
    DELETE FROM interaction_link_interval
    WHERE interaction_link_id = v_new_node_id
    AND interval_id NOT IN (SELECT jsonb_array_elements_text(v_current_intervals)::BIGINT);

    RETURN arg_new_ids;
END;
$$ LANGUAGE plpgsql;
