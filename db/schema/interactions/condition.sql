-- functions
CREATE OR REPLACE FUNCTION fn_build_condition_sql(
    arg_condition record, interaction_schedule record, arg_interaction_link record)
    RETURNS text
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
    sql text = 'true';
    joined_as text = '';
    v_operator shared.enum_operator = arg_condition.operator::shared.enum_operator;
BEGIN
    -- TODO cache this in each condition for speed of query
    -- TODO "not" field

    IF arg_condition.conditional = 'and' THEN
        SELECT '(' || STRING_AGG(fn_build_condition_sql(child.*, interaction_schedule, arg_interaction_link),' AND ') || ')' INTO sql FROM condition as child
        WHERE child.parent_condition_id = arg_condition.id;
    ELSEIF arg_condition.conditional = 'or' THEN
        SELECT '(' || STRING_AGG(fn_build_condition_sql(child.*, interaction_schedule, arg_interaction_link),' OR ') || ')' INTO sql FROM condition as child
        WHERE child.parent_condition_id = arg_condition.id;
    ELSEIF arg_condition.conditional = 'condition' THEN
        SELECT '(' || fn_build_condition_set_sql(subcondition.*, interaction_schedule, arg_interaction_link) || ')' INTO sql FROM condition as subcondition
        WHERE subcondition.id = arg_condition.value::bigint;
    ELSEIF arg_condition.conditional = 'else' THEN
        -- if this is an interaction node interval or we're outside the parent nodes because we're already looking at an "else", then skip this condition
        IF arg_interaction_link IS NOT NULL OR arg_interaction_link.id != ANY(interaction_schedule.interaction_link_path) THEN
            SELECT STRING_AGG(' NOT ' || fn_build_condition_set_sql(condition.*,interaction_schedule, interaction_link.*),' AND ') INTO sql 
            FROM condition, interaction_link
            WHERE interaction_link.parent_interaction_id = arg_interaction_link.parent_interaction_id
            AND interaction_link.id != arg_interaction_link.id
            AND interaction_link.ordinal < arg_interaction_link.ordinal -- else only applies to earlier items in the list
            AND condition.id = interaction_link.condition_id;
        END IF;
    ELSE
        IF arg_condition."from" IS NOT NULL THEN
          joined_as = CASE WHEN arg_condition.as IS NOT NULL THEN arg_condition.as ELSE arg_condition."from" END;
          sql = format('%s.%L = %L',joined_as,arg_condition.key,arg_condition.value);
        ELSE
          joined_as = CASE WHEN arg_condition.as IS NOT NULL THEN arg_condition.as ELSE left(arg_condition.conditional::text, strpos(arg_condition.conditional::text, '_') - 1) END;
          IF arg_condition.conditional = 'anchor_property' THEN
              IF joined_as = 'anchor' AND interaction_schedule.instance ? arg_condition.key THEN
                IF interaction_schedule.instance->>arg_condition.key = arg_condition.value THEN
                  sql = 'TRUE';
                ELSE
                  sql = 'FALSE';
                END IF;
              ELSE
                sql = format('EXISTS (SELECT 1 FROM anchor_property WHERE anchor_property.anchor_id = %s.id AND anchor_property.type = %L AND anchor_property.value %s %L LIMIT 1)',joined_as,arg_condition.key,v_operator,arg_condition.value);
              END IF;
          ELSEIF arg_condition.conditional = 'data_value' THEN
              sql = format('EXISTS (SELECT 1 FROM subject_data_value WHERE subject_data_value.subject_data_id = %s.id AND subject_data_value.key = %L AND subject_data_value.value %s %L LIMIT 1)',joined_as,arg_condition.key,v_operator,arg_condition.value);
          ELSEIF arg_condition.conditional = 'data_instance' THEN
              sql = format('EXISTS (SELECT 1 FROM subject_data_instance WHERE subject_data_instance.subject_data_id = %s.id AND subject_data_instance.type = %L AND subject_data_instance.value %s %L LIMIT 1)',joined_as,arg_condition.key,v_operator,arg_condition.value);
          END IF;
        END IF;
    END IF;

    IF arg_condition."not" THEN
        RETURN 'NOT ' || sql;
    ELSE
        RETURN sql;
    END IF;
END;$$;

/*
    inciting_anchor_id is the anchor that triggered the interaction to be check for conditions
    when the condition is an anchor condition where the as is null or 'this' it will check that 
    condition against the inciting anchor rather than just any old anchor
*/
CREATE OR REPLACE FUNCTION fn_build_condition_set_sql(
    condition record, interaction_schedule record, arg_interaction_link record)
    RETURNS text
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
    from_record record;
    from_clause text = '';
    subject_clause text = '';
    subject_column text = '';
BEGIN
    FOR from_record IN
      SELECT DISTINCT CASE 
        WHEN condition_row."from" IS NOT NULL THEN condition_row."from"
        WHEN condition_row.conditional = 'anchor_property' THEN 'anchor'
        WHEN condition_row.conditional IN ('data_instance', 'data_value') THEN 'subject_data'
        ELSE condition_row.conditional::TEXT  -- Explicitly cast enum to TEXT
      END AS "from", condition_row."as" 
      FROM condition AS condition_row 
      WHERE "root_condition_id" = condition.id AND conditional IN ('anchor_property','data_instance','data_value')
    LOOP
      IF from_record."from" IN ('subject_data','anchor') THEN
        subject_column = 'subject_id';
      ELSE
        subject_column = 'patientid';
      END IF;
      
      IF from_record."as" IS NOT NULL THEN
        from_clause = from_clause || ', ' || from_record."from" || ' as ' || from_record."as";
        subject_clause = subject_clause || ' AND ' || from_record."as" || '.' || subject_column || ' = ' || interaction_schedule.subject_id;
        IF from_record."from" = 'anchor' THEN 
            subject_clause = subject_clause || ' AND ' || from_record."as" || '.active';
            IF from_record."as" = 'this' THEN
                subject_clause = subject_clause || ' AND ' || from_record."as" || '.id = ' || interaction_schedule.anchor_id;
            END IF;
        END IF;
      ELSE
        from_clause = from_clause || ', ' || from_record."from";
        subject_clause = subject_clause || ' AND ' || from_record."from" || '.' || subject_column || ' = ' || interaction_schedule.subject_id;
        IF from_record."from" = 'anchor' THEN
            subject_clause = subject_clause || ' AND ' || from_record."from" || '.active';
            subject_clause = subject_clause || ' AND ' || from_record."from" || '.id = ' || interaction_schedule.anchor_id;
        END IF;
      END IF;
    END LOOP;
    
    IF NOT from_clause = '' THEN
      -- remove preceding ', '  and ' AND ' from the two clauses respectively
      from_clause = right(from_clause,-2);
      subject_clause = right(subject_clause,-5);
      RETURN 'EXISTS( SELECT 1 FROM ' || from_clause || ' WHERE ' || subject_clause || ' AND ' || fn_build_condition_sql(condition.*, interaction_schedule, arg_interaction_link) || ' LIMIT 1 )';
    ELSE
      RETURN '(' || fn_build_condition_sql(condition.*, interaction_schedule, arg_interaction_link) || ')';
    END IF;
END;$$;

CREATE OR REPLACE FUNCTION fn_is_interaction_schedule_active(
    interaction_schedule record)
    RETURNS boolean
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
    condition_met boolean;
    condition_sql text = 'true';
BEGIN
    SELECT COALESCE(STRING_AGG(fn_build_condition_set_sql(condition.*,interaction_schedule, interaction_link.*),' AND '),'true') INTO condition_sql 
    FROM condition, interaction_link
    WHERE interaction_link.id = ANY(interaction_schedule.interaction_link_path)
    AND condition.id = interaction_link.condition_id;
    
    SELECT COALESCE(condition_sql || ' AND ' || STRING_AGG(fn_build_condition_set_sql(condition.*,interaction_schedule, interaction_link.*),' AND '),condition_sql) INTO condition_sql 
    FROM condition, interaction_link_interval, interaction_link
    WHERE interaction_link_interval.id = interaction_schedule.interaction_link_interval_id
    AND condition.id = interaction_link_interval.condition_id
    AND interaction_link.id = interaction_link_interval.interaction_link_id;
    
    IF condition_sql IS NULL THEN
        RETURN true;
    ELSE
        IF COALESCE(current_setting('debug_mode', true) = 'on', FALSE) THEN
            RAISE NOTICE 'anchor: %, link: %, instance: %, interval: %', interaction_schedule.anchor_id, interaction_schedule.interaction_link_id, interaction_schedule.instance, interaction_schedule.interval_id;
            RAISE NOTICE 'condition: %', condition_sql;
        END IF;
        
        EXECUTE 'SELECT (' || condition_sql || ')' INTO condition_met;
        --RAISE NOTICE 'condition met? %', condition_met;
            
        RETURN condition_met;
    END IF;
END;$$;

-- schema
CALL dbinfo.fn_create_or_update_enum('enum_conditional', '{
    and,
    or,
    anchor_property,
    data_instance,
    data_value,
    condition,
    else
    }');
COMMENT ON TYPE shared.enum_conditional IS '
    "and/or" - apply a logical "and/or" operation to all children of this conditon, applying to the same set of anchor/subject_data.
    "anchor_property/data_instance/data_value" - look at the corresponding table for a key value pair that matches this codntion.
    "condition" - this condition has the same logical value as another condition whose id is contained in the "value" column. Valuable for reusing common conditions.
    "else" - this condition is false if any earlier siblings of the interaction it is on have conditions that are true. This condition has no value field.
';

--@todo only = is implemented in the condiitonal functions
CALL dbinfo.fn_create_or_update_enum('enum_operator', '{
    =,
    <,
    <=,
    >,
    >=,
    !=
    }');
COMMENT ON TYPE shared.enum_operator IS '
    This defines the list of valid operators for conditions. Right now just an equals operator, but we could add more later if needed.
';

CREATE OR REPLACE FUNCTION tf_condition_set_to_id_if_null() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF NEW.root_condition_id IS NULL THEN UPDATE condition SET "root_condition_id" = "id" WHERE "id"=NEW.id; END IF;

    RETURN NEW;
END;$$;

CALL dbinfo.fn_create_or_update_table('condition','{
    id,
    root_condition_id bigint,
    parent_condition_id bigint,
    name text UNIQUE,
    shared boolean DEFAULT false NOT NULL,
    conditional shared.enum_conditional NOT NULL,
    from text CHECK(\"from\"<>''''),
    as text CHECK(\"as\"<>''''),
    key text CHECK(\"key\"<>''''),
    value text CHECK(\"value\"<>''''),
    operator shared.enum_operator,
    not boolean DEFAULT false NOT NULL,
    ordinal smallint DEFAULT 0 NOT NULL,
    modified timestamp
    }');
-- @todo operator shared.enum_operator DEFAULT ''=''::shared.enum_operator ... can't do this because you can't use enum types that haven't been committed... so frustrating
COMMENT ON COLUMN condition.root_condition_id IS 'The id of the root condition for a set of conditions, conditions can be organized into sets for the purposes of useability and efficient querying';
COMMENT ON COLUMN condition.parent_condition_id IS 'The id of the parent condition, conditions can have deep structures, this is the immediate parent';
COMMENT ON COLUMN condition.name IS 'The name of a saved condition set for the purposes of reuse, null names should be ignored';
COMMENT ON COLUMN condition.conditional IS 'The type of this condition, some types are specfic for leaf nodes, others are expected to have children, see the enum_conditional type';
COMMENT ON COLUMN condition.key IS 'Most leaf conditons are on a key value pair, this defines the key, i.e. for a data_value condition this would be a key in their subject_data';
COMMENT ON COLUMN condition.value IS 'Most leaf conditions are on a key value pair, this defines the value, i.e. for a data_value condition this would be the value for the key in their subject_data';
COMMENT ON COLUMN condition.not IS 'Apply a logical not operation to the results of this condition';
COMMENT ON COLUMN condition.ordinal IS 'Determines order of conditionals in UI';

DROP TRIGGER IF EXISTS tf_log_change ON condition;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON condition FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON condition;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON condition FOR EACH ROW EXECUTE FUNCTION tf_set_modified();
DROP TRIGGER IF EXISTS tf_condition_set_to_id_if_null ON condition;
CREATE TRIGGER tf_condition_set_to_id_if_null AFTER INSERT ON condition FOR EACH ROW EXECUTE FUNCTION tf_condition_set_to_id_if_null();

CREATE INDEX IF NOT EXISTS idx_condition_conditional ON "condition" ("conditional","key","value");
CREATE INDEX IF NOT EXISTS idx_condition_root_condition_id ON "condition" ("root_condition_id");
CREATE INDEX IF NOT EXISTS idx_condition_parent_condition_id ON "condition" ("parent_condition_id");
CREATE INDEX IF NOT EXISTS idx_condition_shared ON "condition" ("shared","name");

-- meta
CALL dbinfo.fn_alter_table_add_constraint('condition','fk_condition_parent','
    FOREIGN KEY (parent_condition_id) REFERENCES condition(id)
    ON DELETE CASCADE ON UPDATE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('condition','fk_condition_set','
    FOREIGN KEY (root_condition_id) REFERENCES condition(id)
    ON DELETE CASCADE ON UPDATE CASCADE');
    
CREATE OR REPLACE FUNCTION tf_condition()
RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
    v_condition_id bigint;
BEGIN
    v_condition_id := COALESCE(NEW.id, OLD.id);

    INSERT INTO interaction_schedule_recache (join_clause,where_clause) VALUES ('
        JOIN interaction_link_parent 
        ON interaction_schedule_data.interaction_link_id = interaction_link_parent.interaction_link_id
        JOIN interaction_link_condition 
        ON interaction_link_parent.parent_interaction_link_id = interaction_link_condition.interaction_link_id
    ','
        AND interaction_link_condition.condition_id = ' || v_condition_id
    ) ON CONFLICT DO NOTHING;

    RETURN NULL;
END;
$$;
DROP TRIGGER IF EXISTS tf_condition ON condition;
CREATE TRIGGER tf_condition
AFTER UPDATE OR DELETE ON condition
FOR EACH ROW EXECUTE FUNCTION tf_condition();

DROP FUNCTION IF EXISTS json_export_condition;
CREATE OR REPLACE FUNCTION json_export_condition(arg_id bigint, arg_data jsonb DEFAULT NULL)
RETURNS jsonb AS $$  -- Changed to return jsonb instead of VOID
DECLARE
    v_data jsonb;
    v_children jsonb;
    v_child jsonb;
BEGIN
    -- Skip this condition if it's already in the result set 
    IF (arg_data->'condition'->arg_id::text) IS NOT NULL THEN
        RETURN arg_data;
    END IF;

    -- Select the main condition data
    SELECT 
        to_jsonb(condition) INTO v_data
    FROM 
        condition
    WHERE 
        id = arg_id;
            
    -- Select and aggregate children
    SELECT 
        jsonb_agg(id) INTO v_children
    FROM (
        SELECT * 
        FROM condition
        WHERE parent_condition_id = arg_id
        ORDER BY ordinal ASC
    ) ordered_children;
    
    IF v_children IS NOT NULL THEN
        v_data := v_data || jsonb_build_object('condition_ids', v_children);
    END IF;

    -- Update arg_data with the new condition JSON
    arg_data := jsonb_deep_set(
        COALESCE(arg_data, '{}'::jsonb),
        ARRAY['condition'],
        (COALESCE(arg_data->'condition', '{}'::jsonb) || jsonb_build_object(arg_id::text, v_data))
    );

    -- Loop through each node and call export for each referenced item
    FOR v_child IN SELECT * FROM jsonb_array_elements(v_children) LOOP
        arg_data := json_export_condition(v_child::bigint, arg_data);
    END LOOP;
    
    RETURN arg_data;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS json_import_condition;
CREATE OR REPLACE FUNCTION json_import_condition(arg_condition_data JSONB, arg_new_ids JSONB, arg_master_data JSONB)
RETURNS JSONB AS $$
DECLARE
    v_new_condition_id BIGINT;
    v_parent_condition_id BIGINT;
    v_root_condition_id BIGINT := NULL;
    v_condition_id BIGINT := (arg_condition_data->>'id')::BIGINT;
BEGIN
    -- Check if the condition is already processed (in arg_new_ids)
    IF arg_new_ids->'condition'->>(arg_condition_data->>'id') IS NOT NULL THEN
        RETURN arg_new_ids; -- Skip processing if already present
    END IF;

    -- Check and process parent_condition_id if it's negative and not present in new_ids
    IF (arg_condition_data->>'parent_condition_id')::BIGINT < 0 THEN
        IF arg_new_ids->'condition'->>(arg_condition_data->>'parent_condition_id') IS NULL THEN
            -- Process parent condition from master JSON if not already processed
            arg_new_ids := json_import_condition(arg_master_data->'condition'->(arg_condition_data->>'parent_condition_id'), arg_new_ids, arg_master_data);
        END IF;
        v_parent_condition_id := (arg_new_ids->'condition'->>(arg_condition_data->>'parent_condition_id'))::BIGINT;
    ELSE
        v_parent_condition_id := (arg_condition_data->>'parent_condition_id')::BIGINT;
    END IF;

    -- Check and process root_condition_id if it's negative and not present in new_ids
    IF (arg_condition_data->>'root_condition_id')::BIGINT < 0 THEN
        IF arg_condition_data->>'root_condition_id' != arg_condition_data->>'id' THEN
            IF arg_new_ids->'condition'->>(arg_condition_data->>'root_condition_id') IS NULL THEN
                -- Process root condition from master JSON if not already processed
                arg_new_ids := json_import_condition(arg_master_data->'condition'->(arg_condition_data->>'root_condition_id'), arg_new_ids, arg_master_data);
            END IF;
            v_root_condition_id := (arg_new_ids->'condition'->>(arg_condition_data->>'root_condition_id'))::BIGINT;
        END IF;
    ELSE
        v_root_condition_id := (arg_condition_data->>'root_condition_id')::BIGINT;
    END IF;

    IF v_condition_id < 0 THEN
        -- Insert a new condition
        INSERT INTO "condition" ("not", "shared", "ordinal", "operator", "conditional", "key", "value", "parent_condition_id", "root_condition_id")
        VALUES (
            (arg_condition_data->>'not')::BOOLEAN,
            (arg_condition_data->>'shared')::BOOLEAN,
            (arg_condition_data->>'ordinal')::INT,
            (arg_condition_data->>'operator')::shared.enum_operator,
            (arg_condition_data->>'conditional')::shared.enum_conditional,
            (arg_condition_data->>'key')::TEXT,
            (arg_condition_data->>'value')::TEXT,
            v_parent_condition_id,
            v_root_condition_id
        )
        RETURNING id INTO v_new_condition_id;
        

        -- Update new_ids with the mapping from negative to new ID in condition section
        arg_new_ids := jsonb_deep_set(arg_new_ids, ARRAY['condition', (arg_condition_data->>'id')], to_jsonb(v_new_condition_id));
    ELSE
        -- Update the existing condition
        UPDATE "condition"
        SET "not" = (arg_condition_data->>'not')::BOOLEAN,
            "shared" = (arg_condition_data->>'shared')::BOOLEAN,
            "ordinal" = (arg_condition_data->>'ordinal')::INT,
            "operator" = (arg_condition_data->>'operator')::shared.enum_operator,
            "conditional" = (arg_condition_data->>'conditional')::shared.enum_conditional,
            "key" = (arg_condition_data->>'key')::TEXT,
            "value" = (arg_condition_data->>'value')::TEXT,
            "parent_condition_id" = v_parent_condition_id,
            "root_condition_id" = v_root_condition_id
        WHERE id = v_condition_id;

        -- Add the positive ID to new_ids, key being the same as the value
        arg_new_ids := jsonb_deep_set(arg_new_ids, ARRAY['condition', (arg_condition_data->>'id')], to_jsonb(v_condition_id));
    END IF;

    -- Delete any child conditions that are not in the current condition_ids list
    DELETE FROM condition
    WHERE parent_condition_id = (arg_condition_data->>'id')::BIGINT
    AND id NOT IN (
        SELECT (jsonb_array_elements(arg_condition_data->'condition_ids')->>'id')::BIGINT
    );

    RETURN arg_new_ids;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION get_conditions_for_interaction( 
    p_interaction_id bigint)
RETURNS TABLE(key text, value text) 
LANGUAGE 'sql'
COST 100
STABLE PARALLEL UNSAFE
ROWS 1000
AS $BODY$
SELECT vals.key, string_agg(DISTINCT vals.value, '||') AS value
FROM build.interaction_link_parent ilp
JOIN interaction_condition_and_anchor_property_value icap
  ON icap.anchor_interaction_id = ilp.anchor_interaction_id
CROSS JOIN LATERAL (
    VALUES 
      (icap.condition_key, icap.condition_value),
      (icap.property_key, icap.property_value)
) AS vals(key, value)
WHERE ilp.anchor_interaction_id = p_interaction_id
  AND vals.key IS NOT NULL 
  AND vals.value IS NOT NULL
GROUP BY vals.key;
$BODY$;