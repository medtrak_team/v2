
CALL dbinfo.fn_create_or_update_table('interaction','{
    id,
    name text NOT NULL,
    shared boolean DEFAULT false NOT NULL,
    properties jsonb NOT NULL DEFAULT ''\{\}''::jsonb,
    modified timestamp
    }');
DROP TRIGGER IF EXISTS tf_log_change ON interaction;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON interaction FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON interaction;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON interaction FOR EACH ROW EXECUTE FUNCTION tf_set_modified();

CREATE INDEX IF NOT EXISTS idx_interaction_shared ON "interaction" ("shared","name");

CALL dbinfo.fn_create_or_update_table('interaction_instance_property','{
    id,
    interaction_id bigint NOT NULL,
    type text NOT NULL,
    modified timestamp
    }');
DROP TRIGGER IF EXISTS tf_log_change ON interaction_instance_property;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON interaction_instance_property FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON interaction_instance_property;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON interaction_instance_property FOR EACH ROW EXECUTE FUNCTION tf_set_modified();
COMMENT ON TABLE interaction_instance_property IS 'The valid instances of the interaction. 
If something is an instance it means we only collect for anchors that have that type of property,
and for each value of that instance we require a separate interaction_schedule for the interaction.
i.e. if the interaction is a survey with an instance property of anatomy, then it will not generate interaction_schedules for anchors without anatomy,
and it will generate separate interaction_schedules for the left knee and right hip, for example';

CREATE INDEX IF NOT EXISTS idx_interaction_instance_property_interaction_id ON "interaction_instance_property" ("interaction_id");
CREATE INDEX IF NOT EXISTS idx_interaction_instance_property_type ON "interaction_instance_property" ("type");

-- meta
CALL dbinfo.fn_alter_table_add_constraint('interaction_instance_property','fk_interaction_intance_property_interaction','
    FOREIGN KEY (interaction_id) REFERENCES interaction(id)
    ON DELETE CASCADE ON UPDATE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('interaction_instance_property','uq_interaction_id_type','UNIQUE (interaction_id, type)');

CREATE OR REPLACE FUNCTION tf_interaction_instance_property()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    -- Insert or update records in interaction_schedule_cache based on interaction_id
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        INSERT INTO interaction_schedule_recache (join_clause,where_clause) VALUES ('','
            AND interaction_schedule_data.interaction_id = ' || NEW.interaction_id
        ) ON CONFLICT DO NOTHING;
    END IF;

    RETURN NULL;
END;
$$;
DROP TRIGGER IF EXISTS tf_interaction_instance_property ON interaction_instance_property;
CREATE TRIGGER tf_interaction_instance_property
AFTER INSERT OR UPDATE OR DELETE ON interaction_instance_property
FOR EACH ROW EXECUTE FUNCTION tf_interaction_instance_property();

DROP FUNCTION IF EXISTS json_export_interaction;
CREATE OR REPLACE FUNCTION json_export_interaction(arg_id bigint, arg_data jsonb DEFAULT NULL)
RETURNS jsonb AS $$  -- Changed to return jsonb instead of VOID
DECLARE
    v_data jsonb;
    v_nodes jsonb;
    v_node jsonb;
    v_intervals jsonb;
    v_interval_id bigint;
BEGIN
    -- Skip this interaction if it's already in the result set 
    IF (arg_data->'interaction'->arg_id::text) IS NOT NULL THEN
        RETURN arg_data;
    END IF;

    -- Select the main interaction data
    SELECT 
        to_jsonb(interaction) INTO v_data
    FROM 
        interaction 
    WHERE 
        id = arg_id;
        
    -- Don't include shared interaction in the result set (unless this is the root)
    IF (v_data->>'shared')::boolean = false OR arg_data IS NULL THEN
        IF arg_data IS NULL THEN
            arg_data := jsonb_build_object('root', arg_id);
        END IF;
        
        -- Select and aggregate interaction nodes
        SELECT 
            jsonb_agg(to_jsonb(ordered_nodes)) INTO v_nodes
        FROM (
            SELECT * 
            FROM interaction_link
            WHERE parent_interaction_id = arg_id
            ORDER BY ordinal ASC
        ) ordered_nodes;
        
        -- Loop through v_nodes with an index to select intervals for each node
        IF v_nodes IS NOT NULL THEN
            FOR i IN 0 .. jsonb_array_length(v_nodes) - 1 LOOP  -- Loop through each node with index
                v_node := v_nodes->i;  -- Get the current node

                -- Select intervals associated with the interaction_link_id
                SELECT jsonb_agg(interval_id) INTO v_intervals
                FROM interaction_link_interval
                WHERE interaction_link_id = (v_node->>'id')::bigint;

                -- Append intervals to the current node under the key 'intervals'
                IF v_intervals IS NOT NULL THEN
                    v_node := v_node || jsonb_build_object('interval_ids', v_intervals);
                END IF;

                -- Update the v_nodes array with the modified node
                v_nodes := jsonb_deep_set(v_nodes, ARRAY[i::text], v_node);
            END LOOP;

            -- Add the interaction_links array to the main interaction JSON
            v_data := v_data || jsonb_build_object('interaction_links', v_nodes);
        END IF;

        -- Loop through each node and call export for each referenced item
        FOR v_node IN SELECT * FROM jsonb_array_elements(v_nodes) LOOP
            IF (v_node->>'condition_id') IS NOT NULL THEN
                arg_data := json_export_condition((v_node->>'condition_id')::bigint, arg_data);
            END IF;
            IF (v_node->>'interval_ids') IS NOT NULL THEN
                FOR v_interval_id IN SELECT (jsonb_array_elements_text(v_node->'interval_ids'))::bigint LOOP
                    arg_data := json_export_interval(v_interval_id, arg_data);
                END LOOP;
            END IF;
            IF (v_node->>'child_interaction_id') IS NOT NULL THEN
                arg_data := json_export_interaction((v_node->>'child_interaction_id')::bigint, arg_data);
            END IF;
        END LOOP;

    END IF;

    -- Update arg_data with the new interaction JSON
    arg_data := jsonb_deep_set(
        COALESCE(arg_data, '{}'::jsonb),
        ARRAY['interaction'],
        (COALESCE(arg_data->'interaction', '{}'::jsonb) || jsonb_build_object(arg_id::text, v_data))
    );
    
    RETURN arg_data;
END;
$$ LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS json_import_interaction_set;
CREATE OR REPLACE FUNCTION json_import_interaction_set(arg_interaction_data JSONB)
RETURNS BIGINT AS $$
DECLARE
    v_comm_set_data JSONB;
    v_interval_data JSONB;
    v_condition_data JSONB;
    v_interaction_data JSONB;
    v_new_ids JSONB := '{}'::JSONB; -- for storing new id values that replace negative id values
BEGIN
    -- Process Communication Sets
    IF arg_interaction_data ? 'communication_set' THEN
        FOR v_comm_set_data IN SELECT value FROM jsonb_each(arg_interaction_data->'communication_set')
        LOOP
            v_new_ids := json_import_communication_set(v_comm_set_data, v_new_ids);
        END LOOP;
    END IF;
    
    -- Process Intervals
    IF arg_interaction_data ? 'interval' THEN
        FOR v_interval_data IN SELECT value FROM jsonb_each(arg_interaction_data->'interval')
        LOOP
            v_new_ids := json_import_interval(v_interval_data, v_new_ids);
        END LOOP;
    END IF;

    -- Process Conditions
    IF arg_interaction_data ? 'condition' THEN
        FOR v_condition_data IN SELECT value FROM jsonb_each(arg_interaction_data->'condition')
        LOOP
            v_new_ids := json_import_condition(v_condition_data, v_new_ids, arg_interaction_data);
        END LOOP;
    END IF;

    -- Process Interactions
    IF arg_interaction_data ? 'interaction' THEN
        FOR v_interaction_data IN SELECT value FROM jsonb_each(arg_interaction_data->'interaction')
        LOOP
            v_new_ids := json_import_interaction(v_interaction_data, v_new_ids, arg_interaction_data);
        END LOOP;
    END IF;

    IF (arg_interaction_data->>'root')::BIGINT <0 THEN
        RETURN (v_new_ids->'interaction'->>(arg_interaction_data->>'root'))::BIGINT;
    ELSE
        RETURN (arg_interaction_data->>'root')::BIGINT;
    END IF;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS json_import_interaction;
CREATE OR REPLACE FUNCTION json_import_interaction(arg_interaction_data JSONB, arg_new_ids JSONB, arg_master_data JSONB)
RETURNS JSONB AS $$
DECLARE
    v_node_data JSONB;
    v_new_interaction_id BIGINT;
    v_interaction_id BIGINT := (arg_interaction_data->>'id')::BIGINT;
BEGIN
    -- Check if the interaction is already processed (in arg_new_ids)
    IF arg_new_ids->'interaction'->>(arg_interaction_data->>'id') IS NOT NULL THEN
        RETURN arg_new_ids; -- Skip processing if already present
    END IF;

    --@todo if interval_set type interaction need to process the interval_set_id property from arg_new_ids
    IF v_interaction_id < 0 THEN
        -- Insert a new interaction
        INSERT INTO interaction (name, shared, properties)
        VALUES (
            arg_interaction_data->>'name',
            (arg_interaction_data->>'shared')::BOOLEAN,
            arg_interaction_data->'properties'
        )
        RETURNING id INTO v_new_interaction_id;

        -- Update new_ids with the mapping from negative to new ID in interaction section
        arg_new_ids := jsonb_deep_set(arg_new_ids, ARRAY['interaction', (arg_interaction_data->>'id')], to_jsonb(v_new_interaction_id));
    ELSE
        -- Update the existing interaction
        UPDATE interaction
        SET name = arg_interaction_data->>'name',
            shared = (arg_interaction_data->>'shared')::BOOLEAN,
            properties = arg_interaction_data->'properties'
        WHERE id = v_interaction_id;

        -- Add the positive ID to new_ids, key being the same as the value
        arg_new_ids := jsonb_deep_set(arg_new_ids, ARRAY['interaction', (arg_interaction_data->>'id')], to_jsonb(v_interaction_id));
    END IF;

    -- Loop through the interaction nodes and call the node import function
    IF arg_interaction_data ? 'interaction_links' THEN
        FOR i IN 0..(jsonb_array_length(arg_interaction_data->'interaction_links') - 1)
        LOOP
            -- Get the node data and update the ordinal
            v_node_data := jsonb_deep_set(jsonb_array_element(arg_interaction_data->'interaction_links', i), ARRAY['ordinal'], to_jsonb(i + 1));

            -- Process each node with updated ordinal
            arg_new_ids := json_import_interaction_link(v_node_data, arg_new_ids, arg_master_data);
        END LOOP;

        -- Delete any interaction nodes that are not in the current list
        DELETE FROM interaction_link
        WHERE parent_interaction_id = (arg_interaction_data->>'id')::BIGINT
        AND id NOT IN (
            SELECT (jsonb_array_elements(arg_interaction_data->'interaction_links')->>'id')::BIGINT
        );
    END IF;
    
    RETURN arg_new_ids;
END;
$$ LANGUAGE plpgsql;