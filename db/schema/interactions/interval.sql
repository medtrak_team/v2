CALL dbinfo.fn_create_or_update_table('interval_set','{
    id,
    name text UNIQUE,
    shared boolean DEFAULT false NOT NULL,
    modified timestamp
    }');
DROP TRIGGER IF EXISTS tf_log_change ON interval_set;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON interval_set FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON interval_set;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON interval_set FOR EACH ROW EXECUTE FUNCTION tf_set_modified();
COMMENT ON COLUMN interval_set.name IS 'Named interval sets should be searchable and reusable in the ui. If the name is left null it means the interval set is meant to be specific to that one use.';

CREATE INDEX IF NOT EXISTS idx_interval_set_shared ON "interval_set" ("shared","name");

CALL dbinfo.fn_create_or_update_table('interval','{
    id,
    interval_set_id bigint default null,
    name text,
    start_day integer,
    end_day integer,
    after_created integer default null,
    communication_set_id bigint default null,
    recurrence jsonb NOT NULL DEFAULT ''\{\}''::jsonb,
    modified timestamp,
    communication_start integer
    }');
DROP TRIGGER IF EXISTS tf_log_change ON interval;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON interval FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON interval;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON interval FOR EACH ROW EXECUTE FUNCTION tf_set_modified();

CREATE INDEX IF NOT EXISTS idx_interval_interval_set_id ON "interval" ("interval_set_id");
CREATE INDEX IF NOT EXISTS idx_interval_name ON "interval" ("name");
CREATE INDEX IF NOT EXISTS idx_interval_communication_set_id ON "interval" ("communication_set_id");

-- meta
CALL dbinfo.fn_alter_table_add_constraint('interval','fk_interval_interval_set','
    FOREIGN KEY (interval_set_id) REFERENCES interval_set(id)
    ON DELETE SET NULL ON UPDATE CASCADE');
CALL dbinfo.fn_alter_table_add_constraint('interval','fk_interval_communication_set','
    FOREIGN KEY (communication_set_id) REFERENCES communication_set(id)
    ON DELETE SET NULL ON UPDATE CASCADE');

CREATE OR REPLACE FUNCTION tf_interval()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    INSERT INTO interaction_schedule_recache (join_clause,where_clause) VALUES ('','
        AND interaction_schedule_data.interval_id = ' || NEW.id
    ) ON CONFLICT DO NOTHING;

    RETURN NULL;
END;
$$;
DROP TRIGGER IF EXISTS tf_interval ON interval;
CREATE TRIGGER tf_interval
AFTER UPDATE ON interval
FOR EACH ROW EXECUTE FUNCTION tf_interval();


DROP TABLE IF EXISTS json_export_interval;
CREATE OR REPLACE FUNCTION json_export_interval(arg_id bigint, arg_data jsonb DEFAULT NULL)
RETURNS jsonb AS $$  -- Changed to return jsonb instead of VOID
DECLARE
    v_data jsonb;
BEGIN
    -- Skip this interval if it's already in the result set 
    IF (arg_data->'interval'->arg_id::text) IS NOT NULL THEN
        RETURN arg_data;
    END IF;

    -- Select the main interval data
    SELECT 
        to_jsonb("interval") INTO v_data
    FROM 
        "interval" 
    WHERE 
        id = arg_id;

    -- @todo handle interval set data

    -- Update arg_data with the new interval JSON
    arg_data := jsonb_deep_set(
        COALESCE(arg_data, '{}'::jsonb),
        ARRAY['interval'],
        (COALESCE(arg_data->'interval', '{}'::jsonb) || jsonb_build_object(arg_id::text, v_data))
    );

    IF (v_data->>'communication_set_id') IS NOT NULL THEN
        arg_data := json_export_communication_set((v_data->>'communication_set_id')::bigint, arg_data);
    END IF;

    RETURN arg_data;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS json_import_interval;
CREATE OR REPLACE FUNCTION json_import_interval(arg_interval_data JSONB, arg_new_ids JSONB)
RETURNS JSONB AS $$
DECLARE
    v_new_interval_id BIGINT;
    v_communication_set_id BIGINT;
    v_interval_set_id BIGINT := NULL;
BEGIN
    -- Replace negative communication_set_id using new_ids from the communication_set section
    IF (arg_interval_data->>'communication_set_id')::BIGINT < 0 THEN
        v_communication_set_id := (arg_new_ids->'communication_set'->>(arg_interval_data->>'communication_set_id'))::BIGINT;
    ELSE
        v_communication_set_id := (arg_interval_data->>'communication_set_id')::BIGINT;
    END IF;

    -- @todo handle new interval_set imports properly
    IF (arg_interval_data->>'interval_set_id')::BIGINT > 0 THEN
        v_interval_set_id := (arg_interval_data->>'interval_set_id')::BIGINT;
    END IF;

    IF (arg_interval_data->>'id')::BIGINT < 0 THEN
        -- Insert a new interval
        INSERT INTO interval (name, end_day, start_day, after_created, recurrence, interval_set_id, communication_set_id)
        VALUES (
            arg_interval_data->>'name',
            (arg_interval_data->>'end_day')::INT,
            (arg_interval_data->>'start_day')::INT,
            (arg_interval_data->>'after_created')::INT,
            arg_interval_data->'recurrence',
            v_interval_set_id,
            v_communication_set_id
        )
        RETURNING id INTO v_new_interval_id;
        
        -- Update new_ids with the mapping from negative to new ID in interval section
        arg_new_ids := jsonb_deep_set(arg_new_ids, ARRAY['interval', (arg_interval_data->>'id')], to_jsonb(v_new_interval_id));
    ELSE
        -- Update the existing interval
        UPDATE interval
        SET name = arg_interval_data->>'name',
            end_day = (arg_interval_data->>'end_day')::INT,
            start_day = (arg_interval_data->>'start_day')::INT,
            after_created = (arg_interval_data->>'after_created')::INT,
            recurrence = arg_interval_data->'recurrence',
            interval_set_id = v_interval_set_id,
            communication_set_id = v_communication_set_id
        WHERE id = (arg_interval_data->>'id')::BIGINT;
    END IF;

    RETURN arg_new_ids;
END;
$$ LANGUAGE plpgsql;