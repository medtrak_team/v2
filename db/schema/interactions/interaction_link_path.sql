CALL dbinfo.fn_create_or_update_table('interaction_link_path','{
    anchor_interaction_id bigint NOT NULL,
    interaction_link_path bigint[] NOT NULL,
    interaction_link_path_ordinals smallint[] DEFAULT NULL,
    interaction_link_id bigint NOT NULL
}');

CREATE INDEX IF NOT EXISTS idx_interaction_link_path_anchor_interaction_id ON "interaction_link_path" ("anchor_interaction_id");
CREATE INDEX IF NOT EXISTS idx_interaction_link_path_interaction_link_id ON "interaction_link_path" ("interaction_link_id");

CALL dbinfo.fn_create_or_update_table('interaction_link_parent','{
    anchor_interaction_id bigint NOT NULL,
    parent_interaction_link_id bigint NOT NULL,
    interaction_link_id bigint NOT NULL
}');

CREATE INDEX IF NOT EXISTS idx_interaction_link_parent_anchor_interaction_id ON "interaction_link_parent" ("anchor_interaction_id");
CREATE INDEX IF NOT EXISTS idx_interaction_link_parent_parent_interaction_link_id ON "interaction_link_parent" ("parent_interaction_link_id");
CREATE INDEX IF NOT EXISTS idx_interaction_link_parent_interaction_link_id ON "interaction_link_parent" ("interaction_link_id");

-- Function to recursively process children interaction links
CREATE OR REPLACE FUNCTION fn_build_interaction_link_path_for_interaction_link(
    arg_interaction_link RECORD, 
    arg_anchor_interaction_id bigint,
	arg_path bigint[] = ARRAY[]::bigint[],
	arg_path_ordinals smallint[] = ARRAY[]::smallint[]
) RETURNS VOID LANGUAGE plpgsql AS $$
DECLARE
    v_interaction_link RECORD;
BEGIN
    -- Stop recursion if the link is already in the path (avoid loops)
    IF arg_interaction_link.id = ANY(arg_path) THEN
        RETURN;
    END IF;

	-- Append the interaction link id and ordinal to the path arrays
    arg_path := arg_path || arg_interaction_link.id;
    arg_path_ordinals := arg_path_ordinals || arg_interaction_link.ordinal;
	-- Insert the path and ordinals into interaction_link_path
	INSERT INTO interaction_link_path (anchor_interaction_id, interaction_link_path, interaction_link_path_ordinals, interaction_link_id)
	VALUES (arg_anchor_interaction_id, arg_path, arg_path_ordinals, arg_interaction_link.id);
	
	-- Check if the interaction_link has any associated intervals in the interaction_link_interval table and stop recursion if found
	IF EXISTS (
	    SELECT 1 FROM interaction_link_interval 
	    WHERE interaction_link_id = arg_interaction_link.id
	) THEN
        -- Unroll each element in arg_path into interaction_link_parent, this will make it easier to find the interaction_schedule entires that need to be updated
        -- @NOTE I am my own parent according to this table. It simplifies some of the queries so we don't need an or
        FOR i IN 1 .. array_length(arg_path, 1)
        LOOP
            INSERT INTO interaction_link_parent (anchor_interaction_id, parent_interaction_link_id, interaction_link_id)
            VALUES (arg_anchor_interaction_id, arg_path[i], arg_interaction_link.id);
        END LOOP;

	    RETURN;
	END IF;

    -- Otherwise, continue recursively processing the children
    FOR v_interaction_link IN 
        SELECT * FROM interaction_link 
        WHERE parent_interaction_id = arg_interaction_link.child_interaction_id
    LOOP
        -- Pass down the path arrays to the children
        PERFORM fn_build_interaction_link_path_for_interaction_link(
            v_interaction_link, 
            arg_anchor_interaction_id,
			arg_path,
			arg_path_ordinals
        );
    END LOOP;
END;
$$;

-- meta
CALL dbinfo.fn_alter_table_add_constraint('interaction_link_path','fk_interaction_link_path_interaction_link','
    FOREIGN KEY (interaction_link_id) REFERENCES interaction_link (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE'
);
CALL dbinfo.fn_alter_table_add_constraint('interaction_link_path','fk_interaction_link_path_anchor_interaction','
    FOREIGN KEY (anchor_interaction_id) REFERENCES interaction (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE'
);
CALL dbinfo.fn_alter_table_add_constraint('interaction_link_parent','fk_interaction_link_parent_interaction_link','
    FOREIGN KEY (interaction_link_id) REFERENCES interaction_link (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE'
);
CALL dbinfo.fn_alter_table_add_constraint('interaction_link_parent','fk_interaction_link_parent_interaction_link_parent','
    FOREIGN KEY (parent_interaction_link_id) REFERENCES interaction_link (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE'
);
CALL dbinfo.fn_alter_table_add_constraint('interaction_link_parent','fk_interaction_link_parent_anchor_interaction','
    FOREIGN KEY (anchor_interaction_id) REFERENCES interaction (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE CASCADE'
);