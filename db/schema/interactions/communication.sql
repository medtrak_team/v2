CREATE OR REPLACE FUNCTION fn_get_priority_values()
    RETURNS TEXT[]
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
BEGIN
    RETURN ARRAY[
        'first communication important',
        'last chance',
        'reminder interval ending',
        'first reminder important',
        'first communication long term followup',
        'gentle reminder'
        ];
END;$$;

CREATE OR REPLACE FUNCTION fn_priority_value(priority text)
    RETURNS INTEGER
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
    priorities TEXT[] := fn_get_priority_values();
    priority_value INTEGER;
BEGIN
    priority_value := (
        SELECT idx
        FROM unnest(priorities) WITH ORDINALITY t(value, idx)
        WHERE value = priority
    );
    
    IF priority_value IS NULL THEN
        priority_value := array_length(priorities,1)+1;
    END IF;

    RETURN priority_value;
END;$$;

CALL dbinfo.fn_create_or_update_table('communication_set','{
    id,
    name text UNIQUE,
    shared boolean DEFAULT false NOT NULL,
    modified timestamp
    }');
DROP TRIGGER IF EXISTS tf_log_change ON communication_set;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON communication_set FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON communication_set;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON communication_set FOR EACH ROW EXECUTE FUNCTION tf_set_modified();
COMMENT ON COLUMN communication_set.name IS 'Named communication sets should be searchable and reusable in the ui. If the name is left null it means the comm set is meant to be specific to that one use.';
CREATE INDEX IF NOT EXISTS idx_communication_set_shared ON "communication_set" ("shared","name");

CALL dbinfo.fn_create_or_update_table('communication','{
    id,
    communication_set_id bigint NOT NULL,
    start text NOT NULL,
    end text NOT NULL,
    properties jsonb NOT NULL DEFAULT ''\{\}''::jsonb,
    modified timestamp
    }');
DROP TRIGGER IF EXISTS tf_log_change ON communication;
CREATE TRIGGER tf_log_change AFTER UPDATE OR DELETE ON communication FOR EACH ROW EXECUTE FUNCTION tf_log_change();
DROP TRIGGER IF EXISTS tf_set_modified ON communication;
CREATE TRIGGER tf_set_modified BEFORE INSERT OR UPDATE ON communication FOR EACH ROW EXECUTE FUNCTION tf_set_modified();
CREATE INDEX IF NOT EXISTS idx_communication_communication_set_id ON "communication" ("communication_set_id");
CREATE OR REPLACE FUNCTION tf_communication()
RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
    v_communication_id bigint;
BEGIN
    v_communication_id := COALESCE(NEW.id, OLD.id);
    PERFORM fn_communication_cache_generate(NULL, NULL, v_communication_id);
    RETURN NULL;
END;
$$;
DROP TRIGGER IF EXISTS tf_communication ON communication;
CREATE TRIGGER tf_communication
AFTER INSERT OR UPDATE OR DELETE ON communication
FOR EACH ROW EXECUTE FUNCTION tf_communication();
CALL dbinfo.fn_alter_table_add_constraint('communication','communication_properties_time','
    CHECK (properties->>''time'' IS NULL 
    OR properties->>''time'' SIMILAR TO ''(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]'')');

-- meta
CREATE OR REPLACE FUNCTION json_export_communication_set(arg_id bigint, arg_data jsonb DEFAULT NULL)
RETURNS jsonb AS $$  -- Changed to return jsonb instead of VOID
DECLARE
    v_data jsonb;
    v_comms jsonb;
BEGIN
    -- Skip this communication_set if it's already in the result set 
    IF (arg_data->'communication_set'->arg_id::text) IS NOT NULL THEN
        RETURN arg_data;
    END IF;

    -- Select the main communication_set data
    SELECT 
        to_jsonb(communication_set) INTO v_data
    FROM 
        communication_set 
    WHERE 
        id = arg_id;
            
    -- Select and aggregate communication records
    SELECT 
        jsonb_agg(to_jsonb(communication)) INTO v_comms
    FROM 
        communication
    WHERE 
        communication_set_id = arg_id;
    
    IF v_comms IS NOT NULL THEN
        v_data := v_data || jsonb_build_object('communications', v_comms);
    END IF;

    -- Update arg_data with the new communication_set JSON
    arg_data := jsonb_deep_set(
        COALESCE(arg_data, '{}'::jsonb),
        ARRAY['communication_set'],
        (COALESCE(arg_data->'communication_set', '{}'::jsonb) || jsonb_build_object(arg_id::text, v_data))
    );
    
    RETURN arg_data;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS json_import_communication_set;
CREATE OR REPLACE FUNCTION json_import_communication_set(arg_set_data JSONB, arg_new_ids JSONB)
RETURNS JSONB AS $$
DECLARE
    v_comm_data JSONB;
    v_new_comm_set_id BIGINT;
BEGIN
    -- Insert or update communication_set
    IF (arg_set_data->>'id')::BIGINT < 0 THEN
        -- Insert new communication_set
        INSERT INTO communication_set (name, shared)
        VALUES (
            arg_set_data->>'name',
            (arg_set_data->>'shared')::BOOLEAN
        )
        RETURNING id INTO v_new_comm_set_id;

        -- Update new_ids with the mapping from negative to new ID in communication_set section
        arg_new_ids := jsonb_deep_set(arg_new_ids, ARRAY['communication_set', (arg_set_data->>'id')], to_jsonb(v_new_comm_set_id));
    ELSE
        -- Update existing communication_set
        UPDATE communication_set
        SET name = arg_set_data->>'name',
            shared = (arg_set_data->>'shared')::BOOLEAN
        WHERE id = (arg_set_data->>'id')::BIGINT;
    END IF;

    -- Insert or update communications
    IF arg_set_data ? 'communications' THEN
        FOR v_comm_data IN SELECT jsonb_array_elements(arg_set_data->'communications')
        LOOP
            arg_new_ids := json_import_communication(v_comm_data, arg_new_ids);
        END LOOP;
    END IF;
    
    RETURN arg_new_ids;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS json_import_communication;
CREATE OR REPLACE FUNCTION json_import_communication(arg_comm_data JSONB, arg_new_ids JSONB)
RETURNS JSONB AS $$
DECLARE
    v_new_comm_id BIGINT;
    v_communication_set_id BIGINT;
BEGIN
    -- Replace negative communication_set_id using new_ids from the communication_set section
    IF (arg_comm_data->>'communication_set_id')::BIGINT < 0 THEN
        v_communication_set_id := (arg_new_ids->'communication_set'->>(arg_comm_data->>'communication_set_id'))::BIGINT;
    ELSE
        v_communication_set_id := (arg_comm_data->>'communication_set_id')::BIGINT;
    END IF;
    
    IF (arg_comm_data->>'id')::BIGINT < 0 THEN
        -- Insert new communication
        INSERT INTO "communication" ("end", "start", "properties", "communication_set_id")
        VALUES (
            arg_comm_data->>'end',
            arg_comm_data->>'start',
            arg_comm_data->'properties',
            v_communication_set_id
        )
        RETURNING id INTO v_new_comm_id;

        -- Update new_ids with the mapping from negative to new ID in communication section
        arg_new_ids := jsonb_deep_set(arg_new_ids, ARRAY['communication', (arg_comm_data->>'id')], to_jsonb(v_new_comm_id));        
    ELSE
        -- Update existing communication
        UPDATE "communication"
        SET "end" = arg_comm_data->>'end',
            "start" = arg_comm_data->>'start',
            "properties" = arg_comm_data->'properties',
            "communication_set_id" = v_communication_set_id
        WHERE id = (arg_comm_data->>'id')::BIGINT;
    END IF;
    
    RETURN arg_new_ids;
END;
$$ LANGUAGE plpgsql;