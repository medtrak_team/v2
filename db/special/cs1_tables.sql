CREATE FOREIGN TABLE IF NOT EXISTS cs1_big_table(
    pk integer NULL,
    sessionid text NULL,
    status integer NULL,
    owner text NULL,
    entrydate timestamp without time zone NULL,
    patientid integer NULL,
    treatmentid integer NULL,
    anatomy text NULL,
    dayselapsed integer NULL,
    standardet text NULL,
    dynamic smallint NULL,
    category text NULL,
    question text NULL,
    answer text NULL
)
SERVER {{server}} OPTIONS (dbname 'CareSenseData', table_name 'BigTable');
