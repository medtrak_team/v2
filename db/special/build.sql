-- ******************** Versioning ********************
CREATE TABLE IF NOT EXISTS build.interaction_set_update
(
    interaction_set bigint NOT NULL,
    "updated" timestamp,
    "approved" timestamp,
    CONSTRAINT interaction_set_update_pkey PRIMARY KEY (interaction_set)
);
COMMENT ON TABLE build.interaction_set_update IS 'Stores the last time this interaction set was synced with live (usually when committed).
    It also shows when it was last approved by QA so that we can block pushing something that has changed since it was approved.
    Used to identify sets that have content that has changed since last synced with live.';

DROP VIEW IF EXISTS build.interaction_changes CASCADE;
CREATE OR REPLACE VIEW build.interaction_changes AS
SELECT interaction_link.root_interaction_id, 
MAX(interaction_set_update.updated) as "updated",
MIN(interaction_set_update.approved) as "approved",
GREATEST(MAX(interaction_link.modified), MAX(interaction.modified), MAX(interaction_instance_property.modified), MAX(interaction_link_interval.modified), MAX(interval.modified), MAX(condition.modified), MAX(communication.modified)) as "modified"
FROM build.interaction_link
LEFT JOIN build.interaction ON interaction.id = interaction_link.child_interaction_id
LEFT JOIN build.interaction_instance_property ON interaction_instance_property.interaction_id = interaction_link.child_interaction_id
LEFT JOIN build.interaction_link_interval ON interaction_link_interval.interaction_link_id = interaction_link.id
LEFT JOIN build.interval ON interval.id = interaction_link_interval.interval_id
LEFT JOIN build.communication_set ON communication_set.id = interval.communication_set_id OR communication_set.id = interaction_link_interval.communication_set_id
LEFT JOIN build.communication ON communication.communication_set_id = communication_set.id
LEFT JOIN build.condition ON condition.id = interaction_link.condition_id OR condition.id = interaction_link_interval.condition_id
LEFT JOIN build.interaction_set_update ON interaction_link.root_interaction_id = interaction_set_update.interaction_set
GROUP BY build.interaction_link.root_interaction_id;

-- @todo rename interaction_changes_readable for consistency
DROP VIEW IF EXISTS build.interaction_changes_interactio CASCADE;
CREATE OR REPLACE VIEW build.interaction_changes_interaction AS
SELECT root_interaction_id, updated, interaction_changes.modified, id,name,properties FROM build.interaction_changes
join build.interaction on interaction.id = interaction_changes.root_interaction_id
where (interaction_changes.modified > interaction_changes.updated) OR (interaction_changes.updated is null);

COMMENT ON VIEW build.interaction_changes_interaction IS 'joins interaction_changes with interaction table for use in the UI';

DROP VIEW IF EXISTS build.modified_since_last_published CASCADE;
CREATE OR REPLACE VIEW build.modified_since_last_published AS
    SELECT * FROM (
    SELECT  interaction_link.root_interaction_id, interaction_link.id, interaction_link.child_interaction_id, interaction.name, interaction_set_update.updated as last_updated, 
    interaction_link.modified as interaction_link_modified, max(interaction_link_interval.modified) as interaction_link_interval_modified, interaction.modified as interaction_modified 
    FROM build.interaction_set_update
    JOIN build.interaction_link on build.interaction_set_update.interaction_set = build.interaction_link.root_interaction_id
    JOIN build.interaction on build.interaction.id = build.interaction_link.child_interaction_id
    LEFT JOIN build.interaction_link_interval on build.interaction_link_interval.interaction_link_id = build.interaction_link.id
    group by  interaction_link.root_interaction_id, interaction_link.id, interaction_link.child_interaction_id, interaction.name, last_updated, interaction_link_modified, interaction_modified
    ) as t1
    WHERE t1.interaction_link_modified > t1.last_updated or t1.interaction_modified > t1.last_updated or t1.interaction_link_interval_modified > t1.last_updated;

COMMENT ON VIEW build.modified_since_last_published IS 'Stores the last time any root set interactions OR any of their children/any interactions associated with that set/any interaction_link_intervals 
    associated with that set were updated. Allows for a more itemized list of what changed on the commit UI';

DROP PROCEDURE IF EXISTS build.proc_commit_table;
CREATE OR REPLACE PROCEDURE build.proc_commit_table(
	IN root_interaction_id bigint,
	IN committed_table text,
	IN join_clause text,
	IN unselected_ids jsonb)
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    column_names text[];
    omit_interaction_ids text;
    omit_interval_set_ids text;
    omit_interval_ids text;
    omit_communication_set_ids text;
    update_query text;
    insert_query text;
BEGIN
    IF unselected_ids IS NOT NULL THEN
        -- Extract the interaction IDs from the JSONB argument
        omit_interaction_ids := array_to_string(array(
            SELECT jsonb_array_elements_text(unselected_ids->'interaction')
        ), ',');

            -- Extract the interaction IDs from the JSONB argument
        omit_interval_set_ids := array_to_string(array(
            SELECT jsonb_array_elements_text(unselected_ids->'interval_set')
        ), ',');

            -- Extract the interaction IDs from the JSONB argument
        omit_interval_ids := array_to_string(array(
            SELECT jsonb_array_elements_text(unselected_ids->'interval')
        ), ',');

            -- Extract the interaction IDs from the JSONB argument
        omit_communication_set_ids := array_to_string(array(
            SELECT jsonb_array_elements_text(unselected_ids->'communication_set')
        ), ',');

        
        IF omit_interaction_ids IS NULL OR omit_interaction_ids = '' THEN
            omit_interaction_ids := 'NULL';
        END IF;

        IF omit_interval_set_ids IS NULL OR omit_interval_set_ids = '' THEN
            omit_interval_set_ids := 'NULL';
        END IF;

        IF omit_interval_ids IS NULL OR omit_interval_ids = '' THEN
            omit_interval_ids := 'NULL';
        END IF;

        IF omit_communication_set_ids IS NULL OR omit_communication_set_ids = '' THEN
            omit_communication_set_ids := 'NULL';
        END IF;

        RAISE NOTICE 'The value of omit_interaction_ids is: %', omit_interaction_ids;
        RAISE NOTICE 'The value of omit_interval_set_ids is: %', omit_interval_set_ids;
        RAISE NOTICE 'The value of omit_interval_ids is: %', omit_interval_ids;
        RAISE NOTICE 'The value of omit_communication_set_ids is: %', omit_communication_set_ids;
    END IF;

    column_names := ARRAY(
        SELECT ('"' || column_name || '"')
        FROM information_schema.columns
        WHERE columns.table_schema = 'build'
        AND columns.table_name   = committed_table
        AND columns.column_name NOT IN ('id','modified')
    );

    update_query := '
        UPDATE live.' || committed_table || ' as live_' || committed_table || '
        SET (' || array_to_string(column_names, ',') || ') = ROW(' || 'build_' || committed_table || '.' || array_to_string(column_names, ',' || 'build_' || committed_table || '.') || ')
        FROM build.' || committed_table || ' as build_' || committed_table || '
        ' || join_clause || '
        WHERE build_interaction_link.root_interaction_id = ' || root_interaction_id || ' 
        AND build_' || committed_table || '.id = live_' || committed_table || '.id';

    IF unselected_ids IS NOT NULL THEN
        IF (committed_table = 'interaction' OR committed_table = 'interaction_link') AND omit_interaction_ids IS NOT NULL AND omit_interaction_ids <> 'NULL' THEN
            update_query := update_query || ' AND build_interaction_link.child_interaction_id NOT IN (' || omit_interaction_ids || ')';
        ELSIF (committed_table = 'interval_set' ) AND omit_interval_set_ids IS NOT NULL AND omit_interval_set_ids <> 'NULL' THEN
            update_query := update_query || ' AND build_interval_set.id NOT IN (' || omit_interval_set_ids || ')';
        ELSIF (committed_table = 'interval' ) AND omit_interval_ids IS NOT NULL AND omit_interval_ids <> 'NULL' THEN
            update_query := update_query || ' AND build_interval.id NOT IN (' || omit_interval_ids || ')';
        ELSIF (committed_table = 'communication_set' ) AND omit_communication_set_ids IS NOT NULL AND omit_communication_set_ids <> 'NULL' THEN
            update_query := update_query || ' AND build_communication_set.id NOT IN (' || omit_communication_set_ids || ')';
        END IF;
    END IF;

    RAISE NOTICE 'Executing UPDATE query: %', update_query;

    EXECUTE update_query;

    insert_query := '
        INSERT INTO live.' || committed_table || '
        SELECT build_' || committed_table || '.* FROM build.' || committed_table || ' as build_' || committed_table || '
        ' || join_clause || '
        WHERE build_interaction_link.root_interaction_id = ' || root_interaction_id;

    IF unselected_ids IS NOT NULL THEN
        IF (committed_table = 'interaction' OR committed_table = 'interaction_link') AND omit_interaction_ids IS NOT NULL AND omit_interaction_ids <> 'NULL' THEN
            insert_query := insert_query || ' AND build_interaction_link.child_interaction_id NOT IN (' || omit_interaction_ids || ')';
        ELSIF (committed_table = 'interval_set' ) AND omit_interval_set_ids IS NOT NULL AND omit_interval_set_ids <> 'NULL' THEN
            insert_query := insert_query || ' AND build_interval_set.id NOT IN (' || omit_interval_set_ids || ')';
        ELSIF (committed_table = 'interval' ) AND omit_interval_ids IS NOT NULL AND omit_interval_ids <> 'NULL' THEN
            insert_query := insert_query || ' AND build_interval.id NOT IN (' || omit_interval_ids || ')';
        ELSIF (committed_table = 'communication_set' ) AND omit_communication_set_ids IS NOT NULL AND omit_communication_set_ids <> 'NULL' THEN
            insert_query := insert_query || ' AND build_communication_set.id NOT IN (' || omit_communication_set_ids || ')';
        END IF;
    END IF;

    insert_query := insert_query || ' ON CONFLICT DO NOTHING';

    RAISE NOTICE 'Executing INSERT query: %', insert_query;

    EXECUTE insert_query;
END;
$BODY$;

-- FUNCTIONAL version of the above procedure
-- TODO: if there isnt a reason to run the above as a proc, maybe delete that code?
DROP FUNCTION IF EXISTS build.func_commit_interaction_set;
CREATE OR REPLACE FUNCTION build.func_commit_interaction_set(
	root_interaction_id bigint,
	unselected_ids jsonb)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    TABLE_RECORD record;
BEGIN
    set search_path to live;
    -- @todo check that the set is approved by qa
        
    -- Move over each interaction related table where the item in question is related to the given interaction set
    CALL build.proc_commit_table(func_commit_interaction_set.root_interaction_id,'interaction','
        JOIN build.interaction_link as build_interaction_link 
        ON build_interaction.id = build_interaction_link.child_interaction_id 
        OR build_interaction.id = build_interaction_link.parent_interaction_id
        OR build_interaction.id = build_interaction_link.root_interaction_id',unselected_ids
    );
    CALL build.proc_commit_table(func_commit_interaction_set.root_interaction_id,'interaction_instance_property','
        JOIN build.interaction_link as build_interaction_link 
        ON build_interaction_instance_property.interaction_id = build_interaction_link.child_interaction_id 
        OR build_interaction_instance_property.interaction_id = build_interaction_link.root_interaction_id',unselected_ids
    );
    CALL build.proc_commit_table(func_commit_interaction_set.root_interaction_id,'condition','
        JOIN build.interaction_link_interval as build_interaction_link_interval
        ON build_interaction_link_interval.condition_id = build_condition.root_condition_id
        JOIN build.interaction_link as build_interaction_link 
        ON build_interaction_link_interval.interaction_link_id = build_interaction_link.id',unselected_ids
    );
    CALL build.proc_commit_table(func_commit_interaction_set.root_interaction_id,'condition','
        JOIN build.interaction_link as build_interaction_link 
        ON build_interaction_link.condition_id = build_condition.root_condition_id',unselected_ids
    );
    CALL build.proc_commit_table(func_commit_interaction_set.root_interaction_id,'communication_set','
        JOIN build.interval as build_interval
        ON build_interval.communication_set_id = build_communication_set.id
        JOIN build.interaction_link_interval as build_interaction_link_interval
        ON build_interaction_link_interval.interval_id = build_interval.id
        JOIN build.interaction_link as build_interaction_link 
        ON build_interaction_link.id = build_interaction_link_interval.interaction_link_id',unselected_ids
    );
    CALL build.proc_commit_table(func_commit_interaction_set.root_interaction_id,'communication_set','
        JOIN build.interaction_link_interval as build_interaction_link_interval
        ON build_interaction_link_interval.communication_set_id = build_communication_set.id
        JOIN build.interaction_link as build_interaction_link 
        ON build_interaction_link.id = build_interaction_link_interval.interaction_link_id',unselected_ids
    );
    CALL build.proc_commit_table(func_commit_interaction_set.root_interaction_id,'communication','
        JOIN build.communication_set as build_communication_set
        ON build_communication_set.id = build_communication.communication_set_id
        JOIN build.interval as build_interval
        ON build_interval.communication_set_id = build_communication_set.id
        JOIN build.interaction_link_interval as build_interaction_link_interval
        ON build_interaction_link_interval.interval_id = build_interval.id
        JOIN build.interaction_link as build_interaction_link 
        ON build_interaction_link.id = build_interaction_link_interval.interaction_link_id',unselected_ids
    );
    CALL build.proc_commit_table(func_commit_interaction_set.root_interaction_id,'communication','
        JOIN build.communication_set as build_communication_set
        ON build_communication_set.id = build_communication.communication_set_id
        JOIN build.interaction_link_interval as build_interaction_link_interval
        ON build_interaction_link_interval.communication_set_id = build_communication_set.id
        JOIN build.interaction_link as build_interaction_link 
        ON build_interaction_link.id = build_interaction_link_interval.interaction_link_id',unselected_ids
    );    
    CALL build.proc_commit_table(func_commit_interaction_set.root_interaction_id,'interval_set','
        JOIN build.interval as build_interval
        ON build_interval.interval_set_id = build_interval_set.id
        JOIN build.interaction_link_interval as build_interaction_link_interval
        ON build_interaction_link_interval.interval_id = build_interval.id
        JOIN build.interaction_link as build_interaction_link 
        ON build_interaction_link.id = build_interaction_link_interval.interaction_link_id',unselected_ids
    );
    CALL build.proc_commit_table(func_commit_interaction_set.root_interaction_id,'interval','
        JOIN build.interaction_link_interval as build_interaction_link_interval
        ON build_interaction_link_interval.interval_id = build_interval.id
        JOIN build.interaction_link as build_interaction_link 
        ON build_interaction_link.id = build_interaction_link_interval.interaction_link_id',unselected_ids
    );
    CALL build.proc_commit_table(func_commit_interaction_set.root_interaction_id,'interaction_link','',unselected_ids);
    CALL build.proc_commit_table(func_commit_interaction_set.root_interaction_id,'interaction_link_interval','
        JOIN build.interaction_link as build_interaction_link 
        ON build_interaction_link_interval.interaction_link_id = build_interaction_link.id',unselected_ids
    );

    -- Remove any live nodes or schedules from the interaction set if they don't exist at all on build, if a node moved you will need to push the set that now has it
    DELETE FROM live.interaction_link as live_interaction_link
    WHERE live_interaction_link.root_interaction_id = func_commit_interaction_set.root_interaction_id
    AND live_interaction_link.id NOT IN (
        SELECT build_interaction_link.id 
        FROM build.interaction_link as build_interaction_link 
        WHERE build_interaction_link.root_interaction_id = func_commit_interaction_set.root_interaction_id
    );

    DELETE FROM live.interaction_link_interval as live_interaction_link_interval
    USING live.interaction_link as live_interaction_link
    WHERE live_interaction_link_interval.interaction_link_id = live_interaction_link.id
    AND live_interaction_link.root_interaction_id = func_commit_interaction_set.root_interaction_id
    AND live_interaction_link_interval.id NOT IN (
        SELECT build_interaction_link_interval.id 
        FROM build.interaction_link_interval as build_interaction_link_interval
        JOIN build.interaction_link as build_interaction_link
        ON build_interaction_link_interval.interaction_link_id = build_interaction_link.id
        WHERE build_interaction_link.root_interaction_id = func_commit_interaction_set.root_interaction_id
    );
    
    SET search_path TO build;

    -- Update the interaction set update to now    
    INSERT INTO build.interaction_set_update (interaction_set, "updated") VALUES (func_commit_interaction_set.root_interaction_id, NOW()) ON CONFLICT (interaction_set) DO UPDATE SET "updated" = NOW();

    /*
    -- find any child nodes on this set which contain shared interactions (whose children would be on a different set)
    -- commit those shared child interaction sets as well
    -- @TODO this should probably properly handle recurssion before we push it
    -- @TODO do we even want to do this? Should this maybe be handled more by the ui asking you to commit the referenced shared interactions
    FOR TABLE_RECORD in 
        SELECT child_interaction_link.* FROM interaction_link as parent_interaction_link
        join interaction_link as child_interaction_link
        on parent_interaction_link.child_interaction_id = child_interaction_link.parent_interaction_id
        and child_interaction_link.parent_interaction_id = child_interaction_link.root_interaction_id  
        WHERE parent_interaction_link.root_interaction_id = func_commit_interaction_set.root_interaction_id
    LOOP 
        IF TABLE_RECORD.shared = true THEN
		    PERFORM build.func_commit_interaction_set(TABLE_RECORD.child_interaction_id, unselected_ids);
        END IF;
    END LOOP;
    */
    
    END;
$BODY$;

/*
Delay implementing this... 
should user groups and privs just be set up on live and used exactly the same on build?
should user groups and privs be versioned but individual users fall into the same category as anchors and surveys? <= winner so far
should individual users also be versioned? (then we can't just use the mysql users table as a temporary users table)

CREATE OR REPLACE VIEW user_group_changes AS
    SELECT user_group_id, GREATEST(MAX(user_group.modified), MAX(user_group_membership.modified), MAX("user".modified), MAX(user_group_privilege.modified))
    FROM user_group
    LEFT JOIN user_group_membership ON user_group.id = user_group_membership.parent_user_group_id
    LEFT JOIN "user" ON user_group_membership.child_user = "user".id
    LEFT JOIN user_group_privilege ON user_group_privilege.child_user = "user".id OR user_group_privilege.user_group_id = user_group.id;

CREATE OR REPLACE FUNCTION commit_user_group(id bigint, force boolean)
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
BEGIN
    -- TODO I'm not sure what to do with this exactly, what about committing individual users and so forth?
END;$$;
*/


/*
NOTE keeping this for the interesting use of selecting data stored in an array and casting it as a table row

CREATE OR REPLACE FUNCTION tf_customization_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    IF NEW.fk IS NULL THEN
      EXECUTE format('SELECT nextval(%L::regclass)','' || NEW.table || '_id_seq') INTO NEW.fk;
    END IF;

    RETURN NEW;
END;$$;
DROP TRIGGER IF EXISTS tf_customization_insert ON customization;
CREATE TRIGGER tf_customization_insert BEFORE INSERT ON customization FOR EACH ROW EXECUTE FUNCTION tf_customization_insert();


CREATE OR REPLACE FUNCTION fn_customization_push(
    table text, fk bigint)
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
BEGIN
    -- TODO update
    EXECUTE format('INSERT INTO %I 
            SELECT (record::%I).*
            FROM customization
            WHERE customization.table = %L
            AND customization.fk = %L
            AND customization.test = true
            AND customization
        )',table, table, table, fk);
END;$$;
*/