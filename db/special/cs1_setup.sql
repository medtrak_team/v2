-- mysql foreign data wrapper setup
CREATE EXTENSION IF NOT EXISTS mysql_fdw;
CREATE SERVER IF NOT EXISTS mysql_production
	FOREIGN DATA WRAPPER mysql_fdw
	OPTIONS (host 'db_prod.caresense.com', port '3306');
CREATE SERVER IF NOT EXISTS mysql_staging
	FOREIGN DATA WRAPPER mysql_fdw
	OPTIONS (host 'db_staging.caresense.com', port '3306');
CREATE SERVER IF NOT EXISTS mysql_test
	FOREIGN DATA WRAPPER mysql_fdw
	OPTIONS (host 'db_test.caresense.com', port '3306');
CREATE USER MAPPING IF NOT EXISTS FOR postgres
  SERVER mysql_production
  OPTIONS (username 'CSWebUser', password 'Pecan314');
CREATE USER MAPPING IF NOT EXISTS FOR postgres
  SERVER mysql_staging
  OPTIONS (username 'CSWebUser', password 'Pecan314');
CREATE USER MAPPING IF NOT EXISTS FOR postgres
  SERVER mysql_test
  OPTIONS (username 'CSWebUser', password 'Pecan314');