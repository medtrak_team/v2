-- ******************** Auth ********************
CREATE SCHEMA IF NOT EXISTS pgcrypto;
SET search_path TO pgcrypto;
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE OR REPLACE FUNCTION pgcrypto.url_encode(data bytea) RETURNS text LANGUAGE sql AS $$
    SELECT translate(encode(data, 'base64'), E'+/=\n', '-_');
$$ IMMUTABLE;

CREATE OR REPLACE FUNCTION pgcrypto.sign(payload json, secret text)
RETURNS text LANGUAGE sql AS $$
WITH
  header AS (
    SELECT pgcrypto.url_encode(convert_to('{"alg":"HS256","typ":"JWT"}', 'utf8')) AS data
    ),
  payload AS (
    SELECT pgcrypto.url_encode(convert_to(payload::text, 'utf8')) AS data
    ),
  signables AS (
    SELECT header.data || '.' || payload.data AS data FROM header, payload
    )
SELECT
    signables.data || '.' || pgcrypto.url_encode(pgcrypto.hmac(signables.data, secret, 'sha256'))
    FROM signables;
$$ IMMUTABLE;

SET search_path TO shared;

CREATE OR REPLACE FUNCTION shared.tf_encrypt_pass() RETURNS trigger 
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
  IF TG_OP = 'INSERT' or NEW.password <> OLD.password THEN
    NEW.password = pgcrypto.crypt(NEW.password, pgcrypto.gen_salt('bf'));
  END IF;
  RETURN NEW;
END;$$;

CREATE OR REPLACE FUNCTION shared.login(
    email text, password text)
    RETURNS text
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
    userid bigint;
    jwt text;
    display text;
BEGIN
    SELECT "user".id, "user".full_name as display
    FROM "user"
    WHERE "user".email = login.email
    AND "user".password = pgcrypto.crypt(login.password, "user".password)
    INTO userid, display;

    IF userid IS NULL THEN
        raise invalid_password using message = 'invalid user or password';
    END IF;

    SELECT pgcrypto.sign(row_to_json(r), current_setting('auth.secret'))
    FROM (SELECT 'webuser' as role, userid, display, extract(epoch from now())::integer + 60*60 as exp) r
    INTO jwt;

    PERFORM set_config('auth.userid'::text, userid::text, false);

    RETURN jwt;
END;$$;

CREATE OR REPLACE FUNCTION shared.admin_login(
    userid bigint)
    RETURNS text
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
    jwt text;
    display text;
BEGIN
    SELECT "user".id, "user".full_name as display
    FROM "user"
    WHERE "user".id = admin_login.userid
    INTO userid, display;

    IF userid IS NULL THEN
        raise invalid_authorization_specification using message = 'user does not exist';
    END IF;

    SELECT pgcrypto.sign(row_to_json(r), current_setting('auth.secret'))
    FROM (SELECT 'caresense_admin' as role, userid, display, extract(epoch from now())::integer + 60*60 as exp) r
    INTO jwt;

    PERFORM set_config('auth.userid'::text, userid::text, false);

    RETURN jwt;
END;$$;

-- @todo this is just a starting point, currently there is no token table, also there is no adminuser type user
CREATE OR REPLACE PROCEDURE shared.token_login(
    token text)
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
    userid bigint;
    admin boolean;
BEGIN
    -- @todo how do we limit retries? detect same source? in the web server or here?
    SELECT userid,admin FROM "token"
    WHERE "token".token = token
    INTO userid,admin;

    IF userid IS NULL OR admin IS NULL THEN
        raise invalid_password using message = 'invalid token';
    END IF;

    IF current_user = 'authenticator' THEN
        PERFORM set_config('auth.userid', userid::text, false);
        IF admin THEN
            SET ROLE adminuser;
        ELSE
            SET ROLE webuser;
        END IF;
    END IF;
END;$$;

CREATE OR REPLACE FUNCTION fn_get_authenticated_user()
    RETURNS bigint
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
BEGIN
    IF current_setting('auth.userid', true) IS NOT NULL THEN
        RETURN current_setting('auth.userid', true);
    ELSEIF current_setting('request.jwt.claims', true) IS NOT NULL THEN
        PERFORM set_config('auth.userid', current_setting('request.jwt.claims', true)::json->>'userid', false);
        RETURN current_setting('auth.userid', true);
    END IF;
    RETURN NULL;
END;$$;