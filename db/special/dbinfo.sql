CREATE SCHEMA IF NOT EXISTS dbinfo;
SET search_path TO dbinfo;

-- ***************** functions to support schema definition ********************
CREATE OR REPLACE PROCEDURE fn_create_or_update_enum(
    type_name text, attributes text[])
    LANGUAGE 'plpgsql'
    AS $$
DECLARE attribute_definition text;
BEGIN
    IF NOT EXISTS (
        SELECT 1 FROM pg_type WHERE typname = type_name AND typnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'shared')
    ) THEN
        EXECUTE 'CREATE TYPE shared."'||type_name||'" AS ENUM ()';
    END IF;
    FOREACH attribute_definition IN ARRAY attributes LOOP
        EXECUTE 'ALTER TYPE shared."'||type_name||'" ADD VALUE IF NOT EXISTS '''||attribute_definition||'''';
    END LOOP;
END;$$;

CREATE OR REPLACE PROCEDURE fn_create_or_update_type(
    type_name text, attributes text[])
    LANGUAGE 'plpgsql'
    AS $$
DECLARE attribute_definition text;
BEGIN
    IF NOT EXISTS (
        SELECT 1 FROM pg_type WHERE typname = type_name AND typnamespace = (SELECT oid FROM pg_namespace WHERE  nspname = 'shared')
    ) THEN
        EXECUTE 'CREATE TYPE shared."'||type_name||'" AS ()';
    END IF;
    FOREACH attribute_definition IN ARRAY attributes LOOP
        CALL dbinfo.fn_alter_type_add_attribute(type_name,left(attribute_definition,strpos(attribute_definition,' ')-1),right(attribute_definition,-strpos(attribute_definition,' ')+1));
    END LOOP;
END;$$;

CREATE OR REPLACE PROCEDURE fn_alter_type_add_attribute(
    type_name text, attribute text, attribute_type text)
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM pg_type t
        JOIN pg_class c ON c.oid = t.typrelid
        JOIN pg_attribute a ON a.attrelid = c.oid
        WHERE t.typname = type_name
        AND a.attname = attribute
        AND t.typnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'shared')
    ) THEN
        EXECUTE 'ALTER TYPE shared."'||type_name||'" ADD ATTRIBUTE "'||attribute||'" '||attribute_type;        
    END IF;
END;$$;

CREATE OR REPLACE PROCEDURE fn_create_or_update_table(
    table_name text, columns text[])
    LANGUAGE 'plpgsql'
    AS $$
DECLARE column_definition text;
BEGIN
    EXECUTE 'CREATE TABLE IF NOT EXISTS "'||table_name||'" ()';
    FOREACH column_definition IN ARRAY columns LOOP
        IF column_definition = 'id' THEN
            CALL dbinfo.fn_alter_table_add_id(table_name);
        ELSE
            EXECUTE 'ALTER TABLE "'||table_name||'" ADD COLUMN IF NOT EXISTS "'
            ||left(column_definition,strpos(column_definition,' ')-1)||'" '
            ||right(column_definition,-strpos(column_definition,' ')+1);
        END IF;
    END LOOP;
END;$$;

DROP PROCEDURE IF EXISTS fn_alter_table_add_id;
CREATE OR REPLACE PROCEDURE fn_alter_table_add_id(
    t_name text)
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
BEGIN
    IF NOT EXISTS (
        SELECT column_name 
        FROM information_schema.columns 
        WHERE columns.table_schema=current_schema() and columns.table_name=t_name and column_name='id'
    ) THEN
        EXECUTE '
            ALTER TABLE "'||t_name||'" ADD id bigint NOT NULL;
            CREATE SEQUENCE '||t_name||'_id_seq
                START WITH 1
                INCREMENT BY 1024
                NO MINVALUE
                NO MAXVALUE
                CACHE 1;
            ALTER SEQUENCE '||t_name||'_id_seq OWNED BY "'||t_name||'".id;
            ALTER TABLE ONLY "'||t_name||'" ALTER COLUMN id SET DEFAULT nextval('''||t_name||'_id_seq''::regclass);
            ALTER TABLE ONLY "'||t_name||'"
                ADD CONSTRAINT '||t_name||'_pkey PRIMARY KEY (id);        
        ';
    END IF;
END;$$;

CREATE OR REPLACE PROCEDURE fn_alter_table_add_constraint(
    t_name text, c_name text, constraint_sql text)
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
BEGIN
    IF NOT EXISTS (
        SELECT conname
        FROM pg_constraint
        WHERE connamespace = current_schema()::regnamespace
        AND conrelid = t_name::regclass
        AND conname = c_name
    ) THEN
        EXECUTE 'ALTER TABLE '||t_name||' ADD CONSTRAINT '||c_name||' '||constraint_sql;
    END IF;
END;$$;