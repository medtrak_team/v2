CALL dbinfo.fn_create_or_update_table('migration_last_run','{
        filepath text,
        last_run timestamp
    }');
CREATE UNIQUE INDEX IF NOT EXISTS in_migration_last_run_unique ON migration_last_run (filepath);

INSERT INTO migration_last_run
SELECT * FROM dbinfo.migration_last_run
ON CONFLICT DO NOTHING;

/*
	This file is a temporary solution for a more featureful migration system
	Put things here that need to be run once, run them, and then remove them from here or make sure they can only run once
	Put a date on the queries if you do check them in so that we can see if they've already been run more easily
	Make sure anything you commit version control here doesn't cause problems if it's run more than once
*/

DROP FUNCTION IF EXISTS tf_generate_interaction_parent_interaction_link_upsert CASCADE;
DROP FUNCTION IF EXISTS tf_interaction_parent_flatten_insert CASCADE;
DROP FUNCTION IF EXISTS tf_generate_interaction_parent_interaction_node_upsert CASCADE;
DROP FUNCTION IF EXISTS tf_generate_link_path_anchor_upsert CASCADE;
DROP FUNCTION IF EXISTS tf_anchor_update_interaction_schedule_cache CASCADE;
DROP FUNCTION IF EXISTS tr_generate_anchor_property_anchor_upsert CASCADE;
DROP FUNCTION IF EXISTS tf_anchor_property_update_interaction_schedule_cache CASCADE;
DROP FUNCTION IF EXISTS tf_anchor_upsert CASCADE;
DROP FUNCTION IF EXISTS tf_interaction_link_update_interaction_schedule_cache CASCADE;
DROP FUNCTION IF EXISTS tf_interaction_link_interval_update_interaction_schedule_cache CASCADE;
DROP FUNCTION IF EXISTS tf_interval_update_interaction_schedule_cache CASCADE;
DROP FUNCTION IF EXISTS tf_interaction_instance_property_update_interaction_schedule_cache CASCADE;
DROP FUNCTION IF EXISTS tr_generate_subject_data_upsert CASCADE;
DROP FUNCTION IF EXISTS tf_generate_subject_data_upsert CASCADE;
DROP FUNCTION IF EXISTS tf_subject_data_update_interaction_schedule_cache CASCADE;
DROP FUNCTION IF EXISTS tf_subject_data_value_update_interaction_schedule_cache CASCADE;
DROP FUNCTION IF EXISTS tf_subject_data_instance_update_interaction_schedule_cache CASCADE;
DROP FUNCTION IF EXISTS tf_condition_update_interaction_schedule_cache CASCADE;
DROP FUNCTION IF EXISTS tf_generate_link_path_interaction_link_change CASCADE;

ALTER TABLE contact_log
DROP COLUMN IF EXISTS communication_id CASCADE,
DROP COLUMN IF EXISTS interaction_id CASCADE,
DROP COLUMN IF EXISTS related_interaction_ids CASCADE,
DROP COLUMN IF EXISTS instance CASCADE;
