DROP FUNCTION IF EXISTS stress_test_setup;
CREATE OR REPLACE FUNCTION stress_test_setup(size INTEGER)
RETURNS VOID AS $$
DECLARE
    i INT := 1;
BEGIN
	PERFORM truncate_test();

	INSERT INTO condition (id,"root_condition_id",parent_condition_id,conditional,key,value,operator) VALUES 
	(1, NULL, NULL, 'or', NULL, NULL, NULL),
	(2, 1, 1, 'anchor_property', 'anatomy', 'left knee', '='),
	(3, 1, 1, 'anchor_property', 'anatomy', 'right knee', '='),
	(4, NULL, NULL, 'or', NULL, NULL, NULL),
	(5, 4, 4, 'anchor_property', 'anatomy', 'left hip', '='),
	(6, 4, 4, 'anchor_property', 'anatomy', 'right hip', '=');
	
	INSERT INTO interaction (id, name, properties) VALUES (1, 'Pathway', '{}');
	INSERT INTO interaction (id, name, properties) VALUES (2, 'HOOS JR', '{}'); -- interaction id 7
	INSERT INTO interaction (id, name, properties) VALUES (3, 'KOOS JR', '{}'); -- interaction id 8
	INSERT INTO interaction (id, name, properties) VALUES (4, 'PROMIS 10 SF v1.1', '{}'); -- interaction id 10

	INSERT INTO interaction_instance_property VALUES (1, 2, 'anatomy');
	INSERT INTO interaction_instance_property VALUES (2, 3, 'anatomy');

	INSERT INTO interaction_link (id, "root_interaction_id", parent_interaction_id, ordinal, child_interaction_id, condition_id) VALUES (1, 1, 1, 1, 2, 4);
	INSERT INTO interaction_link (id, "root_interaction_id", parent_interaction_id, ordinal, child_interaction_id, condition_id) VALUES (2, 1, 1, 2, 3, 1);
	INSERT INTO interaction_link (id, "root_interaction_id", parent_interaction_id, ordinal, child_interaction_id, condition_id) VALUES (3, 1, 1, 3, 4, NULL);

	INSERT INTO communication_set (id) VALUES (1);
	
	INSERT INTO communication (id,communication_set_id,start,"end",properties) VALUES
	(1,1,'0','-0','{"priority":"first communication important","count":"1","notification_minimum_frequency":"1"}'),
	(2,1,'1','-0','{"priority":"first reminder important","count":"1","notification_minimum_frequency":"1"}'),
	(3,1,'50%','-0','{"priority":"gentle reminder","notification_minimum_frequency":"33%"}');

	INSERT INTO "interval" (id,interval_set_id,name,start_day,end_day,communication_set_id) VALUES 
	(1, NULL, 'Preoperative', -100, -1, 1),
	(2, NULL, '2 Weeks', 0, 28, 1),
	(3, NULL, '3 Months', 29, 120, 1),
	(4, NULL, '6 Months', 121, 240, 1),
	(5, NULL, '1 Year', 121, 240, 1);

	INSERT INTO interaction_link_interval (id, interaction_link_id, interval_id, condition_id, communication_set_id) VALUES 
	(1, 1, 1, NULL, NULL),
	(2, 1, 2, NULL, NULL),
	(3, 1, 3, NULL, NULL),
	(4, 1, 4, NULL, NULL),
	(5, 1, 5, NULL, NULL),
	(6, 2, 1, NULL, NULL),
	(7, 2, 2, NULL, NULL),
	(8, 2, 3, NULL, NULL),
	(9, 2, 4, NULL, NULL),
	(10, 2, 5, NULL, NULL),
	(11, 3, 1, NULL, NULL),
	(12, 3, 2, NULL, NULL),
	(13, 3, 3, NULL, NULL),
	(14, 3, 4, NULL, NULL),
	(15, 3, 5, NULL, NULL);

    WHILE i <= size LOOP
        INSERT INTO anchor (subject_id, interaction_id, active, final, date, timestamp, properties)
        VALUES (
            i,
            1,
            TRUE,
            TRUE,
            NOW() - random()*interval '2 year' + interval '1 year',
            NULL,
			('{"anatomy": "' || CASE
			    WHEN random() < 0.25 THEN 'left knee'
				WHEN random() < 0.33 THEN 'right knee'
				WHEN random() < 0.5 THEN 'left hip'
	            ELSE 'right hip'
			END || '"}')::JSONB
        );
        i := i + 1;
    END LOOP;
END;
$$ LANGUAGE plpgsql;
