--@todo we shouldn't need to run test_setup_condition at the start of each test, but for some reason we do

create or replace function test_setup_condition() returns void as $$
declare
begin
	PERFORM truncate_test();

	INSERT INTO condition (id,"root_condition_id",parent_condition_id,conditional,key,value,operator,"not") VALUES 
	(1, NULL, NULL, 'and', NULL, NULL, NULL, false),
	(2, 1, 1, 'anchor_property', 'test', 'test element condition', '=', false),
	(16, 1, 1, 'anchor_property', 'test', 'test not condition', '=', true),
	(3, NULL, NULL, 'anchor_property', 'test', 'test interval condition', '=', false),
	(4, NULL, NULL, 'anchor_property', 'test', 'test parent condition', '=', false),
	-- any anatomy
	(5, NULL, NULL, 'or', NULL, NULL, NULL, false),
	-- any knee
	(6, NULL, NULL, 'or', NULL, NULL, NULL, false),
	(7, 6, 6, 'anchor_property', 'anatomy', 'left knee', '=', false),
	(8, 6, 6, 'anchor_property', 'anatomy', 'right knee', '=', false),
	-- any hip
	(9, NULL, NULL, 'or', NULL, NULL, NULL, false),
	(10, 9, 9, 'anchor_property', 'anatomy', 'left hip', '=', false),
	(11, 9, 9, 'anchor_property', 'anatomy', 'right hip', '=', false),
	-- back
	(12, NULL, NULL, 'anchor_property', 'anatomy', 'back', '=', false),
	
	(13, 5, 5, 'condition', NULL, 6, NULL, false),
	(14, 5, 5, 'condition', NULL, 9, NULL, false),
	(15, 5, 5, 'condition', NULL, 12, NULL, false);

	INSERT INTO interaction (id,name,properties) VALUES 
	(1, 'test child pathway', '{"type":"pathway"}'),
	(2, 'pain survey', '{"type":"cs1survey"}'),
	(3, 'On a scale from 1 to 10 with 10 being the worst pain imaginable how is your pain?', '{"type":"question"}'),
	(4, 'Welcome to CareSense! We will be sending you a survey before your surgery, 2 weeks after your surgery, and 3 months after your surgery. These surveys will help your doctor understand how you are recovering. Thank you for participating!', '{"type":"question"}'),
	(5, 'test parent pathway', '{"type":"pathway"}'),
	(6, 'test cs1survey pathway', '{"type":"pathway"}'), -- interaction id 6
	(7, 'HOOS JR 2.0', '{"type":"cs1survey","cache_file":"HOOS-JR-2.0"}'), -- interaction id 7
	(8, 'KOOS JR 2.0', '{"type":"cs1survey","cache_file":"KOOS-JR-2.0"}'), -- interaction id 8
	(9, 'Oswestry', '{"type":"cs1survey","cache_file":"Oswestry"}'), -- interaction id 9
	(10, 'PROMIS 10 SF v1.1', '{"type":"cs1survey","cache_file":"PROMIS-10-SF-v1.1"}'), -- interaction id 10
	(99, 'test account', '{"type":"account"}');

	INSERT INTO interaction_instance_property VALUES (2, 7, 'anatomy');
	INSERT INTO interaction_instance_property VALUES (3, 8, 'anatomy');
	INSERT INTO interaction_instance_property VALUES (4, 9, 'anatomy');

	INSERT INTO interaction_link (id, "root_interaction_id", parent_interaction_id, ordinal, child_interaction_id, condition_id) VALUES 
	(1, 1, 1, 2, 2, 1),
	(2, 2, 2, 1, 3, NULL),
	(3, 1, 1, 1, 4, NULL),
	(4, 5, 5, 1, 1, 4),
	-- Put the cs1surveys on the test cs1survey pathway
	(5, 6, 6, 1, 7, 9),  --hoos, interaction_link id 5
	(6, 6, 6, 2, 8, 6),  --koos, interaction_link id 6
	(7, 6, 6, 3, 9, 12),  --oswestry, interaction_link id 7
	(8, 6, 6, 4, 10, 5), --promis10, interaction_link id 8
	(9, 1, 99, 1, 5, NULL); -- link parent pathway to account


	INSERT INTO "interval" VALUES (1, NULL, 'Preoperative', -100, -1);

	INSERT INTO interaction_link_interval (id,interaction_link_id,interval_id,condition_id,communication_set_id) VALUES 
	(1, 1, 1, 3, NULL),
	-- Add hoos, koos, oswestry and promis10 preoperatively
	(5, 5, 1, NULL, NULL),
	(6, 6, 1, NULL, NULL),
	(7, 7, 1, NULL, NULL),
	(8, 8, 1, NULL, NULL);

	INSERT INTO anchor (id, subject_id, interaction_id, "date", properties) VALUES 
	(1, 1, 5, '2022-03-01', '{"test":["test element condition","test interval condition","test parent condition"]}'),
	(2, 1, 6, CURRENT_DATE + 1, '{}');
end;$$ language plpgsql set search_path from current;

drop function if exists test.test_case_conditions_data_change_anchor_property;
create or replace function test_case_conditions_data_change_anchor_property() returns void as $$
declare
	is_active boolean;
begin
	perform pgunit.test_setup_condition();
	UPDATE anchor SET properties = '{"test":["test interval condition","test parent condition"]}' WHERE id = 1;
	PERFORM fn_interaction_schedule_recache();
	SELECT EXISTS( SELECT 1 FROM interaction_schedule WHERE anchor_id = 1 AND interaction_link_interval_id = 1) INTO is_active;
	perform test_assertTrue('Removing anchor property did not turn condition off', NOT is_active);
	UPDATE anchor SET self_reported_properties = '{"test":["test element condition","test interval condition","test parent condition"]}' WHERE id = 1;
	PERFORM fn_interaction_schedule_recache();
	SELECT EXISTS( SELECT 1 FROM interaction_schedule WHERE anchor_id = 1 AND interaction_link_interval_id = 1) INTO is_active;
	perform test_assertTrue('Adding anchor property back did not turn condition on', is_active);
end;$$ language plpgsql set search_path from current;

drop function if exists test.test_case_conditions_not_condition;
create or replace function test_case_conditions_not_condition() returns void as $$
declare
	is_active boolean;
begin
	perform pgunit.test_setup_condition();
	UPDATE anchor SET properties = '{"test":["test element condition","test interval condition","test parent condition","test not condition"]}' WHERE id = 1;
	PERFORM fn_interaction_schedule_recache();
	SELECT EXISTS( SELECT 1 FROM interaction_schedule WHERE subject_id = 1 AND anchor_id = 1 AND interaction_link_interval_id = 1) INTO is_active;
	perform test_assertTrue('Adding "not" anchor property did not turn condition off', NOT is_active);
	UPDATE anchor SET properties = '{"test":["test element condition","test interval condition","test parent condition"]}' WHERE id = 1;
	PERFORM fn_interaction_schedule_recache();
	SELECT EXISTS( SELECT 1 FROM interaction_schedule WHERE subject_id = 1 AND anchor_id = 1 AND interaction_link_interval_id = 1) INTO is_active;
	perform test_assertTrue('Removing "not" anchor property did not turn condition back on', is_active);
end;$$ language plpgsql set search_path from current;

drop function if exists test.test_case_conditions_interaction_link_interval_condition;
create or replace function test_case_conditions_interaction_link_interval_condition() returns void as $$
declare
	is_active boolean;
begin
	perform pgunit.test_setup_condition();
	UPDATE anchor SET properties = '{"test":["test element condition","test parent condition"]}' WHERE id = 1;
	PERFORM fn_interaction_schedule_recache();
	SELECT EXISTS( SELECT 1 FROM interaction_schedule WHERE anchor_id = 1 AND interaction_link_interval_id = 1) INTO is_active;
	perform test_assertTrue('Removing anchor property did not turn condition off', NOT is_active);
	UPDATE anchor SET properties = '{"test":["test element condition","test interval condition","test parent condition"]}' WHERE id = 1;
	PERFORM fn_interaction_schedule_recache();
	SELECT EXISTS( SELECT 1 FROM interaction_schedule WHERE anchor_id = 1 AND interaction_link_interval_id = 1) INTO is_active;
	perform test_assertTrue('Adding anchor property back did not turn condition on', is_active);
end;$$ language plpgsql set search_path from current;

drop function if exists test.test_case_conditions_parent_pathway_condition;
create or replace function test_case_conditions_parent_pathway_condition() returns void as $$
declare
	is_active boolean;
begin
	perform pgunit.test_setup_condition();
	UPDATE anchor SET properties = '{"test":["test element condition","test interval condition"]}' WHERE id = 1;
	PERFORM fn_interaction_schedule_recache();
	SELECT EXISTS( SELECT 1 FROM interaction_schedule WHERE anchor_id = 1 AND interaction_link_interval_id = 1) INTO is_active;
	perform test_assertTrue('Removing anchor property did not turn condition off', NOT is_active);
	UPDATE anchor SET properties = '{"test":["test element condition","test interval condition","test parent condition"]}' WHERE id = 1;
	PERFORM fn_interaction_schedule_recache();
	SELECT EXISTS( SELECT 1 FROM interaction_schedule WHERE anchor_id = 1 AND interaction_link_interval_id = 1) INTO is_active;
	perform test_assertTrue('Adding anchor property back did not turn condition on', is_active);
end;$$ language plpgsql set search_path from current;

drop function if exists test.test_case_conditions_condition_condition_evaluates;
create or replace function test_case_conditions_condition_condition_evaluates() returns void as $$
declare
	row_exists boolean;
begin
	perform pgunit.test_setup_condition();
	UPDATE anchor SET properties = '{"anatomy":["back","left knee"]}' WHERE id = 2;
	PERFORM fn_interaction_schedule_recache();
	SELECT EXISTS( SELECT 1 FROM interaction_schedule where subject_id = 1 and anchor_id = 2 and interaction_link_id = 7 and instance = '{"anatomy":"back"}' and active = true) into row_exists;
	perform test_assertTrue('Adding anchor property did not turn condition on', row_exists);
	PERFORM fn_interaction_schedule_recache();
	SELECT EXISTS( SELECT 1 FROM interaction_schedule where subject_id = 1 and anchor_id = 2 and interaction_link_id = 7 and instance = '{"anatomy":"left knee"}' and active = true) into row_exists;
	perform test_assertTrue('Adding anchor property did turn the condition on for the WRONG instance', NOT row_exists);
	UPDATE anchor SET properties = '{}' WHERE id = 2;
	PERFORM fn_interaction_schedule_recache();
	SELECT EXISTS( SELECT 1 FROM interaction_schedule where subject_id = 1 and anchor_id = 2 and interaction_link_id = 7 and active = false) into row_exists;
	perform test_assertTrue('Removing anchor property did not turn condition off', NOT row_exists);
end;$$ language plpgsql set search_path from current;