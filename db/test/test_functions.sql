/*
-- To run tests execute the following statements adding in the user and password for your local instance
set search_path to test;
select * from pgunit.test_run_all();
*/

-- quickly clear out test schema so we can fill it with new tests
CREATE OR REPLACE FUNCTION truncate_test() RETURNS void AS $$
DECLARE
    table_name text;
    truncate_sql text;
BEGIN
    FOR table_name IN
        SELECT tablename
        FROM pg_tables
        WHERE schemaname = 'test'
    LOOP
        truncate_sql := 'TRUNCATE TABLE ' || quote_ident('test') || '.' || quote_ident(table_name) || ' CASCADE;';
        EXECUTE truncate_sql;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_test_functions()
RETURNS TABLE (
    function_name TEXT,
    function_arguments TEXT
) 
LANGUAGE plpgsql
COST 100
VOLATILE PARALLEL UNSAFE
ROWS 1000
AS $$
BEGIN
    RETURN QUERY
    SELECT
        p.proname::TEXT AS function_name,
        COALESCE(
            STRING_AGG(
                CASE
                    WHEN pg_catalog.pg_get_function_identity_arguments(p.oid) = '' THEN 'No arguments'
                    ELSE pg_catalog.pg_get_function_identity_arguments(p.oid)
                END,
                ', '
            ) FILTER (WHERE pg_catalog.pg_get_function_identity_arguments(p.oid) <> ''),
            'No arguments'
        ) AS function_arguments
    FROM
        pg_proc p
    JOIN
        pg_namespace n ON n.oid = p.pronamespace
    WHERE
        n.nspname IN ('test', 'pgunit')
        AND p.proname ILIKE '%test%'
        AND (
			(array_length(p.proargtypes, 1) = 0) -- the test funcs we care about are the ones without args
	    		OR 
			(p.proname IN ('stress_test_setup')) -- ...save for a few. This is for retrieving test funcs that DO have args
		)
    GROUP BY p.proname;
END;
$$;

