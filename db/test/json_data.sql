-- This function is used to define all of our test data
-- Whenever new JSON is needed for a test case, create a function that inserts it and call it in this function
DROP FUNCTION IF EXISTS define_test_json();
CREATE OR REPLACE FUNCTION define_test_json() RETURNS VOID AS $$
BEGIN
    PERFORM define_small_pathway();
    PERFORM define_sms_data_collection();
    PERFORM define_nested_pathway();
    PERFORM define_nested_interval();
    PERFORM define_nested_interaction();
    PERFORM define_nested_condition();
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS define_small_pathway();
CREATE OR REPLACE FUNCTION define_small_pathway() RETURNS VOID AS $$
BEGIN
    INSERT INTO test_data_interaction_json
    ("name", "interaction_json")
    VALUES
    ('new_small_pathway', '{
        "root": -2,
        "interval": {
            "-1": {
                "id": -1,
                "name": "Test Interval",
                "end_day": "0",
                "start_day": null,
                "recurrence": {},
                "after_created": "0",
                "communication_set_id": -1
            }
        },
        "condition": {},
        "interaction": {
            "-1": {
                "id": "-1",
                "name": "Test Event",
                "shared": false,
                "expanded": false,
                "selected": false,
                "properties": {
                    "text": {
                        "en": "<p dir=\"ltr\" style=\"line-height: 1.2; margin-top: 0pt; margin-bottom: 0pt;\"><span style=\"font-weight: 400;\">Houston Methodist here! Reminder - Dr. { Name from passed data query } would like you to watch this video by clicking this link:</span></p><p><span id=\"docs-internal-guid-7cb74b36-7fff-ef77-070c-e6fdbb7c3c51\">&nbsp;</span></p><p dir=\"ltr\" style=\"line-height: 1.2; margin-top: 0pt; margin-bottom: 0pt;\"><span style=\"font-weight: 400;\">https://player.vimeo.com/video/840233466?h=cda6562f27</span></p>"
                    },
                    "type": "information"
                }
            },
            "-2": {
                "id": -2,
                "name": "Test Small JSON Pathway",
                "shared": true,
                "modified": "2024-09-17T19:42:06.22271",
                "selected": false,
                "properties": {
                    "type": "pathway"
                },
                "interaction_links": [
                    {
                        "id": -1,
                        "interval_ids": [
                            -1
                        ],
                        "child_interaction_id": -1,
                        "parent_interaction_id": -2,
                        "root_interaction_id": -2
                    }
                ]
            }
        },
        "communication_set": {}
    }');
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS define_sms_data_collection();
CREATE OR REPLACE FUNCTION define_sms_data_collection() RETURNS VOID AS $$
BEGIN
    INSERT INTO test_data_interaction_json
    ("name", "interaction_json")
    VALUES
    ('cms_data_collection', '{
        "root": -22529,
        "interval": {
            "-5121": {
            "id": -5121,
            "name": "Preoperative",
            "end_day": 0,
            "modified": "2024-08-02T15:17:42.701493",
            "start_day": -90,
            "recurrence": {},
            "interval_set_id": -4097,
            "communication_set_id": -5121
            },
            "-7169": {
            "id": -7169,
            "name": "1 Year",
            "end_day": 425,
            "modified": "2024-07-18T18:19:03.985665",
            "start_day": 300,
            "recurrence": {},
            "interval_set_id": -4097,
            "communication_set_id": -6145
            }
        },
        "condition": {
            "-15361": {
            "id": -15361,
            "not": false,
            "shared": false,
            "ordinal": 0,
            "modified": "2024-05-24T20:12:06.491949",
            "operator": "=",
            "conditional": "or",
            "condition_ids": [
                -16385,
                -17409
            ],
            "root_condition_id": -15361
            },
            "-16385": {
            "id": -16385,
            "key": "anatomy",
            "not": false,
            "value": "left hip",
            "shared": false,
            "ordinal": 0,
            "modified": "2024-05-24T20:12:06.54014",
            "operator": "=",
            "conditional": "anchor_property",
            "root_condition_id": -15361,
            "parent_condition_id": -15361
            },
            "-17409": {
            "id": -17409,
            "key": "anatomy",
            "not": false,
            "value": "right hip",
            "shared": false,
            "ordinal": 0,
            "modified": "2024-05-24T20:12:06.579547",
            "operator": "=",
            "conditional": "anchor_property",
            "root_condition_id": -15361,
            "parent_condition_id": -15361
            },
            "-18433": {
            "id": -18433,
            "not": false,
            "shared": false,
            "ordinal": 0,
            "modified": "2024-05-24T20:11:44.013077",
            "operator": "=",
            "conditional": "or",
            "condition_ids": [
                -19457,
                -20481
            ],
            "root_condition_id": -18433
            },
            "-19457": {
            "id": -19457,
            "key": "anatomy",
            "not": false,
            "value": "left knee",
            "shared": false,
            "ordinal": 0,
            "modified": "2024-05-24T20:11:44.061651",
            "operator": "=",
            "conditional": "anchor_property",
            "root_condition_id": -18433,
            "parent_condition_id": -18433
            },
            "-20481": {
            "id": -20481,
            "key": "anatomy",
            "not": false,
            "value": "right knee",
            "shared": false,
            "ordinal": 0,
            "modified": "2024-05-24T20:11:44.101054",
            "operator": "=",
            "conditional": "anchor_property",
            "root_condition_id": -18433,
            "parent_condition_id": -18433
            }
        },
        "interaction": {
            "-17409": {
            "id": -17409,
            "name": "KOOS-JR",
            "shared": true,
            "modified": "2024-09-17T19:42:06.22271",
            "properties": {
                "type": "cs1survey",
                "cache_file": "KOOS-JR",
                "instanceFields": [
                "Side"
                ]
            }
            },
            "-22529": {
            "id": -22529,
            "name": "CMS",
            "shared": true,
            "modified": "2024-09-17T19:42:06.22271",
            "properties": {
                "type": "pathway"
            },
            "interaction_links": [
                {
                "id": -19457,
                "ordinal": 1,
                "modified": "2024-05-01T19:01:37.822538",
                "root_interaction_id": -22529,
                "child_interaction_id": -23553,
                "parent_interaction_id": -22529
                }
            ]
            },
            "-23553": {
            "id": -23553,
            "name": "CMS PROs",
            "shared": false,
            "modified": "2024-09-17T19:42:06.22271",
            "properties": {
                "type": "interval_set",
                "interval_set_id": -4097
            },
            "interaction_links": [
                {
                "id": -20481,
                "ordinal": 1,
                "modified": "2024-05-24T20:12:06.629045",
                "condition_id": -15361,
                "interval_ids": [
                    -7169,
                    -5121
                ],
                "root_interaction_id": -22529,
                "child_interaction_id": -24577,
                "parent_interaction_id": -23553
                },
                {
                "id": -21505,
                "ordinal": 2,
                "modified": "2024-05-24T20:11:44.149852",
                "condition_id": -18433,
                "interval_ids": [
                    -7169,
                    -5121
                ],
                "root_interaction_id": -22529,
                "child_interaction_id": -17409,
                "parent_interaction_id": -23553
                },
                {
                "id": -22529,
                "ordinal": 3,
                "modified": "2024-05-01T19:07:10.465475",
                "interval_ids": [
                    -5121,
                    -7169
                ],
                "root_interaction_id": -22529,
                "child_interaction_id": -25601,
                "parent_interaction_id": -23553
                },
                {
                "id": -23553,
                "ordinal": 4,
                "modified": "2024-05-01T19:07:10.465475",
                "interval_ids": [
                    -5121
                ],
                "root_interaction_id": -22529,
                "child_interaction_id": -26625,
                "parent_interaction_id": -23553
                },
                {
                "id": -24577,
                "ordinal": 5,
                "modified": "2024-05-01T19:07:10.465475",
                "interval_ids": [
                    -5121
                ],
                "root_interaction_id": -22529,
                "child_interaction_id": -27649,
                "parent_interaction_id": -23553
                },
                {
                "id": -25601,
                "ordinal": 6,
                "modified": "2024-05-01T19:07:10.465475",
                "interval_ids": [
                    -5121
                ],
                "root_interaction_id": -22529,
                "child_interaction_id": -28673,
                "parent_interaction_id": -23553
                }
            ]
            },
            "-24577": {
            "id": -24577,
            "name": "HOOS-JR-2.0",
            "shared": true,
            "modified": "2024-09-17T19:42:06.22271",
            "properties": {
                "type": "cs1survey",
                "cache_file": "HOOS-JR-2.0",
                "instanceFields": [
                "Side"
                ]
            }
            },
            "-25601": {
            "id": -25601,
            "name": "PROMIS-10-SF-v1.1",
            "shared": true,
            "modified": "2024-09-17T19:42:06.22271",
            "properties": {
                "type": "cs1survey",
                "cache_file": "PROMIS-10-SF-v1.1",
                "instanceFields": []
            }
            },
            "-26625": {
            "id": -26625,
            "name": "SILS-2",
            "shared": true,
            "modified": "2024-09-17T19:42:06.22271",
            "properties": {
                "type": "cs1survey",
                "cache_file": "SILS-2",
                "instanceFields": []
            }
            },
            "-27649": {
            "id": -27649,
            "name": "Total-Painful-Joint-Count",
            "shared": true,
            "modified": "2024-09-17T19:42:06.22271",
            "properties": {
                "type": "cs1survey",
                "cache_file": "Total-Painful-Joint-Count",
                "instanceFields": []
            }
            },
            "-28673": {
            "id": -28673,
            "name": "Quantified-Spinal-Pain",
            "shared": true,
            "modified": "2024-09-17T19:42:06.22271",
            "properties": {
                "type": "cs1survey",
                "cache_file": "Quantified-Spinal-Pain",
                "instanceFields": []
            }
            }
        },
        "communication_set": {
            "-5121": {
            "id": -5121,
            "name": "Standard Preoperative Communications",
            "shared": true,
            "modified": "2024-08-02T15:17:42.968204",
            "communications": [
                {
                "id": -8193,
                "end": "-0 days",
                "start": "1 days",
                "modified": "2024-09-17T19:54:59.427702",
                "properties": {
                    "type": "notification",
                    "count": 1,
                    "message": [
                    {
                        "en": {
                        "text": "This is {{anchor.clinician}}. This survey is important and will only take a few minutes to complete. Once finished, these reminders will stop! Please click this link to take the survey: {{link}}"
                        },
                        "es": {
                        "text": ""
                        },
                        "medium": "sms"
                    },
                    {
                        "en": {
                        "body": "I need to understand how you are doing prior to operating, so that we can see how much you improve after surgery. Please click button below to answer a few questions:",
                        "subject": "{{anchor.clinician}} | Surveys"
                        },
                        "es": {
                        "body": "",
                        "subject": ""
                        },
                        "medium": "email"
                    }
                    ],
                    "priority": "first reminder important",
                    "notification_strategy": "sendmultipletimesperinterval",
                    "notification_minimum_frequency": "1days",
                    "notification_minimum_frequency_unit": "Days"
                },
                "communication_set_id": -5121
                },
                {
                "id": -16385,
                "end": "-2 days",
                "start": "-3 days",
                "modified": "2024-09-17T19:54:59.427702",
                "properties": {
                    "type": "notification",
                    "message": [
                    {
                        "en": {
                        "text": "This is {{anchor.clinician}}. Your surgery is in 3 days. Please fill out a quick survey on your health: {{link}}"
                        },
                        "es": {
                        "text": ""
                        },
                        "medium": "sms"
                    },
                    {
                        "en": {
                        "body": "{{anchor.clinician}} here! Your surgery with me is in 3 days. It is important that you fill out this survey. Please click the following link to take the survey:",
                        "subject": "{{anchor.clinician}} | URGENT - Survey required prior to surgery"
                        },
                        "es": {
                        "body": "",
                        "subject": ""
                        },
                        "medium": "email"
                    }
                    ],
                    "priority": "reminder interval ending",
                    "notification_strategy": "sendonaregularschedule",
                    "notification_minimum_frequency": "1days",
                    "notification_minimum_frequency_unit": "Days"
                },
                "communication_set_id": -5121
                },
                {
                "id": -17409,
                "end": "-1 days",
                "start": "-2 days",
                "modified": "2024-09-17T19:54:59.427702",
                "properties": {
                    "type": "notification",
                    "message": [
                    {
                        "en": {
                        "text": "{{target.first_name}}, this is {{anchor.clinician}}. Before your upcoming surgery, I need to know how you''re doing. Please click this link to take a short survey about your health: {{link}}"
                        },
                        "es": {
                        "text": ""
                        },
                        "medium": "sms"
                    },
                    {
                        "en": {
                        "body": "{{anchor.clinician}} here! Your surgery with me is in 2 days. It is critically important that you fill out this survey. Please click the following link to take the survey:",
                        "subject": "{{anchor.clinician}} | URGENT - Survey required prior to surgery"
                        },
                        "es": {
                        "body": "",
                        "subject": ""
                        },
                        "medium": "email"
                    }
                    ],
                    "priority": "reminder interval ending",
                    "notification_strategy": "sendonaregularschedule",
                    "notification_minimum_frequency": "1days",
                    "notification_minimum_frequency_unit": "Days"
                },
                "communication_set_id": -5121
                },
                {
                "id": -38913,
                "end": "-0 days",
                "start": "0 days",
                "modified": "2024-09-17T19:54:59.427702",
                "properties": {
                    "type": "notification",
                    "count": 1,
                    "message": [
                    {
                        "en": {
                        "text": "{{site_name}} here! {{anchor.clinician}} would like to know how you are doing. Please click this link and answer the questions about your health: {{link}}"
                        },
                        "es": {
                        "text": ""
                        },
                        "medium": "sms"
                    },
                    {
                        "en": {
                        "body": "Before your upcoming surgery with us, we''d like to understand how you are currently feeling. Please click the button below and answer the questions as best you can. This survey will only take a few minutes and is required to move forward:",
                        "subject": "From {{site_name}} | Please read"
                        },
                        "es": {
                        "body": "",
                        "subject": ""
                        },
                        "medium": "email"
                    }
                    ],
                    "priority": "first communication important",
                    "notification_strategy": "sendonceduringinterval",
                    "notification_minimum_frequency": "1"
                },
                "communication_set_id": -5121
                },
                {
                "id": -15361,
                "end": "-0 days",
                "start": "-1 days",
                "modified": "2024-09-17T19:54:59.427702",
                "properties": {
                    "type": "notification",
                    "message": [
                    {
                        "en": {
                        "text": "URGENT - This is {{anchor.clinician}}. Your surgery is tomorrow. I need you to please click this link to complete a quick survey: {{link}}"
                        },
                        "es": {
                        "text": ""
                        },
                        "medium": "sms"
                    },
                    {
                        "en": {
                        "body": "{{anchor.clinician}} here! Your surgery with me is tomorrow. This survey is required. Please click the following link now to complete the survey:",
                        "subject": "{{anchor.clinician}} | URGENT - Survey required prior to surgery"
                        },
                        "es": {
                        "body": "",
                        "subject": ""
                        },
                        "medium": "email"
                    }
                    ],
                    "priority": "last chance",
                    "notification_strategy": "sendonaregularschedule",
                    "notification_minimum_frequency": "1days",
                    "notification_minimum_frequency_unit": "Days"
                },
                "communication_set_id": -5121
                },
                {
                "id": -6145,
                "end": "-3 days",
                "start": "2 days",
                "modified": "2024-09-17T19:54:59.427702",
                "properties": {
                    "type": "notification",
                    "message": [
                    {
                        "en": {
                        "text": "{{site_name}} here! Reminder -  {{anchor.clinician}} would like you to complete the following survey by clicking this link: {{link}}"
                        },
                        "es": {
                        "text": ""
                        },
                        "medium": "sms"
                    },
                    {
                        "en": {
                        "body": "{{anchor.clinician}} here! Reminder - I would like you to complete the following survey by clicking this link before your surgery:",
                        "subject": "{{anchor.clinician}} | Please click to take a quick survey"
                        },
                        "es": {
                        "body": "",
                        "subject": ""
                        },
                        "medium": "email"
                    }
                    ],
                    "priority": "gentle reminder",
                    "notification_strategy": "sendonaregularschedule",
                    "notification_minimum_frequency": "12%",
                    "notification_minimum_frequency_unit": "Percent"
                },
                "communication_set_id": -5121
                }
            ]
            },
            "-6145": {
            "id": -6145,
            "name": "Standard Postoperative Communications",
            "shared": true,
            "modified": "2024-07-18T18:19:04.165783",
            "communications": [
                {
                "id": -20481,
                "end": "-0 days",
                "start": "0 days",
                "modified": "2024-09-17T19:54:59.427702",
                "properties": {
                    "type": "notification",
                    "count": 1,
                    "message": [
                    {
                        "en": {
                        "text": "{{site_name}} here! {{anchor.clinician}} would like to know how you''re doing! Please click the following link to take the survey: {{link}}"
                        },
                        "es": {
                        "text": ""
                        },
                        "medium": "sms"
                    },
                    {
                        "en": {
                        "body": "{{anchor.clinician}} here! I would like to know how you are doing! Please click the following button to take the survey:  ",
                        "subject": "{{anchor.clinician}} | How are you doing at {{interval_name}}?"
                        },
                        "es": {
                        "body": "",
                        "subject": ""
                        },
                        "medium": "email"
                    }
                    ],
                    "priority": "first communication important",
                    "notification_strategy": "sendonceduringinterval",
                    "notification_minimum_frequency": "1"
                },
                "communication_set_id": -6145
                },
                {
                "id": -29697,
                "end": "-0 days",
                "start": "-1 days",
                "modified": "2024-09-17T19:54:59.427702",
                "properties": {
                    "type": "notification",
                    "message": [
                    {
                        "en": {
                        "text": "URGENT - This is {{anchor.clinician}}. This survey needs to be completed. Please click the link to complete a quick survey: {{link}}"
                        },
                        "es": {
                        "text": ""
                        },
                        "medium": "sms"
                    },
                    {
                        "en": {
                        "body": "{{anchor.clinician}} here! URGENT - this survey needs to be completed. Please click the button:",
                        "subject": "{{anchor.clinician}} | URGENT - Survey required"
                        },
                        "es": {
                        "body": "",
                        "subject": ""
                        },
                        "medium": "email"
                    }
                    ],
                    "priority": "last chance",
                    "notification_strategy": "sendonaregularschedule",
                    "notification_minimum_frequency": "1days",
                    "notification_minimum_frequency_unit": "Days"
                },
                "communication_set_id": -6145
                },
                {
                "id": -25601,
                "end": "-2 days",
                "start": "-3 days",
                "modified": "2024-09-17T19:54:59.427702",
                "properties": {
                    "type": "notification",
                    "message": [
                    {
                        "en": {
                        "text": "This is {{anchor.clinician}}. I need to know how you are doing. Please fill out a quick survey on your health:  {{link}}"
                        },
                        "es": {
                        "text": ""
                        },
                        "medium": "sms"
                    },
                    {
                        "en": {
                        "body": "{{anchor.clinician}} here! URGENT - this survey needs to be completed. Please click the button: ",
                        "subject": "{{anchor.clinician}} | URGENT - Survey required"
                        },
                        "es": {
                        "body": "",
                        "subject": ""
                        },
                        "medium": "email"
                    }
                    ],
                    "priority": "reminder interval ending",
                    "notification_strategy": "sendonaregularschedule",
                    "notification_minimum_frequency": "1days",
                    "notification_minimum_frequency_unit": "Days"
                },
                "communication_set_id": -6145
                },
                {
                "id": -21505,
                "end": "2 days",
                "start": "1 days",
                "modified": "2024-09-17T19:54:59.427702",
                "properties": {
                    "type": "notification",
                    "message": [
                    {
                        "en": {
                        "text": "{{site_name}} here! This survey is required by {{anchor.clinician}}. It will only take a few minutes to complete. Once finished, these reminders will stop! Please click the following link now to complete the survey: {{link}}"
                        },
                        "es": {
                        "text": ""
                        },
                        "medium": "sms"
                    },
                    {
                        "en": {
                        "body": "{{anchor.clinician}} here! This survey will only take a few minutes and is required. Once completed, these reminders will stop! Please click the following button to complete the survey: {link}",
                        "subject": "{{anchor.clinician}} | Reminder - I need your {{interval_name}} survey"
                        },
                        "es": {
                        "body": "",
                        "subject": ""
                        },
                        "medium": "email"
                    }
                    ],
                    "priority": "first reminder important",
                    "notification_strategy": "sendonaregularschedule",
                    "notification_minimum_frequency": "1days",
                    "notification_minimum_frequency_unit": "Days"
                },
                "communication_set_id": -6145
                },
                {
                "id": -24577,
                "end": "-3 days",
                "start": "2 days",
                "modified": "2024-09-17T19:54:59.427702",
                "properties": {
                    "type": "notification",
                    "message": [
                    {
                        "en": {
                        "text": "{{site_name}} here! Reminder -  {{anchor.clinician}} would like to know how you are doing. Please click the following link to take the survey: {{link}}"
                        },
                        "es": {
                        "text": ""
                        },
                        "medium": "sms"
                    },
                    {
                        "en": {
                        "body": "{{anchor.clinician}} here! Reminder - I would like to know how you''re doing. Please click the following button to take the survey:  ",
                        "subject": "{{anchor.clinician}} | Please click to complete a survey for your {{interval_name}} postop interval"
                        },
                        "es": {
                        "body": "",
                        "subject": ""
                        },
                        "medium": "email"
                    }
                    ],
                    "priority": "gentle reminder",
                    "notification_strategy": "sendonaregularschedule",
                    "notification_minimum_frequency": "12%",
                    "notification_minimum_frequency_unit": "Percent"
                },
                "communication_set_id": -6145
                },
                {
                "id": -27649,
                "end": "-1 days",
                "start": "-2 days",
                "modified": "2024-09-17T19:54:59.427702",
                "properties": {
                    "type": "notification",
                    "message": [
                    {
                        "en": {
                        "text": "{{target.first_name}}, this is {{anchor.clinician}}. This survey is needed so I can understand how you are doing! Please click the link:  {{link}}"
                        },
                        "es": {
                        "text": ""
                        },
                        "medium": "sms"
                    },
                    {
                        "en": {
                        "body": "{{anchor.clinician}} here! URGENT - this survey needs to be completed. Please click the button ",
                        "subject": "{{anchor.clinician}} | URGENT - Survey required"
                        },
                        "es": {
                        "body": "",
                        "subject": ""
                        },
                        "medium": "email"
                    }
                    ],
                    "priority": "reminder interval ending",
                    "notification_strategy": "sendonaregularschedule",
                    "notification_minimum_frequency": "1days",
                    "notification_minimum_frequency_unit": "Days"
                },
                "communication_set_id": -6145
                }
            ]
            }
        }
        }'
    );
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS define_nested_pathway();
CREATE OR REPLACE FUNCTION define_nested_pathway() RETURNS VOID AS $$
BEGIN
    INSERT INTO test_data_interaction_json
    ("name", "interaction_json")
    VALUES
    ('nested_pathway', '{
        "id": -1,
        "name": "Nested Pathway",
        "shared": true,
        "children": [
            {
                "id": -2,
                "name": "Preoperative Class",
                "shared": false,
                "children": [],
                "expanded": false,
                "selected": false,
                "intervals": [
                    {
                        "id": -1,
                        "name": "Preoperative Class",
                        "shared": false,
                        "end_day": 0,
                        "start_day": null,
                        "after_created": 0,
                        "communication": "Default SMS"
                    }
                ],
                "properties": {
                    "text": {
                        "en": "Don''t forget to attend your preoperative class detailing important information about your upcoming surgery. You can also watch the class virtually by clicking {insert_link_here}."
                    },
                    "type": "sms"
                }
            },
            {
                "id": -3,
                "name": "Welcome Alert",
                "shared": false,
                "children": [],
                "expanded": false,
                "selected": false,
                "intervals": [
                    {
                        "id": -2,
                        "name": "Welcome Alert",
                        "shared": false,
                        "end_day": 0,
                        "start_day": null,
                        "after_created": 0,
                        "communication": "Default SMS"
                    }
                ],
                "properties": {
                    "text": {
                        "en": "Welcome! You will recieve messages based on your surgery."
                    },
                    "type": "sms"
                }
            },
            {
                "id": -4,
                "name": "Welcome to Hospital System!",
                "shared": false,
                "children": [],
                "expanded": false,
                "selected": false,
                "intervals": [
                    {
                        "id": -3,
                        "name": "Welcome to Hospital System!",
                        "shared": false,
                        "end_day": 0,
                        "start_day": null,
                        "after_created": 0,
                        "communication": "Default Email"
                    }
                ],
                "properties": {
                    "text": {
                        "en": "Welcome to the Hospital System!"
                    },
                    "type": "email"
                }
            }
        ],
        "expanded": false,
        "selected": false,
        "intervals": [],
        "properties": {
            "type": "pathway"
        }
    }');
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS define_nested_interval();
CREATE OR REPLACE FUNCTION define_nested_interval() RETURNS VOID AS $$
BEGIN
    INSERT INTO test_data_interaction_json
    ("name", "interaction_json")
    VALUES
    ('nested_interval', '{
        "id": -1,
        "name": "Sample Interval",
        "shared": false,
        "end_day": 1,
        "start_day": 1,
        "after_created": null,
        "communication": "Default SMS"
    }');
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS define_nested_interaction();
CREATE OR REPLACE FUNCTION define_nested_interaction() RETURNS VOID AS $$
BEGIN
    INSERT INTO test_data_interaction_json
    ("name", "interaction_json")
    VALUES
    ('nested_interaction', '{
        "id": -1,
        "name": "Sample Interaction",
        "shared": false,
        "children": [],
        "expanded": false,
        "selected": false,
        "intervals": [
            {
                "id": -1,
                "name": "Sample Interaction",
                "shared": false,
                "end_day": 0,
                "start_day": 0,
                "after_created": null,
                "communication": "Default SMS"
            }
        ],
        "properties": {
            "text": {
                "en": "Some text here"
            },
            "type": "sms"
        }
    }');
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS define_nested_condition();
CREATE OR REPLACE FUNCTION define_nested_condition() RETURNS VOID AS $$
BEGIN
    INSERT INTO test_data_interaction_json
    ("name", "interaction_json")
    VALUES
    ('condition_json', '{
        "not": false,
        "operator": "=",
        "conditional": "or",
        "child_conditions": [
            {
                "not": false,
                "operator": "=",
                "conditional": "anchor_property",
                "key": "ProcedureEpicKey",
                "value": "12345"
            },
            {
                "not": false,
                "operator": "=",
                "conditional": "anchor_property",
                "key": "ProcedureEpicKey",
                "value": "12346"
            }
        ]
    }');
END;
$$ LANGUAGE plpgsql;