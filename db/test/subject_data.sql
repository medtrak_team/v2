DROP FUNCTION IF EXISTS test_setup_subject_data();
CREATE OR REPLACE FUNCTION test_setup_subject_data() RETURNS VOID AS $$
BEGIN
    -- Clear previous test data
    PERFORM truncate_test();  -- Assuming you have this utility function for clearing test data
    
    -- Insert communication set
    INSERT INTO communication_set (id, name) VALUES
        (1, 'Standard Preoperative Communications'),
		(2, 'Standard Postoperative Communications');

    -- Insert communications with start, end, and properties
	INSERT INTO communication (id, communication_set_id, start, "end", properties) VALUES
	    (1, 1, '0 days', '-0 days', '{"type": "notification", "count": 1, "message": [{"en": {"text": "{{site_name}} here! {{anchor.clinician}} would like to know how you are doing. Please click this link and answer the questions about your health: {{link}}"}, "es": {"text": ""}, "medium": "sms"}, {"en": {"body": "Before your upcoming surgery with us, we''d like to understand how you are currently feeling. Please click the button below and answer the questions as best you can. This survey will only take a few minutes and is required to move forward:", "subject": "From {{site_name}} | Please read"}, "es": {"body": "", "subject": ""}, "medium": "email"}], "priority": "first communication important", "notification_strategy": "sendonceduringinterval", "notification_minimum_frequency": "1"}'),
	    (2, 1, '1 days', '-0 days', '{"type": "notification", "count": 1, "message": [{"en": {"text": "This is {{anchor.clinician}}. This survey is important and will only take a few minutes to complete. Once finished, these reminders will stop! Please click this link to take the survey: {{link}}"}, "es": {"text": ""}, "medium": "sms"}, {"en": {"body": "I need to understand how you are doing prior to operating, so that we can see how much you improve after surgery. Please click button below to answer a few questions:", "subject": "{{anchor.clinician}} | Surveys"}, "es": {"body": "", "subject": ""}, "medium": "email"}], "priority": "first reminder important", "notification_strategy": "sendmultipletimesperinterval", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
	    (3, 1, '2 days', '-3 days', '{"type": "notification", "message": [{"en": {"text": "{{site_name}} here! Reminder -  {{anchor.clinician}} would like you to complete the following survey by clicking this link: {{link}}"}, "es": {"text": ""}, "medium": "sms"}, {"en": {"body": "{{anchor.clinician}} here! Reminder - I would like you to complete the following survey by clicking this link before your surgery:", "subject": "{{anchor.clinician}} | Please click to take a quick survey"}, "es": {"body": "", "subject": ""}, "medium": "email"}], "priority": "gentle reminder", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "12%", "notification_minimum_frequency_unit": "Percent"}'),
		(4, 1, '-3 days', '-2 days', '{"type": "notification", "message": [{"en": {"text": "This is {{anchor.clinician}}. Your surgery is in 3 days. Please fill out a quick survey on your health: {{link}}"}, "es": {"text": ""}, "medium": "sms"}, {"en": {"body": "{{anchor.clinician}} here! Your surgery with me is in 3 days. It is important that you fill out this survey. Please click the following link to take the survey:", "subject": "{{anchor.clinician}} | URGENT - Survey required prior to surgery"}, "es": {"body": "", "subject": ""}, "medium": "email"}], "priority": "reminder interval ending", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
		(5, 1, '-2 days', '-1 days', '{"type": "notification", "message": [{"en": {"text": "{{target.first_name}}, this is {{anchor.clinician}}. Before your upcoming surgery, I need to know how you''re doing. Please click this link to take a short survey about your health: {{link}}"}, "es": {"text": ""}, "medium": "sms"}, {"en": {"body": "{{anchor.clinician}} here! Your surgery with me is in 2 days. It is critically important that you fill out this survey. Please click the following link to take the survey:", "subject": "{{anchor.clinician}} | URGENT - Survey required prior to surgery"}, "es": {"body": "", "subject": ""}, "medium": "email"}], "priority": "reminder interval ending", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
	    (6, 1, '-1 days', '-0 days', '{"type": "notification", "message": [{"en": {"text": "URGENT - This is {{anchor.clinician}}. Your surgery is tomorrow. I need you to please click this link to complete a quick survey: {{link}}"}, "es": {"text": ""}, "medium": "sms"}, {"en": {"body": "{{anchor.clinician}} here! Your surgery with me is tomorrow. This survey is required. Please click the following link now to complete the survey:", "subject": "{{anchor.clinician}} | URGENT - Survey required prior to surgery"}, "es": {"body": "", "subject": ""}, "medium": "email"}], "priority": "last chance", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
	    (7, 2, '0 days', '-0 days', '{"type": "notification", "count": 1, "message": [{"en": {"text": "{{site_name}} here! {{anchor.clinician}} would like to know how you''re doing! Please click the following link to take the survey: {{link}}"}, "es": {"text": ""}, "medium": "sms"}, {"en": {"body": "{{anchor.clinician}} here! I would like to know how you are doing! Please click the following button to take the survey:  ", "subject": "{{anchor.clinician}} | How are you doing at {{interval_name}}?"}, "es": {"body": "", "subject": ""}, "medium": "email"}], "priority": "first communication important", "notification_strategy": "sendonceduringinterval", "notification_minimum_frequency": "1"}'),
	    (8, 2, '1 days', '2 days', '{"type": "notification", "message": [{"en": {"text": "{{site_name}} here! This survey is required by {{anchor.clinician}}. It will only take a few minutes to complete. Once finished, these reminders will stop! Please click the following link now to complete the survey: {{link}}"}, "es": {"text": ""}, "medium": "sms"}, {"en": {"body": "{{anchor.clinician}} here! This survey will only take a few minutes and is required. Once completed, these reminders will stop! Please click the following button to complete the survey: {link}", "subject": "{{anchor.clinician}} | Reminder - I need your {{interval_name}} survey"}, "es": {"body": "", "subject": ""}, "medium": "email"}], "priority": "first reminder important", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
	    (9, 2, '2 days', '-3 days', '{"type": "notification", "message": [{"en": {"text": "{{site_name}} here! Reminder -  {{anchor.clinician}} would like to know how you are doing. Please click the following link to take the survey: {{link}}"}, "es": {"text": ""}, "medium": "sms"}, {"en": {"body": "{{anchor.clinician}} here! Reminder - I would like to know how you''re doing. Please click the following button to take the survey:  ", "subject": "{{anchor.clinician}} | Please click to complete a survey for your {{interval_name}} postop interval"}, "es": {"body": "", "subject": ""}, "medium": "email"}], "priority": "gentle reminder", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "12%", "notification_minimum_frequency_unit": "Percent"}'),
	    (10, 2, '-1 days', '-0 days', '{"type": "notification", "message": [{"en": {"text": "URGENT - This is {{anchor.clinician}}. This survey needs to be completed. Please click the link to complete a quick survey: {{link}}"}, "es": {"text": ""}, "medium": "sms"}, {"en": {"body": "{{anchor.clinician}} here! URGENT - this survey needs to be completed. Please click the button:", "subject": "{{anchor.clinician}} | URGENT - Survey required"}, "es": {"body": "", "subject": ""}, "medium": "email"}], "priority": "last chance", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
	    (11, 2, '-2 days', '-1 days', '{"type": "notification", "message": [{"en": {"text": "{{target.first_name}}, this is {{anchor.clinician}}. This survey is needed so I can understand how you are doing! Please click the link:  {{link}}"}, "es": {"text": ""}, "medium": "sms"}, {"en": {"body": "{{anchor.clinician}} here! URGENT - this survey needs to be completed. Please click the button ", "subject": "{{anchor.clinician}} | URGENT - Survey required"}, "es": {"body": "", "subject": ""}, "medium": "email"}], "priority": "reminder interval ending", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
	    (12, 2, '-3 days', '-2 days', '{"type": "notification", "message": [{"en": {"text": "This is {{anchor.clinician}}. I need to know how you are doing. Please fill out a quick survey on your health:  {{link}}"}, "es": {"text": ""}, "medium": "sms"}, {"en": {"body": "{{anchor.clinician}} here! URGENT - this survey needs to be completed. Please click the button: ", "subject": "{{anchor.clinician}} | URGENT - Survey required"}, "es": {"body": "", "subject": ""}, "medium": "email"}], "priority": "reminder interval ending", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}');
    
	-- Insert intervals associated with the communication set
    INSERT INTO "interval" (id, communication_set_id, name, start_day, end_day) VALUES
        (1, 1, 'Preoperative', -90, 0);
    
    -- Insert interactions
    INSERT INTO interaction (id, name, properties) VALUES
        (1, 'Test Pathway Interaction', '{"type":"pathway"}'),
        (2, 'Test Information Interaction', '{"type":"information"}');
    
    -- Insert interaction_link where parent is the pathway and child is the information
    INSERT INTO interaction_link (id, root_interaction_id, parent_interaction_id, ordinal, child_interaction_id) VALUES
        (1, 1, 1, 1, 2);
    
    -- Insert interaction_link_interval to associate interaction node with interval
    INSERT INTO interaction_link_interval (id, interaction_link_id, interval_id) VALUES
        (1, 1, 1);  -- Associate interaction_link with Preoperative interval
    
    -- Assign an anchor to the pathway interaction
    INSERT INTO anchor (id, subject_id, interaction_id, "date") VALUES
        (1, 1, 1, CURRENT_DATE + 90);

END;
$$ LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS test_case_subject_data_update_existing;
CREATE OR REPLACE FUNCTION test_case_subject_data_update_existing() RETURNS VOID AS $$
DECLARE
    v_record_count INT;
    v_value TEXT;
BEGIN
    -- Perform setup for communication
    PERFORM test_setup_communication();

    -- First insert
    INSERT INTO subject_data (subject_id, interaction_id, completed, submitted, data) 
    VALUES (1, 2, NOW(), NOW(), '{"key":"value 1"}');

    -- Check count and value
    SELECT COUNT(*) INTO v_record_count FROM subject_data;
    SELECT data->>'key' INTO v_value FROM subject_data LIMIT 1; 
    PERFORM test_assertTrue(
        CONCAT('Expected one subject_data record with value "value 1" after insertion, got ', v_record_count, ' with value ', v_value), 
        v_record_count = 1 AND v_value = 'value 1'
    );

    -- Second insert with future date (use interval for time addition)
    INSERT INTO subject_data (subject_id, interaction_id, completed, submitted, data) 
    VALUES (1, 2, NOW() + INTERVAL '1 day', NOW() + INTERVAL '1 day', '{"key":"value 2"}');

    -- Check count and value after second insertion
    SELECT COUNT(*) INTO v_record_count FROM subject_data;
    SELECT data->>'key' INTO v_value FROM subject_data LIMIT 1; 
    PERFORM test_assertTrue(
        CONCAT('Expected one subject_data record with value "value 2" after second insertion, got ', v_record_count, ' with value ', v_value), 
        v_record_count = 1 AND v_value = 'value 2'
    );

    -- Third insert with past date (use interval for time subtraction)
    INSERT INTO subject_data (subject_id, interaction_id, completed, submitted, data) 
    VALUES (1, 2, NOW() - INTERVAL '10 days', NOW() - INTERVAL '10 days', '{"key":"value 3"}');

    -- Check count after third insertion
    SELECT COUNT(*) INTO v_record_count FROM subject_data;
    PERFORM test_assertTrue(
        CONCAT('Expected two subject_data records after third insertion, got ', v_record_count), 
        v_record_count = 2
    );

END;
$$ LANGUAGE plpgsql;