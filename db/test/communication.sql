DROP FUNCTION IF EXISTS test_setup_communication();
CREATE OR REPLACE FUNCTION test_setup_communication() RETURNS VOID AS $$
BEGIN
    -- Clear previous test data
    PERFORM truncate_test();  -- Assuming you have this utility function for clearing test data
    
    -- Insert communication set
    INSERT INTO communication_set (id, name) VALUES
        (1, 'Standard Preoperative Communications'),
		(2, 'Standard Postoperative Communications');

    -- Insert communications with start, end, and properties
	INSERT INTO communication (id, communication_set_id, start, "end", properties) VALUES
	    (1, 1, '0 days', '-0 days', '{"type": "notification", "count": 1, "message": {"en": {"text": "{{site_name}} here! {{anchor.clinician}} would like to know how you are doing. Please click this link and answer the questions about your health: {{link}}"}, "es": {"text": ""}}, "medium": "sms", "priority": "first communication important", "notification_strategy": "sendonceduringinterval", "notification_minimum_frequency": "1"}'),
	    (2, 1, '1 days', '-0 days', '{"type": "notification", "count": 1, "message": {"en": {"body": "I need to understand how you are doing prior to operating, so that we can see how much you improve after surgery. Please click button below to answer a few questions:", "subject": "{{anchor.clinician}} | Surveys"}, "es": {"body": "", "subject": ""}}, "medium": "email", "priority": "first reminder important", "notification_strategy": "sendmultipletimesperinterval", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
	    (3, 1, '2 days', '-4 days', '{"type": "notification", "message": {"en": {"text": "{{site_name}} here! Reminder -  {{anchor.clinician}} would like you to complete the following survey by clicking this link: {{link}}"}, "es": {"text": ""}}, "medium": "sms", "priority": "gentle reminder", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "12%", "notification_minimum_frequency_unit": "Percent"}'),
		(4, 1, '-3 days', '-2 days', '{"type": "notification", "count": 1, "message": {"en": {"body": "{{anchor.clinician}} here! Your surgery with me is in 3 days. It is important that you fill out this survey. Please click the following link to take the survey:", "subject": "{{anchor.clinician}} | URGENT - Survey required prior to surgery"}, "es": {"body": "", "subject": ""}}, "medium": "email", "priority": "reminder interval ending", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
		(5, 1, '-2 days', '-1 days', '{"type": "notification", "count": 1, "message": {"en": {"text": "{{target.first_name}}, this is {{anchor.clinician}}. Before your upcoming surgery, I need to know how you''re doing. Please click this link to take a short survey about your health: {{link}}"}, "es": {"text": ""}}, "medium": "sms", "priority": "reminder interval ending", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
	    (6, 1, '-1 days', '-0 days', '{"type": "notification", "count": 1, "message": {"en": {"body": "{{anchor.clinician}} here! Your surgery with me is tomorrow. This survey is required. Please click the following link now to complete the survey:", "subject": "{{anchor.clinician}} | URGENT - Survey required prior to surgery"}, "es": {"body": "", "subject": ""}}, "medium": "email", "priority": "last chance", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
	    (7, 2, '0 days', '-0 days', '{"type": "notification", "count": 1, "message": {"en": {"text": "{{site_name}} here! {{anchor.clinician}} would like to know how you''re doing! Please click the following link to take the survey: {{link}}"}, "es": {"text": ""}}, "medium": "sms", "priority": "first communication important", "notification_strategy": "sendonceduringinterval", "notification_minimum_frequency": "1"}'),
	    (8, 2, '1 days', '2 days', '{"type": "notification", "count": 1, "message": {"en": {"body": "{{anchor.clinician}} here! This survey will only take a few minutes and is required. Once completed, these reminders will stop! Please click the following button to complete the survey: {link}", "subject": "{{anchor.clinician}} | Reminder - I need your {{interval_name}} survey"}, "es": {"body": "", "subject": ""}}, "medium": "email", "priority": "first reminder important", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
	    (9, 2, '2 days', '-4 days', '{"type": "notification", "message": {"en": {"text": "{{site_name}} here! Reminder -  {{anchor.clinician}} would like to know how you are doing. Please click the following link to take the survey: {{link}}"}, "es": {"text": ""}}, "medium": "sms", "priority": "gentle reminder", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "12%", "notification_minimum_frequency_unit": "Percent"}'),
	    (10, 2, '-1 days', '-0 days', '{"type": "notification", "count": 1, "message": {"en": {"body": "{{anchor.clinician}} here! URGENT - this survey needs to be completed. Please click the button:", "subject": "{{anchor.clinician}} | URGENT - Survey required"}, "es": {"body": "", "subject": ""}}, "medium": "email", "priority": "last chance", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
	    (11, 2, '-2 days', '-1 days', '{"type": "notification", "count": 1, "message": {"en": {"text": "{{target.first_name}}, this is {{anchor.clinician}}. This survey is needed so I can understand how you are doing! Please click the link:  {{link}}"}, "es": {"text": ""}}, "medium": "sms", "priority": "reminder interval ending", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}'),
	    (12, 2, '-3 days', '-2 days', '{"type": "notification", "count": 1, "message": {"en": {"body": "{{anchor.clinician}} here! URGENT - this survey needs to be completed. Please click the button: ", "subject": "{{anchor.clinician}} | URGENT - Survey required"}, "es": {"body": "", "subject": ""}}, "medium": "email", "priority": "reminder interval ending", "notification_strategy": "sendonaregularschedule", "notification_minimum_frequency": "1days", "notification_minimum_frequency_unit": "Days"}');
     
	-- Insert intervals associated with the communication set
    INSERT INTO "interval" (id, communication_set_id, name, start_day, end_day) VALUES
        (1, 1, 'Preoperative', -90, 0);
    
    -- Insert interactions
    INSERT INTO interaction (id, name, properties) VALUES
        (1, 'Test Pathway Interaction', '{"type":"pathway"}'),
        (2, 'Test Information Interaction', '{"type":"information"}');
    
    -- Insert interaction_link where parent is the pathway and child is the information
    INSERT INTO interaction_link (id, root_interaction_id, parent_interaction_id, ordinal, child_interaction_id) VALUES
        (1, 1, 1, 1, 2);
    
    -- Insert interaction_link_interval to associate interaction node with interval
    INSERT INTO interaction_link_interval (id, interaction_link_id, interval_id) VALUES
        (1, 1, 1);  -- Associate interaction_link with Preoperative interval
    
    -- Assign an anchor to the pathway interaction
    INSERT INTO anchor (id, subject_id, interaction_id, "date") VALUES
        (1, 1, 1, CURRENT_DATE + 90);
END;
$$ LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS test_case_communication_standard;
CREATE OR REPLACE FUNCTION test_case_communication_standard() RETURNS VOID AS $$
DECLARE
    record_count INT;
    current_communication_id INT;
BEGIN
    -- Step 1: Call the setup function
    PERFORM test_setup_communication();

    -- Step 2: Assert that there is the expected `contact_next` record for the first communication
	SELECT COUNT(*) INTO record_count FROM contact_next;
	SELECT communication_id INTO current_communication_id FROM contact_next;
    PERFORM test_assertTrue(CONCAT('Expected communication_id = 1 in contact_next, got ', record_count, ' records with id ',current_communication_id), record_count = 1 AND current_communication_id = 1);

    -- Step 3: Insert a contact log for today to simulate a communication event and assert that the contact_next has been removed
    WITH new_logs AS (
        INSERT INTO contact_log (subject_id, medium, sent, communication_type, priority_value) 
        VALUES (1, 'sms', CURRENT_DATE, 'notification', 6) RETURNING id
    )
    INSERT INTO contact_log_interaction (contact_log_id, communication_id, interaction_id)
    SELECT id, 1, 1
    FROM new_logs;

    SELECT COUNT(*) INTO record_count FROM contact_next;
    PERFORM test_assertTrue('Expected contact_next records to be removed after contact log 1', record_count = 0);

    -- Step 5: Push anchor date and contact log date back by one day (CURRENT_DATE - 1) and assert day -1 contact
    UPDATE anchor SET "date" = "date" - '1 day'::interval, "created" = "created" - '1 day'::interval WHERE id = 1;
    UPDATE contact_log SET sent = sent - '1 day'::interval WHERE subject_id = 1;

	SELECT COUNT(*) INTO record_count FROM contact_next;
	SELECT communication_id INTO current_communication_id FROM contact_next;
    PERFORM test_assertTrue(CONCAT('Expected communication_id = 2 in contact_next, got ', record_count, ' records with id ',current_communication_id), record_count = 1 AND current_communication_id = 2);

    WITH new_logs AS (
        INSERT INTO contact_log (subject_id, medium, sent, communication_type, priority_value) 
        VALUES (1, 'email', CURRENT_DATE, 'notification', 6) RETURNING id
    )
    INSERT INTO contact_log_interaction (contact_log_id, communication_id, interaction_id)
    SELECT id, 2, 1
    FROM new_logs;
    
    SELECT COUNT(*) INTO record_count FROM contact_next;
    PERFORM test_assertTrue('Expected contact_next records to be removed after contact log 2', record_count = 0);

    -- Step 6: Push anchor date and contact log date back another day and assert day -2 contact
	UPDATE anchor SET "date" = "date" - '1 day'::interval, "created" = "created" - '1 day'::interval WHERE id = 1;
    UPDATE contact_log SET sent = sent - '1 day'::interval WHERE subject_id = 1;

	SELECT COUNT(*) INTO record_count FROM contact_next;
	SELECT communication_id INTO current_communication_id FROM contact_next;
    PERFORM test_assertTrue(CONCAT('Expected communication_id = 3 in contact_next, got ', record_count, ' records with id ',current_communication_id), record_count = 1 AND current_communication_id = 3);

    WITH new_logs AS (
        INSERT INTO contact_log (subject_id, medium, sent, communication_type, priority_value) 
        VALUES (1, 'sms', CURRENT_DATE, 'notification', 6) RETURNING id
    )
    INSERT INTO contact_log_interaction (contact_log_id, communication_id, interaction_id)
    SELECT id, 3, 1
    FROM new_logs;
    
    SELECT COUNT(*) INTO record_count FROM contact_next;
    PERFORM test_assertTrue('Expected contact_next records to be removed after contact log 3', record_count = 0);

    -- Step 7: Push to the end of the interval and assert day 0 contact
	UPDATE anchor SET "date" = "date" - '85 day'::interval, "created" = "created" - '84 day'::interval WHERE id = 1;
    UPDATE contact_log SET sent = sent - '85 day'::interval WHERE subject_id = 1;

	SELECT communication_id INTO current_communication_id FROM contact_next;
    PERFORM test_assertTrue(CONCAT('Expected communication_id = 4 in contact_next, got id ',current_communication_id), current_communication_id = 4);

	INSERT INTO subject_data (subject_id, interaction_id, completed, submitted) VALUES
        (1, 2, NOW(), NOW());
    
    SELECT COUNT(*) INTO record_count FROM contact_next;
    PERFORM test_assertTrue('Expected contact_next records to be removed after subject_data', record_count = 0);

	-- Step 6: Push anchor date and contact log date back another day and assert that we still don't see the contact_next because it's fulfilled
	UPDATE anchor SET "date" = "date" - '1 day'::interval, "created" = "created" - '1 day'::interval WHERE id = 1;
    UPDATE contact_log SET sent = sent - '1 day'::interval WHERE subject_id = 1;

    SELECT COUNT(*) INTO record_count FROM contact_next;
    PERFORM test_assertTrue('Expected contact_next records to still be absent now that we have a subject_data entry', record_count = 0);
END;
$$ LANGUAGE plpgsql;