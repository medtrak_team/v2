DROP FUNCTION IF EXISTS test_setup_json();
CREATE OR REPLACE FUNCTION test_setup_json() RETURNS VOID AS $$
BEGIN
    PERFORM truncate_test();
    -- Create a table to hold some test cases of pathway JSON
    CREATE TEMPORARY TABLE IF NOT EXISTS "test_data_interaction_json" (
        name text NOT NULL PRIMARY KEY,
        interaction_json jsonb NOT NULL DEFAULT '{}'::jsonb
    );
    TRUNCATE "test_data_interaction_json"; -- Delete records that might have been inserted by previous tests
    PERFORM define_test_json();

    -- Insert standard comm sets used by the pathways
    INSERT INTO "communication_set"
    ("name", "shared")
    VALUES
    ('Default SMS', TRUE),
    ('Default Email', TRUE);
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS test_teardown_json();
CREATE OR REPLACE FUNCTION test_teardown_json() RETURNS VOID AS $$
BEGIN
    PERFORM truncate_test(); -- Delete any data we inserted as part of the test
END;
$$ LANGUAGE plpgsql;

drop function if exists test_case_import_interaction_set_runs;
create or replace function test_case_import_interaction_set_runs() returns void as $$
declare
    "json_value" jsonb;
begin
    perform test_setup_json(); -- This test doesn't follow naming conventions, so we have to setup manually
    select "interaction_json" from "test_data_interaction_json" where "name" = 'new_small_pathway' into "json_value";
    perform json_import_interaction_set("json_value");
    perform test_teardown_json(); -- This test doesn't follow naming conventions, so we have to teardown manually
end;$$ language plpgsql set search_path from current;

drop function if exists test_case_json_import_small;
create or replace function test_case_json_import_small() returns void as $$
declare
    "json_value" jsonb;
    "new_root_id" int;
    "pathway_id" int;
    "interaction_link_id" int;
begin
    select "interaction_json" from "test_data_interaction_json" where "name" = 'new_small_pathway' into "json_value";
    select json_import_interaction_set("json_value") into "new_root_id";
    perform test_assertNotNull('New root id was made', "new_root_id");
    select "id" from "interaction" where name = 'Test Small JSON Pathway' into "pathway_id";
    perform test_assertTrue('New root id equals id of pathway', "new_root_id" = "pathway_id");
    select "id" from "interaction_link" where "parent_interaction_id" = "pathway_id" and "root_interaction_id" = "new_root_id" limit 1 into "interaction_link_id";
    perform test_assertNotNull('Pathway has a child in interaction links', "interaction_link_id");
end;$$ language plpgsql set search_path from current;

drop function if exists test_case_json_export_small;
create or replace function test_case_json_export_small() returns void as $$
declare
    "interaction_id" int;
    "export_json" jsonb;
begin
    perform test_case_json_import_small();
    select "id" into "interaction_id" from "interaction" where "name" = 'Test Small JSON Pathway';
    select json_export_interaction("interaction_id") into "export_json";
    perform test_assertNotNull('Exporting a small pathway returned something', "export_json");
end;$$ language plpgsql set search_path from current;

drop function if exists test_case_json_export_small_nested;
create or replace function test_case_json_export_small_nested() returns void as $$
declare
    "interaction_id" int;
    "export_json" jsonb;
begin
    perform test_case_json_import_nested();
    select "id" into "interaction_id" from "interaction" where "name" = 'Nested Pathway';
    select json_export_nested_interaction_set("interaction_id") into "export_json";
    perform test_assertNotNull('Exporting a small nested pathway returned something', "export_json");
end;$$ language plpgsql set search_path from current;

drop function if exists test_case_json_import_data_collection_cms;
create or replace function test_case_json_import_data_collection_cms() returns void as $$
declare
    "json_value" jsonb;
    "new_root_id" int;
    "pathway_id" int;
    "interaction_link_id" int;
begin
    select "interaction_json" from "test_data_interaction_json" where "name" = 'cms_data_collection' into "json_value";
    select json_import_interaction_set("json_value") into "new_root_id";
    perform test_assertNotNull('New root id was made', "new_root_id");
    select "id" from "interaction" where name = 'CMS' into "pathway_id";
    perform test_assertTrue('New root id equals id of pathway', "new_root_id" = "pathway_id");
    select "id" from "interaction_link" where "parent_interaction_id" = "pathway_id" and "root_interaction_id" = "new_root_id" limit 1 into "interaction_link_id";
    perform test_assertNotNull('Pathway has a child in interaction links', "interaction_link_id");
end;$$ language plpgsql set search_path from current;

drop function if exists test_case_json_import_nested;
create or replace function test_case_json_import_nested() returns void as $$
declare
    "root_id" int;
    "json_value" jsonb;
    "pathway_id" int;
    "interaction_links" int;
begin
    insert into "interaction"
    ("name")
    values
    ('Empty Interaction')
    returning "id" into "root_id";

    select "interaction_json" from "test_data_interaction_json" where "name" = 'nested_pathway' into "json_value";
    perform json_import_nested_interaction_set("root_id", "json_value");
    select "id" from "interaction" where name = 'Nested Pathway' into "pathway_id";
    perform test_assertTrue('Root interaction was updated and exists', "root_id" = "pathway_id");
    select count(distinct "id") from "interaction_link" where "parent_interaction_id" = "pathway_id" and "root_interaction_id" = "root_id" into "interaction_links";
    perform test_assertTrue('Pathway has 3 children in interaction links', "interaction_links" = 3);
end;$$ language plpgsql set search_path from current;

drop function if exists test_case_json_import_nested_intervals;
create or replace function test_case_json_import_nested_intervals() returns void as $$
declare
    "root_id" int;
    "json_value" jsonb;
    "json_interval" jsonb;
    "interaction_id" int;
    "interval_count" int;
begin
    -- If the test below fails, there's no value to this test
    -- Test will also handle importing the pathway to begin with
    perform test_case_json_import_nested();
    select "id" from "interaction" where name = 'Nested Pathway' into "root_id";
    select * from json_export_nested_interaction_set("root_id") into "json_value";
    select "interaction_json" from "test_data_interaction_json" where "name" = 'nested_interval' into "json_interval";

    select "id" from "interaction" where "name" = 'Preoperative Class' into "interaction_id";
    select count(distinct "interval_id")
    from "interaction_link_interval"
    where "interaction_link_id" = (
        select "id" from "interaction_link"
        where "child_interaction_id" = "interaction_id"
        and "root_interaction_id" = "root_id"
        and "parent_interaction_id" = "root_id"
    )
    into "interval_count";
    perform test_assertTrue('Interaction has a single interval', "interval_count" = 1);

    "json_value" = jsonb_set("json_value", '{children, 0, intervals}' , "json_value"->'children'->0->'intervals' || "json_interval");
    "json_value" = jsonb_set("json_value", '{children, 0, intervals}' , "json_value"->'children'->0->'intervals' || "json_interval");
    perform json_import_nested_interaction_set("root_id", "json_value");
    select count(distinct "interval_id")
    from "interaction_link_interval"
    where "interaction_link_id" = (
        select "id" from "interaction_link"
        where "child_interaction_id" = "interaction_id"
        and "root_interaction_id" = "root_id"
        and "parent_interaction_id" = "root_id"
    )
    into "interval_count";
    perform test_assertTrue('Interaction has 3 intervals after adding 2 intervals', "interval_count" = 3);

    "json_value" = jsonb_set("json_value", '{children, 0, intervals}' , ("json_value"->'children'->0->'intervals') - 0);
    perform json_import_nested_interaction_set("root_id", "json_value");
    select count(distinct "interval_id")
    from "interaction_link_interval"
    where "interaction_link_id" = (
        select "id" from "interaction_link"
        where "child_interaction_id" = "interaction_id"
        and "root_interaction_id" = "root_id"
        and "parent_interaction_id" = "root_id"
    )
    into "interval_count";
    perform test_assertTrue('Interaction has 2 intervals after adding 2 intervals then removing an interval', "interval_count" = 2);

end;$$ language plpgsql set search_path from current;

drop function if exists test_case_json_import_nested_children;
create or replace function test_case_json_import_nested_children() returns void as $$
declare
    "root_id" int;
    "json_value" jsonb;
    "json_interaction" jsonb;
    "children_count" int;
begin
    -- If the test below fails, there's no value to this test
    -- Test will also handle importing the pathway to begin with
    perform test_case_json_import_nested();
    select "id" from "interaction" where name = 'Nested Pathway' into "root_id";
    select * from json_export_nested_interaction_set("root_id") into "json_value";
    select "interaction_json" from "test_data_interaction_json" where "name" = 'nested_interaction' into "json_interaction";

    select count(distinct "id") 
    from "interaction_link" 
    where "parent_interaction_id" = "root_id" 
    and "root_interaction_id" = "root_id" 
    into "children_count";
    perform test_assertTrue('Pathway has 3 children in interaction links', "children_count" = 3);
    
    "json_value" = jsonb_set("json_value", '{children}' , "json_value"->'children' || "json_interaction");
    perform json_import_nested_interaction_set("root_id", "json_value");
    select count(distinct "id") 
    from "interaction_link" 
    where "parent_interaction_id" = "root_id" 
    and "root_interaction_id" = "root_id" 
    into "children_count";
    perform test_assertTrue('Pathway has 4 children in interaction links after adding a child', "children_count" = 4);

    "json_value" = jsonb_set("json_value", '{children}' , ("json_value"->'children') - 0);
    "json_value" = jsonb_set("json_value", '{children}' , ("json_value"->'children') - 0);
    perform json_import_nested_interaction_set("root_id", "json_value");
    select count(distinct "id") 
    from "interaction_link" 
    where "parent_interaction_id" = "root_id" 
    and "root_interaction_id" = "root_id" 
    into "children_count";
    perform test_assertTrue('Pathway has 2 children in interaction links after adding a child then removing 2 children', "children_count" = 2);

end;$$ language plpgsql set search_path from current;

drop function if exists test_case_json_create_pathway;
create or replace function test_case_json_create_pathway() returns void as $$
declare
    "account_id" int;
    "condition_json" jsonb;
    "pathway_link_id" int;
    "pathway_id" int;
    "json_value" json;
begin
    insert into "interaction"
    ("name", "properties")
    values
    ('Test Account', '{"type": "account"}')
    returning "id" into "account_id";
    select "interaction_json" from "test_data_interaction_json" where "name" = 'condition_json' into "condition_json";

    select "interaction_link_id", "interaction_id", "pathway_json"
    from create_pathway("account_id", 'Test Pathway', "condition_json"::json) 
    into "pathway_link_id", "pathway_id", "json_value";

end;$$ language plpgsql set search_path from current;