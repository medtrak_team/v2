# README

What is this app for?
- v2 db migration tool, used to maintain the state of the db and all db code

DO NOT MODIFY THE V2 DB DIRECTLY

Notes and Gotchas
- DO NOT add new columns to the middle of the list without careful though, there is no "add after" in postgres
- DO NOT modify the type or name of columns, we don't currently support those kinds of alter table statements
- It is possible to do these things, but they have to be done very carefully and manually for now

Testing Your Work
- run `python migrate.py --local --run_tests --install`
- this will run against your local copy of postgres based on your site_settings.py file
- use --help for more options
- use --reinstall to delete your local copy and install it from scratch

Deployment
- use `python migrate.py --install --run_tests` to push on other servers

Freezing
Sometimes it will freeze during deployment. You may need to terminate active processes if that happens.
SELECT pg_terminate_backend(pid) FROM pg_stat_activity where datname = 'postgres' and wait_event IS NOT NULL;