from flask import Flask, request, jsonify

#add parent dir to path before importing parent path scripts
import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from comms.build_message import build_email_message, build_sms_message
from comms.send_comms import send_comms

app = Flask(__name__)

def request_data(key,default):
    if request.method == 'GET':
        return request.args.get(key, default)
    elif request.method == 'POST':
        return request.form.get(key, default)
    return default

@app.route('/comms/view')
def view_comms():
    medium = request_data('medium','email')
    message = request_data('message',"Sample survey message")
    anchor = request_data('anchor',None)
    interval_name = request_data('interval_name',"followup")
    target = None
    if anchor is None:
        clinician_name = request_data('clinician_name',"Surgeon, Test")
        client_name = request_data('client_name',"OrthoNY")
        first_name = request_data('first_name',"Test Patient")
        target = {'FirstName':first_name,'Owner':client_name}
        anchor = {'properties':{'clinician':clinician_name}}
    else:
        anchor=int(anchor)
    
    if medium == 'email':
        template = request_data('template','notification_email')
        language = request_data('language','en')
        return build_email_message(anchor=anchor,interval_name=interval_name,message=message,template=template,language=language,target=target)
    else:
        return build_sms_message(anchor=anchor,interval_name=interval_name,message=message,target=target)

@app.route('/comms/send')
def send_comms():
    passed_args = {
        'subject_id': request_data('subject_id', None),
        'debug': request_data('preview', False)
    }
    
    # Redirect stdout to capture debug output
    output = io.StringIO()
    sys.stdout = output

    send_comms(passed_args)

    sys.stdout = sys.__stdout__
        
    return jsonify(output.getValue())
    
if __name__ == '__main__':
    app.run(debug=True)