# README

What is this app for?
- this is the utility server for v2, it will provide useful functionality that calls into the comms and migrate code
- FOR MEDTRAK ONLY, DO NOT SERVE OUTSIDE VPN

Testing an email view
- run `python server.py`
- server.py is a flask app that allows you to make requests to test_build_email_message to see what a message will look like for a particular patient or just for demo purposes
- access it with /comms/view

TODO: Add instructions for how to update other servers with your code once we get to that point